<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'yandex' => [
        'shopId' => 174898, // demo: 174898, prod: 174898
        'scId' => 708709, // demo: 560852, prod: 708709
        'secret' => 'projectmed',
        'mode' => 'production' // demo | production
    ],
    /*
 * Social Services
 * */
    'yandexOAuth' => [
        'client_id' => env('YANDEX_KEY'),
        'client_secret' => env('YANDEX_SECRET'),
        'redirect' => env('YANDEX_REDIRECT_URI'),
    ],
    'vkontakte' => [
        'client_id' => env('VKONTAKTE_KEY'),
        'client_secret' => env('VKONTAKTE_SECRET'),
        'redirect' => env('VKONTAKTE_REDIRECT_URI'),
    ],
    'google' => [
        'client_id' => env('GOOGLE_KEY'),
        'client_secret' => env('GOOGLE_SECRET'),
        'redirect' => env('GOOGLE_REDIRECT_URI'),
    ],

	'sendpulse' => [
		'id' => 'a0cdd4e600d00aba36a4e58b6daa5f7c',
		'secret' => 'a6f5f3a84c3c79dbf7092979ccf313ad',

	]
];
