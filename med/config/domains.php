<?php
/**
 * Created by PhpStorm.
 * User: NikKho
 * Date: 04.09.2018
 * Time: 11:08
 */

return [
	'local' => [
		'healit.local' => [
			'index' => 'landing.healit'
		],
		'dentist.local' => [
			'index' => 'landing.list.dentist'
		],
		'medcare.local' => [
			'index' => 'landing.list.medcare.index',
			'blog' => 'landing.list.medcare.blog',
			'blog-single' => 'landing.list.medcare.blog-single',
			'contacts' => 'landing.list.medcare.contacts',
			'departments' => 'landing.list.medcare.departments',
			'departments-single' => 'landing.list.medcare.departments-single',
			'doctors' => 'landing.list.medcare.doctors',
			'services' => 'landing.list.medcare.services',
		],
		'asclinic.local' => [
			'index' => 'landing.list.asclinic'
		]
	],


	'production' => [
		'healit.ru' => [
			'index' => 'landing.healit'
		],
		'mydentist.healit.ru' => [
			'index' => 'landing.list.dentist'
		],
		'medcare.healit.ru' => [
			'index' => 'landing.list.medcare.index',
			'blog' => 'landing.list.medcare.blog',
			'blog-single' => 'landing.list.medcare.blog-single',
			'contacts' => 'landing.list.medcare.contacts',
			'departments' => 'landing.list.medcare.departments',
			'departments-single' => 'landing.list.medcare.departments-single',
			'doctors' => 'landing.list.medcare.doctors',
			'services' => 'landing.list.medcare.services',
		],
		'awesomeclinic.healit.ru' => [
			'index' => 'landing.list.asclinic'
		]
	]
];