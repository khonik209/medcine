<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropColumn('author_id');
            $table->dropColumn('is_private');
            $table->enum('type',['text','image','dicom','other'])->default('text');
            $table->string('url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->boolean('is_private')->default(0);
            $table->integer('author_id')->nullable();
            $table->dropColumn('type');
            $table->dropColumn('url');
        });
    }
}
