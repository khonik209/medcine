<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTelegramBot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegrams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('tchat_id')->nullable();
            $table->boolean('confirmed')->default(0);
            $table->timestamps();
        });

        Schema::create('tmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('telegram_id')->unsigned()->nullable();
            $table->foreign('telegram_id')->references('id')->on('telegrams')->onDelete('cascade');
            $table->text('text')->nullable();
            $table->integer('update_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmessages');
        Schema::dropIfExists('telegrams');

    }
}
