<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChoiseOfAccountNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::select("ALTER TABLE `doctors` CHANGE `wallet` `wallet` ENUM('yandex','bank') NULL DEFAULT NULL COMMENT 'Место получения денег';");
        \Illuminate\Support\Facades\DB::select("ALTER TABLE `payments` CHANGE `wallet` `wallet` ENUM('bank','yandex') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Цель получения средств';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::select("ALTER TABLE `doctors` CHANGE `wallet` `wallet` INT(11) NULL DEFAULT NULL;");
        \Illuminate\Support\Facades\DB::select("ALTER TABLE `payments` CHANGE `wallet` `wallet` VARCHAR(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;");
    }
}
