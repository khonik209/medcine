<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       \Illuminate\Support\Facades\DB::select(" ALTER TABLE `payments` CHANGE `type` `type` ENUM('request','subscription') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'request';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::select("ALTER TABLE `payments` CHANGE `type` `type` ENUM('in','out') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'in';");
    }
}
