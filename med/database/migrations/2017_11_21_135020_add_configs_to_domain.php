<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfigsToDomain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function (Blueprint $table) {
            $table->string('company')->nullable()->comment('Название компании');
            $table->string('address')->nullable()->comment('Юридический адрес компании');
            $table->string('yandex_scid')->nullable()->comment('sc_id от Яндекс Кассы');
            $table->string('yandex_shopid')->nullable()->comment('shop_id от Яндекс Кассы');
            $table->string('yandex_secret')->nullable()->comment('secret от Яндекс Кассы');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domains', function (Blueprint $table) {
            $table->dropColumn('company');
            $table->dropColumn('address');
            $table->dropColumn('yandex_scid');
            $table->dropColumn('yandex_shopid');
            $table->dropColumn('yandex_secret');
        });
    }
}
