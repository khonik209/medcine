<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAndAnswersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->enum('type', ['text', 'textarea', 'checkbox', 'radio', 'file', 'select'])->default('text');
            $table->integer('order')->default(0);
            $table->boolean('required')->default(0);
            $table->string('label')->nullable();
            $table->string('class')->nullable();
            $table->string('hint')->nullable();
            $table->longText('options')->nullable();
            $table->timestamps();
            $table->unsignedInteger('domain_id')->nullable();

        });
        Schema::table('questions', function (Blueprint $table) {
            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });

        Schema::create('answers', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->unsignedInteger('question_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('illness_id')->nullable();
            $table->longText('value')->nullable();
            $table->timestamps();
            $table->unsignedInteger('domain_id')->nullable();

        });
        Schema::table('answers', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('illness_id')->references('id')->on('illnesses')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
        Schema::dropIfExists('questions');
    }
}
