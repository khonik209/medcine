<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositionInfoToDoctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->string('skr_destinationCardPanmask')->nullable()->comment('Маска банковской карты');
            $table->string('skr_destinationCardSynonim')->nullable()->comment('Синоним банковской карты');
            $table->string('skr_destinationCardBankName')->nullable()->comment('Банк');
            $table->string('skr_destinationCardType')->nullable()->comment('Платёжная система');
            $table->string('accountNumber')->nullable()->comment('Номер счёта');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('skr_destinationCardPanmask');
            $table->dropColumn('skr_destinationCardSynonim');
            $table->dropColumn('skr_destinationCardBankName');
            $table->dropColumn('skr_destinationCardType');
            $table->dropColumn('accountNumber');
        });
    }
}
