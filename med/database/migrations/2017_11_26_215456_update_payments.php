<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
           // $table->renameColumn('test_deposition', 'confirmed');
            $table->boolean('canceled')->default(0);
            $table->enum('payment_form',['default','safe'])->default('default');
        });
        Schema::table('domains', function (Blueprint $table) {
            // $table->renameColumn('test_deposition', 'confirmed');
            $table->enum('payment_form',['default','safe'])->default('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
          //  $table->renameColumn('confirmed', 'test_deposition');
            $table->dropColumn('canceled')->default(0);
            $table->dropColumn('payment_form');
        });
        Schema::table('domains', function (Blueprint $table) {
            // $table->renameColumn('test_deposition', 'confirmed');
            $table->dropColumn('payment_form');
        });
    }
}
