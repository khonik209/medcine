<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->text('skills')->nullable()->comment('Области, в которых врачи шарит');
            $table->integer('rating')->default(0)->comment('Рейтинг врача');
        });
        Schema::create('reports', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->unsignedInteger('illness_id')->nullable();
            $table->text('text')->nullable();
            $table->integer('rating')->default(0);
            $table->timestamps();
            $table->unsignedInteger('domain_id')->nullable();
        });
        Schema::table('reports', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('illness_id')->references('id')->on('illnesses')->onDelete('cascade');
            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });
        Schema::create('skills', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
            $table->integer('domain_id')->unsigned()->nullable();
        });
        Schema::table('skills', function (Blueprint $table) {
            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });
        Schema::create('skill_user', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('skill_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('skill_user', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('skill_id')->references('id')->on('skills')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_user');
        Schema::dropIfExists('skills');
        Schema::dropIfExists('reports');
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('skills');
            $table->dropColumn('rating');
        });
    }
}
