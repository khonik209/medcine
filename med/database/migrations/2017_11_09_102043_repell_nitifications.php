<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RepellNitifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropColumn('notificated');
        });
        Schema::table('user_message', function (Blueprint $table) {
            $table->boolean('notificated')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->boolean('notificated')->default(0);
        });
        Schema::table('user_message', function (Blueprint $table) {
            $table->dropColumn('notificated');
        });
    }
}
