<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('domains', function (Blueprint $table) {
		    $table->text('email_books')->nullable();
	    });
	    Schema::create('email_templates', function (Blueprint $table) {
		    $table->engine = 'MyISAM';
		    $table->bigIncrements('id');
		    $table->text('body')->nullable();
		    $table->timestamps();
		    $table->integer('domain_id')->unsigned()->nullable();
		    $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('domains', function (Blueprint $table) {
		    $table->dropColumn('email_books');
	    });
	    Schema::dropIfExists('email_templates');
    }
}
