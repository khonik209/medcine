<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->dropColumn('author');
            $table->dropColumn('avatar');
            $table->dropColumn('author_id');
            $table->dropColumn('url');
        });

        Schema::table('messages', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->unsignedInteger('author_id')->nullable();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('file_message', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->unsignedInteger('file_id')->nullable();
            $table->unsignedInteger('message_id')->nullable();
            $table->timestamps();
        });
        Schema::table('file_message', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_message');

        Schema::table('messages', function (Blueprint $table) {
            $table->string('author')->nullable();
            $table->string('avatar')->nullable();
            $table->dropForeign('messages_author_id_foreign');
            $table->string('url')->nullable();
        });


    }
}
