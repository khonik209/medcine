<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDoctorProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('wallet')->nullable();
            $table->string('job')->nullable();
            $table->string('specialty')->nullable();
            $table->float('experience')->nullable();
            $table->string('education')->nullable();
            $table->string('level')->nullable();
            $table->longText('infomation')->nullable();
            $table->enum('status',['online','offline','holiday'])->default('online');
            $table->dateTime('holiday_until')->nullable();
            $table->dateTime('free_from')->nullable();
            $table->dateTime('free_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
