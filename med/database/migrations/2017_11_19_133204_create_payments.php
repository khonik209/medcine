<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('balance')->nullable();
        });
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('illness_id')->unsigned()->nullable();
            $table->foreign('illness_id')->references('id')->on('illnesses')->onDelete('cascade');
            $table->integer('doctor_id')->unsigned()->nullable();
            $table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('email')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->text('log_check')->nullable();
            $table->text('log_aviso')->nullable();
            $table->boolean('check')->default(0);
            $table->boolean('aviso')->default(0);
            $table->boolean('test_deposition')->default(0);
            $table->enum('type', ['in', 'out'])->default('in');
            $table->string('wallet')->nullable();
            $table->dropColumn('receiver');
            $table->dropColumn('status');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('illness_id');
            $table->dropColumn('doctor_id');

          $table->dropColumn('email');
            $table->dropColumn('invoice_id');
            $table->dropColumn('log_check');
            $table->dropColumn('log_aviso');
            $table->dropColumn('check');
            $table->dropColumn('aviso');
            $table->dropColumn('test_deposition');
            $table->dropColumn('type');
            $table->dropColumn('wallet');
            $table->string('receiver')->nullable();
            $table->string('status')->nullable();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
        });
    }
}
