<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailCampaignsToDomains extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('domains', function (Blueprint $table) {
		    $table->text('email_campaigns')->nullable();
	    });
	    Schema::table('email_templates', function (Blueprint $table) {
		    $table->string('name')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('domains', function (Blueprint $table) {
		    $table->dropColumn('email_campaigns');
	    });
	    Schema::table('email_templates', function (Blueprint $table) {
		    $table->dropColumn('name');
	    });
    }
}
