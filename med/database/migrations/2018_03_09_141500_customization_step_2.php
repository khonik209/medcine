<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomizationStep2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable();
        });
        Schema::table('doctors', function (Blueprint $table) {
            $table->integer('base_payment')->nullable();
        });
        Schema::table('domains', function (Blueprint $table) {
            $table->integer('balance')->nullable();
            $table->text('custom_functions')->nullable();
        });
        Schema::create('records', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();

            $table->integer('doctor_id');
            //$table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->longText('comment')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
            $table->unsignedInteger('domain_id')->nullable();

        });
        Schema::table('records', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
        Schema::table('domains', function (Blueprint $table) {
            $table->dropColumn('balance');
            $table->dropColumn('custom_functions');
        });
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('base_payment');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
        });

    }
}
