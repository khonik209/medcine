<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('role',['user','doc','admin'])->nullable();
            $table->integer('rating')->nullable();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('receiver')->nullable();
            $table->float('sum')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
        Schema::create('illnesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('doctor_id');
            //$table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->enum('status',['new','active','denied','archive'])->default('new');
            $table->timestamps();
        });
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('doctor_id')->unsigned()->nullable();
            //$table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('illness_id')->unsigned()->nullable();
            $table->foreign('illness_id')->references('id')->on('illnesses')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author')->nullable();
            $table->integer('chat_id')->unsigned()->nullable();
            $table->foreign('chat_id')->references('id')->on('chats')->onDelete('cascade');
            $table->longText('text')->nullable();
            $table->integer('file_id')->nullable();
            $table->timestamps();
        });

        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();

        });
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->longText('text')->nullable();
            $table->enum('type',['note','recommendation','entry','other'])->default('other');
            $table->timestamps();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
        Schema::dropIfExists('chats');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('requests');
        Schema::dropIfExists('files');
    }
}
