<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateIllnesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('illnesses', function (Blueprint $table) {
            $table->enum('result',['none','success','fail','conflict'])->default('none')->comment('Итог обращения');
            $table->enum('doc_opinion',['success','fail','none'])->default('none')->comment('Доктор завершает обращение');
            $table->enum('pac_opinion',['success','fail','none'])->default('none')->comment('Пациент завершает обращение');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('illnesses', function (Blueprint $table) {
            $table->dropColumn('result');
            $table->dropColumn('doc_opinion');
            $table->dropColumn('pac_opinion');
        });
    }
}
