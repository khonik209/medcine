<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Группа доменов
Route::domain('{domain}')->group(function () {
//Route::group(['domain' => '{domain}'], function () {
    /* GROUP: Посредник, проверяющий наличие поддомена */
    Route::group(['middleware' => 'domain'], function ($domain) {

        /* Маршруты яндекс кассы */
        Route::group(['prefix' => 'yandex'], function () {
            // Регистрация
            Route::get('/getcode', 'LandingController@getCode'); // Получаем токен от Яндекс
            // Платежи
            Route::post('/payment/create', 'YandexController@newPayment'); // Общий маршрут создания платежа
            Route::get('/payment/{id}', 'YandexController@redirectToPay'); // Перенаправление на оплату
            Route::post('/check', 'YandexController@check'); // Проверка боевых платежей
            Route::post('/check/test', 'YandexController@check'); // Проверка тестовых платежей
            Route::post('/aviso', 'YandexController@aviso'); // Подтверждение боевых платежей
            Route::post('/aviso/test', 'YandexController@aviso'); // Подтверждение тестовых платежей
            Route::post('/quickpay', 'YandexController@quickpayConfirm'); // Подтверждение платежей от яндекс Кнопки

            Route::get('/success', 'YandexController@successPayment'); // Оплата прошла успешно
            Route::get('/success/test', 'YandexController@successPayment'); // Оплата прошла успешно
            Route::get('/fail', 'YandexController@failPayment'); // При оплате произошла ошибка
            Route::get('/fail/test', 'YandexController@failPayment'); // При оплате произошла ошибка

            Route::get('/cardsave', 'YandexController@cardSave'); // Успех привязки карты
            Route::get('/cardfail', 'YandexController@cardFail'); // Ошибка привязки карты

            /* Завершение безопасной сделки */
            Route::post('/safe', 'YandexController@finishSafeIllness'); // Маршрут для разруливания по безопасной сделке
        });

        /* Регистрация */
        Auth::routes();
        Route::get('/register', 'LandingController@registerPage'); // Регистрация
        Route::post('/register', 'LandingController@register'); // Регистрация
        Route::post('/register_with_form', 'LandingController@registerWithForm'); // Регистрация через анкету
        Route::get('/setrolepage', 'LandingController@setRolePage'); // Стр. выбора роли
        Route::post('/setrole', 'LandingController@setRole'); // Выбрать роль
        Route::post('/password/remind', 'Auth\ResetPasswordController@remindPassword'); // Сброс пароля

        /* Лендинг */
        Route::get('/', 'LandingController@index'); // Лендинг
        Route::post('/clinic/register', 'LandingController@registerClinic'); // Регистрация новой клиники
        Route::post('/feedback', 'LandingController@feedback'); // Форма обратной связи

        /* Маршруты авторизованных пользователей */
        Route::group(['middleware' => 'auth'], function () {
            /* Чат */
            Route::group(['prefix' => 'chat'], function () {

                Route::post('/list', 'ChatController@getChats'); // Список JSON
                Route::get('/check/{id}', 'ChatController@fetchMessages');
                Route::post('/add', 'ChatController@sendMessage');
                Route::post('/document/add/', 'ChatController@addChatDocument'); // Прикрепепить документ
                Route::post('/document/delete/', 'ChatController@deleteChatDocument'); // Удалить из превью документ
                Route::get('/dicom/{id}', 'ChatController@dcmView'); // Открыть dicom-viewer
            });

            /* Записи */
            Route::group(['prefix' => 'records'], function () {
                Route::any('/all', 'RecordController@allRecords'); //Получить все записи
                Route::post('/new', 'RecordController@newRecord'); // Создание новой записи (AJAX)
                Route::post('/edit', 'RecordController@editRecord'); // Создание редактирование записи (AJAX)
                Route::post('/delete', 'RecordController@deleteRecord'); // Удаление записи (AJAX)
                Route::post('/data', 'RecordController@getData'); // Все данные для календаря на выбранную дату
            });

            /* Обращения */
            Route::group(['prefix' => 'illnesses'], function () {
                Route::get('/get/{id}', 'IllnessController@getOne'); //Просмотр обращения
                Route::post('/new', 'IllnessController@newIllness'); // Создание нового Обращения (AJAX)
                Route::post('/edit', 'IllnessController@editIllness'); // Создание редактирование Обращения (AJAX)
                Route::post('/delete', 'IllnessController@deleteIllness'); // Удаление Обращения (AJAX)
                Route::post('/cancel', 'IllnessController@cancelRequest'); // Отмена обращения
                Route::post('/finish', 'IllnessController@finishRequest'); // Завершение обращения
                Route::post('/report', 'IllnessController@addReport'); // Оставить отзыв
            });

            /* Маршруты пациента */
            Route::group(['prefix' => 'user'], function () {
                //Главная
                Route::get('/', 'UserController@index');
                /* Заметки */
                Route::post('/note/add', 'UserController@addNote');
                Route::post('/note/edit', 'UserController@editNote');
                Route::post('/note/delete', 'UserController@deleteNote');
                /* Чат */
                Route::get('/requests/', 'UserController@getRequestsPage'); //Список всех обращений
                Route::get('/request/{id}', 'UserController@getRequestPage'); //Страница обращения
                /* Врачи */
                Route::get('/doctors', 'UserController@getDoctorsList');
                Route::get('/doctor/{id}', 'UserController@getDoctor');
                /* История болезни */
                Route::get('/history', 'UserController@getHistoryPage');
                Route::get('/disease_history/{id}', 'UserController@getDiseaseHistory');
                /* Профиль */
                Route::get('/settings', 'UserController@getAccountPage');
                Route::get('/account/document/{id}/delete', 'UserController@deleteDocument'); // Удалить документ
                Route::post('/edit', 'UserController@editUser'); // Сохранить изменения
                Route::get('/resetpassword', 'UserController@getResetPasswordPage'); // Страница смены пароля
                Route::post('/resetpassword/reset', 'UserController@resetPassword'); // Сменить пароль
                Route::post('/avatar', 'UserController@updateAvatar'); // Сменить аватар
            });

            /* Маршруты врачей */
            Route::group(['prefix' => 'doc'], function () {
                Route::post('/form/add', 'DocController@addForm'); // Анкета врача
                Route::post('/finish_guide', 'DocController@denyStartGuide'); // Отказаться от обучения
                /* Главная */
                Route::get('/', 'DocController@index');
                /* Обращения */
                Route::get('/requests', 'DocController@getRequestsPage'); // Страница заявок
                Route::group(['prefix' => 'request'], function () {
                    Route::get('/{id}', 'DocController@getRequestPage'); // Страница обращения
                    Route::get('/accept/{id}', 'DocController@acceptRequest'); //Принять обращение
                    Route::get('/deny/{id}', 'DocController@denyRequest'); // Отклонить обращение
                    Route::post('/finish', 'DocController@finishRequest'); // Завершить обращение

                    Route::post('/add_member', 'DocController@addMember'); // Добавить участника
                    Route::post('/note/add', 'DocController@addRequestNote'); // Написать рекомендацию
                });

                Route::get('/users', 'DocController@getUsersPage'); /* Все пациенты*/
                Route::group(['prefix' => 'user'], function () {
                    /* Пациенты */
                    Route::get('/{id}', 'DocController@getUserPage'); /* Добавить пациента */
                    Route::get('/add', 'DocController@getAddUserPage');
                    Route::post('/save', 'DocController@addUser');
                });
                /* Чат */
                Route::get('/chats', 'DocController@getChatsPage'); // Страница списка чатов
                Route::group(['prefix' => 'chat'], function () {
                    Route::post('/delete', 'DocController@deleteChat'); //Удалить чат
                    Route::post('/create', 'DocController@addChat'); // Создать чат
                    Route::post('/rename', 'DocController@renameChat'); // Переименовать чат
                });
                /* Помощник */
                Route::group(['prefix' => 'helper'], function () {
                    Route::get('/', 'DocController@getHelperPage'); // Страница помощника
                    Route::post('/note/add', 'DocController@addNote');
                    Route::post('/note/edit', 'DocController@editNote');
                    Route::get('/note/{id}/delete', 'DocController@deleteNote');
                    Route::post('/remember/entry', 'DocController@rememberEntry'); // Запоминаем порядок записей в списке
                });
                /* Другие врачи */
                Route::get('/doctors', 'DocController@getDoctorsList'); // Список врачей
                Route::any('/doctors/list', 'DocController@doctorsList'); // autocomplete список врачей
                Route::get('/doctor/{id}', 'DocController@getDoctor');

                Route::get('/history', 'DocController@getHistoryPage'); // Закрытые заявки
                /* История болезни */
                Route::group(['prefix' => 'disease_history'], function () {
                    Route::post('/new', 'DocController@newDiseaseHistoryPage'); // Страница создания новой истории болезни
                    Route::post('/save', 'DocController@saveDiseaseHistory'); // Сохранение новой истории болезни
                    Route::get('/{id}', 'DocController@getDiseaseHistoryPage'); // Страница истории болезни
                    Route::get('/{id}/edit', 'DocController@diseaseHistoryEditPage'); // Страница Редактирование истории болезни
                    Route::post('/edit', 'DocController@editDiseaseHistory'); // Редактирование истории болезни
                });
                /* Профиль */
                Route::group(['prefix' => 'account'], function () {
                    Route::get('/', 'DocController@getAccountPage');
                    //   Route::post('/document/add', 'DocController@addDocument'); // Добавить документ
                    Route::get('/document/{id}/delete', 'DocController@deleteDocument'); // Удалить документ
                    Route::post('/edit', 'DocController@editUser'); // Сохранить изменения
                    Route::post('/work_time', 'DocController@setWorkTime'); // Сохранить график работы
                    Route::post('/base_payment', 'DocController@setBasePayment'); // Настроить базовый платёж
                    Route::post('/setwallet', 'DocController@setWallet'); // Выбор способа платежа
                    Route::post('/status', 'DocController@setDocStatus'); // Сменить статус
                    Route::post('/magicbutton', 'DocController@generateButton'); // Сгенерировать магическую кнопку
                });
                Route::post('/avatar', 'DocController@updateAvatar'); // Сменить аватар
                Route::get('/resetpassword', 'DocController@getResetPasswordPage'); // Страница смены пароля
                Route::post('/resetpassword/reset', 'DocController@resetPassword'); // Сменить пароль
                /* Управление чёрным списком */
                Route::group(['prefix' => 'blacklist'], function () {
                    Route::post('/add', 'DocController@addToBlackList'); // Добавить пациента в черный список
                    Route::get('/remove/{id}', 'DocController@removeFromBlackList'); // Удалить пациента из черного списока
                });
            });

            /* Маршруты администратора */
            Route::group(['prefix' => 'admin'], function () {
                Route::get('/', 'AdminController@index'); // домашняя страница админа
                /* Список врачей */
                Route::get('/doctors', 'AdminController@getDoctors');
                /* Работа с доктором */
                Route::group(['prefix' => 'doctor'], function () {
                    Route::get('/{id}', 'AdminController@getDoctor');
                    Route::get('/{id}/edit', 'AdminController@editDoctorPage');
                    Route::post('/{id}/edit', 'AdminController@editDoctor');
                    Route::post('/add', 'AdminController@addDoctor');
                    Route::post('/delete', 'AdminController@deleteDoctor');
                    Route::post('/verify', 'AdminController@verifyDoctor'); // Проверка врача
                    Route::post('/work_time', 'AdminController@setWorkTime'); // Проверка врача
                });
                /* Список пациентов */
                Route::get('/users', 'AdminController@getUsers');
                Route::post('/export/users', 'AdminController@exportUsers'); // скачать excel
                Route::group(['prefix' => 'user'], function () {
                    Route::post('/add', 'AdminController@addUser');
                    Route::get('/{id}', 'AdminController@getUser');
                    Route::get('/{id}/edit', 'AdminController@editUserPage');
                    Route::post('/edit', 'AdminController@editUser');
                    Route::get('/disease_history/{id}', 'AdminController@getDiseaseHistory');
                    Route::post('/delete', 'AdminController@deleteUser');
                    Route::post('/email', 'AdminController@sendEmailToUser');
                });
                Route::get('/chats', 'AdminController@getChatsPage'); // Страница списка чатов
                Route::group(['prefix' => 'request'], function () {
                    Route::get('/{id}', 'AdminController@getRequestPage'); // Страница обращения
                    Route::get('/accept/{id}', 'AdminController@acceptRequest'); //Принять обращение
                    Route::get('/deny/{id}', 'AdminController@denyRequest'); // Отклонить обращение
                    Route::post('/finish', 'AdminController@finishRequest'); // Завершить обращение
                    Route::post('/add_member', 'AdminController@addMember'); // Добавить участника
                });
                /* Email рассылки */
                Route::group(['prefix' => 'emails'], function () {
                    Route::get('/', 'EmailController@getEmailsPage');

                    Route::get('/templates', 'EmailController@getTemplatesPage'); // список шаблонов
                    Route::group(['prefix' => 'template'], function () {
                        Route::get('/new', 'EmailController@newEmailsPage'); // конструктор шаблона
                        Route::post('/save', 'EmailController@saveTemplate'); // сохранить новый шаблон
                        Route::post('/delete', 'EmailController@deleteTemplate'); // удалить шаблон
                    });

                    Route::get('/books', 'EmailController@getEmailBooks'); // список книг
                    Route::group(['prefix' => 'book'], function () {
                        Route::get('/get/{id}', 'EmailController@getEmailBook'); // страница редактирования книги
                        Route::post('/delete', 'EmailController@deleteEmailBook'); // удалить книгу
                        Route::post('/new', 'EmailController@createEmailBook'); // создать новую книгу
                        Route::any('/users', 'EmailController@getDomainUsers'); // Запросить юзеров из БД на добавление в SendPulse
                        Route::post('/sync', 'EmailController@syncEmailBook'); // Синхронизировать выбранных пользователей с SendPulse
                    });

                    Route::get('/campaigns', 'EmailController@getCampaigns'); // список кампаний
                    Route::group(['prefix' => 'campaign'], function () {
                        Route::post('/getbookinfo', 'EmailController@getBookInfo'); // информация покниге
                        Route::post('/send', 'EmailController@campaignSend'); // Создать кампанию (отправить рассылку)
                        Route::get('/{id}', 'EmailController@getCampaign'); // Посмотреть информацию о кампании
                    });
                });
                /* История */
                Route::group(['prefix' => 'history'], function () {
                    Route::get('/', 'AdminController@getHistory');
                    Route::post('/dashboard', 'AdminController@dashboard');
                    Route::get('/illnesses', 'AdminController@getIllnesses');
                    Route::get('/payments', 'AdminController@getPayments');
                    Route::get('/records', 'AdminController@getRecords');
                    Route::post('/export/illnesses', 'AdminController@exportIllnesses');
                    Route::post('/export/payments', 'AdminController@exportPayments');
                    Route::post('/export/records', 'AdminController@exportRecords');
                    Route::get('/payment/{id}/delete', 'AdminController@deletePayment'); // удаление платежа
                    Route::post('/illness/update', 'AdminController@updateIllness'); //Изменение статуса обращения
                });

                /* Конфликты */
                Route::get('/conflicts', 'AdminController@getConflicts');
                Route::post('/conflict/solve', 'AdminController@solveConflict');

                /* Настройки */
                Route::group(['prefix' => 'settings'], function () {
                    Route::get('/', 'AdminController@getAccountPage');
                    Route::post('/admin', 'AdminController@adminSettings');
                    Route::get('/reset_password', 'AdminController@getResetPasswordPage'); // Страница смены пароля
                    Route::post('/reset_password/reset', 'AdminController@resetPassword'); // Сменить пароль
                    Route::post('/company', 'AdminController@companySettings');
                    Route::post('/yandex', 'AdminController@yandexSettings');
                    Route::post('/functions', 'AdminController@saveFunctions');
                    Route::post('/skills', 'AdminController@syncSkills'); // Сохранение специальностей
                    Route::post('/questions', 'AdminController@syncQuestions'); // Сохранение вопросов анкеты
                    Route::post('/business_hours', 'AdminController@saveBusinessHours'); // Сохранить график работы клиники
                });

                /* Редактор новостей */
                Route::group(['prefix' => 'news'], function () {
                    Route::get('/', 'NewsController@getList');
                });
            });
        });

        /* Блог */
        Route::resource('news', 'NewsController');

        // Авторизация через соц сети
        Route::group(['prefix' => 'login'], function () {
            Route::get('/{driver}', 'LandingController@redirectToProvider');
            Route::get('/{driver}/callback', 'LandingController@handleProviderCallback');
        });


        /**
         * КАСТОМНЫЕ МАРШРУТЫ
         * Служебные маршруты:
         * /yandex/*,
         * /login/*,
         * /feedback,
         * /clinic/register/,
         * /chat/*,
         * /records/*,
         * /illnesses/*,
         * /user/*,
         * /doc/*,
         * /admin/*
         **/

        Route::get('/contacts', 'LandingController@contacts');
        Route::get('/services', 'LandingController@services');
        Route::get('/departments', 'LandingController@departments');
        Route::get('/department/{id}', 'LandingController@department');
        Route::get('doctors', 'LandingController@doctors');
        Route::get('blog', 'LandingController@blog');
        Route::get('blog/{id}', 'LandingController@blogOne');
    });
});

/* SEO: генерация robots.txt для поддоменов и основного домена */
Route::get('robots.txt', function () {
    return "User-agent: *
Allow: /
Allow: /faq
Allow: /clinic
Allow: /login
Allow: /register
Disallow: /storage
Disallow: /admin
";
});