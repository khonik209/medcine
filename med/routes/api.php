<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Группа доменов
Route::group(['domain' => '{domain}'], function () {
    /* GROUP: Посредник, проверяющий наличие поддомена */
    Route::group(['middleware' => 'domain'], function () {
        // Кнопка врача со сторонних сервисов
        Route::get('/getemail', 'LandingController@getEmail'); // Запрос от кнопки врача на получение email пациента
        Route::post('/checkuser', 'LandingController@checkUser'); // Проверяем, есть ли такой юзер в системе
        Route::post('/adduser', 'LandingController@addUser'); // Запрос от кнопки врача на получение email пациента

        Route::any('/user/register','ApiController@userRegister'); // Регистрация пользователя.
        Route::any('/user/info','ApiController@getUser'); // Информация о пользователе


        Route::any('/records','ApiController@getRecords'); // Создание записи
    });
});