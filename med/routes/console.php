<?php

use Illuminate\Foundation\Inspiring;
use App\Message;
use App\User;
use App\Illness;
use App\Tmessage;
use App\Domain;
use App\Skill;
use App\Plan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

\Illuminate\Support\Facades\Artisan::command('set_message_author', function () {

    $messages = Message::all();
    foreach ($messages as $message) {
        $author_id = $message->author_id;
        if (!isset($author_id)) {
            $author_name = $message->author;
            $user = User::where('name', $author_name)->first();
            $message->author_id = $user->id;
            $message->save();
        }
    }

});
/* Напоминание о новых сообщениях */
Artisan::command('new_messages', function () {
    $users = User::where('notification', 1)->get();
    foreach ($users as $user) {
        $messages = $user->unreadMessages()->wherePivot('notificated', '0')->get();
        if (count($messages) > 0) {
            foreach ($messages as $message) {
                try {
                    Mail::send('email.newMessage', ['user' => $user, 'newMessage' => $message], function ($m) use ($user) {
                        $m->to($user->email, $user->name)->subject('Новое сообщение на ' . $user->domain->name);
                    });
                    Log::info('Напоминании о новом сообщении отправлено ' . $user->email);

                } catch (\Exception $e) {
                    Log::error('Ошибка при отправке напоминании о новом сообщении ' . $user->email . ': ' . $e);
                }
                //echo $message->id;
                $user->messages()->updateExistingPivot($message->id, ['notificated' => 1]);
            }
        }
    }
});

/* Напоминание о новых обращениях */
Artisan::command('new_request', function () {
    $users = User::doctors()->where('notification', 1)->get();
    foreach ($users as $user) {
        $newIllnesses = Illness::doctorNewIllnesses($user)->get();
        if (count($newIllnesses) > 0) {
            try {
                Mail::send('email.newRequest', ['user' => $user, 'illnesses' => $newIllnesses], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Новое обращение на ' . $user->domain->name);
                });
                Log::info('Напоминании о новом обращении отправлено ' . $user->email);
            } catch (\Exception $e) {
                Log::error('Ошибка при отправке напоминании о новом обращении ' . $user->email . ': ' . $e);
            }
        }
    }
});
/* Сбросить токены видеочатов */
Artisan::command('reset_video_chats', function () {
    $chats = \App\Chat::all();
    foreach ($chats as $chat) {
        $chat->session = null;
        $chat->token = null;
        $chat->save();
    }
});

Artisan::command('testemail', function () {
    Mail::raw('Text to e-mail', function ($message) {
        //       $message->from('us@example.com', 'Laravel');

        $message->to('nkhoreff@yandex.ru');
    });
});
/**
 * НИЖЕ КОМАНДЫ ПЕРЕХОДА ПРИЛОЖЕНИЯ НА v.2.0
 */
Artisan::command('new_domain', function () {
    $domain = new Domain;
    $domain->name = 'teledoctor-pro';
    $domain->active_to = Carbon::now()->addDays(14);
    $domain->save();

    $user = new User;
    $user->name = 'admin';
    $user->password = bcrypt('khoreva1');
    $user->domain_id = $domain->id;
    $user->email = 'admin@' . $domain->name . '.ru';
    $user->role = 'admin';
    $user->save();
});

Artisan::command('update_domain', function () {
    \Illuminate\Support\Facades\DB::table('users')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('messages')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('chats')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('doctors')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('files')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('illnesses')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('notes')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('payments')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('telegrams')->whereNull('domain_id')->update(['domain_id' => 1]);
    \Illuminate\Support\Facades\DB::table('tmessages')->whereNull('domain_id')->update(['domain_id' => 1]);
});

Artisan::command('update_files', function () {
    $files = \App\File::all();
    foreach ($files->where('domain_id', 1) as $file) {
        $oldUrl = $file->url;
        $newUrl = '1/' . $oldUrl;
        $file->url = $newUrl;
        $file->save();
    }
});

Artisan::command('update_avatar', function () {
    $users = User::all();
    foreach ($users as $user) {
        foreach ($user->files as $file) {
            if ($file->name == 'avatar' . $user->id) {
                $user->avatar = $file->url;
                $user->save();
                foreach ($user->messages()->where('author', $user->name)->get() as $message) {
                    $message->avatar = $user->avatar;
                    $message->save();
                }
            } else {
                foreach ($user->messages()->where('author', $user->name)->get() as $message) {
                    $message->avatar = 'doctors/emptyavatar.png';
                    $message->save();
                }
            }
        }

    }
});

Artisan::command('add_plans', function () {
    $start = new Plan;
    $start->name = 'Стартовый';
    $start->limit = 100;
    $start->price = 4900;
    $start->save();

    $advanced = new Plan;
    $advanced->name = "Продвинутый";
    $advanced->limit = 300;
    $advanced->price = 11900;
    $advanced->save();

    $business = new Plan;
    $business->name = "Бизнес";
    $business->limit = 0;
    $business->price = 24900;
    $business->save();

});
/**
 *  /// КОМАНДЫ ПЕРЕХОДА НА v.2.0
 */
Artisan::command('easy_password', function () {
    \Illuminate\Support\Facades\DB::table('users')->update(['password' => bcrypt(123456)]);
});

Artisan::command('test_aviso', function () {
    $query = array(
        'scid' => 1234,
        'shopId' => 5678,
        'customerNumber' => 'email:console@console',
        'sum' => 1
    );
    $query = http_build_query($query);

    // Формирование заголовков POST-запроса
    $header = "Content-type: application/x-www-form-urlencoded";

    // Выполнение POST-запроса и вывод результата
    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' => $header,
            'content' => $query
        )
    );
    $context = stream_context_create($opts);
    $result = file_get_contents('http://teledoctor-pro.local/yandex/aviso/test', false, $context);
    $result = json_decode($result);
    echo $result;
});

Artisan::command('test_cron', function () {
    Mail::raw('CRON IS WORKING', function ($m) {
        $m->to('nkhoreff@yandex.ru', 'TEST_CRON')->subject('CRON');
    });
});
/**
 * Регистрация демо домена teledoctor-demo.ru
 */
Artisan::command('new_demo_domain', function () {
    $domain = new Domain;
    $domain->name = 'teledoctor-demo';
    $domain->active_to = Carbon::now()->addDays(14);
    $domain->save();

    $user = new User;
    $user->name = 'admin';
    $user->password = bcrypt('khoreva1');
    $user->domain_id = $domain->id;
    $user->email = 'admin@' . $domain->name . '.ru';
    $user->role = 'admin';
    $user->save();
});

Artisan::command('payment_news', function () {
    $users = User::doctors()->get();
    foreach ($users as $user) {
        Mail::send('email.news.paymentOn', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Сообщение от ' . $user->domain->name);
        });
    }
});

Artisan::command('excuse_bugs', function () {
    $users = User::doctors()->get();
    foreach ($users as $user) {
        Mail::send('email.news.exuseBugs', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Тех. неполадки на ' . $user->domain->name . ' устранены');
        });
    }
});

Artisan::command('create_base_skills', function () {
    $a = [
        'Терапевт',
        'Анестезиолог',
        'Реаниматолог',
        'Травматолог',
        'Ортопед',
        'Нейрохирург',
        'Эндокринолог',
        'Нефролог',
        'Онколог',
        'Маммолог',
        'Хирург',
        'Эндоскопист',
        'Патологоанатом',
        'Дерматолог',
        'Венеролог',
        'Акушер',
        'Гинеколог',
        'Педиатр',
        'Диетолог',
        'Кардиолог',
        'Отоларинголог',
        'Вертебролог',
        'Рентгенолог',
        'Ревматолог',
        'Психолог',
        'Общая медицина',
        'Гастроэнтеролог',
        'Косметолог',
        'Гематолог',
        'Аллерголог',
        'Андролог',
        'Гомеопат',
        'Иммунолог',
        'Инфекционист',
        'Кардиохирург',
        'Кинезиолог',
        'Колопроктолог',
        'Логопед',
        'Мануальный терапевт',
        'Массажист',
        'Миколог',
        'Нарколог',
        'Невролог',
        'Неонатолог',
        'Окулист (офтальмолог)',
        'Онкогинеколог',
        'Онкодерматолог',
        'Остеопат',
        'Пластический хирург',
        'Подолог',
        'Проктолог',
        'Психиатр',
        'Психотерапевт',
        'Пульмонолог',
        'Реабилитолог',
        'Репродуктолог',
        'Рефлексотерапевт',
        'Сексолог',
        'Сомнолог',
        'Сосудистый хирург',
        'Спортивный врач',
        'Стоматолог',
        'Сурдолог',
        'Трихолог',
        'Уролог',
        'Физиотерапевт',
        'Флеболог',
        'Фтизиатр',
        'Челюстно-лицевой хирург',
        'Эпилептолог',


    ];

    foreach ($a as $i) {
        $skill = new Skill;
        $skill->name = $i;
        $skill->domain_id = 1;
        $skill->save();
    }
});

Artisan::command('check_guide', function () {
    $users = User::doctors()->has('doctor')->get();
    foreach ($users as $user) {
        $checkIllness = Illness::where('doctor_id', $user->id)->first();
        if ($checkIllness) {
            print 'USER ' . $user->email . ' done' . "\n";
        } else {
            $user->start_guide = 1;
            $user->save();
            print 'USER ' . $user->email . ' NOT' . "\n";
        }
    }
    print $users->count();
});
Artisan::command('easy_password', function () {
    foreach (User::all() as $user) {
        $user->password = bcrypt(123456);
        $user->save();
    }
});

Artisan::command('control_chat_1', function () {
    $messages = Message::whereNull('author_id')->get();
    print 'Кол-во ошибок  ' . $messages->count() . "\n";
    $messages->map(function ($v) {
        print 'author: ' . $v->author;
        $chat = $v->chat()->first();
        if ($chat) {
            print ' count of users: ' . $chat->users()->count() . "\n";
            if ($chat->users()->count() == 0) {
                $v->delete();
                print 'msg is deleted' . "\n";
            } else {
                if ($chat->users()->where('name', '=', $v->author)->count() != 1) {
                    print 'invalid user count:' . $chat->users()->where('name', '=', $v->author)->count() . "\n";
                } else {
                    $user = $chat->users()->where('name', '=', $v->author)->first();
                    if ($user) {
                        print 'user found: ' . $user->email . "\n";
                        $v->author_id = $user->id;
                        $v->save();
                    } else {
                        print 'user not found' . "\n";
                    }
                }
            }
        } else {
            $v->delete();
            print 'chat not fount' . "\n";
        }
    });
    $messagesV2 = Message::whereNull('author_id')->get();
    $messagesV2->map(function ($v) {
        $users = User::where('name', $v->author)->get();
        if ($users->count() == 0) {
            $v->delete();
        } else {
            $v->author_id = $users->first()->id;
            $v->save();
        }
    });
});

Artisan::command('create_base_questions', function () {
    $sourse = [
        [
            'type' => 'text',
            'label' => 'Ф.И.О.',
            'required' => 1,
            'class' => '',
            'hint' => '',
            'options' => []
        ],
        [
            'type' => 'text',
            'required' => 1,
            'label' => 'Возраст',
            'class' => 'js-validate-number',
            'hint' => 'Полных лет',
            'options' => []
        ],
        [
            'type' => 'text',
            'required' => 0,
            'class' => 'js-validate-phone',
            'label' => 'Телефон',
            'options' => [],
            'hint' => ''
        ],
        [
            'type' => 'text',
            'required' => 1,
            'label' => 'Email',
            'class' => 'js-validate-email',
            'hint' => '',
            'options' => ''
        ]
    ];

    foreach ($sourse as $key => $item) {
        $question = new \App\Question;
        $question->type = $item['type'];
        $question->required = $item['required'];
        $question->class = $item['class'];
        $question->label = $item['label'];
        $question->hint = $item['hint'];
        $question->options = $item['options'];
        $question->domain_id = null;
        $question->order = $key;
        $question->save();
    }
});
Artisan::command('create_healit', function () {
    $domain = new Domain;
    $domain->name = 'healit.ru';
    $domain->active_to = Carbon::now()->addYears(100);
    $domain->company = 'Heal It';
    $domain->save();
});
Artisan::command('update_plans', function () {
    $plan2 = Plan::find(2);
    $plan2->price = 9900;
    $plan2->save();

    $plan3 = Plan::find(3);
    $plan3->price = 14900;
    $plan3->save();
});
Artisan::command('push_examples', function () {
    $names = [
        'mydentist.healit.ru',
        'medcare.healit.ru',
        'awesomeclinic.healit.ru'
    ];
    foreach ($names as $name) {
        $domain = new Domain;
        $domain->name = $name;
        $domain->save();
    }
});
