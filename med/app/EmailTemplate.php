<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
	/**
	 * ОТНОШЕНИЕ: ДОМЕНЫ
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function domain()
	{
		return $this->belongsTo('App\Domain');
	}
}
