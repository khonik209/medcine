<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $hidden = [
        'domain_id'
    ];

    /**
     * ОТНОШЕНИЯ: ДОМЕН
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * ОТНОШЕНИЯ: ПОЛЬЗОВАТЕЛИ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * ОТНОШЕНИЯ: Обращения
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function illness()
    {
        return $this->belongsTo('App\Illness');
    }

    /**
     * Возвращает врача в записи
     * @return mixed|static
     */
    public function doctor()
    {
        return User::find($this->doctor_id);
    }

    /**
     *
     * Вернуть все записи на текущую дату
     * @param null $day
     * @return $this
     */
    public function recordsOfDay($day = null)
    {
        if (!$day) {
            $day = Carbon::now();
        }
        return $this->where('start_date', '>=', $day->startOfDay())->where('end_date', '<=', $day->endOfDay());

    }
}
