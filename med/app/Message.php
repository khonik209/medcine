<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_message', 'message_id', 'user_id')->withPivot('is_read')->withPivot('notificated');
    }

    public function files()
    {
        return $this->belongsToMany('App\File')->withTimestamps();
    }

    /* Заготовки */
    public function author()
    {
        $author = $this->users()->find($this->author_id);
        return $author;
    }
}
