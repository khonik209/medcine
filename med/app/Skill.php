<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    /**
     * ОТНОШЕНИЕ: ДОМЕНЫ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }
    /**
     * ПОЛЬЗОВАТЕЛИ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User','skill_user', 'skill_id', 'user_id');
    }
}
