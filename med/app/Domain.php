<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Domain extends Model
{
    /* ОТНОШЕНИЯ */
    /**
     * ЧАТЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chats()
    {
        return $this->hasMany('App\Chat');
    }

    /**
     * ПРОФИЛИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function doctors()
    {
        return $this->hasMany('App\Doctor');
    }

    /**
     * ФАЙЛЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('App\File');
    }

    /**
     * ОБРАЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function illnesses()
    {
        return $this->hasMany('App\Illness');
    }

    /**
     * СООБЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    /**
     * ЗАПИСИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    /**
     * ПЛАТЕЖИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * ТЕЛЕГРАММ СООБЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tmessages()
    {
        return $this->hasMany('App\Tmessage');
    }

    /**ПОЛЬЗОВАТЕЛИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * ПОДПИСКИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany('App\Subscription');
    }

    /**
     * ОТЗЫВЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    /**
     * СПЕЦИАЛЬНОСТИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function skills()
    {
        return $this->hasMany('App\Skill');
    }

    /**
     * ШАБЛОНЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emailTemplates()
    {
        return $this->hasMany('App\EmailTemplate');
    }

    /**
     * Отношение: Ответы
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Отношение: Ответы
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * Отношение: Новости
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function news()
    {
        return $this->hasMany('App\News');
    }

    /* JSON */

    /**
     * ЧИТАТЕЛЬ: Получаем массив заблокированных пользователем юзеров
     * @param $value
     *
     * @return mixed
     */
    public function getEmailBooksAttribute($value)
    {
        return json_decode($value, true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив заблокированных пользователем юзеров
     * @param $value
     */
    public function setEmailBooksAttribute($value)
    {
        if ($value != null) {
            $this->attributes['email_books'] = json_encode($value);
            //$this->attributes['black_list'] = ($value);
        } else {
            $this->attributes['email_books'] = null;
        }
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив заблокированных пользователем юзеров
     * @param $value
     *
     * @return mixed
     */
    public function getEmailCampaignsAttribute($value)
    {
        return json_decode($value, true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив заблокированных пользователем юзеров
     * @param $value
     */
    public function setEmailCampaignsAttribute($value)
    {
        if ($value != null) {
            $this->attributes['email_campaigns'] = json_encode($value);
            //$this->attributes['black_list'] = ($value);
        } else {
            $this->attributes['email_campaigns'] = null;
        }
    }

    /**
     * ЗАПИСИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany('App\Record');
    }

    /**
     * ИСТОРИЯ БОЛЕЗНИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function diseaseHistories()
    {
        return $this->hasMany('App\DiseaseHistory');
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив доступных поддомену дополнительных функций
     * @param $value
     *
     * @return mixed
     */
    public function getCustomFunctionsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем доступные поддомену дополнительные функции
     * @param $value
     */
    public function setCustomFunctionsAttribute($value)
    {
        if ($value != null) {
            $this->attributes['custom_functions'] = json_encode($value);
        } else {
            $this->attributes['custom_functions'] = null;
        }
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив настроек поддомена
     * @param $value
     *
     * @return mixed
     */
    public function getOptionsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив настроек поддомена
     * @param $value
     */
    public function setOptionsAttribute($value)
    {
        if ($value != null) {
            $this->attributes['options'] = json_encode($value);
        } else {
            $this->attributes['options'] = null;
        }
    }

    /* ЗАГОТОВКИ */
    /**
     * Возвращает последнюю подписку домена
     * @return Model|null|static
     */
    public function lastSubscription()
    {
        return $this->subscriptions()->orderBy('created_at', 'desc')->first();
    }

    /**
     * Возвращает число обращений за последний оплаченный период     *
     * @return mixed|null
     */
    public function numberOfIllnesses()
    {

        if ($this->lastSubscription()) {
            // Если подписка есть
            $illnesses = $this->lastSubscription()->illnesses;
        } else {
            $illnesses = null;
        }
        return $illnesses;
    }

    /**
     * Проверка Доступности аккаунта
     *
     * @return bool
     */
    public function isActive()
    {
        if (Carbon::parse($this->active_to) < Carbon::now()) {
            return false; // Если время подписки закончилось
        }
        if ($this->lastSubscription()) { // Если период не тестовый
            if ($this->lastSubscription()->plan->limit != 0 && $this->numberOfIllnesses() >= $this->lastSubscription()->plan->limit) {
                return false; // Если тариф не безлимитный и лимит превышен
            }
        }
        return true;
    }

    /**
     * Возвращает ответ по наличию кастомной функции в аккаунте
     *
     * @param $name string Лейбл кастомной функции
     *
     * @return bool
     */
    public function custom($name)
    {

        if ($this->custom_functions && in_array($name, $this->custom_functions)) {
            return true;
        }
        return false;
    }


    public function getDoctors()
    {
        return $this->users()->where('role', 'doc');
    }


    public function getAdmin()
    {
        return $this->users()->where('role', 'admin');
    }

    /**
     * Проверяет, рабочий ли текущий день
     * $day - integer, номер дня недели (0-6)
     * @param $day
     * @return bool
     */
    public function busyDay($day)
    {

        if (!isset($this->options)) {
            return false;
        }

        if (!isset($this->options->business_hours)) {
            return false;
        }

        if (!isset($this->options->business_hours->days)) {
            return false;
        }

        if (!isset($this->options->business_hours->days[$day])) {
            return false;
        }

        return true;
    }

    /**
     * Часы работы кдиники
     *
     * @return mixed
     */
    public function workingHours()
    {
        if(!isset($this->options))
        {
            return [];
        }
        if(!isset($this->options->business_hours))
        {
            return [];
        }
        return $this->options->business_hours->days;
    }

    public function getLogoUrlAttribute()
    {
        if(!$this->logo)
        {
            return null;
        }
        return Storage::disk('s3')->url($this->logo);
    }
}
