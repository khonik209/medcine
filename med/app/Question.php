<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * Отношение: Домен
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * Отношение: Ответы
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив опций
     * @param $value
     *
     * @return mixed
     */
    public function getOptionsAttribute($value)
    {
        return json_decode($value,true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив опций
     * @param $value
     */
    public function setOptionsAttribute($value)
    {
        if($value != null){
            $this->attributes['options'] = json_encode($value);
            //$this->attributes['black_list'] = ($value);
        }else{
            $this->attributes['options'] = null;
        }
    }

}
