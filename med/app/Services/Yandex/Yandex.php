<?php

namespace App\Services\Yandex;


use App\Illness;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Yandex
{

    private $settings;

    /**
     * Yandex constructor.
     */
    function __construct($domainObj, $type)
    {
        $settings = new Settings($domainObj, $type);
        $this->settings = $settings;
    }


    /**
     * Отправляет юзера на Яндекс-Кассу с нужными параметрами.
     *
     * @param Payment $payment
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function redirectToYandex(Payment $payment)
    {
        return view('payment.form', [
            'payment' => $payment,
            'settings' => $this->settings,
        ]);
    }

    /**
     * Проверяем наличие заказа и отвечаем Яндексу.
     *
     * @param Request $request
     */
    public function checkOrder($domainObj, Request $request)
    {
        $settings = new Settings($domainObj, $request->payment_type);
        $http = new HTTP('checkOrder', $settings);

        if (!$http->checkMD5($request)) { // Хеш не совпадает - выдаем ошибку
            Log::error('Ошибка ХЭШ:' . $request->all());
            $response = $http->buildResponse('checkOrder', $request->invoiceId, 1);
            $http->sendResponse($response);
        }

        $checkPayment = Payment::find($request->payment);

        if ($checkPayment) {

            $checkPayment->invoice_id = $request->invoiceId;
            $checkPayment->log_check = json_encode($request->all());
            $checkPayment->sum = $request->orderSumAmount;
            $checkPayment->check = 1;
            $checkPayment->aviso = 0;
            $checkPayment->save();

        } else { // платеж не найден, отвечаем ошибкой

            $response = $http->buildResponse('checkOrder', $request->invoiceId, 100, "Payment with current ID not found in system.");
            $http->sendResponse($response);
        }

        $response = $http->buildResponse('checkOrder', $request->invoiceId, 0);

        $http->sendResponse($response);
    }

    /**
     * Подтверждаем заказ, возвращаем индикатор выдачи заказа.
     *
     * @param Request $request
     * @return Payment|Request
     */
    public function paymentAviso($domainObj, Request $request)
    {

        $settings = new Settings($domainObj, $request->payment_type);
        $http = new HTTP('paymentAviso', $settings);
        if (!$http->checkMD5($request)) { // Хеш не совпадает - выдаем ошибку
            Log::error('AVISO: ' . $request->all());
            $response = $http->buildResponse('paymentAviso', $request->invoiceId, 1);
            $http->sendResponse($response);
        }
        $checkPayment = Payment::find($request->payment);
        if ($checkPayment) {

            if ($checkPayment->aviso == 1) { // платеж уже подтверждался
                $response = $http->buildResponse('paymentAviso', $request->invoiceId, 0);
                $http->sendResponse($response);
            }

            $checkPayment->log_aviso = json_encode($request->all());
            $checkPayment->sum = $request->orderSumAmount;
            $checkPayment->invoice_id = $request->invoiceId;
            $checkPayment->check = 1;
            $checkPayment->aviso = 1;
            $checkPayment->save();

        } else { // платеж не найден, отвечаем ошибкой
            Log::error('PAYMENT NOT FOUND');
            $response = $http->buildResponse('paymentAviso', $request->invoiceId, 100, "Payment with current ID not found in system.");
            $http->sendResponse($response);
        }
        return $checkPayment; // возвращаем платеж, который надо выдать юзеру
    }

    /**
     * Просто отвечаем Яндексу успехом на авизо по команде сверху (из контроллеров)
     *
     * @param $domainObj
     * @param $payment
     */
    public function paymentAvisoSuccess($domainObj, $payment)
    {
        $settings = new Settings($domainObj, $payment->type);
        $http = new HTTP('paymentAviso', $settings);
        $response = $http->buildResponse('paymentAviso', $payment->invoice_id, 0);
        $http->sendResponse($response);
    }

    /**
     * Направляет запрос на повтор платежа в Я.Кассу
     *
     * @param Payment $base - базовый платеж, по которому будет осуществлен повтор
     *
     * @return mixed
     */
    public function repeatPayment($domainObj, $base) // TODO не используется, удалить
    {

        $invoice_id = $base->invoice_id;
        $cost = $base->sum;

        $settings = $this->settings;
        $mws = new MWS($settings);

        $string = $mws->repeatCardPayment($invoice_id, $cost, $base->id);
        $xml = new \SimpleXMLElement($string);

        $np = new Payment;
        $np->user_id = $base->user->id;
        $np->plan_id = $base->plan->id;
        $np->invoice_id = $xml['invoiceId'];
        $np->sum = $base->sum;
        $np->repeat = 1;
        $np->check = 0;
        $np->aviso = 0;
        $np->email = $base->user->email;
        $np->name = $base->user->name;
        $np->phone = $base->user->phone;
        $np->log_repeat = $string;

        if (isset($xml['techMessage'])) {
            $np->repeat_response = $xml['techMessage'];
        }

        $np->save();

        if ($xml['status'] == 0 && $xml['error'] == 0) { // Запрос успешно принят Яндексом, см. https://tech.yandex.ru/money/doc/payment-solution/reference/status-codes-docpage/

            Log::info($string);
            return $xml['invoiceId']; // Возвращаем номер новой транзакции

        } else {

            /* Отключаем автоплатежи у юзера */
            $u = $base->user;
            $u->rebilling = 0;
            $u->save();

            Log::error("Ошибка при попытке повтора платежа. Параметры: invoice_id: $invoice_id, сумма: $cost, ответ от яндекса: $string");
            return false;

        }
    }

    /**
     * Переводим замороженную сумму Исполнителю
     * Возвращает bool (успешная / ошибка операции)
     *
     * @param $payment
     * @return bool
     */
    public function confirmDeposition($domainObj, $payment)
    {
        $settings = $this->settings;
        $mws = new MWS($settings);
        $cardSynonym = null;
        if ($payment->wallet == "bank") {
            $cardSynonym = $payment->card_synonym;
        }
        $destination = $payment->destination;
        $paramsReady = false;
        $params = [];

        if ($destination) {
            $params = [
                'invoiceId' => $payment->invoice_id,
                'destination' => $destination,
                'cardSynonym' => $cardSynonym,
                'amount' => $payment->sum,
            ];
            $paramsReady = true;
        }

        if ($paramsReady) {

            $confirm = $mws->confirmDeposition($params);
            Log::info('Ans: ' . $confirm);
            $answer = new \SimpleXMLElement($confirm);

            if ($answer['status'] == 0) {
                $payment->test_deposition = 1;
                $payment->save();
                return true;
            } else {
                Log::error('Confirm Payment Error: Ошибка при подтверждении платежа. Яндекс-статус: ' . $answer['status']);
                return false;
            }

        } else {
            Log::error('Confirm Payment Error: Не готовы параметры платежа');
            return false;
        }
    }


    /**
     * Отменяем платёж. Сумма возвращается Заказчику.
     * Возвращает bool (Успех / Ошибка операции)
     *
     * @param $payment
     * @return bool
     */
    public function cancelPayment($domainObj, $payment)
    {
        $settings = $this->settings;
        $mws = new MWS($settings);

        $result = $mws->cancelPayment($payment->invoice_id);
        $answer = new \SimpleXMLElement($result);

        if ($answer['status'] == 0) {
            $payment->canceled = 1;
            $payment->save();
            return true;

        } else {
            Log::error('Ошибка при отмене платежа. Яндекс.статус: ' . $answer['status']);
            return false;
        }
    }
}