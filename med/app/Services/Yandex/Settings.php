<?php

namespace App\Services\Yandex;

use App\Domain;

class Settings {

    public $shop_password = "projectmed";
    public $security_type = "MD5"; // MD5 | PKCS7
    public $shop_id = 0;
    public $sc_id = 0;
    public $currency = 10643;
    public $request_source= "php://input"; // for XML/PKCS#7 scheme
    public $mws_cert_password = "khoreva1";
    public $mws_cert;
    public $mws_private_key;

    function __construct($domainObj, $type) {

        if($type=="request") {
            $this->shop_password = $domainObj->yandex_secret;
            $this->shop_id = $domainObj->yandex_shopid;
            $this->sc_id = $domainObj->yandex_scid;
        } else {
            $this->shop_password = config('services.yandex.secret');
            $this->shop_id = config('services.yandex.shopId');
            $this->sc_id = config('services.yandex.scId');
        }
        $this->mws_cert = dirname(__FILE__)."/teledoctor-pro.cer";
        $this->mws_private_key = dirname(__FILE__)."/private.key";
    }
}