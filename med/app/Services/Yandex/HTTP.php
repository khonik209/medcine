<?php

namespace App\Services\Yandex;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

require_once 'Utils.php';

use DateTime;

/**
 * The implementation of payment notification methods.
 */
class HTTP
{

    private $action;
    private $settings;

    public function __construct($action, Settings $settings)
    {
        $this->action = $action;
        $this->settings = $settings;
    }

    /**
     * Checking the MD5 sign.
     * @param Request $request
     * @return bool true if MD5 hash is correct
     */
    public function checkMD5(Request $request) {
        $str = $request->action . ";" .
            $request->orderSumAmount . ";" . $request->orderSumCurrencyPaycash . ";" .
            $request->orderSumBankPaycash . ";" . $request->shopId . ";" .
            $request->invoiceId . ";" . trim($request->customerNumber) . ";" . $this->settings->shop_password;
        $md5 = strtoupper(md5($str));

        if ($md5 != strtoupper($request->md5)) {
            Log::error("Wait for md5:" . $md5 . ", recieved md5: " . $request->md5);
            return false;
        }
        return true;
    }

    /**
     * Building XML response.
     * @param  string $functionName "checkOrder" or "paymentAviso" string
     * @param  string $invoiceId transaction number
     * @param  string $result_code result code
     * @param  string $message error message. May be null.
     * @return string                prepared XML response
     */
    public function buildResponse($functionName, $invoiceId, $result_code, $message = null)
    {
        try {
            $performedDatetime = Utils::formatDate(new DateTime());
            $response = '<?xml version="1.0" encoding="UTF-8"?><' . $functionName . 'Response performedDatetime="' . $performedDatetime .
                '" code="' . $result_code . '" ' . ($message != null ? 'message="' . $message . '"' : "") . ' invoiceId="' . $invoiceId . '" shopId="' . $this->settings->shop_id . '"/>';
            return $response;
        } catch (\Exception $e) {
            Log::error($e);
        }
        return null;
    }

    public function sendResponse($responseBody)
    {
        header("HTTP/1.0 200");
        header("Content-Type: application/xml");
        echo $responseBody;
        exit;
    }
}

?>