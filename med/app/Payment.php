<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function illness()
    {
        return $this->belongsTo('App\Illness');
    }

    /**
     * Возвращает получателя (врача) средств
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function doctor()
    {
        return $this->belongsTo('App\User')->find($this->doctor_id);
    }

    public function getStatus()
    {
        $domain = $this->domain;
        if ($domain->payment_form == "safe") {
            if ($this->aviso == 1) { // Деньги с пациента сняли
                if ($this->test_deposition == 1) // Перевели врачу
                {
                    $result = [
                        'text' => "Переведено",
                        'level' => 'success'
                    ];
                } elseif ($this->canceled) // Вернули пациенту
                {
                    $result = [
                        'text' => "Возвращено",
                        'level' => 'default'
                    ];
                } else { // Пока еще удерживаем
                    $result = [
                        'text' => "Удержано",
                        'level' => 'warning'
                    ];
                }
            } else {
                $result = [
                    'text' => "Не оплачено",
                    'level' => 'danger'
                ];
            }
        } else {
            if ($this->aviso == 1) {
                $result = [
                    'text' => "Оплачено",
                    'level' => 'success'
                ];
            } else {
                $result = [
                    'text' => "Не оплачено",
                    'level' => 'danger'
                ];
            }
        }
        return json_encode($result);
    }
}
