<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class Illness extends Model {
	/**
	 * ОТНОШЕНИЯ: ДОМЕН
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function domain() {
		return $this->belongsTo( 'App\Domain' );
	}

	/**
	 * ОТНОШЕНИЯ: ЧАТ
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function chat() {
		return $this->hasOne( 'App\Chat' );
	}

	/**
	 * ОТНОШЕНИЯ: ЗАПИСИ
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function record() {
		return $this->hasOne( 'App\Record' );
	}

	/**
	 * ОТНОШЕНИЯ: ПЛАТЕЖИ
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function payments() {
		return $this->hasMany( 'App\Payment' );
	}

	/**
	 * ОТНОШЕНИЯ: ПОЛЬЗОВАТЕЛИ
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo( 'App\User' );
	}

	/**
	 * ОТНОШЕНИЯ: ОТЗЫВ
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function report() {
		return $this->hasOne( 'App\Report' );
	}

	/**
	 * ОТНОШЕНИЯ: ИСТОРИЯ БОЛЕЗНИ
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function diseaseHistory() {
		return $this->hasOne( 'App\DiseaseHistory' );
	}

	/**
	 * Отношение: Ответы
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function answers() {
		return $this->hasMany( 'App\Answer' );
	}

	/* Свои заготовки */

	public function scopeDoctorActiveIllnesses( $query, $doctor ) {
		return $query->where( 'doctor_id', $doctor->id )->whereIn( 'status', [ 'new', 'active' ] );
	}

	public function scopeDoctorNewIllnesses( $query, $doctor ) {
		return $query->where( 'doctor_id', $doctor->id )->where( 'status', 'new' );
	}

	public function scopeDoctorArchiveIllnesses( $query, $doctor ) {
		return $query->where( 'doctor_id', $doctor->id )->where( 'status', 'archive' );
	}

	/* Внешние заготовки */
	public function scopeNewPayments() {
		return $this->hasMany( 'App\Payment' )->where( 'aviso', 0 );
	}

	/**
	 * Возвращает лечащего врача в обращении
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function doctor() {
		return $this->belongsTo( 'App\User' )->find( $this->doctor_id );
	}

	/**
	 * Возвращает метку обращения (текущий статус)
	 * @return string
	 */
	public function status() {
		if ( $this->domain->payment_form == "safe" ) {
			if ( $this->status == "new" ) {
				return '<span class="label label-warning" >Новое</span>';
			} elseif ( $this->status == "active" ) {
				if ( $this->result == "conflict" ) {
					return '<span class="label label-danger">Конфликт</span>';
				}
				return '<span class="label label-info" > В работе </span>';

			} elseif ( $this->status == "denied" ) {
				return '<span class="label label-danger" > Отклонено</span>';
			} else {
				if ( $this->result == "success" ) {
					return '<span class="label label-success">Выполнено</span>';
				} elseif ( $this->result == "fail" ) {
					return '<span class="label label-warning" > Возвращено</span>';
				} else {
					return '<span class="label label-default" > Завершено</span>';
				}
			}
		} else {
			if ( $this->status == "active" ) {
				return '<span class="label label-info" > В работе </span>';
			} elseif ( $this->status == "new" ) {
				return '<span class="label label-warning"> Новое</span>';
			} elseif ( $this->status == "archive" ) {
				return '<span class="label label-default" > Завершено</span>';
			} else {
				return '<span class="label label-danger" > Отклонено</span>';
			}
		}

	}

	/**
	 * Возвращает логи обращения
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function activities() {
		return Activity::where( 'content_type', 'Illness' )->where( 'content_id', $this->id )->get();
	}
}
