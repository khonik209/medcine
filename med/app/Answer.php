<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * Отношение: Домен
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * Отношение: Вопрос анкеты
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * Отношение: Пользователь
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Отношение: Обращение
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function illness()
    {
        return $this->belongsTo('App\Illness');
    }
}
