<?php

namespace App\Scopes;

use App\Domain;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DomainScope implements Scope
{
    /**
     * Применение заготовки к данному построителю запросов Eloquent.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $domainObj = Domain::where('name', request()->route('domain'))->first();
        if (!$domainObj) {
            $domainObj = Domain::find(1);
        }

        if ($model->getTable() == "email_templates") {
            $builder->where('domain_id', '=', $domainObj->id)->orWhereNull('domain_id');
        } else {
            $builder->where('domain_id', '=', $domainObj->id);
        }
    }
}