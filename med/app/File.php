<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function user()
    {

        return $this->belongsTo('App\User');
    }

    public function chats()
    {
        return $this->belongsToMany('App\Chat', 'chat_file', 'file_id', 'chat_id');
    }

    public function messages()
    {
        return $this->belongsToMany('App\Message')->withTimestamps();
    }

    /* Свои заготовки */
    public function scopeDicom($query)
    {
        return $query->where('type', 'dicom');
    }

    public function getFullUrlAttribute()
    {
        if (!$this->url) {
            return null;
        }
        return Storage::disk('s3')->url($this->url);
    }
}
