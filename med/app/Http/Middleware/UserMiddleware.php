<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role == 'doc') {
            return redirect('/doc');
        }
        if ($request->user()->role == 'admin') {
            return redirect('/admin');
        }
        if ($request->user()->role == null) {
            return redirect('/setrolepage');
        }

        return $next($request);
    }
}
