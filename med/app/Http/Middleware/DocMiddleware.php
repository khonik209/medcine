<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DocMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role == 'user') {
            return redirect('/user');
        }
        if ($request->user()->role == 'admin') {
            return redirect('/admin');
        }
        if ($request->user()->role == null) {
            return redirect('/setrolepage');
        }

        return $next($request);
    }
}
