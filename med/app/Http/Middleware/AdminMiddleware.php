<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role == 'user') {
            return redirect('/user');
        }
        if ($request->user()->role == 'doc') {
            return redirect('/doc');
        }
        if ($request->user()->role == null) {
            return redirect('/setrolepage');
        }

        return $next($request);
    }
}
