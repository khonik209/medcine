<?php

namespace App\Http\Middleware;

use App\Activity;
use App\Answer;
use App\Chat;
use App\DiseaseHistory;
use App\Doctor;
use App\Domain;
use App\EmailTemplate;
use App\File;
use App\Illness;
use App\Message;
use App\News;
use App\Note;
use App\Payment;
use App\Question;
use App\Record;
use App\Report;
use App\Scopes\DomainScope;
use App\Session;
use App\Skill;
use App\Tmessage;
use App\User;
use Closure;

class DomainMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkAccount = Domain::where('name', '=', $request->route('domain'))->first();

        if (!$checkAccount) {
            return redirect('https://teledoctor-pro.ru');
        }

        // Фильтрация запросов КУРСОВ из БД: только для текущего поддомена
        Chat::addGlobalScope(new DomainScope);

        // Фильтрация запросов ПОЛЬЗОВАТЕЛЕЙ из БД: только для текущего поддомена
        User::addGlobalScope(new DomainScope);

        // Фильтрация запросов ПЛАТЕЖЕЙ из БД: только для текущего поддомена
        Payment::addGlobalScope(new DomainScope);

        // Фильтрация запросов ПРОДУКТОВ из БД: только для текущего поддомена
        Doctor::addGlobalScope(new DomainScope);

        // Фильтрация запросов ПИТАНИЯ из БД: только для текущего поддомена
        File::addGlobalScope(new DomainScope);

        // Фильтрация запросов УПРАЖНЕНИЙ из БД: только для текущего поддомена
        Illness::addGlobalScope(new DomainScope);

        // Фильтрация запросов ОТЧЕТОВ из БД: только для текущего поддомена
        Message::addGlobalScope(new DomainScope);

        // Фильтрация запросов СТАТЕЙ из БД: только для текущего поддомена
        Note::addGlobalScope(new DomainScope);

        // Фильтрация запросов ОТЗЫВОВ из БД: только для текущего поддомена
        Report::addGlobalScope(new DomainScope);

        // Фильтрация запросов НАВЫКОВ из БД: только для текущего поддомена
        Skill::addGlobalScope(new DomainScope);

        // Фильтрация запросов ЗАПИСЕЙ из БД: только для текущего поддомена
        Record::addGlobalScope(new DomainScope);

        // Фильтрация запросов Истории болезни из БД: только для текущего поддомена
        DiseaseHistory::addGlobalScope(new DomainScope);

        // Фильтрация запросов шаблонов из БД: только для текущего поддомена
        EmailTemplate::addGlobalScope(new DomainScope);

        // Фильтрация запросов вопросов анкеты из БД: только для текущего поддомена
        Question::addGlobalScope(new DomainScope);

        // Фильтрация запросов ответов анкеты из БД: только для текущего поддомена
        Answer::addGlobalScope(new DomainScope);

        // Фильтрация запросов ТРЕНИРОВОК из БД: только для текущего поддомена
        //Session::addGlobalScope(new DomainScope); TODO сделать ДОМЕНЫ для СЕССИИ

        Activity::addGlobalScope(new DomainScope);

        News::addGlobalScope(new DomainScope());

        return $next($request);
    }
}
