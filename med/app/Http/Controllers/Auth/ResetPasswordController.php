<?php

namespace App\Http\Controllers\Auth;

use App\Domain;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share( 'domainObj', $this->domain);
    }

    public function remindPassword(Request $request){
        $email = $request->email;
        $user = User::where('email',$email)->first();
        if(!$user){
            return back()->with('info','Пользователь с таким e-mail не найден');
        }
        $password = str_random(8);
        $user->password = bcrypt($password);
        $user->save();
        try {
            Mail::send('email.resetPassword', ['user' => $user, 'password' => $password], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Сброс пароля от '.$this->domain->name);
            });
        } catch (\Exception $e) {
            Log::error('Email не отправилось: ' . $e);
        }

        return redirect('/')->with('success','Вам на почту отправлено письмо с новым паролем');
    }
}
