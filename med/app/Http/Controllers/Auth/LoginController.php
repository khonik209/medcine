<?php

namespace App\Http\Controllers\Auth;

use App\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest', ['except' => 'logout']);
        /* Определяем текущий поддомен */
        $domain = Domain::where('name', $request->route('domain'))->first();
        if(!$domain)
        {
            return redirect('https://healit.ru/');
        }
        if($domain->name=="healit.ru")
        {
            abort(404);
        }
        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $domain);
    }
}
