<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Record;
use App\Traits\UserTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    use UserTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();
        if (!$this->domain->custom('api')) {
            return abort(404);
        }
    }

    public function userRegister($domain,Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'string',
            'phone' => 'string',
        ]);

        if ($validation->fails()) {
            return json_encode([
                'status' => 0,
                'msg' => 'email is required'
            ]);
        }
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            return json_encode([
                'status' => 0,
                'msg' => 'email is already busy'
            ]);
        }
        $user = $this->createUser($request->email,null,$request->name,$request->phone,$this->domain->id,$request->role,$request->age);

        return json_encode([
            'status' => 1,
            'msg' => 'user has been created',
            'info' => [
                'id' => $user->id,
                'email' => $user->email,
            ]
        ]);
    }

    /**
     * Возвращает все записи на день
     * @param Request $request
     * @return array|string
     */
    public function getRecords(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        if ($validation->fails()) {
            return json_encode([
                'error' => 'wrong date',
            ]);
        }
        $start = Carbon::parse($request->date)->format('Y-m-d H:i:s');
        $end = Carbon::parse($request->date)->addDay()->format('Y-m-d H:i:s');

        $records = Record::where('start_date', '>=', $start)->where('end_date', '<=', $end)->get()->toArray();

        foreach ($records as $key => $record) {
            $user = User::find($record['user_id']);
            $doctor = User::find($record['doctor_id']);
            $records[$key]['user_name'] = $user ? $user->name : 'deleted';
            $records[$key]['user_email'] = $user ? $user->email : 'deleted';
            $records[$key]['user_phone'] = $user ? $user->phone : 'deleted';
            $records[$key]['doctor_name'] = $doctor ? $doctor->name : 'deleted';
        }
        return $records;
    }

    /**
     * Возвращает информацию о враче
     * @param Request $request
     * @return string
     */
    public function getUser(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if ($validation->fails()) {
            return json_encode([
                'error' => 'id is required and must be an integer',
            ]);
        }
        $user = User::with('doctor')->findOrFail($request->id);
        return $user->toJson();
    }
}
