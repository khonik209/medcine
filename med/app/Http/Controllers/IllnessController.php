<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Chat;
use App\Domain;
use App\File;
use App\Illness;
use App\Payment;
use App\Record;
use App\Report;
use App\Services\Yandex\Yandex;
use App\Traits\AnswerTrait;
use App\Traits\ChatTrait;
use App\Traits\IllnessTrait;
use App\Traits\LogTrait;
use App\Traits\PaymentTrait;
use App\Traits\RecordTrait;
use App\Traits\ReturnTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class IllnessController extends Controller
{
    use IllnessTrait;
    use ReturnTrait;
    use RecordTrait;
    use ChatTrait;
    use AnswerTrait;
    use PaymentTrait;
    use LogTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

    }

    /**
     * Создание обращения. Написано для пациента пока что
     *
     * Ждем параметры: start, end,user_id,target_id,name,description,questions,place
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function newIllness($domain, Request $request)
    {
        $accept = true;

        if (!$this->domain->isActive()) {
            $accept = false;
        }

        $target = User::find($request->target_id);
        if (!$target) {
            $target = $this->domain->getAdmin()->first();
        }
        $user = User::find($request->user_id);

        /* Если доступны записи, проверяем свободное время */
        if ($this->domain->custom('records')) {
            $start = $request->start ? $request->start : null;
            $end = $request->end ? $request->end : null;
            if (!$target->isFree($request->start, $request->end)) {
                $accept = false;
            }
        }

        $black_list = [];
        if ($target->black_list) {
            $black_list = $target->black_list;
        }
        /*Проверяем на ЧС*/
        if (in_array($user->id, $black_list)) {
            $accept = false;
        }

        if (!$accept) {
            return back()->with('error', 'Создание обращения недоступно. Обратитесь к администратору.');
        }

        $name = $request->name;
        $description = $request->description;

        $illness = $this->createIllness($user, $target, $name, $description);
        try {
            $this->logIt( 'Illness', $illness->id, 'CreateIllness', 'Обращение создано', '',$illness->domain_id );
        } catch ( \Exception $e ) {
            Log::error( 'Не удалось записать активность: ' . $e );
        }
        if ($this->domain->custom('records')) {
            $start = $request->start ? $request->start : null;
            $end = $request->end ? $request->end : null;
            $record = $this->createRecord($user, $target, $start, $end, $illness, $name, $description);

        }

        if ($this->domain->custom('chat')) {
            if ($request->place == "online") {
                $chat = $this->createChat($user, $target, $illness);
            }
        }

        if ($this->domain->custom('custom_form')) {
            if ($request->questions) {
                $saved = $this->saveAnswers($user, $illness, $request->questions);
            }
        }

        if ($this->domain->custom('payments')) {
            $base_payment = $target->doctor && $target->doctor->base_payment?$target->doctor->base_payment:null;
            if ($base_payment) {
                $payment = $this->createPayment($user, $target, $illness,$base_payment);
            }
        }

        return back()->with('success', 'Обращение создано! Скоро мы вам ответим');
    }

    /**
     * Редактирование обращения. Доступно только админу.
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editIllness($domain, Request $request)
    {
        $illness = Illness::findOrFail($request->illness_id);
        if ($request->status) {
            $illness->status = $request->status;
        }
        if ($request->doctor_id) {
            $illness->doctor_id = $request->doctor_id;//Меняем ответственное тело
            if ($illness->chat) {
                $chat = $illness->chat;
                try {
                    $chat->users()->detach($request->doctor_id);
                    $chat->users()->attach($request->doctor_id); // Добавляем пацана в чат
                } catch (\Exception $e) {
                    Log::error('Admin@editIllness. Ошибка при изменении состава чата. Chat ID: ' . $chat->id);
                }
            }
        }
        if ($request->name) {
            $illness->name = $request->name;
        }
        if ($request->description) {
            $illness->description = $request->description;
        }
        if ($request->result) {
            $illness->result = $request->result;
        }
        if ($request->doc_opinion) {
            $illness->doc_opinion = $request->doc_opinion;
        }
        if ($request->pac_opinion) {
            $illness->pac_opinion = $request->pac_opinion;
        }
        if ($this->domain->custom('custom_form')) {
            if ($request->questions) {
                $user = User::find($request->user_id);
                $saved = $this->saveAnswers($user, $illness, $request->questions);
            }
        }
        $illness->save();
	    try {
		    $this->logIt( 'Illness', $illness->id, 'EditIllness', 'Обращение отредактировано', '',$illness->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return back()->with('success', 'Сохранено');
    }

    /**
     * Удаление обращения. Доступно админу
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteIllness($domain, Request $request)
    {
        $illness = Illness::findOrFail($request->illness_id);
        if ($illness->chat) {
            $chat = $illness->chat;
            $chat->delete();
        }
        if ($illness->record) {
            $record = $illness->record;
            $record->delete();
        }
        if ($illness->answers) {
            foreach ($illness->answers as $answer) {
                $answer->delete();
            }
        }
        $illness->delete();
        return back()->with('success', 'Удалено');
    }

    /**
     * Отменить новое обращение к врачу. Доступно пациенту
     * @param $domain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelRequest($domain, Request $request)
    {
        $illness = Illness::find($request->illness_id);

        if (!$illness) {
            return redirect('/user')->with('error', 'Обращение не найдено');
        }

        $chat = $illness->chat;

        if (!$chat) {
            return redirect('/user')->with('error', 'Страница не найдена');
        }

        $illness->status = "denied";
        $illness->save();

        $user = $illness->user;
        $doctor = $illness->doctor();

        /* Если есть запись на это событие - удаляем следом */
        if ($illness->record) {
            $record = $illness->record;
            if ($record) {
                $record->delete();
            }
        }

        if ($illness->answers) {
            foreach ($illness->answers as $answer) {
                $answer->delete();
            }
        }

        if ($illness->chat) {
            $chat = $illness->chat;
            $chat->delete();
        }

        // email Юзеру
        try {
            Mail::send('email.cancelIllness', ['illness' => $illness, 'user' => $user, 'doctor' => $doctor], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Отмена обращения');
            });
        } catch (\Exception $e) {
            Log::error('Ошибка при отправке email юзеру ' . $user->email . '_' . $e);
        }
        try {
            // email Доктору
            Mail::send('email.cancelIllness', ['illness' => $illness, 'user' => $user, 'doctor' => $doctor], function ($m) use ($doctor) {
                $m->to($doctor->email, $doctor->name)->subject('Отмена обращения');
            });
        } catch (\Exception $e) {
            Log::error('Ошибка при отправке email доктору ' . $doctor->email . '_' . $e);
        }
	    try {
		    $this->logIt( 'Illness', $illness->id, 'CancelIllness', 'Обращение отменено', '',$illness->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return back()->with('success', 'Обращение отменено');
    }

    /**
     * Завершение обращения и решение по вопросам платежа
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finishRequest($domain, Request $request)
    {
        $illness = Illness::find($request->illness_id);

        // Выкидываем, если обращение не найдено
        if (!$illness) {
            return back()->with('info', 'Обращение не найдено');
        }

        // Выкидываем, если в доступе отказано
        if ($illness->user_id != Auth::user()->id) {
            return back()->with('info', 'В доступе отказано');
        }

        $doctor = User::find($illness->doctor_id);

        //Проверяем форму оплаты. Простая или "Безопасная"
        if ($this->domain->payment_form == "safe") {

            // Записываем решение врача
            $illness->pac_opinion = $request->opinion;
            $illness->save();

            // Если пациент еще не принял решение - ожидаем
            if ($illness->doc_opinion == "none") {
                /* Отправляем пациенту email уведомление */
                try {
                    Mail::send('email.illnessFinishSafe', ['user' => $doctor, 'illness' => $illness], function ($m) use ($doctor) {
                        $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $this->domain->name);
                    });
                } catch (\Exception $e) {
                    Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
                }

                $payments = $illness->payments->where('aviso', '=', 1);
                if (count($payments) == 0) {
                    $illness->status = "archive";
                    $illness->save();
                    return back()->with('info', 'Обращение закрыто!');
                }

                return back()->with('success', 'Решение принято, ожидайте ответа второй стороны');

            } else { // Если врач принял решение, завершаем обращение

                return $this->finishSafe($domain, $request);

            }
        } else { // Если сделка обычная, а не безопасная
            /* Отправляем пациенту email уведомление */
            try {
                Mail::send('email.illnessFinishDefault', ['user' => $doctor, 'illness' => $illness], function ($m) use ($doctor) {
                    $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
            }
            $illness->status = "archive";
            $illness->save();
            return back()->with('success', 'Обращение успешно завершено');
        }

    }


    /**
     * Метод закрытия обращения по принципу Безопасной сделки
     *
     * @param $domain
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function finishSafe($domain, $request)
    {
        // Инициализируем завершение сделки
        $illness = Illness::find($request->illness_id);
        $payments = $illness->payments;
        $domainObj = $this->domain;

        if (count($payments) == 0) { // Если платежей нет, то нехрен разруливать, закрываем
            $illness->status = "archive";
            $illness->save();
        }

        if ($illness->pac_opinion == $illness->doc_opinion) { // Если стороны сошлись во мнении

            if ($illness->pac_opinion == "success" && $illness->doc_opinion == "success") {

                $illness->result = 'success';
                $illness->save();

            } else {

                $illness->result = 'fail';
                $illness->save();

            }

            // Разруливаем бабки
            return view('payment.finishSafe', [
                'illness' => $illness
            ]);
        } else {

            $user = $illness->user;
            $doctor = User::find($illness->doctor_id);
            $admin = $domainObj->users->where('role', 'admin')->first();


            // Если возник конфликт
            $illness->result = 'conflict';
            $illness->save();

            try {
                Mail::send('email.illnessConflict', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $doctor, 'illness' => $illness], function ($m) use ($doctor) {
                    $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $admin, 'illness' => $illness], function ($m) use ($admin) {
                    $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
            }
            return back()->with('info', 'В ходе завершения обращения возник конфликт сторон. Решение о закрытии обращения в пользу одной из сторон будет принято администрацией в ближайшее время');

        }
    }


    /**
     * Оставить отзыв о враче
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addReport(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'rating' => 'required',
        ]);

        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Вы не выбрали оценку');
        }

        $illness = Illness::findOrFail($request->illness_id);

        $doctor = $illness->doctor();
        if (!$doctor) {
            return back()->with('error', 'Ошибка! Врач не найден. Обратитесь в тех.поддержку');
        }

        $report = new Report();
        $report->user_id = Auth::id();
        $report->doctor_id = $doctor->id;
        $report->illness_id = $illness->id;
        $report->text = $request->text;
        $report->rating = $request->rating;
        $report->domain_id = $this->domain->id;
        $report->save();

        if ($doctor->doctor) {
            $form = $doctor->doctor;
            $form->rating = $form->rating + $request->rating;
            $form->save();
        }
	    try {
		    $this->logIt( 'Illness', $illness->id, 'Other', 'Новый отзыв', '',$illness->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return back()->with('success', 'Спасибо за отзыв!');
    }

    public function getOne($domain,$id)
    {
        $illness = Illness::findOrFail($id);

        return view('admin.illness',compact('illness'));
    }
}
