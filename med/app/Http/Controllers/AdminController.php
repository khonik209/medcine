<?php

namespace App\Http\Controllers;

use App\Chat;
use App\DiseaseHistory;
use App\Doctor;
use App\Domain;
use App\EmailTemplate;
use App\Illness;
use App\Payment;
use App\Plan;
use App\Question;
use App\Record;
use App\Services\Yandex\Yandex;
use App\Skill;
use App\Traits\LogTrait;
use App\Traits\ReturnTrait;
use App\Traits\UserTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Jenssegers\Agent\Agent;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Activity;
use Mockery\Exception;
use OpenTok\Role;
use PulkitJalan\GeoIP\GeoIP;
use Sendpulse\RestApi\ApiClient;
use Tomcorbett\OpentokLaravel\Facades\OpentokApi;

class AdminController extends Controller
{
    use ReturnTrait;
    use UserTrait;
    use LogTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        $this->middleware(['auth', 'admin']);
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);
    }

    public function index()
    {
        /* Строим график записей */
        $records = Record::with('user')->get();
        $recordsData = [
            'label' => 'Количество записей',
            'type' => 'line',
            'options' => [
                'displayAxes' => true,
                'dataType' => [
                    'xAxe' => 'date',
                    'yAxe' => 'integer'
                ],
            ],
            'data' => []
        ];
        /* Строим график онлайн-консультаций */
        $illnesses = Illness::with('user', 'chat')->get();
        $illnessesData = [
            'label' => 'Количество онлайн консультаций',
            'type' => 'line',
            'options' => [
                'displayAxes' => true,
                'dataType' => [
                    'xAxe' => 'date',
                    'yAxe' => 'integer'
                ],
            ],
            'data' => []
        ];

        for ($i = Carbon::parse($this->domain->created_at)->startOfWeek(); $i <= Carbon::now()->endOfWeek(); $i = $i->addWeek()) {
            $start = $i->format('Y-m-d');
            $end = Carbon::parse($i)->addWeek()->format('Y-m-d');

            $recordsData['data'][] = [
                //'start' => $start,
                'xAxe' => $end,
                'yAxe' => $records->where('start_date', '>=', $start)->where('end_date', '<', $end)->count(),
            ];
            $illnessesData['data'][] = [
                'xAxe' => $end,
                'yAxe' => $illnesses->where('created_at', '>=', $start)->where('created_at', '<', $end)->count(),
            ];
        }

        /* Запишемся в активность */
        $checkActivity = Activity::where('user_id', '=', Auth::id())->first();
        if (!$checkActivity) {
            Activity::log([
                'contentId' => Auth::id(),
                'contentType' => 'Patient',
                'action' => 'Opened Index',
                'description' => '',
                'details' => '',
                'domain_id' => $this->domain->id
            ]);
        } else {
            $checkActivity->updated_at = Carbon::now();
            $checkActivity->save();
        }
        /* Собираем статистику по юзерам */
        $activities = Activity::patients()->get();
        $cities = [
            'label' => 'Города',
            'type' => 'doughnut',
            'options' => [
                'displayAxes' => false,
                'dataType' => [
                    'xAxe' => 'string',
                    'yAxe' => 'integer'
                ],
            ],
            'data' => [

            ]
        ];
        $platforms = [
            'label' => 'Устройства',
            'type' => 'doughnut',
            'options' => [
                'displayAxes' => false,
                'dataType' => [
                    'xAxe' => 'string',
                    'yAxe' => 'integer'
                ],
            ],
            'data' => [

            ]
        ];
        $browsers = [
            'label' => 'Браузеры',
            'type' => 'doughnut',
            'options' => [
                'displayAxes' => false,
                'dataType' => [
                    'xAxe' => 'string',
                    'yAxe' => 'integer'
                ],
            ],
            'data' => [

            ]
        ];
        $agent = new Agent;
        $geo = new GeoIP;
        foreach ($activities as $activity) {
            /* Собираем данные по локации наших клиентов */
            try {
                $geo->setIp($activity->ip_address);
                $city = $geo->getCity();
            } catch (\Exception $e) {
                Log::error('Не удаётся подключиться к GeoIP');
                $city = 'unknown';
            }
            if (collect($cities['data'])->where('xAxe', '=', $city)->first()) {
                $cityInCollection = collect($cities['data'])->where('xAxe', '=', $city)->first();
                $cityKey = array_search($cityInCollection, $cities['data']);
                $cities['data'][$cityKey]['yAxe']++;
            } else {
                $cities['data'][] = [
                    'xAxe' => $city,
                    'yAxe' => 1
                ];
            }
            /* Собираем данные по платформе наших клиентов */
            $agent->setUserAgent($activity->user_agent);
            $platform = $agent->platform();
            if (collect($platforms['data'])->where('xAxe', '=', $platform)->first()) {
                $platformInCollection = collect($platforms['data'])->where('xAxe', '=', $platform)->first();
                $platformKey = array_search($platformInCollection, $platforms['data']);
                $platforms['data'][$platformKey]['yAxe']++;
            } else {
                $platforms['data'][] = [
                    'xAxe' => $platform,
                    'yAxe' => 1
                ];
            }
            /* Собираем данные по браузерам клиентов */
            $browser = $agent->browser();
            if (collect($browsers['data'])->where('xAxe', '=', $browser)->first()) {
                $browserInCollection = collect($browsers['data'])->where('xAxe', '=', $browser)->first();
                $browserKey = array_search($browserInCollection, $browsers['data']);
                $browsers['data'][$browserKey]['yAxe']++;
            } else {
                $browsers['data'][] = [
                    'xAxe' => $browser,
                    'yAxe' => 1
                ];
            }
        }
        $doctors = User::doctors()->get();
        return view('admin.home', [
            'records' => json_encode($recordsData),
            'illnesses' => json_encode($illnessesData),
            'cities' => json_encode($cities),
            'platforms' => json_encode($platforms),
            'browsers' => json_encode($browsers),
            'doctors' => json_encode($doctors)
        ]);
    }

    /**
     * Данные для DASHBOARD
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dashboard(Request $request)
    {
        $filter = $request->filter;
        /*
         * FILTER:
         *
         * Ближайшие записи null/records
         * Новые обращения illnesses
         * Новые пользователи users
         *
         */

        if ($filter == "illnesses") {
            $response = Illness::orderBy('created_at', 'desc')->limit(5)->with('chat', 'record', 'user')->get();
            $response->map(function ($v, $k) {
                $v->doctor = $v->doctor();
                if ($v->doctor) {
                    $v->doctor->avatar = $v->doctor->avatar_url;
                }
                return $v;
            });
        } elseif ($filter == "users") {
            $response = User::orderBy('created_at', 'desc')->where('role', '=', 'user')->limit(5)->get();
            $response->map(function ($v, $k) {
                $v->avatar = $v->avatar_url;
                return $v;
            });
        } else {
            $response = Record::where('start_date','>=',Carbon::now()->subHour())->orderBy('created_at', 'asc')->limit(5)->with('user', 'illness')->get();
            $response->map(function ($v, $k) {
                $v->doctor = $v->doctor();
                if ($v->doctor) {
                    $v->doctor->avatar = $v->doctor->avatar_url;
                }
                return $v;
            });
        }
        return response()->json([
            'type' => $filter,
            'data' => $response->toArray()
        ]);
    }

    /* О врачах */
    public function getDoctors()
    {
        $doctors = User::where('role', 'doc')->get()->sortByDesc(function ($user) {
            $la = Activity::where('user_id', $user->id)->orderByDesc('created_at')->first();
            if ($la) {
                return $la->created_at;
            } else {
                return false;
            }
        });
        return view('admin.doctors', [
            'doctors' => $doctors
        ]);
    }

    public function getDoctor($domain, $id, Request $request)
    {
        $doctor = User::find($id);
        $illnesses = Illness::where('doctor_id', '=', $doctor->id)->orderBy('created_at', 'desc')->paginate(10);
        $payments = Payment::where('doctor_id', '=', $doctor->id)->get();
        if ($this->domain->id == 1) {
            $activities = Activity::where('user_id', $doctor->id)->orderBy('created_at', 'desc')->get();
        } else {
            $activities = Activity::where('user_id', $doctor->id)->orderBy('created_at', 'desc')->first();
        }
        $form = $doctor->doctor;
        $skills = $doctor->skills;
        $lastActivity = Activity::where('user_id', $doctor->id)->orderBy('created_at', 'desc')->first();
        $reports = $doctor->allReports();
        return view('admin.doctor', [
            'doctor' => $doctor,
            'illnesses' => $illnesses,
            'payments' => $payments,
            'request' => $request,
            'activities' => $activities,
            'skills' => $skills,
            'form' => $form,
            'lastActivity' => $lastActivity,
            'reports' => $reports
        ]);
    }

    /**
     * Проверка врача
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verifyDoctor(Request $request)
    {
        $doctor = User::find($request->doctor_id);
        $form = $doctor->doctor;
        $form->verified = $request->verified;
        $form->save();
        return back()->with('success', 'Успешно');
    }

    public function addDoctor($domain, Request $request)
    {
        $doctor = $this->createUser($request->email, null, $request->name, null, $this->domain->id, 'doc');
        try {
            $this->logIt('User', $doctor->id, 'CreateUser', 'Пользователь создан', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return back()->with('success', 'Врач создан, доступы от кабинета отправлены на почту');
    }

    public function editDoctorPage($domain, $id)
    {
        $doctor = User::find($id);
        if (!$doctor) {
            return redirect('/admin/doctors')->with('error', 'Врач не найден');
        }
        $form = $doctor->doctor;
        $skills = Skill::orderBy('name')->get();
        $days = [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ];

        return view('admin.doctorEdit', [
            'doctor' => $doctor,
            'form' => $form,
            'skills' => $skills,
            'days' => $days
        ]);
    }

    public function editDoctor(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'job' => 'required',
            'experience' => 'required',
            'education' => 'required',
            'level' => 'required',
            'skills' => 'required'
        ]);
        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Указаны не все обязательные поля');
        }
        $doctor = User::find($request->doctor_id);
        if (!$doctor) {
            return back()->with('error', 'Ошибка! Врач не найден');
        }
        $form = $doctor->doctor;
        if (!$form) {
            $form = new Doctor;
            $form->user_id = $doctor->id;
            $form->domain_id = $this->domain->id;
        }
        $form->job = $request->job;
        $form->education = $request->education;
        $form->experience = $request->experience;
        $form->level = $request->level;
        $form->specialty = $request->specialty;
        $form->infomation = $request->infomation;
        $form->save();

        if (count($request->skills) > 0) {
            $toSync = [];
            foreach ($request->skills as $id) {
                $toSync[] = $id;
            }
            $doctor->skills()->sync($toSync);
        }
        try {
            $this->logIt('User', $doctor->id, 'EditUser', 'Пользователь отредактирован', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return back()->with('success', 'Сохранено!');
    }

    /**
     * Сохраняем рабочий график врача
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setWorkTime(Request $request)
    {

        $doctor = User::find($request->doctor_id);
        if (!$doctor || !$doctor->doctor) {
            return back()->with('error', 'Ошибка при сохранении! Попробуйте обновить страницу и сохранить еще раз');
        }
        $form = $doctor->doctor;

        $data = collect($request->all());
        $data->forget('_token');
        $data->forget('doctor_id');
        $form->work_time = $data;
        $form->save();
        try {
            $this->logIt('User', $doctor->id, 'EditUser', 'Установлено рабочее время', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return back()->with('success', 'Сохранено!');
    }

    public function deleteDoctor($domain, Request $request)
    {
        $doctor = User::findOrFail($request->doctor_id);
        $account = $doctor->doctor;
        if ($account) {
            $account->delete();
        }
        $doctor->delete();
        try {
            $this->logIt('User', $doctor->id, 'DeleteUser', 'Пользователь создан', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return redirect('/admin/doctors')->with('success', 'Врач удалён');
    }

    /**
     * Вывести список всех пациентов
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function getUsers($domain, Request $request)
    {
        $searchQuery = urldecode($request->filter);
        if ($searchQuery) {
            $users = User::where('role', 'user')->where(function ($q) use ($searchQuery) {
                $q->where('email', 'like', "%$searchQuery%")->orWhere('phone', 'like', "%$searchQuery%")->orWhere('name', 'like', "%$searchQuery%");
            });
        } else {
            $users = User::where('role', 'user');
        }

        if ($request->dir == "asc") {
            $dir = 'asc';
        } else {
            $dir = 'desc';
        }

        if ($request->sort) {
            $sortBy = $request->sort;
            if ($sortBy == "id") {
                $users = $users->orderBy('id', $dir);
            } elseif ($sortBy == "email") {
                $users = $users->orderBy('email', $dir);
            } elseif ($sortBy == "name") {
                $users = $users->orderBy('name', $dir);
            } elseif ($sortBy == "phone") {
                $users = $users->orderBy('phone', $dir);
            } elseif ($sortBy == "illnesses") {
                $users = $users->withCount('illnesses')->orderBy('illnesses_count', $dir);
            } else {
                $users = $users->orderBy('created_at', $dir);
            }
        }

        $users = $users->paginate(30);

        $doctors = User::doctors()->has('doctor')->get();

        return view('admin.users', [
            'users' => $users,
            'activeDoctors' => $doctors,
            'request' => $request
        ]);
    }

    /**
     * Создание пациента
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addUser(Request $request)
    {
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            $user = $checkUser;
        } else {
            // Создаем Пациента
            $user = $this->createUser($request->email, null, $request->name, null, $this->domain->id, 'user', $request->age);
        }
        try {
            $this->logIt('User', $user->id, 'CreateUser', 'Пользователь создан', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return redirect('/admin/user/' . $user->id)->with('success', 'Пациент добавлен!');

    }

    /**
     * Отрыть информацию о пользователе
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUser($domain, $id)
    {
        $user = User::findOrFail($id);
        $illnesses = $user->illnesses;
        $lastActivity = Activity::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
        return view('admin.user', [
            'user' => $user,
            'illnesses' => $illnesses,
            'lastActivity' => $lastActivity
        ]);
    }

    /**
     * Страница редактирования пользователя
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUserPage($domain, $id)
    {
        $user = User::findOrFail($id);

        return view('admin.userEdit', compact('user'));
    }

    /**
     * Редактирование профиля пользователя
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editUser($domain, Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->name = $request->name;
        $user->age = $request->age;
        $user->phone = $request->phone;
        $user->save();
        try {
            $this->logIt('User', $user->id, 'EditUser', 'Пользователь отредактирован', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return back()->with('success', 'Сохранено!');
    }

    /**
     * Открыть историю болезни
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDiseaseHistory($domain, $id)
    {
        $diseaseHistory = DiseaseHistory::findOrFail($id);

        $text = (object)$diseaseHistory->text;
        $user = $diseaseHistory->user;
        return view('admin.diseaseHistory', compact('diseaseHistory', 'text', 'user'));
    }

    /**
     * Удалить пользователя
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteUser($domain, Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $user->delete();
        try {
            $this->logIt('User', $user->id, 'DeleteUser', 'Пользователь удалён', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return redirect('/admin/users')->with('success', 'Пользователь удалён');
    }

    /**
     * Скачать клиентов в формате EXCEL
     */
    public function exportUsers()
    {

        $users = User::orderBy('created_at', 'desc')->where('role', 'user')->get(['id', 'name', 'email', 'phone', 'created_at', 'age']);
        //        $users = $pre_users->only(['id', 'name', 'email', 'phone', 'created_at', 'weight', 'height', 'waist', 'chest', 'hip']);
        Excel::create('users_list_' . Carbon::now()->format('Y-m-d'), function ($excel) use ($users) {

            $excel->sheet('Клиенты клиники', function ($sheet) use ($users) {

                $sheet->fromArray($users);

                $sheet->row(1, array(
                    'id',
                    'Имя',
                    'Email',
                    'Телефон',
                    'Дата регистрации',
                    'Возраст',
                ));

            });

        })->download('xls');
    }

    /**
     * Отправить сообщение пользователю по email
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendEmailToUser($domain, Request $request)
    {
        $validation = Validator::make($request->all(), [
            'user_id' => 'required',
            'text' => 'required|string',
        ]);
        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Нельзя отправить пустое письмо');
        }
        $user = User::findOrFail($request->user_id);

        try {
            Mail::send('email.customEmail', ['text' => $request->text], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject("Сообщение от " . $this->domain->name);
            });
            try {
                $this->logIt('User', $user->id, 'Other', 'Письмо отправлено', $request->text, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность: ' . $e);
            }
            return back()->with('success', 'Письмо отправлено');
        } catch (\Exception $e) {
            Log::error('Письмо от админу пользователю' . $user->email . ' не отправились' . $e);

            return back()->with('error', 'Ошибка! Не удалось отправить email');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChatsPage()
    {
        return view('admin.chats');
    }


    public function getRequestPage($domain, $id)
    {
        $chat = Chat::find($id);
        $currentUser = Auth::user(); // Тот, кто зашел на страницу

        if (!$currentUser->chats->contains($chat)) {
            return back()->with('info', 'В доступе отказано');
        }

        $illness = $chat->illness;

        $user = $chat->users()->patients()->first(); // Пациент

        $doctors = User::doctors()->get(); // Все врачи - для поиска при добалении

        $doctor = $doctors->where('id', $chat->doctor_id)->first(); // Лечащий доктор
        if (!$doctor) {
            $doctor = Auth::user();
        }
        $members = $chat->users()->get(); // Список участников беседы

        //все прикреплённые документы
        $urls = $chat->files()->paginate(10);
        if ($this->domain->custom('videochat')) {
            try {
                // Получаем токены OpenTok

                // new session
                $session = OpentokApi::createSession();
                $sessionId = $session->getSessionId();

                // check if it's been created or not (could have failed)
                if (empty($sessionId)) {
                    throw new \Exception("An open tok session could not be created");
                }
                // get your API key from config
                $api_key = Config::get('opentok.api_key');

                // then create a token (session created in previous step)
                try {
                    // note we're create a publisher token here, for subscriber tokens we would specify.. yep 'subscriber' instead
                    $token = OpentokApi::generateToken($sessionId, array(
                        'role' => Role::PUBLISHER
                    ));
                } catch (OpenTokException $e) {
                    // do something here for failure
                }
                $diff = Carbon::parse($chat->updated_at)->diffInHours(Carbon::now());

                if ($chat->session == null || $chat->token == null || $diff >= 2) {
                    $chat->session = $sessionId;
                    $chat->token = $token;
                    $chat->save();
                }

                $session = Chat::find($chat->id)->session;
                $currentToken = Chat::find($chat->id)->token;
            } catch (\Exception $e) {
                $session = null;
                $api_key = null;
                $currentToken = null;
                Log::error('Не удалось подключиться к opentok: ' . $e);
            }
        } else {
            $session = null;
            $api_key = null;
            $currentToken = null;
        }
        if ($this->domain->id == 1) {
            try {
                $this->log($doctor->id, 'Page', 'Открыл страницу', $chat->name, '');
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('admin.request', [
            'illness' => $illness,
            'patient' => $user,
            'doctor' => $doctor,
            'chat' => $chat,
            'id' => $id,
            'doctors' => $doctors,
            'members' => $members,
            'urls' => $urls,
            'currentUser' => $currentUser,
            'session_id' => $session,
            'api_key' => $api_key,
            'token' => $currentToken,
        ]);
    }

    /**
     * Завершение обращения и решение по вопросам платежа
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finishRequest($domain, Request $request)
    {
        $illness = Illness::find($request->illness_id);

        // Выкидываем, если обращение не найдено
        if (!$illness) {
            return back()->with('info', 'Обращение не найдено');
        }

        $user = $illness->user;

        //Проверяем форму оплаты. Простая или "Безопасная"
        if ($this->domain->payment_form == "safe") {

            // Записываем решение врача
            $illness->doc_opinion = $request->opinion;
            $illness->save();

            try {
                $this->log($illness->user->id, 'Illness', 'Завершил обращение', $user->name, 'Illness: ' . $illness->name);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }

            // Если пациент еще не принял решение - ожидаем
            if ($illness->pac_opinion == "none") {
                /* Отправляем пациенту email уведомление */
                try {
                    Mail::send('email.illnessFinishSafe', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                        $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                    });
                } catch (\Exception $e) {
                    Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
                }

                $payments = $illness->payments->where('aviso', '=', 1);
                if (count($payments) == 0) {
                    $illness->status = "archive";
                    $illness->save();
                    return back()->with('info', 'Обращение закрыто!');
                }

                return back()->with('success', 'Решение принято, ожидайте ответа второй стороны');

            } else { // Если пациент принял решение, завершаем обращение

                return $this->finishSafe($domain, $request);

            }
        } else { // Если сделка обычная, а не безопасная

            /* Отправляем пациенту email уведомление */
            try {
                Mail::send('email.illnessFinishDefault', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
            }

            $illness->status = "archive";
            $illness->save();

            try {
                $this->logIt('Illness', $illness->id, 'FinishRequest', 'Обращение завершено', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность: ' . $e);
            }

            return back()->with('success', 'Обращение успешно завершено');
        }
    }

    /**
     * Метод закрытия обращения по принципу Безопасной сделки
     *
     * @param $domain
     * @param $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function finishSafe($domain, $request)
    {
        // Инициализируем завершение сделки
        $domainObj = $this->domain;
        $illness = Illness::find($request->illness_id);
        $payments = $illness->payments->where('aviso', '=', 1);
        if (count($payments) == 0) {
            $illness->status = "archive";
            $illness->save();
            return back()->with('info', 'Обращение закрыто!');
        }
        if ($illness->pac_opinion == $illness->doc_opinion) { // Если стороны сошлись во мнении

            if ($illness->pac_opinion == "success" && $illness->doc_opinion == "success") {

                $illness->result = 'success';
                $illness->save();

            } else {

                $illness->result = 'fail';
                $illness->save();

            }

            // Разруливаем бабки
            return view('payment.finishSafe', [
                'illness' => $illness
            ]);
        } else {

            $user = $illness->user;
            $doctor = User::find($illness->doctor_id);
            $admin = $domainObj->users->where('role', 'admin')->first();


            // Если возник конфликт
            $illness->result = 'conflict';
            $illness->save();

            try {
                Mail::send('email.illnessConflict', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $doctor, 'illness' => $illness], function ($m) use ($doctor) {
                    $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $admin, 'illness' => $illness], function ($m) use ($admin) {
                    $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
            }

            return back()->with('info', 'В ходе завершения обращения возник конфликт сторон. Решение о закрытии обращения в пользу одной из сторон будет принято администрацией в ближайшее время');
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptRequest($domain, $id)
    {
        $illness = Illness::find($id);
        if ($illness->doctor_id == Auth::user()->id) {
            $illness->status = 'active';
            $illness->save();

            // Отправляем уведомление пациенту
            $user = $illness->user;
            try {
                Mail::send('email.activeRequestUser', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Врач ответил на Ваше обращение на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о принятии обращения пользователю ' . $user->email . ' не отправились' . $e);
            }
            if ($this->domain->id == 1) {
                try {
                    $this->log($user->id, 'Illness', 'Принял заявку', 'Принял заявку от ' . $user->name, $illness->name);
                } catch (\Exception $e) {
                    Log::error('Не удалось записать активность врача: ' . $e);
                }
            }
            try {
                $this->logIt('Illness', $illness->id, 'EditRequest', 'Заявка принята', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность: ' . $e);
            }
            return back()->with('success', 'Заявка принята');
        } else {
            return back()->with('info', 'В доступе отказано');
        }

    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function denyRequest($domain, $id)
    {
        $illness = Illness::find($id);
        if ($illness->doctor_id == Auth::user()->id) {
            $illness->status = 'denied';
            $illness->save();
            if ($this->domain->id == 1) {
                try {
                    $this->log($illness->user->id, 'Illness', 'Отклонил заявку', 'Отклонил заявку от ' . $illness->user->name, 'Illness: ' . $illness->name);
                } catch (\Exception $e) {
                    Log::error('Не удалось записать активность врача: ' . $e);
                }
            }
            try {
                $this->logIt('Illness', $illness->id, 'EditRequest', 'Заявка отклонена', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность: ' . $e);
            }
            return back()->with('success', 'Заявка отклонена');
        } else {
            return back()->with('info', 'В доступе отказано');
        }
    }

    /**
     * Страница истории (выбор между платежами и обращениями)
     *
     * @param $domain
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHistory($domain)
    {
        /* Убираем из выборки Пупкина */
        $user = User::where('email', 'vasya@pupkin.net')->first();
        if ($user) {
            $payments = Payment::where('user_id', '!=', $user->id)->where('type', 'request')->count();
            $illnesses = Illness::where('user_id', '!=', $user->id)->count();
            $records = Record::where('user_id', '!=', $user->id)->count();
        } else {
            $payments = Payment::where('type', 'request')->count();
            $illnesses = Illness::count();
            $records = Record::count();
        }
        return view('admin.history', [
            'illnesses' => $illnesses,
            'payments' => $payments,
            'records' => $records
        ]);
    }

    /**
     * Выводим все обращения
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIllnesses($domain, Request $request)
    {
        /* Применяем фильтры */
        $illnesses = Illness::orderBy('created_at', 'desc'); // Берём всё
        $filtered = false;
        // Фильтруем
        if ($request->search) {
            $search = $request->search;
            $illnesses = $illnesses->whereHas('user', function ($q) use ($search) {
                $q->where('email', 'like', "%$search%")->orWhere('name', 'like', "%$search%")->orWhere('phone', 'like', "%$search%");
            });
            $filtered = true;
        }

        if ($request->status) {
            $illnesses = $illnesses->where('status', $request->status);
            $filtered = true;
        }

        if ($request->result) {
            $illnesses = $illnesses->where('result', $request->result);
            $filtered = true;
        }
        if ($request->form) {
            if ($request->form == "online") {
                $illnesses = $illnesses->has('chat');
                $filtered = true;
            } elseif ($request->form == "offline") {
                $illnesses = $illnesses->has('chat', '<', 1);
                $filtered = true;
            }
        }
        /* Убираем из выборки Пупкина */
        $user = User::where('email', 'vasya@pupkin.net')->first();
        if ($user) {
            $illnesses = $illnesses->where('user_id', '!=', $user->id);
        }
        // Выводим
        $illnesses = $illnesses->paginate(30);

        return view('admin.components.history.illnesses', [
            'illnesses' => $illnesses,
            'request' => $request,
            'filtered' => $filtered
        ]);
    }

    /**
     * Изменение статуса обращения
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateIllness(Request $request)
    {
        $illness = Illness::find($request->illness_id);
        if (!$illness) {
            return back()->with('error', 'Обращение не найдено');
        }
        $illness->status = $request->status;
        $illness->save();
        try {
            $this->logIt('Illness', $illness->id, 'EditRequest', 'Статус изменён', 'Новый статус: ' . $illness->status, $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return back()->with('success', 'Изменено!');
    }

    /**
     * Скачать Обращения в формате EXCEL
     */
    public function exportIllnesses()
    {

        $illnesses = Illness::orderBy('created_at', 'desc')->get();
        $toExport = [];
        foreach ($illnesses as $illness) {
            $payments = $illness->payments->where('aviso', '=', 1);
            if ($payments->count() > 0) {
                $sum = $payments->sum('sum');
            } else {
                $sum = '-';
            }
            $doc = $illness->doctor();
            if ($doc) {
                $docName = $doc->name;
            } else {
                $docName = '';
            }
            $user = $illness->user;
            if ($user) {
                $userEmail = $user->email;
            } else {
                $userEmail = $user->email;
            }
            $toExport[] = [
                'ID' => $illness->id,
                'Врач' => $docName,
                'Пациент' => $userEmail,
                'Название' => $illness->name,
                'Описание' => $illness->description,
                'Дата создания' => Carbon::parse($illness->created_at)->format('d.m.Y H:i:s'),
                'Статус' => $illness->status,
                'Платёж' => $sum
            ];
        }
        $toExport = collect($toExport);
        Excel::create('illnesses_list_' . Carbon::now()->format('Y-m-d'), function ($excel) use ($toExport) {

            $excel->sheet('Обращения клиники', function ($sheet) use ($toExport) {

                $sheet->fromArray($toExport);

                $sheet->row(1, array(
                    'id',
                    'Врач',
                    'Пациент',
                    'Название',
                    'Описание',
                    'Дата создания',
                    'Статус',
                    'Платёж'
                ));
            });

        })->download('xls');
    }

    /**
     * Показать все платежи
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPayments($domain, Request $request)
    {
        if (!$this->domain->custom('payments')) {
            abort(404);
        }
        $filtered = false;
        /* Ловим фильтры */
        if ($request->search) {
            $search = urldecode($request->search);
        }

        /* Применяем фильтры */
        $payments = Payment::orderBy('created_at', 'desc'); // Берём всё
        // Фильтруем
        if (isset($search)) {
            $payments = $payments->whereHas('user', function ($q) use ($search) {
                $q->where('email', 'like', "%$search%")->orWhere('name', 'like', "%$search%")->orWhere('phone', 'like', "%$search%");
            });
            $filtered = true;
        }
        /* Убираем из выборки Пупкина */
        $user = User::where('email', 'vasya@pupkin.net')->first();
        if ($user) {
            $payments = $payments->where('user_id', '!=', $user->id);
        }
        // Выводим
        $payments = $payments->paginate(30);
        return view('admin.components.history.payments', [
            'payments' => $payments,
            'request' => $request,
            'filtered' => $filtered
        ]);
    }

    /**
     * Скачать платежи в формате EXCEL
     */
    public function exportPayments()
    {

        $payments = Payment::where('type', 'request')->orderBy('created_at', 'desc')->get();
        $toExport = [];
        foreach ($payments as $payment) {
            $doc = $payment->doctor();
            if ($doc) {
                $docName = $doc->name;
            } else {
                $docName = '';
            }
            $toExport[] = [
                'ID' => $payment->id,
                'Дата создания' => Carbon::parse($payment->created_at)->format('d.m.Y H:i:s'),
                'Дата оплаты' => Carbon::parse($payment->updated_at)->format('d.m.Y H:i:s'),
                'Пациент' => $payment->email,
                'Врач' => $docName,
                'Сумма' => $payment->sum,
                'Оплата' => $payment->aviso,
                'Обращение' => $payment->illness->name
            ];
        }
        Excel::create('payments_list_' . Carbon::now()->format('Y-m-d'), function ($excel) use ($toExport) {

            $excel->sheet('Платежи клиники', function ($sheet) use ($toExport) {

                $sheet->fromArray($toExport);

                $sheet->row(1, array(
                    'id',
                    'Дата создания',
                    'Дата оплаты',
                    'Пациент',
                    'Врач',
                    'Сумма',
                    'Оплата',
                    'Обращение'
                ));

            });

        })->download('xls');
    }


    /**
     * Удаление платежа
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deletePayment($domain, $id)
    {
        $payment = Payment::find($id);
        if (!$payment) {
            return back()->with('error', 'Платёж не найден');
        }
        $payment->delete();

        return back()->with('success', 'Платёж удален');

    }

    /**
     * Возвращает все записи
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRecords($domain, Request $request)
    {
        $filtered = false;
        /* Ловим фильтры */
        if ($request->search) {
            $search = urldecode($request->search);
        }
        $records = Record::orderBy('start_date', 'desc');

        if (isset($search)) {
            $records = $records->whereHas('user', function ($q) use ($search) {
                $q->where('email', 'like', "%$search%")->orWhere('name', 'like', "%$search%")->orWhere('phone', 'like', "%$search%");
            });

            $filtered = true;
        }
        if ($request->start) {
            $records = $records->whereBetween('start_date', [Carbon::parse($request->start)->format('Y-m-d'), Carbon::parse($request->start)->addDay()->format('Y-m-d')]);
            $filtered = true;
        }
        $records = $records->paginate(30);
        return view('admin.components.history.records', [
            'records' => $records,
            'request' => $request,
            'filtered' => $filtered
        ]);
    }

    /**
     * Скачать записи в формате EXCEL
     */
    public function exportRecords()
    {

        $records = Record::orderBy('created_at', 'desc')->get();
        $toExport = [];
        foreach ($records as $record) {
            $toExport[] = [
                'ID' => $record->id,
                'Дата создания' => Carbon::parse($record->created_at)->format('d.m.Y H:i:s'),
                'Дата записи' => Carbon::parse($record->start_date)->format('d.m.Y H:i:s'),
                'Пациент' => $record->user ? $record->user->email : 'удалён',
                'Врач' => $record->doctor() ? $record->doctor()->name : 'удалён',
                'Описание' => $record->name,
                'Комментарии' => $record->comment
            ];
        }
        Excel::create('records_list_' . Carbon::now()->format('Y-m-d'), function ($excel) use ($toExport) {

            $excel->sheet('Записи клиники', function ($sheet) use ($toExport) {

                $sheet->fromArray($toExport);

                $sheet->row(1, array(
                    'id',
                    'Дата создания',
                    'Дата записи',
                    'Пациент',
                    'Врач',
                    'Описание',
                    'Комментарии',
                ));
            });

        })->download('xls');
    }

    /* Конфликты */
    public function getConflicts()
    {
        $conflicts = Illness::where('result', 'conflict')->get();

        return view('admin.conflicts', [
            'conflicts' => $conflicts
        ]);
    }

    public function solveConflict($domain, Request $request)
    {
        $checkResult = $request->result;
        if ($checkResult) {
            $illness = Illness::find($request->illness_id);
            $result = $checkResult;

            if ($result == "doc") {

                $illness->result = 'success';
                $illness->status = "archive";
                $illness->save();
            } else {

                $illness->result = 'fail';
                $illness->status = "archive";
                $illness->save();
            }

            // Разруливаем бабки
            return view('payment.finishSafe', [
                'illness' => $illness
            ]);

        } else {
            return back()->with('info', 'Выберите решение');
        }
    }

    /* Настройки */
    /**
     * Страница настроек кабинета
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccountPage()
    {
        $admin = Auth::user();
        $plans = Plan::all();
        $skills = Skill::orderBy('name')->get()->toJson();
        $questions = Question::orderBy('order')->get()->toJson();
        $days = [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ];

        return view('admin.account', [
            'admin' => $admin,
            'plans' => $plans,
            'skills' => $skills,
            'questions' => $questions,
            'days' => $days
        ]);
    }

    /**
     * Сохранить данные администратора
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function adminSettings($domain, Request $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();
        return back()->with('success', 'Сохранено');
    }

    /**
     * Страница смены пароля
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getResetPasswordPage()
    {
        return view('admin.resetPassword');
    }

    /**
     * Сменить пароль
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword($domain, Request $request)
    {
        $user = Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            if ($request->new_password == $request->confirm_password) {
                $user->password = bcrypt($request->new_password);
                $user->save();
            } else {
                return back()->with('info', 'Введённые пароли не совпадают!');
            }
        } else {
            return back()->with('info', 'Неправильный пароль');
        }

        return redirect('/admin/settings/')->with('success', 'Сохранение прошло успешно!');
    }

    /**
     * Сохранение данных о компании
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function companySettings($domain, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company' => 'required',
            'address' => 'required',
            'feedback_email' => 'required|email',
            'logo' => 'image|max:5120',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $domain = $this->domain;
        $domain->company = $request->company;
        $domain->address = $request->address;
        $domain->feedback_email = $request->feedback_email;

        if ($request->phone) {
            $domain->phone = $request->phone;
        }

        if ($request->color) {
            $domain->color = $request->color;
        }

        if ($request->hasFile('logo')) {
            if ($domain->logo) {
                /* Удаляем старый файл */
                Storage::disk('s3')->delete($domain->logo);
            }
            /* Сохраняем новый */
            $file = $request->file('logo');
            $extension = $file->clientExtension();
            $filename = $this->domain->name . '/logo/' . time() . "." . $extension;
            $s3 = Storage::disk('s3');
            $s3->put($filename, file_get_contents($file), 'public');
            $domain->logo = $filename;
        }

        $domain->save();
        return back()->with('success', 'Сохранено');
    }

    /**
     * Сохраняем настройки ЯНДЕКС КАССЫ
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function yandexSettings($domain, Request $request)
    {

        $domain = $this->domain; // Получаем экземпляр домена
        /* Обновляем значение формы приема платежей (kassa || button) */
        $options = $domain->options;
        if (!$options) {
            $options = (object)[];
        }
        $options->payment_type = $request->payment_type;
        $domain->options = $options;

        if ($request->payment_type == "button") {
            if (!$request->payment_account || !$request->secret_button) {
                return back()->with('error', 'Указаны не все данные');
            }
            $domain->yandex_secret = $request->secret_button;
            $domain->payment_account = $request->payment_account;
        } else {
            if (!$request->sc_id || !$request->shop_id || !$request->secret_kassa) {
                return back()->with('error', 'Указаны не все данные');
            }
            $domain->yandex_scid = $request->sc_id;
            $domain->yandex_shopid = $request->shop_id;
            $domain->yandex_secret = $request->secret_kassa;
        }

        if ($domain->id == 1) {
            $domain->payment_form = 'safe';
        } else {
            $domain->payment_form = 'default';
        }
        $domain->save();

        return back()->with('success', 'Сохранено');
    }

    /**
     * Сохраняем настройки для кабинета
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveFunctions(Request $request)
    {
        $domain = $this->domain;
        if ($request->custom_functions) {
            $domain->custom_functions = $request->custom_functions;
        } else {
            $domain->custom_functions = null;
        }
        $domain->save();

        return back()->with('success', 'Настройки сохранены');
    }

    /**
     * Сохранение специальностей
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function syncSkills(Request $request)
    {
        $newSkills = [];
        if ($request->new_skills) {
            foreach ($request->new_skills as $skill) {
                $newSkill = new Skill;
                $newSkill->name = $skill['name'];
                $newSkill->domain_id = $this->domain->id;
                $newSkill->save();
                $newSkills[] = $newSkill;
            }
        }

        if ($request->deleted_skills) {
            try {
                $deletedIds = collect($request->deleted_skills)->pluck('id'); // array of skill`s ID to delete
                Skill::whereIn('id', $deletedIds)->delete();
            } catch (\Exception $e) {
                return json_encode([
                    'error' => 1,
                    'msg' => $e
                ]);
            }
        }
        return json_encode([
            'error' => 0,
            'msg' => $newSkills
        ]);
    }

    /**
     * Обновить список вопросов анкеты
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function syncQuestions($domain, Request $request)
    {
        $currentQuestions = $request->currentQuestions;
        $deletedQuestions = $request->deletedQuestions;
        foreach ($deletedQuestions as $q) {
            $question = Question::find($q);
            if ($question) {
                if ($question->domain_id) { // не удаляем общие вопросы
                    $question->delete();
                }
            }
        }
        foreach ($currentQuestions as $key => $q) {
            $checkQ = isset($q['id']) ? Question::find($q['id']) : null;
            if ($checkQ) {
                $checkQ->order = $q['order'];
                $checkQ->save();
            } else {
                $newQuestion = new Question;
                $newQuestion->label = $q['label'];
                $newQuestion->type = $q['type'];
                if (isset($q['options'])) {
                    $newQuestion->options = $q['options'];
                }
                if (isset($q['hint'])) {
                    $newQuestion->hint = $q['hint'];
                }
                if (isset($q['required'])) {
                    $newQuestion->required = $q['required'];
                }
                $newQuestion->domain_id = $this->domain->id;
                $newQuestion->order = $q['order'];
                $newQuestion->save();
            }
        }
        return $this->printResponse(0, 'Список вопросов обновлен', $this->domain->questions);
    }

    /**
     * Сохранить часы работы клиники
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveBusinessHours(Request $request)
    {
        $domain = $this->domain;
        /* Обновляем значение формы приема платежей (kassa || button) */
        $options = $domain->options;
        if (!$options) {
            $options = (object)[];
        }

        $data = collect($request->all());
        $data->forget('_token');

        $options->business_hours = $data;
        $domain->options = $options;

        $domain->save();

        return back()->with('success', 'Сохранено!');
    }

    /**
     * Логирование действий
     *
     * @param null $contentId
     * @param string $contentType
     * @param string $action
     * @param string $description
     * @param string $details
     */
    private function log($contentId = null, $contentType = "User", $action = "", $description = "", $details = "")
    {
        Activity::log([
            'contentId' => $contentId,
            'contentType' => $contentType,
            'action' => $action,
            'description' => $description,
            'details' => $details,
            //  'updated'     => (bool) $id,
        ]);
    }
}
