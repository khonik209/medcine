<?php

namespace App\Http\Controllers;

use App\Domain;
use App\EmailTemplate;
use App\User;
use Illuminate\Http\Request;
use Sendpulse\RestApi\ApiClient;

class EmailController extends Controller
{
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        $this->middleware(['auth', 'admin']);
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);
    }

    public function getEmailsPage($domain)
    {
        return view('admin.emails');
    }

    /**
     * Страница шаблонов email-ов
     *
     * @param $domain
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTemplatesPage($domain)
    {
        $templates = EmailTemplate::all();

        return view('admin.emailTemplates.emailTemplates', compact('templates'));
    }

    /**
     * Страница шаблонов email-ов
     *
     * @param $domain
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newEmailsPage($domain)
    {

        return view('admin.emailTemplates.newTemplate');
    }

    /**
     * Сохраняем шаблон
     *
     * @param Request $request
     *
     * @return int
     */
    public function saveTemplate(Request $request)
    {
        $body = $request->body;
        $domain = $this->domain;
        $template = new EmailTemplate;
        $template->domain_id = $domain->id;
        $template->body = base64_encode($body);
        $template->name = $request->name;
        $template->save();
        return 1;
    }

    /**
     * Удаление шаблона
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteTemplate(Request $request)
    {
        $template = EmailTemplate::findOrFail($request->template_id);
        $template->delete();
        return back()->with('success', 'Шаблон удален');
    }

    /**
     * Список адресных email книг
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEmailBooks($domain)
    {
        $books = $this->domain->email_books;
        return view('admin.emailTemplates.books', [
            'books' => json_encode($books)
        ]);
    }


    /**
     * Создать книгу в SendPulse
     *
     * @param Request $request
     *
     * @return bool
     * @throws \Exception
     */
    public function createEmailBook(Request $request)
    {

        $name = $request->name;
        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));
        $response = $sp->createAddressBook($this->domain->id . '_' . $name);
        if (isset($response->id)) {

            $domain = $this->domain;

            $id = $response->id;

            $books = $domain->email_books;
            $books[] = [
                'sp_id' => $id,
                'name' => $name
            ];

            $domain->email_books = $books;
            $domain->save();

            return json_encode([
                'error' => 0,
                'msg' => [
                    'sp_id' => $id,
                    'name' => $name
                ]
            ]);
        } elseif (isset($response->error_code)) {
            $error = $response->error_code;
            if ($error == 203) {
                $errMsg = "Название книги уже занято!";
            } else {
                $errMsg = "Неизвестная ошибка";
            }
        } else {
            $error = 1;
            $errMsg = "Неизвестная ошибка";
        }


        return json_encode([
            'error' => $error,
            'msg' => $errMsg
        ]);
    }

    /**
     * Посмотреть все записи в книге
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getEmailBook($domain, $id, Request $request)
    {
        $book = collect($this->domain->email_books)->where('sp_id', '=', $id)->first();

        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));

        $contacts = $sp->getEmailsFromBook($id);

        return view('admin.emailTemplates.book', compact('book', 'contacts', 'users'));
    }

    /**
     * Выбрать пользователей на добавление (VUE AXIOS)
     *
     * @param $domain
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|string|static[]
     */
    public function getDomainUsers($domain, Request $request)
    {
        if ($request->filter) {
            $filter = $request->filter;
            if ($filter == "last_day") {
                return User::where('role', 'user')->where('created_at', '>=', Carbon::now()->subDay())->get()->toJson();
            } elseif ($filter == "last_week") {
                return User::where('role', 'user')->where('created_at', '>=', Carbon::now()->subWeek())->get()->toJson();
            } elseif ($filter == "last_month") {
                return User::where('role', 'user')->where('created_at', '>=', Carbon::now()->subMonth())->get()->toJson();
            } elseif ($filter == "with_payment") {
                return User::where('role', 'user')->whereHas('payments', function ($q) {
                    $q->where('aviso', '=', 1);
                })->get();
            } elseif ($filter == "without_consultation") {
                return User::where('role', 'user')->has('illnesses', '=', '0')->get();
            }
        }
        $patients = User::where('role', '=', 'user');
        return $patients->where('email', 'like', "%$request->q%")->orWhere('name', 'like', "%$request->q%")->orWhere('phone', 'like', "%$request->q%")->get()->toJson();

    }

    /**
     * Синхронизировать книгу SendPulse (VUE AXIOS)
     *
     * @param Request $request
     *
     * @return int|mixed
     * @throws \Exception
     */
    public function syncEmailBook(Request $request)
    {
        $book = $request->book;

        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));


        if ($request->has('new')) {
            $emails = [];
            foreach ($request->new as $user) {
                $emails[] = [
                    'email' => $user['email'],
                    'variables' => [
                        'Name' => $user['name'],
                        'Phone' => $user['phone']
                    ]
                ];
            }

            $sp->addEmails($book, $emails);
        }

        if ($request->has('deleted')) {
            $emails = [];
            foreach ($request->deleted as $email) {
                $emails[] = $email;
            }
            $sp->removeEmails($book, $emails);
        }

        return 1;
    }

    /**
     * Удаление книги
     *
     * @param Request $request
     *
     * @return int
     * @throws \Exception
     */
    public function deleteEmailBook(Request $request)
    {
        $sp_id = $request->sp_id;
        $domainBooks = collect($this->domain->email_books);
        $domainBooks = $domainBooks->filter(function ($v, $k) use ($sp_id) {
            if ($v['sp_id'] != $sp_id) {
                return true;
            } else {
                return false;
            }
        });

        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));
        $response = $sp->removeAddressBook($sp_id);
        if ($response) {
            $domain = $this->domain;
            $domain->email_books = $domainBooks;
            $domain->save();
            return json_encode([
                'error' => 0,
                'msg' => 'book has been deleted'
            ]);
        } else {
            return json_encode([
                'error' => 1,
                'msg' => 'error while deleting book'
            ]);
        }
    }

    /**
     * Смотрим все email кампании аккаунта
     *
     * @param $domain
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCampaigns($domain)
    {
        $campaigns = json_encode($this->domain->email_campaigns);
        $books = json_encode($this->domain->email_books);
        $templates = json_encode($this->domain->emailTemplates);

        return view('admin.emailTemplates.campaigns', compact('campaigns', 'books', 'templates'));
    }

    /**
     * Формируем информацию по книге из SP
     *
     * @param $domain
     * @param Request $request
     *
     * @return string
     * @throws \Exception
     */
    public function getBookInfo($domain, Request $request)
    {
        $book = collect($this->domain->email_books)->where('sp_id', '=', $request->id)->first();
        if ($book) {
            $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));

            $contacts = $sp->getEmailsFromBook($request->id);
            return json_encode($contacts);
        }
        return json_encode([
            'error' => 1,
            'msg' => 'book has not been found'
        ]);
    }

    /**
     * Отправка письма через SP
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function campaignSend(Request $request)
    {
        $template = EmailTemplate::findOrFail($request->template);
        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));
        $response = $sp->createCampaign('ТелеДоктор', 'info@teledoctor-pro.ru', $request->subject, base64_decode($template->body), $request->book, $request->name, '', '');
        // {"id":6379463,"status":1,"count":1,"tariff_email_qty":1,"paid_email_qty":0,"overdraft_price":0,"ovedraft_currency":"RUR"} успешный ответ
        // {"error_code":711,"message":"You need to wait before create new campaign by this book","is_error":true,"http_code":400} ошибка.
        if (!isset($response->error_code)) {
            $id = $response->id;
            $status = $request->status;
            $count = $request->count;

            $domain = $this->domain;
            $currentCampaigns = $domain->email_campaigns;
            $currentCampaigns[] = [
                'id' => $id,
                'name' => 'название кампании'
            ];
            $domain->email_campaigns = $currentCampaigns;
            $domain->save();

        }
        return json_encode($response);

    }

    /**
     * Информация о кампании
     *
     * @param $domain
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getCampaign($domain, $id)
    {
        $sp = new ApiClient(config('services.sendpulse.id'), config('services.sendpulse.secret'));
        $campaign = $sp->getCampaignInfo($id);
        if (isset($campaign->error_code)) {
            $error = $campaign->error_code;
            if ($error == 802) {
                return redirect('/admin/emails/campaigns')->with('error', 'Кампания не найдена');
            } else {
                return redirect('/admin/emails/campaigns')->with('error', $campaign->message);
            }
        }
        $statuses = [
            0 => [
                'name' => 'Новая'
            ],
            1 => [
                'name' => 'Кампания на модерации'
            ],
            2 => [
                'name' => 'Отправляется'
            ],
            3 => [
                'name' => 'Отправлена'
            ],
            5 => [
                'name' => 'Заблокирована'
            ],
            7 => [
                'name' => 'Обновление статуса'
            ],
            9 => [
                'name' => 'Идёт отправка'
            ],
            10 => [
                'name' => 'Обрабатывается'
            ],
            12 => [
                'name' => 'Нет активных адресов'
            ],
            13 => [
                'name' => 'Создается'
            ],
            14 => [
                'name' => 'В очереди на отправку'
            ],
            16 => [
                'name' => 'Отменена пользователем'
            ],
            22 => [
                'name' => 'Отправляется частично'
            ],
            23 => [
                'name' => 'Отправлена частично'
            ],
            25 => [
                'name' => 'Заблокировано после частичной отправки'
            ],
            27 => [
                'name' => 'Необходимо редактирование'
            ]

        ];
        //dd($response);
        $status = $statuses[$campaign->status]['name'];

        return view('admin.emailTemplates.campaign', compact('campaign', 'status'));
    }
}
