<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Telegram;
use App\Tmessage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Telegram\Bot\Laravel\Facades\Telegram as TlgrAPI;

class TelegramController extends Controller
{
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share( 'domainObj', $this->domain);
    }

    public function getHome()
    {
        return view('home');
    }

    /**
     * Получаем сообщения от Телеграма, проверяем регистрацию телеграмма, регистрируем телеграм
     * @return int
     */
    public function webHookAction()
    {

        // Получаем последнее записанное обновление от Телеграма
        $lastUpdate = Tmessage::orderBy('update_id', 'desc')->first();
        if ($lastUpdate) {
            $update_id = $lastUpdate->update_id + 1;
        } else {
            $update_id = 0;
        }
        $params = [
            'offset' => $update_id,
            'limit' => '',
            'timeout' => '',
        ];
        // Ищем новые поступления от Телеграма
        if (config('app.env') == 'production') {
            TlgrAPI::removeWebhook();
            $updates = TlgrAPI::getUpdates($params);
        } else {
            TlgrAPI::setWebhook(['url' => 'https://teledoctor-pro.ru/telegram']);
            $updates = [TlgrAPI::getWebhookUpdates()];
        }
        foreach ($updates as $update) {
            $chat_id = $update->getMessage()->getChat()->getId();       // Получаем ID чата сообщения

            $text = $update->getMessage()->getText();

            $checkTelegram = Telegram::where('tchat_id', $chat_id)->first(); // Проверяем, есть ли уже такой телеграмм в системе

            if ($checkTelegram) {
                // Если сообщение от известного чата
                $telegram = $checkTelegram;

                $tmessage = new Tmessage;
                //$tmessage->domain_id = $this->domain->id;
                $tmessage->telegram_id = $telegram->id;
                $tmessage->update_id = $update['update_id'];
                $tmessage->text = $text;
                $tmessage->save();

                if ($checkTelegram->confirm == 1) {

                    TlgrAPI::sendMessage([
                        'chat_id' => $telegram->tchat_id,
                        'parse_mode' => 'HTML',
                        'text' => 'Активировано',
                    ]);
                } else {
                    // Запрашиваем email
                    TlgrAPI::sendMessage([
                        'chat_id' => $telegram->tchat_id,
                        'parse_mode' => 'HTML',
                        'text' => '<a href="https://teledoctor-pro.ru/telegram/confirm?chat=' . $chat_id . '&hash=' . md5('confirm_user_' . $chat_id) . '">Нажмите для подтверждения аккаунта</a>',
                    ]);
                }

            } else {
                // Если это новый пользователей с т.з. телеграма
                $telegram = new Telegram;
                //$telegram->domain_id = $this->domain->id;
                $telegram->tchat_id = $chat_id;
                $telegram->confirmed = 0;
                $telegram->save(); // Создаём заготовку телеграма

                $tmessage = new Tmessage;
                //$tmessage->domain_id = $this->domain->id;
                $tmessage->telegram_id = $telegram->id;
                $tmessage->text = $text;
                $tmessage->update_id = $update['update_id'];
                $tmessage->save();
                // Запрашиваем email
                TlgrAPI::sendMessage([
                    'chat_id' => $telegram->tchat_id,
                    'parse_mode' => 'HTML',
                    'text' => '<a href="https://teledoctor-pro.ru/telegram/confirm?chat=' . $chat_id . '&hash=' . md5('confirm_user_' . $chat_id) . '">Нажмите для подтверждения аккаунта</a>',
                ]);
            }
        }
        return 0;
    }

    public function confirm($domain,Request $request)
    {
        $chat_id = $request->chat;
        if ($request->hash == md5('confirm_user_' . $chat_id)) {
            $user = Auth::user();
            // Юзер найден
            if($user) {
                $telegram = Telegram::where('tchat_id', $chat_id)->first();
                if ($telegram) {
                    $telegram->user_id = $user->id;
                    $telegram->confirmed = 1;
                    $telegram->save();

                    TlgrAPI::sendMessage([
                        'chat_id' => $telegram->tchat_id,
                        'parse_mode' => 'HTML',
                        'text' => 'Поздравляем! Теперь Вы сможете получать уведомления о новых сообщениях на свой Telegram!',
                    ]);
                    return redirect('/doc/account')->with('success', 'Telegram активирован');
                }
                return redirect('/doc/account')->with('telegram', 'Чат не найден, обратитесь в поддержку teledoctor-pro.ru');
                // Юзер не найден
            } else {
                return redirect('/login')->with('success','Авторизуйтесь и попробуйте подтвердить Telegram еще раз, пожалуйста');
            }
        }
        return redirect('/doc/account')->with('info', 'Ошибка! В доступе отказано.');
    }

    public function getWebHookInfo()
    {
        // массив для переменных, которые будут переданы с запросом
        $paramsArray = array();
        // преобразуем массив в URL-кодированную строку
        $vars = http_build_query($paramsArray);
// создаем параметры контекста
        $options = array(
            'http' => array(
                'method' => 'POST',  // метод передачи данных
                'header' => 'Content-type: application/x-www-form-urlencoded',  // заголовок
                'content' => $vars,  // переменные
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents('https://api.telegram.org/bot495610226:AAFvkycXlj4HcoGdYfgaknNEAyBLWxluK1Y/getWebhookInfo', false, $context);
        var_dump($result);
    }
}
