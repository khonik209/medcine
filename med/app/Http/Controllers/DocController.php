<?php

namespace App\Http\Controllers;

use App\Chat;
use App\DiseaseHistory;
use App\Doctor;
use App\Domain;
use App\File;
use App\Illness;
use App\Message;
use App\Note;
use App\Payment;
use App\Record;
use App\Services\Yandex\Yandex;
use App\Skill;
use App\Traits\ChatTrait;
use App\Traits\IllnessTrait;
use App\Traits\LogTrait;
use App\Traits\ReturnTrait;
use App\Traits\UserTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Mockery\Exception;
use App\Activity;
use Tomcorbett\OpentokLaravel\Facades\OpentokApi;
use OpenTok\Role;
use OpenTok\Exception as OpenTokException;

class DocController extends Controller
{
    use IllnessTrait;
    use ReturnTrait;
    use UserTrait;
    use ChatTrait;
    use LogTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        $this->middleware(['auth', 'doc']);
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index()
    {
        $doctor = Auth::user();

        if (!$doctor->doctor) {
            $skills = Skill::orderBy('name')->get();
            return view('doc.form', [
                'skills' => $skills
            ]);
        }
        $activeIllnesses = Illness::doctorActiveIllnesses($doctor)->orderBy('created_at', 'desc')->get();
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Домашняя', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('doc.home', [
            'doctor' => $doctor,
            'illnesses' => $activeIllnesses
        ]);

    }

    /**
     * Сохраняем анкету Врача
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addForm($domain, Request $request)
    {
        $validation = Validator::make($request->all(), [
            'job' => 'required',
            'experiance' => 'required',
            'education' => 'required',
            'level' => 'required',
            'skills' => 'required',
            'document' => 'image|max:1000'
        ]);

        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Указаны не все обязательные поля');
        }

        $user = Auth::user();

        $doctor = new Doctor;
        $doctor->user_id = $user->id;
        $doctor->domain_id = $this->domain->id;
        $doctor->job = $request->job;
        $doctor->education = $request->education;
        $doctor->level = $request->level;
        $doctor->experience = $request->experiance;
        if ($request->infomation) {
            $doctor->infomation = $request->infomation;
        }
        if ($request->specialty) {
            $doctor->specialty = $request->specialty;
        }
        if ($request->hasFile('document')) {
            if ($doctor->document) {
                /* Удаляем старый файл */
                Storage::disk('s3')->delete($doctor->document);
            }
            $file = $request->file('document');
            $extension = $file->clientExtension();
            $filename = $this->domain->name . '/doctors/' . time(). '.' . $extension;
            $s3 = Storage::disk('s3');
            $s3->put($filename, file_get_contents($file), 'public');
            $doctor->document = $filename;
        }
        $doctor->save();

        if (count($request->skills) > 0) {
            $toSync = [];
            foreach ($request->skills as $id) {
                $toSync[] = $id;
            }
            $user->skills()->sync($toSync);
        }
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Doctor', $user->id, 'Сохранил анкету', 'User ' . $user->id . ' saved form', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return redirect('/doc')->with('guide', 1);
    }

    /**
     * Отказаться от обучения
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function denyStartGuide(Request $request)
    {
        $user = Auth::user();
        $user->start_guide = 1;
        $user->save();
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Guide', $user->id, 'Завершение обучения', $request->step, '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptRequest($domain, $id)
    {
        $illness = Illness::find($id);
        if ($illness->doctor_id == Auth::user()->id) {
            $illness->status = 'active';
            $illness->save();

            // Отправляем уведомление пациенту
            $user = $illness->user;
            try {
                Mail::send('email.activeRequestUser', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Врач ответил на Ваше обращение на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о принятии обращения пользователю ' . $user->email . ' не отправились' . $e);
            }
            if ($this->domain->id == 1) {
                try {
                    $this->logIt('Illness', $user->id, 'Принял заявку', 'Принял заявку от ' . $user->name, $illness->name, $this->domain->id);
                } catch (\Exception $e) {
                    Log::error('Не удалось записать активность врача: ' . $e);
                }
            }
            return back()->with('success', 'Заявка принята');
        } else {
            return back()->with('info', 'В доступе отказано');
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function denyRequest($domain, $id)
    {
        $illness = Illness::find($id);
        if ($illness->doctor_id == Auth::user()->id) {
            $illness->status = 'denied';
            $illness->save();
            if ($this->domain->id == 1) {
                try {
                    $this->logIt('Illness', $illness->user_id, 'Отклонил заявку', 'Отклонил заявку от ' . $illness->user->name, 'Illness: ' . $illness->name, $this->domain->id);
                } catch (\Exception $e) {
                    Log::error('Не удалось записать активность врача: ' . $e);
                }
            }
            return back()->with('success', 'Заявка отклонена');
        } else {
            return back()->with('info', 'В доступе отказано');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRequestsPage($domain)
    {

        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }
        $illnesses = Illness::doctorActiveIllnesses($doctor)->orderBy('created_at', 'desc')->paginate(10);

        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Заявки', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('doc.requests', [
            'doctor' => $doctor,
            'illnesses' => $illnesses,
        ]);
    }

    /**
     * @param $id
     * @return $this
     * @throws \Exception
     */
    public function getRequestPage($domain, $id)
    {
        $chat = Chat::find($id);
        $currentUser = Auth::user(); // Тот, кто зашел на страницу

        if (!$currentUser->chats->contains($chat)) {
            return back()->with('info', 'В доступе отказано');
        }

        $illness = $chat->illness;

        $user = $chat->users()->patients()->first(); // Пациент

        $doctors = User::doctors()->get(); // Все врачи - для поиска при добалении

        $doctor = $doctors->where('id', $chat->doctor_id)->first(); // Лечащий доктор
        if (!$doctor) {
            $doctor = Auth::user();
        }
        $members = $chat->users()->get(); // Список участников беседы

        //все прикреплённые документы
        $urls = $chat->files()->paginate(10);
        if ($this->domain->custom('videochat')) {
            try {
                // Получаем токены OpenTok

                // new session
                $session = OpentokApi::createSession();
                $sessionId = $session->getSessionId();

                // check if it's been created or not (could have failed)
                if (empty($sessionId)) {
                    throw new \Exception("An open tok session could not be created");
                }
                // get your API key from config
                $api_key = Config::get('opentok.api_key');

                // then create a token (session created in previous step)
                try {
                    // note we're create a publisher token here, for subscriber tokens we would specify.. yep 'subscriber' instead
                    $token = OpentokApi::generateToken($sessionId,
                        array(
                            'role' => Role::PUBLISHER
                        )
                    );
                } catch (OpenTokException $e) {
                    // do something here for failure
                }
                $diff = Carbon::parse($chat->updated_at)->diffInHours(Carbon::now());

                if ($chat->session == null || $chat->token == null || $diff >= 2) {
                    $chat->session = $sessionId;
                    $chat->token = $token;
                    $chat->save();
                }

                $session = Chat::find($chat->id)->session;
                $currentToken = Chat::find($chat->id)->token;
            } catch (\Exception $e) {
                $session = null;
                $api_key = null;
                $currentToken = null;
                Log::error('Не удалось подключиться к opentok: ' . $e);
            }
        } else {
            $session = null;
            $api_key = null;
            $currentToken = null;
        }
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', $chat->name, '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('doc.request', [
            'illness' => $illness,
            'patient' => $user,
            'doctor' => $doctor,
            'chat' => $chat,
            'id' => $id,
            'doctors' => $doctors,
            'members' => $members,
            'urls' => $urls,
            'currentUser' => $currentUser,
            'session_id' => $session,
            'api_key' => $api_key,
            'token' => $currentToken,
        ]);
    }

    /**
     * Завершение обращения и решение по вопросам платежа
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finishRequest($domain, Request $request)
    {
        $illness = Illness::find($request->illness_id);

        // Выкидываем, если обращение не найдено
        if (!$illness) {
            return back()->with('info', 'Обращение не найдено');
        }

        // Выкидываем, если в доступе отказано
        if ($illness->doctor_id != Auth::user()->id) {
            return back()->with('info', 'В доступе отказано');
        }

        $user = $illness->user;

        //Проверяем форму оплаты. Простая или "Безопасная"
        if ($this->domain->payment_form == "safe") {

            // Записываем решение врача
            $illness->doc_opinion = $request->opinion;
            $illness->save();

            try {
                $this->logIt('Illness', $illness->user_id, 'Завершил обращение', $user->name, 'Illness: ' . $illness->name, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }

            // Если пациент еще не принял решение - ожидаем
            if ($illness->pac_opinion == "none") {
                /* Отправляем пациенту email уведомление */
                try {
                    Mail::send('email.illnessFinishSafe', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                        $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                    });
                } catch (\Exception $e) {
                    Log::error('Уведомление о завершении обращения пользователю не отправились' . $e);
                }

                $payments = $illness->payments->where('aviso', '=', 1);
                if (count($payments) == 0) {
                    $illness->status = "archive";
                    $illness->save();
                    return back()->with('info', 'Обращение закрыто!');
                }

                return back()->with('success', 'Решение принято, ожидайте ответа второй стороны');

            } else { // Если пациент принял решение, завершаем обращение

                return $this->finishSafe($domain, $request);

            }
        } else { // Если сделка обычная, а не безопасная

            /* Отправляем пациенту email уведомление */
            try {
                Mail::send('email.illnessFinishDefault', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
            }

            $illness->status = "archive";
            $illness->save();

            return back()->with('success', 'Обращение успешно завершено');
        }
    }

    /**
     * Метод закрытия обращения по принципу Безопасной сделки
     *
     * @param $domain
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function finishSafe($domain, $request)
    {
        // Инициализируем завершение сделки
        $domainObj = $this->domain;
        $illness = Illness::find($request->illness_id);
        $payments = $illness->payments->where('aviso', '=', 1);
        if (count($payments) == 0) {
            $illness->status = "archive";
            $illness->save();
            return back()->with('info', 'Обращение закрыто!');
        }
        if ($illness->pac_opinion == $illness->doc_opinion) { // Если стороны сошлись во мнении

            if ($illness->pac_opinion == "success" && $illness->doc_opinion == "success") {

                $illness->result = 'success';
                $illness->save();

            } else {

                $illness->result = 'fail';
                $illness->save();

            }

            // Разруливаем бабки
            return view('payment.finishSafe', [
                'illness' => $illness
            ]);
        } else {

            $user = $illness->user;
            $doctor = User::find($illness->doctor_id);
            $admin = $domainObj->users->where('role', 'admin')->first();


            // Если возник конфликт
            $illness->result = 'conflict';
            $illness->save();

            try {
                Mail::send('email.illnessConflict', ['user' => $user, 'illness' => $illness], function ($m) use ($user) {
                    $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $doctor, 'illness' => $illness], function ($m) use ($doctor) {
                    $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
            }
            try {
                Mail::send('email.illnessConflict', ['user' => $admin, 'illness' => $illness], function ($m) use ($admin) {
                    $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $this->domain->name);
                });
            } catch (\Exception $e) {
                Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
            }

            return back()->with('info', 'В ходе завершения обращения возник конфликт сторон. Решение о закрытии обращения в пользу одной из сторон будет принято администрацией в ближайшее время');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addRequestNote($domain, Request $request)
    {
        $doctor = Auth::user();
        $note = new Note;
        $note->domain_id = $this->domain->id;
        $note->user_id = $request->user_id;
        $note->name = $request->note_name . ' (автор:' . $doctor->name . ')';
        $note->text = $request->note_text;
        $note->type = 'recommendation';
        $note->save();
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Note', $doctor->id, 'Сохранил заметку', '', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return back()->with('success', 'Рекомендация добавлена пользователю');

    }

    public function getHistoryPage($domain)
    {
        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }
        $illnesses = Illness::doctorArchiveIllnesses($doctor)->orderBy('created_at', 'desc')->paginate(10);

        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Заявки', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('doc.requests', [
            'doctor' => $doctor,
            'illnesses' => $illnesses,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getChatsPage()
    {
        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Сообщения', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return view('doc.chats');
    }


    /**
     * Выйти из чата
     * @param $domain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function deleteChat($domain, $id)
    {
        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }
        $doctor->chats()->detach($id);
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Chat', $doctor->id, 'Покинул чат', 'Doctor ' . Auth::id() . ' deleted ' . $id, 'Chat: ' . $id, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return back()->with('success', 'Чат удалён');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addMember($domain, Request $request)
    {
        /*$validation = Validator::make($request->all(), [
            'members' => 'required',
            'chat_id' => 'required'
        ]);

        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Выберите нового участника чата');
        }*/

        $chat = Chat::findOrFail($request->chat_id);

        foreach ($request->members as $id) {
            try {
                $chat->users()->detach($id);
                $chat->users()->attach($id);
            } catch (\Exception $e) {

            }
        }
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Chat', Auth::id(), 'Добавил участников', 'Doctor ' . Auth::id() . ' invited ' . count($request->members) . ' members', 'Chat: ' . $chat->id, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return $this->printResponse(0, 'member saved');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inviteDoctor($domain, Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Введите корректный email');
        }

        $chat = Chat::find($request->chat_id);
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            $user = $checkUser;
        } else {
            // создаем профиль
            $user = $this->createUser($request->email,null,$request->name,null,$this->domain->id,'doc');
        }
        // Привязываем нового участника к чату
        $user->chats()->detach($chat->id);
        $user->chats()->attach($chat->id);
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Chat', Auth::id(), 'Пригласил врача', 'Doctor ' . Auth::id() . ' invited ' . $user->id, 'Illness: ' . $chat->id, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return back()->with('success', 'Операция успешна!');

    }

    public function renameChat($domain, Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Введите новое название чата');
        }
        $chat = Chat::find($request->chat_id);
        $chat->name = $request->name;
        $chat->save();

        return back()->with('success', 'Чат переименован');
    }

    /**
     * type = private | public
     * Принимает member_id для type==private
     * Принимает members для type==public
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addChat($domain, Request $request)
    {

        $user = Auth::user();
        $chat = new Chat;
        $chat->domain_id = $this->domain->id;
        if ($request->type == "private") {
            $member = User::findOrFail($request->member_id); // тут должен быть user`s ID
            /* Проверка на то, что у них уже есть один приватный чат */
            // get common chats between user1 and user2
            $checkChat = $member->chats()->withUser($user)->get()->where('type', 'private')->first();
            if ($checkChat) {
                return redirect('/doc/request/' . $checkChat->id);
            }
            $chat->type = "private";

            $chat->name = null;
        } else {
            $chat->type = "public";
            $members = $request->members; // тут должны быть ID всех участников
            if ($request->chat_name) {
                $chat->name = $request->chat_name;
            } else {
                $chat->name = "Новый групповой чат. Создатель: " . $user->name;
            }

        }
        $chat->save();

        $chat->users()->attach($user->id); // Добавляем в чат создателя
        if (isset($member)) {
            $chat->users()->attach($member->id); // Добавляем собеседник
        }
        if (isset($members)) {
            try {
                $chat->users()->attach($members); // Добавляем всех остальных
            } catch (\Exception $e) {
                Log::error('Не удалось добавить участников в чат. chat_id: ' . $chat->id);
            }
        }

        if ($this->domain->id == 1) {
            try {
                $this->logIt('Chat', $user->id, 'Создал чат', 'Doctor ' . Auth::id() . ' created ' . $chat->id, $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        return redirect('/doc/request/' . $chat->id);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddUserPage()
    {
        $doctor = Auth::user();
        return view('doc.addUser', [
            'doctor' => $doctor
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addUser($domain, Request $request)
    {
        if (!$this->domain->isActive()) {
            return back()->with('info', 'Создание обращений временно недоступно. Обратитесь к администратору');
        }

        $validator = Validator::make($request->all(), [
            'date' => 'required_if:form,offline',
            'time' => 'required_if:form,offline',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $doctor = Auth::user();
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            $user = $checkUser;
        } else {
            // Создаем Пациента
            $user = $this->createUser($request->email,null, $request->name, null, $this->domain->id);
        }

        if ($user->id == $doctor->id) {
            return back()->with('info', 'Нельзя добавить самого себя');
        }

        // Создаем активную заявку
        $illness = $this->createIllness($user, $doctor, '', '');
        try {
            $this->logIt('Illness', $illness->id, 'CreateIllness', 'Обращение создано', '', $illness->domain_id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        if ($request->form == "online") { /* Если прием онлайн - создаем чат */
            // Создаем чат
            $chat = $this->createChat($user, $doctor, $illness);
            if ($this->domain->id == 1) {
                try {
                    $this->logIt('User', $doctor->id, 'Добавил пациента', 'Doctor ' . Auth::id() . ' added ' . $user->name, 'Illness: ' . $illness->name, $this->domain->id);
                } catch (\Exception $e) {
                    Log::error('Не удалось записать активность врача: ' . $e);
                }
            }
        } else { /* Если прием оффлайн - создаем запись */

            $record = new Record;
            $record->user_id = $user->id;
            $record->doctor_id = $doctor->id;
            $record->start_date = Carbon::parse($request->date . ' ' . $request->time);
            $record->end_date = Carbon::parse($request->date . ' ' . $request->time)->addMinutes(30);
            $record->domain_id = $this->domain->id;
            $record->illness_id = $illness->id;
            $record->save();

        }

        return redirect('/doc')->with('success', 'Пользователь добавлен');

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDoctorsList(Request $request)
    {
        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }


        $doctors = User::doctors()->has('doctor');
        if ($request->filter) {
            $filer = $request->filter;
            $doctors = $doctors->where(function ($q) use ($filer) {
                $q->where('name', 'like', "%$filer%")->orWhere('email', 'like', "%$filer%")->orWhere('phone', 'like', "%$filer%");
            });
        }
        if ($request->skill) {
            $skill = Skill::find($request->skill);
            $doctors = $doctors->whereHas('skills', function ($q) use ($skill) {
                return $q->where('name', '=', $skill->name);
            });
        }
        $doctors = $doctors->get()->sortByDesc(function ($user) {
            $la = Activity::where('user_id', $user->id)->orderByDesc('created_at')->first();
            if ($la && $la->created_at->diffInDays(Carbon::now()) < 7) {
                return $user->name;
            } else {
                return false;
            }
        });
        if ($this->domain->id == 1) {
            try {
                $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Все врачи', '', $this->domain->id);
            } catch (\Exception $e) {
                Log::error('Не удалось записать активность врача: ' . $e);
            }
        }
        $skills = Skill::orderBy('name')->get();
        return view('doc.doctors', [
            'activeDoctors' => $doctors,
            'skills' => $skills,
            'request' => $request
        ]);
    }

    public function doctorsList(Request $request)
    {
        $filter = $request->q;
        return User::doctors()/*->has('doctor')*/
        ->where('id', '!=', Auth::id())
            ->where(function ($q) use ($filter) {
                $q->where('email', 'like', "%$filter%")->orWhere('name', 'like', "%$filter%")->orWhere('phone', 'like', "%$filter%");
            })->get()->toJson();
    }

    /**
     * Открываем страницу конкретного врача
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDoctor($domain, $id)
    {
        $doctor = User::find($id);
        if (!$doctor) {
            return redirect('/doc/doctors')->with('error', 'Врач не найден');
        }
        $form = $doctor->doctor;
        $lastActivity = Activity::where('user_id', $doctor->id)->orderBy('created_at', 'desc')->first();
        $reports = $doctor->allReports();
        $skills = $doctor->skills()->get();
        return view('doc.doctor', [
            'doctor' => $doctor,
            'form' => $form,
            'lastActivity' => $lastActivity,
            'reports' => $reports,
            'skills' => $skills
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHelperPage()
    {
        $doctor = Auth::user();
        if (!$doctor->doctor) {
            return view('doc.form');
        }
        //все прикреплённые документы
        $urls = $doctor->files;
        try {
            $this->logIt('Page', $doctor->id, 'Открыл страницу', 'Помощник', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }
        return view('doc.helper', [
            'doctor' => $doctor,
            'urls' => $urls
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addNote($domain, Request $request)
    {
        $note = new Note;
        $note->domain_id = $this->domain->id;
        $note->user_id = Auth::user()->id;
        $note->name = $request->note_name;
        $note->text = $request->note_text;
        $note->type = 'note';
        $note->save();
        return back()->with('success', 'Заметка создана!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editNote($domain, Request $request)
    {
        $note = Note::find($request->note_id);
        $note->name = $request->note_name;
        $note->text = $request->note_text;
        $note->type = 'note';
        $note->save();
        return back()->with('success', 'Заметка сохранена!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteNote($domain, $id)
    {
        $note = Note::find($id);
        $note->delete();
        return back()->with('success', 'Заметка удалена!');
    }

    /**
     * Запоминаем порядок заметок в Помощнике
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rememberEntry($domain, Request $request)
    {

        if ($request->entry_orders) {
            $entry_orders = explode(',', $request->entry_orders);
            foreach ($entry_orders as $key => $id) {
                if ($entry_orders[$key] != '') {
                    $entry = Note::find($entry_orders[$key]);
                    $entry->order = $key;
                    $entry->save();
                }
            }
        }

        if ($request->note_orders) {

            $note_orders = explode(',', $request->note_orders);
            foreach ($note_orders as $key => $id) {
                if ($note_orders[$key] != '') {

                    $entry = Note::find($note_orders[$key]);
                    $entry->order = $key;
                    $entry->save();
                }
            }
        }
        return back()->with('success', 'Данные сохранены');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setDocStatus($domain, Request $request)
    {
        $doctor = Auth::user();
        $profile = $doctor->doctor;
        $profile->status = $request->status;

        $validator = Validator::make($request->all(), [
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            return back()->withInput()->with('info', 'Не указан статус');
        }
        try {
            if ($request->status == "holiday") {
                if ($request->holiday_until_d) {
                    //$date = $request->holiday_until_h . ':' . $request->holiday_until_i . ' ' . $request->holiday_until_d;

                    $profile->holiday_until = Carbon::createFromFormat('Y-m-d', $request->holiday_until_d);
                } else {
                    return back()->withInput()->with('info', 'Не указано окончание отпуска');
                }
            }

            $profile->save();
            return back()->with('success', 'Статус сохранён!');
        } catch (\Exception $e) {
            return back()->with('info', 'Неверный формат даты (правильный: чч:мм гггг-мм-дд). Попробуйте еще раз, пожалуйста');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccountPage()
    {
        $user = Auth::user();
        if (!$user->doctor) {
            return view('doc.form');
        }
        //все прикреплённые документы
        $urls = $user->files;
        $img = Image::canvas(800, 600, '#ccc');
        $avatar = $user->avatar;
        $payments = Payment::where('doctor_id', $user->id)->where('aviso', 1)->where('test_deposition', 1)->get();
        try {
            $this->logIt('Page', $user->id, 'Открыл страницу', 'Настройки профиля', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }
        $skills = Skill::orderBy('name')->get();

        $days = [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ];
        return view('doc.account', [
            'user' => $user,
            'urls' => $urls,
            'img' => $img,
            'avatar' => $avatar,
            'payments' => $payments,
            'skills' => $skills,
            'days' => $days,
            'form' => $user->doctor
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAvatar($domain, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'avatar' => 'required|image|max:5120',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $user = Auth::user();

        if ($user->avatar) {
            /* Удаляем старый файл */
            Storage::disk('s3')->delete($user->avatar);
        }

        $file = $request->file('avatar');
        $extension = $file->clientExtension();
        $filename = $this->domain->name.'/avatars/' . time(). '.' . $extension;
        $s3 = Storage::disk('s3');
        $s3->put($filename, file_get_contents($file), 'public');

        $user->avatar = $filename;

        $user->save();

        try {
            $this->logIt('Page', $user->id, 'Обновил аватар', '', '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }

        return back()->with('success', 'Аватар сохранён!');
    }

    /**
     * Генерация волшебной кнопки через AJAX
     * @param $domain
     * @param Request $request
     */
    public function generateButton($domain, Request $request)
    {
        if ($request->sum < 100) {
            print 'Сумма минимального платежа составлят 100 рублей. Укажите корректную сумму.';
            exit;
        }

        $sum = $request->sum;
        $id = $request->id;
        $keyword = "magicbutton";
        $md5 = md5($id . $sum . $keyword);
        if ($request->form == "button") {
            $code = '<a href="http://' . $domain . '/api/getemail?id=' . $id . '&sum=' . $sum . '&md5=' . $md5 . '" style="color:#636363;background:#f0f0f0;padding: 10px 15px 10px 15px;box-shadow: 1px 1px 1px 1px rgba(1,1,1,0.5);font-weight: 600;border-width: 3px;border-radius: 10px;">Оплатить</a>';
        } elseif ($request->form == "link") {
            $code = 'http://' . $domain . '/api/getemail?id=' . $id . '&sum=' . $sum . '&md5=' . $md5;
        } else {
            $code = '';
        }

        try {
            $this->logIt('Account', Auth::id(), 'GenerateButton', 'Doctor ' . Auth::id() . ' generated button', 'Request: ' . $request->sum, $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }

        print $code;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editUser($domain, Request $request)
    {
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->notification) {
            $user->notification = $request->notification;
        } else {
            $user->notification = 0;
        }
        $user->save();

        $doctor = $user->doctor;
        $doctor->experience = $request->experience;
        $doctor->job = $request->job;
        $doctor->education = $request->education;
        $doctor->level = $request->level;
        $doctor->specialty = $request->specialty;
        $doctor->infomation = $request->infomation;
        $doctor->save();

        try {
            $this->logIt('Account', $user->id, 'Обновил аккаунт', $user->name, '', $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }

        if (count($request->skills) > 0) {
            $toSync = [];
            foreach ($request->skills as $id) {
                $toSync[] = $id;
            }
            $user->skills()->sync($toSync);
        }

        return back()->with('success', 'Данные успешно сохранены');
    }

    /**
     * Сохраняем рабочий график врача
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setWorkTime(Request $request)
    {

        $doctor = Auth::user();
        if (!$doctor || !$doctor->doctor) {
            return back()->with('error', 'Ошибка при сохранении! Попробуйте обновить страницу и сохранить еще раз');
        }
        $form = $doctor->doctor;

        $data = collect($request->all());
        $data->forget('_token');
        $form->work_time = $data;
        $form->save();

        return back()->with('success', 'Сохранено!');
    }

    /**
     * Сохранить настройки базового платежа
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setBasePayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'image|max:5120',
        ]);
        $validator->sometimes('sum', 'required|int|min:100', function ($input) {
            return $input->base_payment == 1;
        });
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $doctor = Auth::user();
        if (!$doctor || !$doctor->doctor) {
            return back()->with('error', 'Ошибка при сохранении! Попробуйте обновить страницу и сохранить еще раз');
        }
        $form = $doctor->doctor;
        if ($request->base_payment) {
            $form->base_payment = $request->sum;
        } else {
            $form->base_payment = null;
        }
        $form->save();

        return back()->with('success', 'Сохранено!');
    }

    /**
     * Выбираем способ получения средств
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setWallet($domain, Request $request)
    {
        $user = Auth::user();
        $profile = $user->doctor;
        // Сохраняем выбор
        $profile->wallet = $request->wallet;
        $profile->save();
        try {
            $this->logIt($user->id, 'Wallet', 'Указал счёт', 'Настройки профиля', $request->wallet, $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }
        if ($request->wallet == "bank") {
            // Перенаправляем на привязку карты
            return redirect('https://money.yandex.ru/cardauth?url_success=https://' . $domain . '/yandex/cardsave&url_error=https://' . $domain . '/yandex/cardfail&agentID=&identify=true');

        } elseif ($request->wallet == "yandex") {
            // Сохраняем номер кошелька
            $profile->accountNumber = $request->account_number;
            $profile->save();
            return back()->with('success', 'Номер кошелька успешно сохранён');
        } else {
            // Ошибка
            return back()->with('info', 'Ошибка при выборе способа получения средств');
        }
    }

    /**
     * Прикрепление документа, подтверждающий квалификацию
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    /*public function addDocument($domain, Request $request)
    {
        $user = Auth::user();
        $doctor = $user->doctor;
        if ($request->hasFile('document')) {
            // Документ, подтверждающий квалификацию
            $file = $request->file('document');
            $extension = $file->clientExtension();

            if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'jpeg') {
                $filename = $user->id . '_' . str_random(4) . '.' . $extension;
                $filePath = 'images/';
                $doctor->document = $this->domain->id . '/' . $user->id . '/' . $filePath . $filename;
                Storage::put(
                    '/public/' . $this->domain->id . '/' . $user->id . '/' . $filePath . '/' . $filename,
                    file_get_contents($file->getRealPath())
                );
            } else {
                return back()->with('error', 'Загрузите, пожалуйта, изображение: скан или фото документа');
            }
            $doctor->save();
        }

        return back()->with('success', 'Документ загружен');
    }*/

    /**
     * Удаление документа
     * @param $domain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteDocument($domain, $id)
    {
        $file = File::find($id);
        if (Auth::user()->id == $file->user_id) {
            $filename = $file->url;
            if ($file->type == 'dicom') {
                Storage::disk('public')->deleteDirectory("/" . $filename);
            } else {
                Storage::disk('public')->delete("/" . $filename);
            }
            $file->delete();
            return back()->with('success', 'Файл удалён!');
        } else {
            return back()->with('info', 'В доступе отказано');
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getResetPasswordPage()
    {
        return view('doc.resetPassword');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword($domain, Request $request)
    {
        $user = Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            if ($request->new_password == $request->confirm_password) {
                $user->password = bcrypt($request->new_password);
                $user->save();
            } else {
                return back()->with('info', 'Введённые пароли не совпадают!');
            }
        } else {
            return back()->with('info', 'Неправильный пароль');
        }
        try {
            $this->logIt('User', $user->id, 'ResetPassword', 'Doctor ' . Auth::id() . ' reset password ', "", $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }
        return redirect('/doc/account/')->with('success', 'Сохранение прошло успешно!');
    }

    /**
     * Добавить пользователя в черный список
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToBlackList($domain, Request $request)
    {
        $doctor = Auth::user();
        $user = User::find($request->user);
        if (!$user) {
            return back()->with('info', 'Такой пользователь не найден');
        }
        $currentList = $doctor->black_list; // Получаем массив текущих заблокированных пользователей
        if (!$currentList) {
            $currentList = [];
        }
        if (in_array($request->user, $currentList)) {
            // Пользователь уже в списке
            return back()->with('info', 'Пользователь уже добавлен в чёрный список');
        }
        $currentList[] = $request->user;
        $doctor->black_list = $currentList;
        $doctor->save();

        try {
            $this->logIt('User', $user->id, 'AddToBlackList', 'Doctor ' . Auth::id() . ' added to blacklist ' . $user->id, "", $this->domain->id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность врача: ' . $e);
        }

        return back()->with('success', 'Пользователь больше не сможет создавать новые обращения к Вам');
    }

    /**
     * Убираем пользователя из чёрного списка
     * @param $domain
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeFromBlackList($domain, $id)
    {
        $doctor = Auth::user();
        $user = User::find($id);

        $list = $doctor->black_list;

        $newList = [];
        if ($user && in_array($user->id, $list)) {
            // Если пользователь найден и находится в чёрном списке
            foreach ($list as $id) {
                if ($id != $user->id) {
                    // Собираем новый массив всех ID, кроме удалённого
                    $newList[] = $id;
                }
            }

            $doctor->black_list = $newList;
            $doctor->save();

            Activity::log([
                'contentId' => $user->id,
                'contentType' => 'User',
                'action' => 'RemoveBlackList',
                'description' => $doctor->id . ' removed ' . $user->id . ' form black list',
                //    'details'     => 'Username: '.$user->username,
                //  'updated'     => (bool) $id,
            ]);

            return back()->with('success', 'Пользователь может снова обращаться к Вам за помощью!');
        } else {
            return back()->with('info', 'Пользователь не найден');
        }
    }

    /**
     * Список всех юзеров
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsersPage($domain, Request $request)
    {
        $searchQuery = urldecode($request->filter);
        if ($searchQuery) {
            $users = User::where('role', 'user')->where(function ($q) use ($searchQuery) {
                $q->where('email', 'like', "%$searchQuery%")->orWhere('phone', 'like', "%$searchQuery%")->orWhere('name', 'like', "%$searchQuery%");
            });
        } else {
            $users = User::where('role', 'user');
        }

        if ($request->dir == "asc") {
            $dir = 'asc';
        } else {
            $dir = 'desc';
        }

        if ($request->sort) {
            $sortBy = $request->sort;
            if ($sortBy == "id") {
                $users = $users->orderBy('id', $dir);
            } elseif ($sortBy == "email") {
                $users = $users->orderBy('email', $dir);
            } elseif ($sortBy == "name") {
                $users = $users->orderBy('name', $dir);
            } elseif ($sortBy == "phone") {
                $users = $users->orderBy('phone', $dir);
            } elseif ($sortBy == "illnesses") {
                $users = $users->withCount('illnesses')->orderBy('illnesses_count', $dir);
            } else {
                $users = $users->orderBy('created_at', $dir);
            }
        }

        $users = $users->paginate(30);

        $doctors = User::doctors()->has('doctor')->get();

        return view('doc.users', [
            'users' => $users,
            'activeDoctors' => $doctors,
            'request' => $request
        ]);
    }

    /**
     * Открыть страницу пациента
     * @param $domain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUserPage($domain, $id)
    {
        $user = User::findOrFail($id);
        $illnesses = $user->illnesses;
        $lastActivity = Activity::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
        return view('doc.user', compact('user', 'illnesses', 'lastActivity'));
    }

    /**
     * Страница создания новой истории болезни
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newDiseaseHistoryPage(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $illness = Illness::findOrFail($request->illness_id);
        $doctor = Auth::user();
        return view('doc.addDiseaseHistory', compact('user', 'doctor', 'illness'));
    }

    /**
     * Сохранение новой истории болезни
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveDiseaseHistory(Request $request)
    {

        $user_data = $request->user;
        $history_date = $request->history;

        $doctor = Auth::user();

        $user = User::findOrFail($user_data['id']);
        $user->name = $user_data['name'];
        $user->age = $user_data['age'];
        $user->save();

        $diseaseHistory = new DiseaseHistory;
        $diseaseHistory->domain_id = $this->domain->id;
        $diseaseHistory->user_id = $user->id;
        $diseaseHistory->doctor_id = $doctor->id;
        $diseaseHistory->illness_id = $request->illness_id;
        $diseaseHistory->text = $request->history;
        $diseaseHistory->save();

        return $this->printResponse(0, 'saved', $diseaseHistory);
    }

    /**
     * Показать страницу истории болезни
     * @param $domain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDiseaseHistoryPage($domain, $id)
    {
        $diseaseHistory = DiseaseHistory::findOrFail($id);
        $text = (object)$diseaseHistory->text;
        return view('doc.diseaseHistory', compact('diseaseHistory', 'text'));
    }

    /**
     * Страница редактирования истории болезни
     * @param $domain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function diseaseHistoryEditPage($domain, $id)
    {
        $history = DiseaseHistory::findOrFail($id);
        if ($history->doctor->id != Auth::id()) {
            return back()->with('error', 'В доступе отказано');
        }
        $user = $history->user;

        $text = (object)$history->text;
        return view('doc.editDiseaseHistory', compact('history', 'user', 'text'));
    }

    /**
     * Сохранение отредактированной истории болезни
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editDiseaseHistory(Request $request)
    {

        $diseaseHistory = DiseaseHistory::findOrFail($request->id);
        $diseaseHistory->text = $request->text;
        $diseaseHistory->save();

        return $this->printResponse(0, 'saved', '');
    }
}
