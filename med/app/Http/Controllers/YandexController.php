<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Domain;
use App\Illness;
use App\Jobs\FinishSafeIllness;
use App\Jobs\GiveDefaultOrder;
use App\Jobs\GiveSafeOrder;
use App\Jobs\GiveSubscribeOrder;
use App\Payment;
use App\Plan;
use App\Record;
use App\Scopes\DomainScope;
use App\Services\Yandex\HTTP;
use App\Services\Yandex\Settings;
use App\Services\Yandex\Yandex;
use App\Subscription;
use App\Traits\PaymentTrait;
use App\Traits\ReturnTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class YandexController extends Controller
{
    use PaymentTrait;
    use ReturnTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public $yandex;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

        /* Создаём экземпляр яндекса */
        $yandex = new Yandex($this->domain, $request->type);
        $this->yandex = $yandex;

    }

    /**
     * Разделение платежа по типу
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newPayment($domain, Request $request)
    {
        if ($request->type == "subscription") {

            return $this->createSubscriptionPayment($domain, $request);
        } else {

            $validation = Validator::make($request->all(), [
                'sum' => 'required|int|min:100'
            ]);

            if ($validation->fails()) {
                return back()->with('error', 'Сумма должна быть не менее 100 рублей.');
            }

            $illness = Illness::find($request->illness_id);
            $user = $illness->user;
            $target = $illness->doctor();

            if (!$target->isReadyToGetPayment()) {
                return back()->with('error', 'Нельзя создать платеж. Недостаточно платежных данных');
            }

            $payment = $this->createPayment($user, $target, $illness, $request->sum);

            return back()->with('success', 'Платёж создан, ожидание оплаты от пациента.');
        }
    }

    /**
     * Создание платежа подписки и редирект пользователя на оплату
     *
     * @param $domain
     * @param $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function createSubscriptionPayment($domain, $request)
    {
        $domainObj = $this->domain;
        $user = Auth::user();
        try {
            $payment = new Payment;
            $payment->user_id = $user->id;
            $payment->check = 0;
            $payment->aviso = 0;
            $payment->email = $user->email;
            $payment->sum = $request->sum;
            $payment->type = "subscription";
            $payment->payment_form = 'default';
            $payment->domain_id = 1; // все платежи - подписки должны быть видны на teledoctor-pro
            $payment->save();
        } catch (\Exception $e) {
            $payment = null;
            Log::error('Ошибка при создании платежа подписки. Админ: ' . $user->email . ' Ошибка: ' . $e);
        }
        $yandex = $this->yandex;
        if ($payment) {
            return $yandex->redirectToYandex($payment);
        } else {
            return back()->with('info', 'Ошибка при создании платежа');
        }
    }

    /**
     * Перенаправляем пациента на оплату выставленного счёта
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */

    public function redirectToPay($domain, $id)
    {
        $yandex = $this->yandex;
        $payment = Payment::find($id);
        if ($payment) {
            return $yandex->redirectToYandex($payment);
        } else {
            return back()->with('info', 'Платёж не найден');
        }
    }

    /**
     * Проверка платежа яндекса
     *
     * @param $domain
     * @param Request $request
     */
    public function check($domain, Request $request)
    {
        $yandex = $this->yandex;
        $yandex->checkOrder($this->domain, $request);
    }

    /**
     * Проверка платежа + распределение на выдачу результата
     *
     * @param $domain
     * @param Request $request
     */
    public function aviso($domain, Request $request)
    {
        $yandex = $this->yandex;
        $avisoPayment = $yandex->paymentAviso($this->domain, $request);

        // Если всё ок, деньги получены, выполняем бизнес-логику
        $payment = $avisoPayment;
        if ($payment->type == "subscription") {
            return $this->giveSubscriptionOrder($domain, $payment);
        } else {
            if ($payment->payment_form == "safe") {
                return $this->giveSafeOrder($domain, $payment);
            } else {
                return $this->giveDefaultOrder($domain, $payment);
            }
        }
    }

    /**
     * Продление подписки
     *
     * @param $domain
     * @param $payment
     */
    public function giveSubscriptionOrder($domain, $payment)
    {
        $user = User::withoutGlobalScope(DomainScope::class)->where('id', $payment->user_id)->first();
        $domain = $user->domain; // домен определяем по плательщику, т.к. payment->domain всегда teledoctor-pro
        $plan = Plan::where('price', $payment->sum)->first();

        if ($plan) {
            // Создаём текущую подписку
            $subscription = new Subscription;
            $subscription->domain_id = $domain->id;
            $subscription->plan_id = $plan->id;
            $subscription->illnesses = 0; // Обнуляем лимит обращений подписки
            $subscription->save();
            /* Обновляем дедлайны */
            if (Carbon::parse($domain->active_to) > Carbon::now()) {
                // Если подписка еще не окончилась
                $domain->active_to = Carbon::parse($domain->active_to)->addDays(30);
            } else {
                $domain->active_to = Carbon::now()->addDays(30);
            }
            $domain->save();
            /* Рассылаем обновления */

            dispatch(new GiveSubscribeOrder($payment, $user));

        } else { // Если подходящего по оплате плана подписки нет, выдаём ошибку
            Log::error('Ошибка платежа ' . $payment);
            // Выдаём ошибку
            $settings = new Settings($this->domain, 'subscription');
            $http = new HTTP('paymentAviso', $settings);
            $response = $http->buildResponse('paymentAviso', $payment->invoiceId, 100, "Payment with current ID not found in system.");
            $http->sendResponse($response);
        }

        // Отправляем яндексу сообщение, что всё ок
        $yandex = $this->yandex;
        $yandex->paymentAvisoSuccess($this->domain, $payment);
    }

    /**
     * Выдаём заказ по принципу Безопасной сделки. Уведомляем стороны, отвечаем Яндексу успехом
     *
     * @param $domain
     * @param $payment
     */
    public function giveSafeOrder($domain, $payment)
    {
        // Сообщаем сторонам, что платёж удержан и можно оказывать услугу
        $this->dispatch(new GiveSafeOrder($payment));

        // Отправляем яндексу сообщение, что всё ок
        $yandex = $this->yandex;
        $yandex->paymentAvisoSuccess($this->domain, $payment);
    }

    /**
     * Уведомляем стороны об успехе оплаты, отвечаем яндексу успехом
     *
     * @param $domain
     * @param $payment
     */
    public function giveDefaultOrder($domain, $payment)
    {
        $payment->test_deposition = 1; // Сообщаем, что получателю переведены деньги
        $payment->save();
        // Сообщаем, что оплата прошла успешно.
        dispatch(new GiveDefaultOrder($payment));

        // Отправляем яндексу сообщение, что всё ок
        $yandex = $this->yandex;
        $yandex->paymentAvisoSuccess($this->domain, $payment);
    }

    /**
     * Привязываем карту врача
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cardSave($domain, Request $request)
    {
        $user = Auth::user();
        try {
            $doctor = $user->doctor;
            $doctor->skr_destinationCardPanmask = $request->skr_destinationCardPanmask;
            $doctor->skr_destinationCardSynonim = $request->skr_destinationCardSynonim;
            $doctor->skr_destinationCardBankName = $request->skr_destinationCardBankName;
            $doctor->skr_destinationCardType = $request->skr_destinationCardType;
            $doctor->accountNumber = $request->accountNumber;
            $doctor->save();
            Log::info('Привязка к карте прошла успешно');

            return redirect('/doc/account')->with('success', 'Карта успешно привязана');
        } catch (\Exception $e) {
            Log::error('Ошибка при привязывании карты ' . $user->email . ' :' . $e);
            return redirect('/doc/account')->with('info', 'Ошибка при привязке карты');
        }

    }

    /**
     * Обработчик при ошибки привязывания карты врача
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cardFail($domain, Request $request)
    {
        $user = Auth::user();
        Log::error('Ошибка при привязке карты юзера ' . $user->email . ': ' . $request->identificationError);
        return redirect('/doc/account')->with('info', 'Ошибка при привязке карты');
    }

    /**
     * Разруливам деньги на безопасной сделке
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finishSafeIllness($domain, Request $request)
    {

        $illness = Illness::find($request->illness);
        $this->dispatch(new FinishSafeIllness($illness, $this->yandex));
        $role = Auth::user()->role;
        return redirect('/' . $role)->with('success', 'Обращение завершается. Потребуется некоторое время на расчёты платежей. После этого обращение будет завершено. Дополнительная информация будет выслана Вам на email');
    }

    /**
     * Редирект после успешной оплаты
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function successPayment(Request $request)
    {
        if ($request->payment) {
            $payment = Payment::find($request->payment); // Поиск по кассе
        }
        if ($request->label) {
            $payment = Payment::find($request->label); // Поиск по кнопке
        }
        if (!isset($payment)) {
            return redirect('/user');
        }
        if ($payment->type == "request") {
            $chat = Chat::illnessChat($payment->illness)->first();
            return redirect('/user/requests/' . $chat->id)->with('success', 'Оплата прошла успешно. Дополнительная информация уже у Вас на почте');
        } else {
            $user = User::withoutGlobalScope(DomainScope::class)->where('id', $payment->user_id)->first();
            $domain = $user->domain; // домен определяем по плательщику, т.к. payment->domain всегда teledoctor-pro
            return redirect('http://' . $domain->name . '/admin/account')->with('success', 'Оплата прошла успешно. Дополнительная информация уже у Вас на почте');
        }
    }

    /**
     * Редирект после неуспешной оплаты
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function failPayment(Request $request)
    {
        if ($request->payment) {
            $payment = Payment::find($request->payment); // Поиск по кассе
        } else {
            $payment = Payment::find($request->label); // Поиск по кнопке
        }

        if ($payment->type == "request") {
            $chat = Chat::illnessChat($payment->illness)->first();
            return redirect('/user/requests/' . $chat->id)->with('info', 'Ошибка при оплате. Попробуйте еще раз или обратитесь в службу поддержки.');
        } else {
            $user = User::withoutGlobalScope(DomainScope::class)->where('id', $payment->user_id)->first();
            $domain = $user->domain; // домен определяем по плательщику, т.к. payment->domain всегда teledoctor-pro
            return redirect('http://' . $domain->name . '/admin/account')->with('info', 'Ошибка при оплате. Попробуйте еще раз или обратитесь в службу поддержки.');
        }
    }

    /**
     * Метод приема платежа от Яндекс Кнопки.
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function quickpayConfirm($domain, Request $request)
    {
        $secret = $this->domain->yandex_secret; // секрет, который мы получили в первом шаге от яндекс.

        if ( sha1( $request->notification_type . '&' . $request->operation_id . '&' . $request->amount . '&' . $request->currency . '&' . $request->datetime . '&' . $request->sender . '&' . $request->codepro . '&' . $secret . '&' . $request->label ) != $request->sha1_hash ) {

            Log::error( 'Верификация не пройдена. SHA1_HASH не совпадает. Operation_id: ' . $request->operation_id. ' SECRET:'.$secret );
            abort( 400 ); // запрос не совпадает

        }
        $payment = Payment::find($request->label);
        if (!$payment) {
            Log::error('Payment not found: ' . json_encode($request->all()));
            return $this->printResponse(2, 'payment not found', '');
        }
        $payment->sum = $request->withdraw_amount;
        $payment->check = 1;
        $payment->aviso = 1;
        $payment->log_aviso = json_encode($request->all());
        $payment->invoice_id = $request->operation_id;
        $payment->save();

        return $this->printResponse(0, 'all is ok', '');
    }
}
