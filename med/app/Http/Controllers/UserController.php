<?php

namespace App\Http\Controllers;

use App\Chat;
use App\DiseaseHistory;
use App\Domain;
use App\File;
use App\Illness;
use App\Message;
use App\Note;
use App\Payment;
use App\Question;
use App\Report;
use App\Services\Yandex\Yandex;
use App\Skill;
use App\Subscription;
use App\Traits\LogTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use App\Activity;
use Tomcorbett\OpentokLaravel\Facades\OpentokApi;
use OpenTok\Role;
use OpenTok\Exception as OpenTokException;


class UserController extends Controller
{
	use LogTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        $this->middleware(['auth', 'user']);
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {

        $user = Auth::user();
        if ($this->domain->custom('custom_form')) {
            $questions = Question::orderBy('order')->whereNotNull('domain_id')->get();
        } else {
            $questions = null;
        }
        $illnesses = $user->userIllnesses()->get();
        if (!$this->domain->custom('hide_doctors')) {
            $doctors = $this->domain->getDoctors()->whereHas('doctor', function ($q) {
                $q->where('status', '=', 'online');
            })->get();
            $doctor = [];
        } else {
            $doctors = [];
            $doctor = $this->domain->getAdmin()->first();
        }

        $checkActivity = Activity::where('user_id', '=', $user->id)->first();
        if (!$checkActivity) {
            Activity::log([
                'contentId' => $user->id,
                'contentType' => 'Patient',
                'action' => 'Opened Index',
                'description' => '',
                'details' => '',
                'domain_id' => $this->domain->id
            ]);
        } else {
            $checkActivity->updated_at = Carbon::now();
            $checkActivity->save();
        }

        return view('user.home', [
            'illnesses' => $illnesses,
            'user' => $user,
            'questions' => $questions,
            'doctors' => $doctors,
            'doctor'=>$doctor
        ]);
    }

    /**
     *
     *Страница всех обращений пользователя
     * @param $domain
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRequestsPage($domain)
    {
        $user = Auth::user();
        $records = $user->records;
        $illnesses = $user->userIllnesses()->get();
        return view('user.requests', compact('user', 'records', 'illnesses'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function getRequestPage($domain, $id)
    {

        if(!$this->domain->custom('chat')){
            return redirect('/user/requests');
        }
        $chat = Chat::find($id);
        $illness = $chat->illness;

        if ($illness->status == 'new') {
            return back()->with('info', 'Дождитесь, пока врач рассмотрит и примет заявку. Вы сможете зайти, когда у обращения будет статус "В работе"');
        }

        $user = Auth::user();

        if (!$user->chats->contains($chat)) {
            return back()->with('error', 'В доступе отказано');
        }

        if ($illness->record) {

            if (Carbon::parse($illness->record->start_date) > Carbon::now()) {
                $startTime = Carbon::parse($illness->record->start_date)->format('d.m.Y H:i');
                return back()->with('info', 'Страница станет доступна после ' . $startTime);
            }
        }

        $doctor = User::where('id', $illness->doctor_id)->first();
        //все прикреплённые документы
        $urls = $chat->files;

        $userFiles = $user->files;
        if ($this->domain->custom('videochat')) {
            //Получаем токены для видеочата
            // new session
            $session = OpentokApi::createSession();
            $sessionId = $session->getSessionId();

            // check if it's been created or not (could have failed)
            if (empty($sessionId)) {
                throw new \Exception("An open tok session could not be created");
            }
            // get your API key from config
            $api_key = Config::get('opentok.api_key');

            // then create a token (session created in previous step)
            try {
                // note we're create a publisher token here, for subscriber tokens we would specify.. yep 'subscriber' instead
                $token = OpentokApi::generateToken($sessionId,
                    array(
                        'role' => Role::PUBLISHER
                    )
                );
            } catch (OpenTokException $e) {
                // do something here for failure
            }
            $diff = Carbon::parse($chat->updated_at)->diffInHours(Carbon::now());

            if ($chat->session == null || $chat->token == null || $diff >= 2) {
                $chat->session = $sessionId;
                $chat->token = $token;
                $chat->save();
            }
            $session = Chat::find($chat->id)->session;
            $currentToken = Chat::find($chat->id)->token;
        } else {
            $session = null;
            $api_key = null;
            $currentToken = null;
        }
        return view('user.request', [
            'illness' => $illness,
            'user' => $user,
            'doctor' => $doctor,
            'chat' => $chat,
            'id' => $id,
            'userFiles' => $userFiles,
            'session_id' => $session,
            'api_key' => $api_key,
            'token' => $currentToken,
            'urls' => $urls,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addNote($domain, Request $request)
    {
        $user = Auth::user();
        $note = new Note;
        $note->domain_id = $this->domain->id;
        $note->user_id = $user->id;
        $note->name = 'Запись от' . time();
        $note->text = $request->note_text;
        $note->type = $request->type;
        $note->save();
        return back()->with('success', 'Заметка сохранена');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editNote($domain, Request $request)
    {
        $note = Note::find($request->note_id);
        $note->text = $request->note_text;
        $note->save();
        return back()->with('success', 'Заметка сохранена');
    }

    /**
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteNote($domain, Request $request)
    {
        $note = Note::find($request->note_id);
        $note->delete();
        return back()->with('success', 'Заметка удалена');
    }


    /**
     * Открываем список всех врачей
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDoctorsList(Request $request)
    {
        if ($this->domain->custom('hide_doctors')) {
            return redirect('/user');
        }
        $doctors = User::doctors()->has('doctor');
        if ($request->filter) {
            $filter = $request->filter;
            $doctors = $doctors->where(function ($q) use ($filter) {
                $q->where('name', 'like', "%$filter%")->orWhere('email', 'like', "%$filter%")->orWhere('phone', 'like', "%$filter%");
            });
        }

        if ($request->skill) {
            $skill = Skill::find($request->skill);
            $doctors = $doctors->whereHas('skills', function ($q) use ($skill) {
                return $q->where('name', '=', $skill->name);
            });
        }
        $doctors = $doctors->get()->sortByDesc(function ($user) {
            if($user->doctor->status!="online")
            {
                return 0;
            } else {
                $la = Activity::where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
                if ($la && $la->created_at->diffInDays(Carbon::now()) < 7) {
                    return $user->name;
                } else {
                    return 1;
                }
            }

        });
        $skills = Skill::orderBy('name')->get();
        return view('user.doctors', [
            'activeDoctors' => $doctors,
            'skills' => $skills,
            'request' => $request
        ]);
    }

    /**
     * Открываем страницу конкретного врача
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDoctor($domain, $id)
    {
        if ($this->domain->custom('hide_doctors')) {
            return redirect('/user');
        }
        $doctor = User::find($id);
        if (!$doctor) {
            return redirect('/user/doctors')->with('error', 'Врач не найден');
        }
        $form = $doctor->doctor;
        $lastActivity = Activity::where('user_id', $doctor->id)->orderBy('created_at', 'desc')->first();
        $reports = $doctor->allReports();
        return view('user.doctor', [
            'doctor' => $doctor,
            'form' => $form,
            'lastActivity' => $lastActivity,
            'reports' => $reports
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHistoryPage()
    {
        $user = Auth::user();
        $illnesses = $user->illnesses()->where('status', 'archive')->get();
        return view('user.history', [
            'user' => $user,
            'illnesses' => $illnesses
        ]);
    }

    /**
     * Показать историю болезни
     * @param $domain
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDiseaseHistory($domain, $id)
    {
        $diseaseHistory = DiseaseHistory::findOrFail($id);
        if ($diseaseHistory->user_id != Auth::id()) {
            abort(404);
        }
        $text = (object)$diseaseHistory->text;
        return view('user.diseaseHistory', compact('diseaseHistory', 'text'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccountPage()
    {
        $user = Auth::user();
//все прикреплённые документы
        $urls = $user->files;
        $files = [];
        foreach ($urls as $url) {
            $file = Storage::url('public/' . $this->domain->id . '/');
            $files[] = $file;

        }
        return view('user.account', [
            'user' => $user,
            'files' => $files,
            'urls' => $urls
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAvatar($domain, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => 'required|image|max:5120',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $user = Auth::user();

        if ($user->avatar) {
            /* Удаляем старый файл */
            Storage::disk('s3')->delete($user->avatar);
        }
        $file = $request->file('avatar');
        $extension = $file->clientExtension();
        $filename = $this->domain->name.'/avatars/' . time(). '.' . $extension;
        $s3 = Storage::disk('s3');
        $s3->put($filename, file_get_contents($file), 'public');

        $user->avatar = $filename;

        $user->save();

        return back()->with('success', 'Аватар сохранён!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse,
     */
    public function deleteDocument($domain, $id)
    {
        // todo OLD STORAGE
        $file = File::findOrFail($id);
        if (Auth::user()->id == $file->user_id) {
            $filename = $file->url;
            if ($file->type == 'dicom') {
                Storage::disk('public')->deleteDirectory("/" . $filename);
            } else {
                Storage::disk('public')->delete("/" . $filename);
            }
            $file->delete();
            return back()->with('success', 'Файл удалён!');
        } else {
            return back()->with('info', 'В доступе отказано');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editUser($domain, Request $request)
    {
        $user = Auth::user();
        if ($request->name) {
            $user->name = $request->name;
        }
        if ($request->age) {
            $user->age = $request->age;
        }
        if ($request->notification) {
            $user->notification = $request->notification;
        } else {
            $user->notification = 0;
        }
        $user->save();
        return back()->with('success', 'Сохранено!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getResetPasswordPage()
    {
        return view('user.resetPassword');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword($domain, Request $request)
    {
        $user = Auth::user();

        if (Hash::check($request->old_password, $user->password)) {
            if ($request->new_password == $request->confirm_password) {
                $user->password = bcrypt($request->new_password);
                $user->save();
            } else {
                return back()->with('info', 'Введённые пароли не совпадают!');
            }
        } else {
            return back()->with('info', 'Неправильный пароль');
        }

        return redirect('/user/account')->with('success', 'Сохранение прошло успешно!');
    }
}
