<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Domain;
use App\Illness;
use App\News;
use App\Payment;
use App\Plan;
use App\Question;
use App\Services\Yandex\Yandex;
use App\Traits\AnswerTrait;
use App\Traits\ChatTrait;
use App\Traits\IllnessTrait;
use App\Traits\PaymentTrait;
use App\Traits\RecordTrait;
use App\Traits\UserTrait;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Mockery\Exception;
use Symfony\Component\Debug\Tests\Fixtures\FinalClass;
use Sendpulse\RestApi\ApiClient as SendPulse;
use Sendpulse\RestApi\Storage\FileStorage;

class LandingController extends Controller
{
    use UserTrait;
    use IllnessTrait;
    use ChatTrait;
    use PaymentTrait;
    use AnswerTrait;
    use RecordTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();
        // Лендос TELEDOCTOR PRO. Закрыли teledoctor-pro.ru

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

    }

    /**
     * Возвращаем основной лэндинг teledoctor-pro
     *
     * @param $domain
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index($domain)
    {
        $doctors = User::where('role', 'doc')->get();
        $news = News::orderBy('created_at', 'desc')->get();
        if (isset(config('domains.' . env('APP_ENV'))[$this->domain->name]['index'])) {
            return view(config('domains.' . env('APP_ENV'))[$this->domain->name]['index'], compact('doctors', 'news'));
        }

        // ЛЕНДОСЫ КЛИЕНТОВ
        if ($this->domain->id == 1) {
            return redirect('http://healit.ru');
        }

        if (Auth::check()) {
            return view('landing.landing');
        }
        if ($this->domain->custom('custom_form')) {
            $questions = Question::orderBy('order')->whereNotNull('domain_id')->get();
            return view('landing.form', compact('questions'));
        } else {
            $questions = null;
            return view('landing.landing');
        }
    }

    /**
     * Отправка писем админу от юзеров
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function feedback($domain, Request $request)
    {
        $data = [
            'secret' => '6LfTHH8UAAAAAAqeCJgYQ055xss7pR9IBSRFLlHs',
            'response' => $request->{'g-recaptcha-response'}
        ];

        $client = new Client([
            'base_uri' => "https://www.google.com",
        ]);

        $response = $client->request("POST", "recaptcha/api/siteverify", [
            'form_params' => $data,
        ]);

        $captcha = json_decode($response->getBody())->success;

        if(!$captcha)
        {
            return back()->with('error',"Вы робот?!");
        }
        try {
            Mail::send('email.feedback', ['request' => $request], function ($m) use ($request) {
                $m->to('nkhoreff@yandex.ru')->subject('Feedback от Юзера');
            });
            return back()->with('success', 'Сообщение отправлено');
        } catch (\Exception $e) {
            Log::error('Ошибка при отправке feedback ' . $request->email . '_' . $e);
            return back()->with('error', 'Ошибка при отправке сообщения');
        }
    }

    /**
     * Открываем страницу регистрации
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registerPage(Request $request)
    {
        if (Auth::check()) {
            return redirect('/login');
        }
        if ($this->domain->name == "healit.ru") {
            abort(404);
        }
        return view('auth.register', [
            'request' => $request
        ]);
    }

    /**
     * Маршрут регистрации пользователей
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function register($domain, Request $request)
    {
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            return back()->with('info', 'Пользователь с таким email уже зарегистрирован');
        }
        $user = $this->createUser($request->email, $request->password, $request->name, $request->phone, $this->domain->id, $request->role, $request->age);
        Auth::login($user);
        return redirect('/user');
    }

    /**
     * Регистрация с лендинга
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerWithForm($domain, Request $request)
    {
        if ($request->is_robot) {
            return 'Мы не регистрируем роботов';
        }
        $validation = Validator::make($request->all(), [
            'user_email' => 'required',
            'user_name' => 'required'
        ]);
        if ($validation->fails()) {
            return back()->withInput()->with('error', 'Указаны не все обязательные поля');
        }
        $checkUser = User::where('email', '=', $request->user_email)->first();
        if ($checkUser) {
            return redirect('/login')->with('info', 'Пользователь с таким email уже зарегистрирован в системе. Пожалуйста, введите пароль');
        }
        $user = $this->createUser($request->user_email, null, $request->user_name, $request->phone, $this->domain->id, 'user', $request->age);
        Auth::login($user, true);

        $illness = $this->createIllness($user, $this->domain->getAdmin()->first(), $request->name, $request->description);
        try {
            $this->logIt('Illness', $illness->id, 'CreateIllness', 'Обращение создано', '', $illness->domain_id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        if ($this->domain->custom('records')) {
            $record = $this->createRecord($user, $this->domain->getAdmin()->first(), $request->start, $request->end, $illness, $request->name, $request->description);
        }

        if ($this->domain->custom('chat')) {
            if ($request->place == "online") {
                $chat = $this->createChat($user, $this->domain->getAdmin()->first(), $illness);
            }
        }

        if ($this->domain->custom('custom_form')) {
            if ($request->questions) {
                $saved = $this->saveAnswers($user, $illness, $request->questions);
            }
        }

        if ($this->domain->custom('payments')) {
            $target = $this->domain->getAdmin()->first(); // т.к. тут ответственный админ, платеж создаваться не будет.
            $base_payment = $target->doctor && $target->doctor->base_payment ? $target->doctor->base_payment : null;
            if ($base_payment) {
                $payment = $this->createPayment($user, $target, $illness, $base_payment);
            }
        }
        return redirect('/user')->with('success', 'Обращение создано! Скоро мы вам ответим');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($domain, $driver)
    {
        return Socialite::with($driver)->redirect();
    }

    /**
     * Получаем callback от Провайдера
     *
     * @param $driver
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleProviderCallback($domain, $driver)
    {
        $response = Socialite::driver($driver)->user();
        $checkUser = User::where('email', $response->getEmail())->first();
        if ($checkUser) {
            $user = $checkUser;
        } else {
            $user = $this->createUser($response->getEmail(), null, $response->getName(), null, $this->domain->id, 'user', null);
        }
        Auth::login($user, true);

        return redirect('http://' . $this->domain->name . '/user');
    }

    /**
     * Страница выбора роли после регистрации с помощью сторонних сервисов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setRolePage()
    {
        if (!Auth::check()) {
            return redirect('/')->with('info', 'Требуется авторизация');
        }
        if (Auth::user()->role) {
            return redirect('/')->with('info', 'Нельзя поменять свою роль');
        }
        return view('landing.chooseRole');
    }

    /**
     * Сохранение роли пользователя в системе
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setRole($domain, Request $request)
    {
        $user = Auth::user();
        $user->role = $request->role;
        $user->save();
        if ($user->role == 'user') {
            return redirect('/user');
        } elseif ($user->role == 'doc') {
            try {
                /* SendPulse */
                define('API_USER_ID', 'a0cdd4e600d00aba36a4e58b6daa5f7c');
                define('API_SECRET', 'a6f5f3a84c3c79dbf7092979ccf313ad');
                define('/storage/', __FILE__);

                $sp = new SendPulse(API_USER_ID, API_SECRET, new FileStorage());

                $bookID = 1531735;
                $emails = array(
                    array(
                        'email' => $request->email,
                        'variables' => array(
                            'name' => $request->name,
                        )
                    )
                );
                $sp->addEmails($bookID, $emails);
            } catch (\Exception $e) {
                Log::error('Ошибка при добавлении ' . $request->email . ' в SendPulse');
            }
            return redirect('/doc');
        } else {
            return redirect('/');
        }

    }

    /**
     * Страница получения email пациента после нажатия "магической кнопки врача" на стороннем сервисе
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEmail(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->role == "doc") {
                return redirect('/doc')->with('info', 'Рыночные отношения между врачами не поддерживаются сервисом');
            } else {
                $user = Auth::user();
                return $this->addUser($request, $user);
            }
        }

        $doctor = User::findOrFail($request->id);
        return view('landing.getEmail', [
            'request' => $request,
            'doctor' => $doctor,
        ]);
    }

    /**
     * Проверяем, есть ли юзер в системе. Если есть - спрашиваем пароль, если нет - регистрируем
     * Работает для AJAX
     *
     * @param Request $request
     */
    public function checkUser(Request $request)
    {
        $checkUser = User::where('email', $request->email)->first();
        if ($checkUser) {
            print true;
        } else {
            print false;
        }
    }

    /**
     * Регистрируем пользователя и перекидываем до оплаты
     * TODO:ОТРЕФАКТРИТЬ
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function addUser(Request $request, User $user = null)
    {
        /* Проверяем корректность запроса */
        $keyword = 'magicbutton';
        if (md5($request->id . $request->sum . $keyword) != $request->md5) {
            // Если ХЭШ не совпадает - возвращаем ошибку.
            return back()->with('info', 'Ошибка! Запрос сломан, попробуйте еще раз.');
        }
        /* Проверяем пользователя */
        if (!Auth::check()) {
            $checkUser = User::where('email', $request->email)->first();

            if ($checkUser) {
                if (Auth::attempt(['email' => $checkUser->email, 'password' => $request->password])) {
                    // Авторизовываем
                    $user = $checkUser; // Юзер найден, спрашиваем пароль
                } else {
                    // Возвращаем с ошибкой
                    return back()->with('info', 'Неправильный пароль. Попробуйте еще раз.');
                }
            } else {
                $user = $this->createUser($request->email, null, null, $this->domain->id, 'user', null);
            }
            Auth::login($user, true);
        } else {
            $user = Auth::user();
        }
        /* Создаём обращение с врачом */
        $doctor = User::find($request->id);
        $black_list = [];
        if ($doctor->black_list) {
            $black_list = $doctor->blacklist;
        }
        /*Проверяем на ЧС*/
        if (in_array($user->id, $black_list)) {
            return back()->with('info', 'Данный врач ничем не может Вам помочь :(');
        }

        $illness = $this->createIllness($user, $doctor);
        try {
            $this->logIt('Illness', $illness->id, 'CreateIllness', 'Обращение создано', '', $illness->domain_id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        if ($this->domain->custom('chat')) {
            $chat = $this->createChat($user, $doctor, $illness);
        }
        if ($this->domain->custom('payments')) {
            $payment = $this->createPayment($user, $doctor, $illness, $request->sum);
            // Отправляем пользователя в кассу
            if ($payment) {
                $yandex = new Yandex($this->domain, 'request');
                return $yandex->redirectToYandex($payment);
            }
        }

        return redirect('http://' . $this->domain->name . '/user/requests/' . $chat->id)->with('success', 'Обращение создано');

    }

    /**
     * Регистрируем новый домен
     *
     * @param $domain
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registerClinic($domain, Request $request)
    {
        if ($request->is_robot) {
            return 'Ботов не регистрируем';
        }
        /* Получаем данные из формы */
        $email = $request->email; // Почта администратора
        $newDomain = $request->domain; // Выбранное доменное имя

        $userName = $request->user_name; // Имя админа
        if (!$userName) {
            $userName = explode('@', $email)[0];
        }

        $clinicName = $request->clinic_name; // Название клиники
        if (!$clinicName) {
            $clinicName = explode('.', $newDomain)[0]; // Выбранное доменное имя
        }

        $phone = $request->phone;
        if (!$phone) {
            $phone = '';
        }

        $checkDomain = Domain::where('name', $newDomain)->first();
        if ($checkDomain) {
            return back()->with('info', 'Доменное имя занято. Попробуйте другое.');
        }
        $checkUser = User::where('email', $email)->first();
        if ($checkUser) {
            return back()->with('info', 'Пользователь с данным email уже зарегистрирован. Попробуйте другой email');
        }
        /* Создаём домен */
        $domain = new Domain;
        $domain->name = $newDomain . '.healit.ru';
        $domain->active_to = Carbon::now()->addDays(14);
        $domain->company = $clinicName;
        $domain->save();

        /* Создаём админа */
        $user = $this->createUser($email, null, $userName, $phone, $domain->id, 'admin', null);
        Auth::login($user, true);

        Mail::raw('email: ' . $email . ', phone: ' . $phone . ', name: ' . $userName . ', clinic: ' . $clinicName, function ($message) {
            $message->to('projmed@yandex.ru', 'SUPERADMIN')->subject('Новая заявка на подключение!');
        });

        return redirect('http://' . $domain->name . '/admin')->with('success', 'Кабинет создан! Добро пожаловать в свою онлайн клинику!');
    }

    /**
     *
     * ОБСЛУЖИВАНИЕ КАСТОМНЫХ МАРШРУТОВ
     *
     */

    public function contacts($domain)
    {
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['contacts']) ? config('domains.' . env('APP_ENV'))[$domain]['contacts'] : null;
        return $route ? view($route) : abort(404);
    }

    public function services($domain)
    {
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['services']) ? config('domains.' . env('APP_ENV'))[$domain]['services'] : null;
        return $route ? view($route) : abort(404);
    }

    public function departments($domain)
    {
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['departments']) ? config('domains.' . env('APP_ENV'))[$domain]['departments'] : null;
        return $route ? view($route) : abort(404);
    }

    public function department($domain, $id)
    {
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['departments-single']) ? config('domains.' . env('APP_ENV'))[$domain]['departments-single'] : null;
        return $route ? view($route) : abort(404);
    }

    public function doctors($domain)
    {
        $doctors = User::where('role', 'doc')->get();
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['doctors']) ? config('domains.' . env('APP_ENV'))[$domain]['doctors'] : null;
        return $route ? view($route, compact('doctors')) : abort(404);
    }

    public function blog($domain)
    {
        $news = News::all();
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['blog']) ? config('domains.' . env('APP_ENV'))[$domain]['blog'] : null;
        return $route ? view($route, compact('news')) : abort(404);
    }

    public function blogOne($domain, $id)
    {
        $news = News::findOrFail($id);
        $route = isset(config('domains.' . env('APP_ENV'))[$domain]['blog-single']) ? config('domains.' . env('APP_ENV'))[$domain]['blog-single'] : null;
        return $route ? view($route, compact('news')) : abort(404);
    }
}
