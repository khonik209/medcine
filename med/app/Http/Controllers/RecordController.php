<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Domain;
use App\Illness;
use App\Payment;
use App\Record;
use App\Traits\ChatTrait;
use App\Traits\IllnessTrait;
use App\Traits\PaymentTrait;
use App\Traits\RecordTrait;
use App\Traits\ReturnTrait;
use App\Traits\UserTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RecordController extends Controller
{
    use IllnessTrait;
    use RecordTrait;
    use UserTrait;
    use ChatTrait;
    use PaymentTrait;
    use ReturnTrait;
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

    }

    /**
     * Запрос всех записей за данный период
     * @param $domain
     * @param Request $request
     * @return array
     */
    public function allRecords($domain, Request $request)
    {
        $records = Record::where('start_date', '>=', Carbon::parse($request->start)->startOfDay())->where('end_date', '<=', Carbon::parse($request->end)->startOfDay());
        if ($request->doc) {
            $records = $records->where('doctor_id', '=', $request->doc);
        }
        $records = $records->get();
        $source = [];
        foreach ($records as $record) {
            $chatId = null;
            if ($record->illness) {
                $illness = $record->illness;
                if ($illness->chat) {
                    $chatId = $illness->chat->id;
                }
            }
            $source[] = [
                'id' => $record->id,
                'name' => $record->name,
                'patient' => $record->user ? $record->user->name : "Пользователь удалён",
                'patient_id' => $record->user ? $record->user->id : '',
                'email' => $record->user ? $record->user->email : "Пользователь удалён",
                'start' => $record->start_date,
                'end' => $record->end_date,
                'comment' => $record->comment,
                'chat_id' => $chatId,
                'doctor_name' => $record->doctor() ? $record->doctor()->name : 'Врач удален',
                'doctor_id' => $record->doctor() ? $record->doctor()->id : null
            ];
        }
        return ($source);
    }

    /**
     * Создаёт новую запись
     *
     * @param Request $request
     * @return string
     */
    public function newRecord(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'email',
            'start' => 'required',
            'doctor_id' => 'required'
        ]);

        if ($validation->fails()) {
            return json_encode([
                'error' => 'start is required'
            ]);
        }

        $target = User::find($request->doctor_id);
        if (!$target) {
            $target = $this->domain->getAdmin()->first();
        }
        if (!$request->email && !$request->user_id) {
            return json_encode(['error' => 'can`n determine user']);
        }

        $start = Carbon::parse($request->start);
        if ($request->end) {
            $end = Carbon::parse($request->end);
        } else {
            $end = Carbon::parse($request->start)->addMinutes(30); // Базовая продолжительность приёма - полчаса
        }

        /* Нельзя записываться в прошлое =( */
        if ($start < Carbon::now() || $end < Carbon::now()) {
            return json_encode([
                'error' => 'record in the past'
            ]);
        }

        /* Проверяем, есть ли уже запись на это время */

        if (!$target->isFree($start, $end)) {
            return json_encode([
                'error' => 'doctor is busy'
            ]);
        }

        /**
         * С данными всё хорошо, переходим к логике
         */

        if ($request->email) {
            $checkUser = User::where('email', $request->email)->first();
        } else {
            $checkUser = User::find($request->user_id);
        }
        if ($checkUser) {
            $user = $checkUser;
            if ($request->phone) {
                $user->phone = $request->phone;
            }
            $user->save();
        } else {
            $user = $this->createUser($request->email, null,$request->patient, $request->phone, $this->domain->id);
        }


        /* Если передаем illness, то прикрепляем к нему record */
        $illness = Illness::find($request->illness_id);
        if (!$illness) {
            /* Создаем обращение */
            $illness = $this->createIllness($user, $target, $request->name, $request->comment);
            try {
                $this->logIt( 'Illness', $illness->id, 'CreateIllness', 'Обращение создано', '',$illness->domain_id );
            } catch ( \Exception $e ) {
                Log::error( 'Не удалось записать активность: ' . $e );
            }
        }

        $chatId = null;
        if ($this->domain->custom('chat')) {
            if ($request->place == "online") {
                /* Для онлайн консультации - готовим онлайн кабинет */
                $chat = $this->createChat($user, $target, $illness);
                $chatId = $chat->id;
            }
        }

        $record = $this->createRecord($user, $target, $start, $end, $illness, $request->name, $request->comment);

        if ($this->domain->custom('payments')) {
            if ($target->doctor && $target->doctor->base_payment) {
                $payment = $this->createPayment($user, $target, $illness, $target->doctor->base_payment);
            }
        }

        $array = [
            'id' => $record->id,
            'name' => $record->name,
            'patient' => $record->user->name,
            'patient_id' => $record->user->id,
            'email' => $record->user->email,
            'start' => $record->start_date->format('Y-m-d H:i'),
            'end' => $record->end_date->format('Y-m-d H:i'),
            'comment' => $record->comment,
            'chat_id' => $chatId,
            'doctor' => $record->doctor() ? $record->doctor()->name : 'Врач удален'
        ];
        return $this->printResponse(0, 'done', $array);
    }

    /**
     * Редактирование записи
     * @param Request $request
     * @return string
     */
    public function editRecord(Request $request)
    {
        $record = Record::find($request->id);
        if (!$request) {
            return json_encode([
                'error' => 'record not found'
            ]);
        }
        if ($record->place != $request->place) {
            /* Смена формата мероприятия */
            if ($request->place == 'online') {
                $illness = $this->createOnlineRequest($record->user, $record->doctor(), $request->name);
                $record->illness_id = $illness->id;
            }
        }
        if ($request->name) {
            $record->name = $request->name;
        }
        if ($request->comment) {
            $record->comment = $request->comment;
        }
        if ($request->start) {
            $record->start_date = $request->start;
        }
        if ($request->end) {
            $record->end_date = $request->end;
        }
        $record->save();
        $array = [
            'id' => $record->id,
            'name' => $record->name,
            'patient' => $record->user->name,
            'patient_id' => $record->user->id,
            'email' => $record->user->email,
            'start' => $record->start_date,
            'end' => $record->end_date,
            'comment' => $record->comment,
            'chat_id' => $record->illness ? $record->illness->chat_id : null,
            'doctor' => $record->doctor() ? $record->doctor()->name : 'Врач удален'
        ];
	    try {
		    $this->logIt( 'Illness', $record->illness_id, 'EditIllness', 'Время записи изменено', '',$record->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return json_encode($array);
    }

    /**
     * Удаление записи AJAX
     * @param Request $request
     * @return int
     * @throws \Exception
     */
    public function deleteRecord(Request $request)
    {
        $record = Record::find($request->id);
        if ($record) {
            $record->delete();
            return $this->printResponse(0, 'done');
        }
	    try {
		    $this->logIt( 'Illness', $record->illness_id, 'EditIllness', 'Запись отменена', '',$record->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return json_encode([
            'error' => 'can`t delete'
        ]);
    }

    /**
     * Все данные по конкретному дню календаря
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData(Request $request)
    {
        /* Бизнес-часы клиники */
        $dayNum = Carbon::parse($request->day)->dayOfWeek - 1;

        /* Текущие записи */
        $startDay = Carbon::parse($request->day)->startOfDay();
        $endDay = Carbon::parse($request->day)->endOfDay();
        $records = Record::where('start_date', '>=', $startDay)->where('end_date', '<=', $endDay)->with('user')->get();
        //print json_encode($records);
        /* Врачи, которые в этот день "на смене" */
        $doctors = User::doctors()->has('doctor')->get();
        $doctors = $doctors->filter(function ($v, $k) use ($dayNum) {
            $worktime = $v->doctor->work_time;
            if (!$worktime) {
                return false;
            }
            if (isset($worktime['days'][$dayNum])) {
                return true;
            } else {
                return false;
            }
        });
        /* Готовим "сетку" */
        $data = [];
        $loop = 0;
        foreach ($doctors as $doctor) {
            $business_hours = $this->domain->workingHours();
            //var_dump($business_hours);
            $start = Carbon::now()->startOfDay();
            $end = Carbon::now()->endOfDay();
            //var_dump(isset($business_hours[$dayNum]));
            if (isset($business_hours[$dayNum])) {

                $hours = $business_hours[$dayNum];
                $start = Carbon::parse($request->day . ' ' . $hours->starts);
                $end = Carbon::parse($request->day . ' ' . $hours->ends);
            }
            //print $start."\n";
            //print $end."\n";
            $docRecords = $records->where('doctor_id', $doctor->id);
            $i = null;
            $data[$loop]['doctor'] = $doctor;
            for ($i = $start; $i < $end; $i->addMinutes(30)) {
                $doctorStarts = Carbon::parse($request->day . ' ' . $doctor->doctor->work_time['days'][$dayNum]['starts']);
                $doctorEnds = Carbon::parse($request->day . ' ' . $doctor->doctor->work_time['days'][$dayNum]['ends']);
                //print $doctorStarts."\n";
                //print $i."\n";
                //print $doctorEnds."\n";
                if ($i >= $doctorStarts && $i < $doctorEnds) {
                   // print 1;
                    /* Врач на месте,  ищем запись */
                    $startI = Carbon::parse($i);
                    $endI = Carbon::parse($i)->addMinutes(30);

                    $checkRecord = $docRecords->where('start_date', '>=', $startI)->where('end_date', '<=', $endI)->first();
                    if ($checkRecord) {
                        /* Запись уже есть */
                        if (Auth::user()->role == "user") {
                            $data[$loop]['times'][] = [
                                'time' => $i->format('H:i'),
                                'status' => 'busy'
                            ];
                        } else {
                            $data[$loop]['times'][] = [
                                'record' => $checkRecord,
                                'time' => $i->format('H:i'),
                                'status' => 'edit'
                            ];
                        }
                    } else {
                        /* Записи нет */
                        $data[$loop]['times'][] = [
                            'time' => $i->format('H:i'),
                            'status' => 'free'
                        ];
                    }
                } else {
                    /* В это время врача нет на месте */
                    $data[$loop]['times'][] = [
                        'status' => 'home'
                    ];
                }
            };
            $loop++;
        }

        return $this->printResponse(0, 'done', $data);
    }
}
