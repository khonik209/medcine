<?php

namespace App\Http\Controllers;

use App\Domain;
use App\News;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /* Глобальная переменная для текущего поддомена */
    public $domain = null;

    public function __construct(Request $request)
    {
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($domain)
    {
        $news = News::orderBy('created_at', 'desc')->get();
        return response()->json([
            $news
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($domain)
    {
        if (Gate::denies('news.create')) {
            abort(404);
        }
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($domain, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|max:5120',
            'text' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }
        if (Gate::denies('news.create')) {
            abort(404);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $extension = $image->clientExtension();
            $filename = $this->domain->name . '/news/' . time() . '.' . $extension;
            $s3 = Storage::disk('s3');
            $s3->put($filename, file_get_contents($image), 'public');
        }
        $news = new News;
        $news->title = $request->title;
        $news->text = $request->text;
        $news->image = '';
        $news->domain_id = $this->domain->id;
        $news->image = $filename;
        $news->save();

        return redirect('/admin/news/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function show($domain, News $news)
    {
        return response()->json([
            $news
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function edit($domain, News $news)
    {
        if (Gate::denies('news.create')) {
            abort(404);
        }
        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function update($domain, Request $request, News $news)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'image|max:5120',
            'text' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }
        if (Gate::denies('news.create')) {
            abort(404);
        }

        if ($request->hasFile('image')) {
            if ($news->image) {
                Storage::disk('s3')->delete($news->image);
            }
            $image = $request->file('image');
            $extension = $image->clientExtension();
            $filename = $this->domain->name . '/news/' . time() . '.' . $extension;
            $s3 = Storage::disk('s3');
            $s3->put($filename, file_get_contents($image), 'public');

            $news->image = $filename;
        }

        $news->title = $request->title;
        $news->text = $request->text;

        $news->save();
        return redirect('/admin/news/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($domain, News $news)
    {
        if (Gate::denies('news.create')) {
            abort(404);
        }
        $news->delete();
        return redirect('/admin/news');
    }

    public function getList($domain)
    {
        $news = News::orderBy('created_at', 'desc')->get();
        return view('admin.news.list', compact('news'));
    }

    public function getOne($domain, $id)
    {
        $news = News::findOrFail($id);
        return view('admin.news.single', compact('news'));
    }
}
