<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Domain;
use App\Events\MessageSent;
use App\File;
use App\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        /* Определяем текущий поддомен */
        $this->domain = Domain::where('name', $request->route('domain'))->first();

        /* Передаем параметры поддомена во все представления данного контроллера */
        view()->share('domainObj', $this->domain);
    }


    public function getChats($domain, Request $request)
    {
        $user = Auth::user();
        $chats = $user->chats()->get()->map(function ($chat) {
            if ($chat->messages->count() > 0) {
                $last_message = $chat->messages->sortByDesc('created_at')->first();
                $chat->last_message = $last_message;
                if ($last_message->users()->find(Auth::id())) {
                    $is_read = $last_message->users()->find(Auth::id())->pivot->is_read;
                } else {
                    $is_read = 1;
                }
                $chat->last_message->is_read = $is_read;
                $chat->last_message->avatar = $last_message->author() && $last_message->author()->avatar ? $last_message->author()->avatar : 'doctors/emptyavatar.png';
            } else {
                $chat->last_message = null;
            }
            if ($chat->type == "private") {
                $companion = $chat->users->where('id', '!=', Auth::id())->first();
                $chat->avatar = $companion && $companion->avatar ? $companion->avatar : 'doctors/emptyavatar.png';
                $chat->name = $companion ? $companion->name : 'Пользователь удалён';
            } else {
                $companions = $chat->users->where('id', '!=', Auth::id());
                if ($companions->count() > 0) {
                    $chat->avatars = collect($companions)->map(function ($c) {
                        return $c->avatar ? $c->avatar : 'doctors/emptyavatar.png';
                    });
                }
            }
            return $chat;
        });
        /* Пагинация */
        $page = $request->page;
        $chats = $chats->sortByDesc(function ($chat, $key) {
            return $chat->last_message ? $chat->last_message->created_at : false;
        })->values()->chunk(10);
        try {
            $response = $chats[$page - 1]; // Показываем контент нужной страницы
        } catch (\Exception $e) {
            $response = 'full';
        }
        return $response;
    }

    /**
     * Show chats
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chat');
    }

    /**
     *
     * @param $domain
     * @param $id
     * @return mixed
     */
    public function fetchMessages($domain, $id)
    {
        $chat = Chat::findOrFail($id);
        $user = Auth::user();

        $messages = $chat->messages->map(function ($value) use ($user) {
            $user->messages()->updateExistingPivot($value->id, ['is_read' => 1]);
            $author_user = $value->author();
            if ($author_user) {
                $value->author = $author_user->name ?: $author_user->email;
                $value->avatar = $author_user->avatar_url ?: '/storage/doctors/emptyavatar.png';
            }
            $value->files = $value->files->map(function ($v) {
                $v->full_url = $v->full_url;
                return $v;
            });
            return $value->toArray();
        })->sortBy('created_at')->values();
        $previewFiles = $chat->files->map(function ($v) {
            $v->full_url = $v->full_url;
            return $v;
        });
        return response()->json([
            'messages' => $messages,
            'previewFiles' => $previewFiles
        ]);
    }

    /**
     * @param $domain
     * @param Request $request or (array)
     * @return Message
     */
    public function sendMessage($domain, Request $request)
    {
        $id = Auth::id();
        $user = User::find($id);
        $message = new Message;
        $message->author_id = $user->id;
        $message->chat_id = $request->chat;
        $message->text = $request->text;
        $message->domain_id = $this->domain->id;
        $message->type = 'text';

        $message->save();

        $chat = Chat::find($request->chat);
        $chat->updated_at = Carbon::now();
        $chat->save();

        $message->users()->attach($chat->users->pluck('id'));
        $user->messages()->updateExistingPivot($message->id, ['is_read' => 1]);

        $message->avatar = $user->avatar_url ?: '/storage/doctors/emptyavatar.png';
        $message->author = $user->name;

        if ($request->previews) {

            $files = collect($request->previews)->pluck('id')->toArray();

            $message->files()->attach($files);
            $chat->files()->detach($files);
        }

        $packFiles = $message->files->map(function ($v, $k) {
            $v->full_url = $v->full_url;
            return $v;
        });

        $pack = [
            'text' => $message->text,
            'user' => $user,
            'author' => $message->author,
            'avatar' => $message->avatar,
            'type' => $message->type,
            'files' => $packFiles,
            'chat_id' => $message->chat_id
        ];

        broadcast(new MessageSent(collect($pack)))->toOthers();

        return $message;
    }

    /**
     * @param $domain
     * @param Request $request
     * @return File|null
     */
    public function addChatDocument($domain, Request $request)
    {
        $user = Auth::user();

        $now = Carbon::now();
        $chat = Chat::findOrFail($request->chat_id);
        /* Создали пустое сообщение */
        // $message = $this->addFileMessage($request);

        /* Загрузка новых файлов */
        if ($request->hasFile('files')) {
            $filesToUploads = $request->file('files');
            $files = [];
            foreach ($filesToUploads as $key => $fileToUpload) {

                $dicomPath = $now->year . "_" . $now->month . '_' . $now->day . '_' . $now->hour . '_' . $now->minute . "_" . $now->second;
                $file = new File;
                $file->user_id = $user->id;
                $file->domain_id = $this->domain->id;

                $extension = $fileToUpload->clientExtension();
                if ($extension == 'bin') {
                    $extension = 'dcm';
                }

                if ($request->file_name) {
                    $file->name = $request->file_name;
                } else {
                    if ($extension == 'dcm') {
                        $file->name = 'DICOM_' . $user->id . '_' . time();
                    } else {
                        $file->name = $fileToUpload->getClientOriginalName();
                    }
                }

                if ($extension == 'dcm') {
                    $filename = $this->domain->name . '/file/' . $dicomPath . '/' . $filename = $key . '.' . $extension;;
                    $s3 = Storage::disk('s3');
                    $s3->put($filename, file_get_contents($fileToUpload), 'public');
                    $file->url = $filename;
                    $file->type = 'dicom';

                    if ($key == 0) {
                        $file->save();

                        $file->chats()->attach($chat->id);
                    }
                } else {

                    $extension = strtolower($extension);
                    if ($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "bmp") {
                        $file->type = 'image';
                    } else {
                        $file->type = "other";
                    }

                    $filename = $this->domain->name . '/file/' . str_random(8) . time(). '.' . $extension;
                    $s3 = Storage::disk('s3');
                    $s3->put($filename, file_get_contents($fileToUpload), 'public');
                    $file->url = $filename;
                    $file->save();

                    $file->chats()->attach($chat->id);
                }
            }

        }
        $response = $chat->files->map(function ($v, $k) use ($chat) {
            $v->full_url = $v->full_url;
            return $v;
        });
        return $response;
    }

    public function deleteChatDocument(Request $request)
    {
        $file = File::findOrFail($request->file_id);
        $file->delete();

        return 1;
    }

    public function addFileMessage($request)
    {
        $user = Auth::user();
        $message = new Message;
        $message->author_id = $user->id;
        $message->chat_id = $request->chat_id;
        $message->text = '';
        $message->domain_id = $this->domain->id;
        $message->type = 'other';
        $message->save();

        $chat = Chat::find($request->chat_id);
        $chat->updated_at = Carbon::now();
        $chat->save();

        foreach ($chat->users()->get() as $gUser) {
            if ($gUser->id == $user->id) {
                $gUser->messages()->attach($message->id, ['is_read' => 1]);
            } else {
                $gUser->messages()->attach($message->id, ['is_read' => 0]);
            }
        }

        $message->avatar = $user->avatar_url ?: '/storage/doctors/emptyavatar.png';
        $message->author = $user->name;

        return $message;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function dcmView($domain, $id)
    {
        $user = Auth::user();
        $accessedFiles = [];
        $userFiles = $user->files()->dicom()->get();
        foreach ($userFiles as $file) {
            $accessedFiles[] = $file->id;
        }
        $userChats = $user->chats;
        foreach ($userChats as $chat) {
            $chatFiles = $chat->files()->dicom()->get();
            foreach ($chatFiles as $file) {
                $accessedFiles[] = $file->id;
            }
        }
        $file = File::find($id);
        $fileCount = count(Storage::files('public/' . $file->url));
        if (!in_array($id, $accessedFiles)) {
            return back()->with('error', 'Отказано в доступе');
        }
        if ($user->role == "doc") {
            return view('doc.dcm', [
                'file' => $file,
                'fileCount' => $fileCount
            ]);
        } else {
            return view('user.dcm', [
                'file' => $file,
                'fileCount' => $fileCount
            ]);
        }
    }
}
