<?php

namespace App\Jobs;

use App\Payment;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GiveDefaultOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment=$payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payment = $this->payment;
        $user = $payment->user;
        $doctor = User::find($payment->doctor_id);
        // Уведомление пациенту
        try {
            Mail::send('email.avisoPac', ['payment' => $payment, 'user' => $user], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Оплата медицинских услуг');
            });
        } catch (\Exception $e) {
            Log::error('Сообщение пациенту об оплате услуг' . $user->email . ' не отправились' . $e);
        }
        // Уведомление врачу
        try {
            Mail::send('email.avisoDoc', ['payment' => $payment, 'user' => $user], function ($m) use ($doctor) {
                $m->to($doctor->email, $doctor->name)->subject('Оплата медицинских услуг');
            });
        } catch (\Exception $e) {
            Log::error('Сообщение врачу об оплате услуг' . $user->email . ' не отправились' . $e);
        }
	    try {
		    $this->logIt( 'Illness', $payment->illness_id, 'Payment', 'Платеж оплачен', '',$payment->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
    }
}
