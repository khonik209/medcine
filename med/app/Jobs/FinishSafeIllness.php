<?php

namespace App\Jobs;

use App\Illness;
use App\Services\Yandex\Yandex;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class FinishSafeIllness implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $illness;
    public $yandex;

    /**
     * FinishSafeIllness constructor.
     * @param Illness $illness
     * @param Yandex $yandex
     */
    public function __construct(Illness $illness, Yandex $yandex)
    {
        $this->illness = $illness;
        $this->yandex = $yandex;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $illness = $this->illness;
        $domainObj = $illness->domain;
        $user = $illness->user;
        $doctor = User::find($illness->doctor_id);
        $admin = $domainObj->users->where('role', 'admin')->first();
        $payments = $illness->payments;
        $yandex = $this->yandex;

        if ($illness->result == "success") {
            // Переводим бабки Исполнителю
            foreach ($payments as $payment) {
                if ($payment->aviso == 1 && $payment->test_deposition == 0 && $payment->canceled == 0) {
                    // Обновим платёжки доктора
                    if($doctor->doctor->wallet =="bank"){
                        $payment->card_synonym = $doctor->doctor->skr_destinationCardSynonim; // Опредилили карту получателя;
                    }
                    $payment->wallet = $doctor->doctor->wallet; // Не путать! Тип платежа (bank/yandex)
                    $payment->destination = $doctor->doctor->accountNumber; // Опредилили счёт получателя Не путать! Счёт: Номер кошелька / номер расчётного счета
                    $payment->save();

                    $confirm = $yandex->confirmDeposition($domainObj, $payment);
                    if ($confirm) {

                        $illness->status = "archive";
                        $illness->save();

                        try {
                            Mail::send('email.illnessSuccess', ['user' => $user, 'payment' => $payment], function ($m) use ($user) {
                                $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $user->domain->name);
                            });
                        } catch (\Exception $e) {
                            Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
                        }
                        try {
                            Mail::send('email.illnessSuccess', ['user' => $doctor, 'payment' => $payment], function ($m) use ($doctor) {
                                $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $doctor->domain->name);
                            });
                        } catch (\Exception $e) {
                            Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
                        }
                        try {
                            Mail::send('email.illnessSuccess', ['user' => $admin, 'payment' => $payment], function ($m) use ($admin) {
                                $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $admin->domain->name);
                            });
                        } catch (\Exception $e) {
                            Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
                        }
                    } else {
                        Log::error('Ошибка при переводе денег по платежу ' . $payment->id);
                    }
                }
            }
        } else {
            // Возвращаем деньги Заказчику
            foreach ($payments as $payment) {
                $cancel = false;
                if ($payment->aviso == 1 && $payment->test_deposition == 0 && $payment->canceled == 0) {
                    $cancel = $yandex->cancelPayment($domainObj, $payment);
                }
                if ($cancel) {

                    $illness->status = "archive";
                    $illness->save();

                    try {
                        Mail::send('email.illnessFail', ['user' => $user, 'payment' => $payment], function ($m) use ($user) {
                            $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $user->domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessFail', ['user' => $doctor, 'payment' => $payment], function ($m) use ($doctor) {
                            $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $doctor->domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessFail', ['user' => $admin, 'payment' => $payment], function ($m) use ($admin) {
                            $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $admin->domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
                    }
                } else {
                    Log::error('Ошибка при переводе денег по платежу ' . $payment->id);
                }
            }
        }
    }
}
