<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendUserAccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $password;


    /**
     * SendUserAccess constructor.
     * @param User $user
     * @param $password
     */
    public function __construct(User $user,$password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $password = $this->password;
        // Отправляем пациенту доступы
        try {
            Mail::send('email.addUser', ['user' => $user, 'password' => $password], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Ваш доступ на ' . $user->domain->name);
            });
        } catch (\Exception $e) {
            Log::error('Email не отправилось');
        }
    }
}
