<?php

namespace App\Jobs;

use App\Payment;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class GiveSubscribeOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $payment;

    /**
     * GiveSubscribeOrder constructor.
     * @param Payment $payment
     * @param User $user
     */
    public function __construct(Payment $payment, User $user)
    {
        $this->user = $user;
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        try {
            Mail::send('email.avisoAdmin', ['payment' => $this->payment, 'user' => $user], function ($m) use ($user) {
                $m->to($user->email, $user->name)->subject('Подписка продлена');
            });
        } catch (\Exception $e) {
            Log::error('Сообщение об оплате подписки администратору ' . $user->email . ' не отправились' . $e);
        }
    }
}
