<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends \Regulus\ActivityLog\Models\Activity
{
    protected $fillable = [
        'user_id',
        'content_type',
        'content_id',
        'action',
        'description',
        'details',
        'data',
        'language_key',
        'public',
        'developer',
        'ip_address',
        'user_agent',
        'domain_id',
        'updated_at'
    ];

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function scopePatients($query)
    {
        return $query->where('content_type','=','Patient');
    }

}
