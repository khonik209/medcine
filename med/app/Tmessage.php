<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;

class Tmessage extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }
    public function Telegram()
    {
        return $this->belongsTo('App\Telegram');
    }
}
