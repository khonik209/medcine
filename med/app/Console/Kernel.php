<?php

namespace App\Console;

use App\Console\Commands\CheckDoctorActivity;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\notifyOldPayments::class,
        Commands\FinishOldPayments::class,
        Commands\SubscriptionNotification::class,
        Commands\CheckDoctorActivity::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('new_messages')->everyThirtyMinutes(); //Уведомление о новых сообщениях
        $schedule->command('new_request')->dailyAt('16:00'); // Уведомление о новом обращении
        $schedule->command('notify_subscription_payment')->daily(); // Напоминание об окончании подписки
        $schedule->command('notify_old_payments')->dailyAt('17:00'); // Напоминание об скором завершении safe платежей
        $schedule->command('finish_old_payments')->dailyAt('18:00'); // Напоминание об скором завершении safe платежей
        $schedule->command('check_doctor_activity')->dailyAt('21:00'); // Проверяем пользователей - переводим лишний в оффлайн.

        //$schedule->command('test_cron')->withoutOverlapping(); // Тест CRON
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
