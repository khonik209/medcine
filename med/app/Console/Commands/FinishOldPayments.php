<?php

namespace App\Console\Commands;

use App\Payment;
use App\Services\Yandex\Yandex;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class FinishOldPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finish_old_payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finishes 30-days payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $minusTir = Carbon::now()->subDays(7);
        // Берём платежи (уже оплаченные, но еще переведённые и не возвращённые)
        $payments = Payment::where('canceled', 0)
            ->where('test_deposition', 0)
            ->where('payment_form', '=', 'safe')
            ->where('aviso', '=', 1)
            ->where('updated_at','<=', $minusTir)->get();

        foreach ($payments as $payment){
            $illness = $payment->illness; // Находим обращение
            /* Запоминаем мнения */
            $d_o = $illness->doc_opinion;
            $p_o = $illness->pac_opinion;
            /* Находим домен и создаём экземпляр Яндекса */
            $domain = $payment->domain;
            $yandex = new Yandex($domain,$payment->type);
            /* Находим все участвующие лица */
            $user = $illness->user;
            $doctor = User::find($illness->doctor_id);
            $admin = $domain->users->where('role', 'admin')->first();
            /* Начинаем разруливать */
            if(($d_o =="success" && $p_o=="none")||($d_o =="none" && $p_o=="success")){ // Если ответ один - и он успех

                // Закрываем обращение успехом
                $illness->result = 'success';
                $illness->status = "archive";
                $illness->save();
                // Переводим бабки врачу
                $confirm = $yandex->confirmDeposition($domain, $payment);
                if ($confirm) {
                    try {
                        Mail::send('email.illnessSuccess', ['user' => $user, 'payment' => $payment], function ($m) use ($user,$domain) {
                            $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessSuccess', ['user' => $doctor, 'payment' => $payment], function ($m) use ($doctor,$domain) {
                            $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessSuccess', ['user' => $admin, 'payment' => $payment], function ($m) use ($admin,$domain) {
                            $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
                    }
                } else {
                    Log::error('Ошибка при переводе денег по платежу ' . $payment->id);
                }

            } else { // другие случаи: один ответ - отказ или ниодного ответа

                // Возвращаем пациенту
                $illness->result = 'fail';
                $illness->status = "archive";
                $illness->save();

                $cancel = false;
                if ($payment->aviso) {
                    $cancel = $yandex->cancelPayment($domain, $payment);
                }
                if ($cancel) {

                    try {
                        Mail::send('email.illnessFail', ['user' => $user, 'payment' => $payment], function ($m) use ($user,$domain) {
                            $m->to($user->email, $user->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $user->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessFail', ['user' => $doctor, 'payment' => $payment], function ($m) use ($doctor,$domain) {
                            $m->to($doctor->email, $doctor->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $doctor->email . ' не отправились' . $e);
                    }
                    try {
                        Mail::send('email.illnessFail', ['user' => $admin, 'payment' => $payment], function ($m) use ($admin,$domain) {
                            $m->to($admin->email, $admin->name)->subject('Завершение обращения на ' . $domain->name);
                        });
                    } catch (\Exception $e) {
                        Log::error('Уведомление о завершении обращения пользователю ' . $admin->email . ' не отправились' . $e);
                    }

                } else {
                    Log::error('Ошибка при переводе денег по платежу ' . $payment->id);
                }

            }
        }
    }
}
