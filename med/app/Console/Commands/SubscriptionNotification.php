<?php

namespace App\Console\Commands;

use App\Domain;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SubscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify_subscription_payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a bill to pay domain`s subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domains = Domain::all();
        foreach ($domains as $domain) {
            $activeTo = Carbon::parse($domain->active_to);
            $now = Carbon::now();
            $receintDays = Carbon::now()->addDays(3);
            if($activeTo<=$receintDays && $activeTo>=$now) {
                $admin = $domain->users->where('role', '=', 'admin')->first();
                try {
                    Mail::send('email.subscriptionBill', ['domain' => $domain,'admin'=>$admin], function ($m) use ($admin) {
                        $m->to($admin->email, $admin->name)->subject('Ваша подписка на ТелеДоктор');
                    });
                } catch (\Exception $e) {
                    Log::error('Ошибка при отпавке уведомления о продлении подписке. ' . $e);
                }
            }
        }
    }
}
