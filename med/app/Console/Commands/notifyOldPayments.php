<?php

namespace App\Console\Commands;

use App\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class notifyOldPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify_old_payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind about 30-days period in safe-mode payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Даты

        $minusTir = Carbon::now()->subDays(7);
        $minusTwFive = Carbon::now()->subDays(5);
        // Берём платежи
        $payments = Payment::where('canceled', 0)->orWhere('test_deposition', 0)->get();
        $payments = $payments->where('payment_form', '=', 'safe')->where('aviso', '=', 1)
        ->where('created_at','>', $minusTir)->where('created_at','<', $minusTwFive);

        foreach ($payments as $payment) {
            $doctor = User::find($payment->doctor_id);
            $targetUsers = [
                $payment->user,
                $doctor
            ];
            foreach ($targetUsers as $user) {
                try {
                    Mail::send('email.notifyOldPayments', ['payment' => $payment, 'user' => $user], function ($m) use ($user) {
                        $m->to($user->email, $user->name)->subject('Ваша подписка на ТелеДоктор');
                    });
                } catch (\Exception $e) {
                    Log::error('Ошибка при отпавке уведомления notifyOldPayments ' . $e);
                }
            }
        }
    }
}
