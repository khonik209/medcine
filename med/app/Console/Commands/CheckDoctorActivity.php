<?php

namespace App\Console\Commands;

use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Regulus\ActivityLog\Models\Activity;

class CheckDoctorActivity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check_doctor_activity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check doctor`s activity: offline or online';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $doctors = User::doctors()->whereHas('doctor',function ($q){
            $q->where('status', '!=', 'holiday');
        })->get();
        foreach ($doctors as $doctor){
            $la = Activity::where('user_id',$doctor->id)->orderByDesc('created_at')->first();
            if (!$la || $la->created_at->diffInDays(Carbon::now()) > 14) {
                $form = $doctor->doctor;
                $form->status = 'holiday';
                $form->holiday_until = Carbon::now()->addWeek();
                $form->save();
                try{
                    Mail::send('email.sendDocToOffline', ['doctor' => $doctor, 'profile' => $form], function ($m) use ($doctor) {
                        $m->to($doctor->email, $doctor->name)->subject('Принудительная смена статуса на OFFLINE');
                    });
                } catch (\Exception $e){
                    Log::error('Ошибка при уведомлении врача о переводе в статус OFFLINE: '.$e);
                }
            }

        }
    }
}
