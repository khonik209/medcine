<?php

namespace App;

use App\Scopes\DomainScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'wallet', 'domain_id', 'skr_destinationCardPanmask', 'skr_destinationCardSynonim', 'skr_destinationCardBankName', 'skr_destinationCardType','accountNumber','document'
    ];

    /**
     * ОТНОШЕНИЯ: ДОМЕН
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * ОТНОШЕНИЯ: ПОЛЬЗОВАТЕЛЬ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив рабочих дней врача
     * @param $value
     *
     * @return mixed
     */
    public function getWorkTimeAttribute($value)
    {
        return json_decode($value, true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив рабочих дней врача
     * @param $value
     */
    public function setWorkTimeAttribute($value)
    {
        if ($value != null) {
            $this->attributes['work_time'] = json_encode($value);
            //$this->attributes['work_time'] = ($value);
        } else {
            $this->attributes['work_time'] = null;
        }
    }

    /**
     * Определяем рабочее ли сейчас время у врача
     * @return null|string
     */
    public function workTimeStatus()
    {
        $workTime = $this->work_time;
        if(!$workTime)
        {
            return null;
        }
        $now = Carbon::now();
        /* Проверяем день */
        $day = $now->dayOfWeek-1;
        if(!isset($workTime['days'][$day]))
        {
            return 'holiday';
        }
        $date = $now->format('Y-m-d');
        $start = Carbon::parse($date.' '.$workTime['days'][$day]['starts']);
        $end = Carbon::parse($date.' '.$workTime['days'][$day]['ends']);
        if(Carbon::now()>$start && Carbon::now()<$end)
        {
            return 'work_time';
        } else {
            return 'free_time';
        }
    }

    public function isWorkDay($day)
    {
        $workTime = $this->work_time;
        if(!$workTime)
        {
            return null;
        }
        /* Проверяем день */
        $dayNum = $day->dayOfWeek-1;
        if(!isset($workTime['days'][$day]))
        {
            return false;
        } else {
            return true;
        }
        /*$date = $day->format('Y-m-d');
        $start = Carbon::parse($date.' '.$workTime['days'][$dayNum]['starts']);
        $end = Carbon::parse($date.' '.$workTime['days'][$dayNum]['ends']);
        if(Carbon::now()>$start && Carbon::now()<$end)
        {
            return 'work_time';
        } else {
            return 'free_time';
        }*/
    }
}
