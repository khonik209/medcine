<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function users()
    {

        return $this->belongsToMany('App\User', 'user_chat', 'chat_id', 'user_id')->withPivot('type');
    }

    public function illness()
    {

        return $this->belongsTo('App\Illness');
    }

    public function messages()
    {

        return $this->hasMany('App\Message');
    }

    public function files()
    {
        return $this->belongsToMany('App\File', 'chat_file', 'chat_id', 'file_id');
    }

    /* Свои заготовки */
    public function scopeIllnessChat($query, $illness)
    {
        return $query->where('illness_id', $illness->id);
    }

    public function doctor()
    {
        return $this->belongsTo('App\User')->find($this->doctor_id);
    }
    public function scopeWithUser($query, User $user)
    {
        $query->whereHas('users', function ($q) use ($user) {
            $q->where('user_id', '=', $user->id);
        });
    }
}
