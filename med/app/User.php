<?php

namespace App;

use App\Scopes\DomainScope;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'balance', 'domain_id',
    ];

    /**
     * ПЛАТЕЖИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * ЗАПИСИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany('App\Record');
    }

    /**
     * ЧАТЫ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chats()
    {
        return $this->belongsToMany('App\Chat', 'user_chat', 'user_id', 'chat_id')->withPivot('type');
    }

    /**
     * ОБРАЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function illnesses()
    {
        return $this->hasMany('App\Illness');
    }

    /**
     * ФАЙЛЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('App\File');
    }

    /**
     * АНКЕТЫ ВРАЧЕЙ
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function doctor()
    {
        return $this->hasOne('App\Doctor');
    }

    /**
     * ЗАПИСИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    /**
     * СООБЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function messages()
    {
        return $this->belongsToMany('App\Message', 'user_message', 'user_id', 'message_id')->withPivot('is_read')->withPivot('notificated');
    }

    /**
     * ОТЗЫВЫ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    /**
     * НАВЫКИ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'skill_user', 'user_id', 'skill_id');
    }

    /**
     * ИСТОРИЯ БОЛЕЗНИ
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function diseaseHistories()
    {
        return $this->hasMany('App\DiseaseHistory');
    }

    /**
     * ОТНОШЕНИЕ: сессии пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function sessions()
    {
        return $this->hasMany('App\Session')
            ->orderBy('last_activity', 'desc');
    }

    /**
     * Отношение: Ответы
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /* Свои заготовки */
    /**
     * Возвращает врачей
     * @param $query
     * @return mixed
     */
    public function scopeDoctors($query)
    {
        return $query->where('role', 'doc');
    }

    /**
     * Возвращает пациентов
     * @param $query
     * @return mixed
     */
    public function scopePatients($query)
    {
        return $query->where('role', 'user');
    }
    /* Внешние заготовки */
    /**
     * Возвращает непрочитанные сообщения
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scopeUnreadMessages()
    {
        return $this->belongsToMany('App\Message', 'user_message', 'user_id', 'message_id')->wherePivot('is_read', '0');
    }

    /**
     * Возвращает чаты обращений
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scopeCurrentChats()
    {
        return $this->belongsToMany('App\Chat', 'user_chat', 'user_id', 'chat_id')->whereNotNull('illness_id');
    }

    /**
     * Возвращает текущие обращения пользователя
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scopeUserIllnesses()
    {
        return $this->illnesses()->whereIn('status', ['active', 'new'])->orderBy('created_at', 'desc');
    }

    /**
     * Возвращает записки
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scopeUserNotes()
    {
        return $this->hasMany('App\Note')->where('type', 'note');
    }

    /**
     * Возвращает рекомендации пациента
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scopeUserRecommendations()
    {
        return $this->hasMany('App\Note')->where('type', 'recommendation');
    }

    /**
     * Возвращает новые обращения
     * @return \Illuminate\Support\Collection
     */
    public function scopeNewIllnesses()
    {
        return Illness::where('doctor_id', '=', $this->id)->where('status', '=', 'new')->get();
    }

    /**
     * Проверка нахождения юзера в выбранном чате
     * @param $chat_id
     *
     * @return bool
     */
    public function isMember($chat_id)
    {
        if ($this->chats()->find($chat_id)) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает все отзывы о враче
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function allReports()
    {
        return Report::where('doctor_id', $this->id)->orderBy('created_at', 'desc')->paginate(10);
    }

    /**
     * ЧИТАТЕЛЬ: Получаем массив заблокированных пользователем юзеров
     * @param $value
     *
     * @return mixed
     */
    public function getBlackListAttribute($value)
    {
        return json_decode($value, true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем массив заблокированных пользователем юзеров
     * @param $value
     */
    public function setBlackListAttribute($value)
    {
        if ($value != null) {
            $this->attributes['black_list'] = json_encode($value);
            //$this->attributes['black_list'] = ($value);
        } else {
            $this->attributes['black_list'] = null;
        }
    }

    /**
     * Возвращает записи врача
     */
    public function docRecords()
    {
        return Record::where('doctor_id', '=', $this->id);
    }

    /**
     * Проверяем, занят ли врач на это время
     * @param null $start
     * @param null $end
     * @return bool
     */
    public function isFree($start = null, $end = null)
    {
        if (!$start) {
            $start = Carbon::now();
        }
        if (!$end) {
            $end = $start->addMinutes(30);
        }

        $check1 = $this->docRecords()->where('start_date', '>', $start)->where('start_date', '<', $end)->first();
        $check2 = $this->docRecords()->where('start_date', '<', $start)->where('end_date', '>', $start)->first();
        $check3 = $this->docRecords()->where('start_date', '=', $start)->first();
        if ($check1 || $check2 || $check3) {
            return false;
        }

        return true;
    }

    /**
     * Проверяем готовность врача принять платёж
     * @return bool
     */
    public function isReadyToGetPayment()
    {
        $domainObj = $this->domain;
        $form = $this->doctor;
        if ($domainObj->payment_form == "safe") { // Безопасный прием платежей

            if ($domainObj->id == 1 && $form) { // На теледокторе - проверяем готовность врача к приему платежей
                if ($form->wallet == "yandex") {
                    /* Проверяем готовность принять платёж по яндексу */
                    $wallet = $form->accountNumber; // Опредилили счёт получателя
                    if ($wallet) {
                        return true;
                    }
                } elseif ($form->wallet == "bank") {
                    /* Проверяем готовность принять платеж на карту */
                    $wallet = $form->accountNumber; // Опредилили счёт получателя
                    $docCard = $form->skr_destinationCardSynonim; // Опредилили карту получателя
                    if ($wallet && $docCard) {
                        return true;
                    }
                } else {
                    return false; // Другого пути у нас нет
                }
            } else {
                // Не на теледокторе - (РЕНАТ) - проверяем готовность аккаунта

            }
        } else {
            return true; // При обычном сборе средств - готов априори.
        }

        return false;
    }

    public function getAvatarUrlAttribute()
    {
        if (!$this->avatar) {
            return '/storage/doctors/emptyavatar.png';
        }
        return Storage::disk('s3')->url($this->avatar);
    }
}
