<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiseaseHistory extends Model
{
    /**
     * ОТНОШЕНИЯ: ПАЦИЕНТ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /**
     * ОТНОШЕНИЯ: ВРАЧ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function doctor()
    {
        return $this->belongsTo('App\User');
    }

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * ОТНОШЕНИЕ: ОБРАЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function illness()
    {
        return $this->belongsTo('App\Illness');
    }

    /**
     * ЧИТАТЕЛЬ: Получаем текст истории болезни
     * @param $value
     *
     * @return mixed
     */
    public function getTextAttribute($value)
    {
        return json_decode($value, true);// возвращает объект
        // return $value; // возвращает массив. Массив пока удобней
    }

    /**
     * ПРЕОБРАЗОВАТЕЛЬ: Сохраняем текст истории болезни
     * @param $value
     */
    public function setTextAttribute($value)
    {
        if ($value != null) {
            $this->attributes['text'] = json_encode($value);
            //$this->attributes['black_list'] = ($value);
        } else {
            $this->attributes['text'] = null;
        }
    }
}
