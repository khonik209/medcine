<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }
    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }
}
