<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * ОТНОШЕНИЕ: ДОМЕНЫ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    /**
     * ОТНОШЕНИЯ: ПОЛЬЗОВАТЕЛИ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * ОТНОШЕНИЕ: ОБРАЩЕНИЯ
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function illnesses()
    {
        return $this->belongsTo('App\Illness');
    }
}
