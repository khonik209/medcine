<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 21.05.2018
 * Time: 13:25
 */

namespace App\Traits;


use App\Answer;
use App\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait AnswerTrait
{
    public function saveAnswers($user, $illness, $answers)
    {
        foreach ($answers as $key => $value) {

            $answer = new Answer();
            $answer->question_id = $key;
            $answer->domain_id = $this->domain->id;
            $answer->user_id = $user->id;
            $answer->illness_id = $illness->id;

            if (is_object($value)) {
                if ($value->isFile()) {
                    $fileUpld = $value;

                    $extension = $fileUpld->clientExtension();

                    if ($extension == 'bin') {
                        $extension = 'dcm';
                    }
                    if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'jpeg') {
                        $type = 'image';
                    } elseif ($extension == 'dcm') {
                        $type = 'dicom';
                    } else {
                        $type = 'other';
                    }
                    $file = new File();
                    $file->user_id = $user->id;
                    $file->domain_id = $this->domain->id;
                    $file->name = $fileUpld->getClientOriginalName();
                    $file->type = $type;

                    $filename = $this->domain->name . '/answers/' . str_random(8) . time() . '.' . $extension;
                    $s3 = Storage::disk('s3');
                    $uploaded = $s3->put($filename, file_get_contents($fileUpld), 'public');

                    $file->url = $filename;

                    if ($uploaded) {
                        $file->save();
                        $answer->value = $file->url;
                    }
                }
            } else {
                $answer->value = $value;
            }
            $answer->save();
        }
        try {
            $this->logIt('Illness', $illness->id, 'Other', 'Анкета заполнена', '', $illness->domain_id);
        } catch (\Exception $e) {
            Log::error('Не удалось записать активность: ' . $e);
        }
        return true;
    }
}