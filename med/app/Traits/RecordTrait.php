<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 21.05.2018
 * Time: 13:03
 */

namespace App\Traits;


use App\Record;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait RecordTrait
{

    public function createRecord($user, $doctor, $start = null, $end = null, $illness, $name = null, $comment = null)
    {
        if (!$start) {
            $start = Carbon::now();
        }
        if (!$end) {
            $end = Carbon::now()->addMinutes(30);
        }
        $record = new Record();
        $record->user_id = $user->id;
        $record->doctor_id = $doctor->id;
        if ($name) {
            $record->name = $name;
        }
        if ($comment) {
            $record->comment = $comment;
        }

        $record->start_date = $start;
        $record->end_date = $end;
        $record->illness_id = $illness->id;
        $record->domain_id = $user->domain_id;
        $record->save();
	    try {
		    $this->logIt( 'Record', $record->id, 'CreateRecord', 'Запись создана', '',$record->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return $record;
    }
}