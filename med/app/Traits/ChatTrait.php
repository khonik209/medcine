<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 21.05.2018
 * Time: 13:20
 */

namespace App\Traits;


use App\Chat;
use Illuminate\Support\Facades\Log;

trait ChatTrait
{
    public function createChat($user, $target, $illness)
    {
        $chat = new Chat();
        $chat->illness_id = $illness->id;
        $chat->type = 'private';
        $chat->user_id = $user->id;
        $chat->doctor_id = $target->id;
        $chat->name = "#" . $user->name;
        $chat->domain_id = $this->domain->id;
        $chat->save();

        $user->chats()->attach($chat->id);
        $target->chats()->attach($chat->id);
	    try {
		    $this->logIt( 'Illness', $illness->id, 'CreateChat', 'Беседа создана', '',$illness->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return $chat;
    }
}