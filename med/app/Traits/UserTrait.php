<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 21.05.2018
 * Time: 13:58
 */

namespace App\Traits;


use App\Jobs\SendUserAccess;
use App\User;

trait UserTrait
{
    public function createUser($email, $password = null, $name = null, $phone = null, $domain_id, $role = 'user', $age = null)
    {
        // Создаем Пациента
        $user = new User();
        if ($name) {
            $user->name = $name;
        } else {
            $user->name = explode('@', $email)[0];
        }
        $user->email = $email;
        if (!$password) {
            $password = str_random(8);
        }
        $user->password = bcrypt($password);
        $user->role = $role;
        if ($phone) {
            $user->phone = $phone;
        }
        $user->age = $age;
        $user->domain_id = $domain_id;
        $user->save();

        dispatch(new SendUserAccess($user, $password));

        return $user;
    }
}