<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 18.05.2018
 * Time: 13:06
 */

namespace App\Traits;


use App\Domain;
use App\Illness;
use Illuminate\Support\Facades\Log;

trait IllnessTrait
{
    /**
     * @param $user
     * @param $target
     * @param null $name
     * @param null $description
     * @return Illness
     */
    public function createIllness($user,$target,$name=null,$description=null)
    {
        $illness = new Illness;
        $illness->name = $name;
        $illness->description = $description;
        $illness->user_id = $user->id;
        $illness->doctor_id = $target->id;
        $illness->domain_id = $user->domain_id;
        $illness->save();
        /* Наматываем обращение на счётчик */
        $domain = $user->domain;
        $subscription = $domain->subscriptions->last();
        if ($subscription) {
            $countIllnesses = $subscription->illnesses;
            $subscription->illnesses = $countIllnesses + 1;
            $subscription->save();
        }

        return $illness;
    }
}