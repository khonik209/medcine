<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 18.05.2018
 * Time: 14:45
 */

namespace App\Traits;


trait ReturnTrait
{
    public function printResponse($code=0, $msg='',$object=null)
    {
        return response()->json([

            'code' => $code,
            'msg' => $msg,
            'object'=>$object

        ], 200, [], JSON_UNESCAPED_UNICODE);
    }
}