<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 21.05.2018
 * Time: 13:27
 */

namespace App\Traits;


use App\Payment;
use App\Services\Yandex\Yandex;
use Illuminate\Support\Facades\Log;

trait PaymentTrait
{
    public function createPayment($user, $target, $illness, $sum=0)
    {
        /* Если есть базовый платёж, создаем и оплачиваем */
        $domain = $user->domain;

        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->illness_id = $illness->id;
        $payment->doctor_id = $target->id;
        $payment->check = 0;
        $payment->aviso = 0;
        $payment->email = $user->email;
        $payment->type = 'request';
        $payment->domain_id = $this->domain->id;
        $payment->payment_form = $this->domain->payment_form;

        if ($domain->payment_form == "safe") {
            /* Если безопасная сделся - и это теледоктор, бабки уходят врачу */
            if ($domain->id == 1) {
                if (!$target->isReadyToGetPayment()) {
                   return null;
                }
                $form = $target->doctor;
                if (!$sum || $sum < 150) {
                    $sum = $form->base_payment;
                }
                if ($sum < 150) {
                    return back()->with('error', 'Сумма платежа не может быть меньше 150 рублей');
                }

                $payment->wallet = $target->doctor->wallet; // Не путать! Тип платежа (bank/yandex)
                $payment->destination = $form->accountNumber; // Не путать! Счёт: Номер кошелька / номер расчётного счета
                $payment->card_synonym = $form->skr_destinationCardSynonim;
            } else {
                // todo: Подставлять данные карты админа. Обсудить с Ренатом, надо ли оно ему.
                // Если сделка безопасная, но это не теледоктор - бабки админу
               // $payment->wallet = $target->doctor->wallet; // Не путать! Тип платежа (bank/yandex)
              //  $payment->destination = $form->accountNumber; // Не путать! Счёт: Номер кошелька / номер расчётного счета
              //  $payment->card_synonym = $form->skr_destinationCardSynonim;
            }
        }
        $payment->sum = $sum;
        $payment->save();
	    try {
		    $this->logIt( 'Illness', $payment->id, 'Other', 'Платёж создан', '',$illness->domain_id );
	    } catch ( \Exception $e ) {
		    Log::error( 'Не удалось записать активность: ' . $e );
	    }
        return $payment;
    }
}