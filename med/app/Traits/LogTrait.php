<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 07.06.2018
 * Time: 21:35
 */

namespace App\Traits;


use App\Activity;

trait LogTrait {

	public function logIt( $type, $id, $action = null, $description = null, $details = null, $domain_id ) {
		Activity::log( [
			'contentId' => $id,
			'contentType' => $type,
			'action' => $action,
			'description' => $description,
			'details' => $details,
			'domain_id' => $domain_id
		] );
	}
}