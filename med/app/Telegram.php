<?php

namespace App;

use App\Scopes\DomainScope;
use Illuminate\Database\Eloquent\Model;

class Telegram extends Model
{

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function tmessages()
    {
        return $this->hasMany('App\Tmessage');
    }
}
