<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    protected $table = "news";

    public function domain()
    {
        return $this->belongsTo('App\Domain');
    }

    public function getCreatedAtAttribute($value)
    {
        if (!$value) {
            return null;
        }
        return Carbon::parse($value)->format('d.m.Y H:i');
    }

    public function getImageUrlAttribute()
    {
        if (!$this->image) {
            return '/storage/doctors/emptyavatar.png';
        }

        return Storage::disk('s3')->url($this->image);
    }
}
