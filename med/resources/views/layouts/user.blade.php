<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/img/stethoscope_logo.png"/>

    <title>{{ $domainObj->company }}</title>

    <!-- Styles -->
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/jquery-ui.theme.min.css" rel="stylesheet">
    @stack('styles')
    <link href="/css/app.css?20180528" rel="stylesheet">
    <link href="/css/main.css?20180528" rel="stylesheet">
    <!-- Scripts -->

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body class="{{$domainObj->color}}">
<div id="app">

    <div class="content">
        <div class="clearfix">
            <div class="nav-side-menu">
                <div class="brand">
                    {{$domainObj->company}}
                </div>
                <img class="toggle-btn img collapsed" data-toggle="collapse" data-target="#menu-content"
                     aria-expanded="false"
                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACHSURBVGhD7dZBEYAwDAXRikABJ0RwRAAiGDQgAQWIQEqbaoLQqQZIyr6ZFfAvbQIA4BMisjfQ/Ay5vJdSOhhiqbaG5JxX7+mYsb5dAAD8nP6Kp/f0RFk4Gi3FEGuVITHGyXs6ZqhvFwAAP6ffu3hPbRyNlmKItcoQPYN77+mYrr5dAIA3hXADRypAzQ//PN4AAAAASUVORK5CYII=">


                <div class="menu-list">

                    <ul id="menu-content" class="menu-content collapse out" style="height: 0px;" aria-expanded="false">
                        <li>
                            <a href="{{url('/user')}}">
                                <i class="fa fa-dashboard fa-lg"></i>

                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAjSSURBVGhD7Zl3bNVVHMXrVhxonLgRHFGJcSEOXIQIUVTUqCgECYggiiIqJi7cEBOjlhAxxomoxag466KKDS2ltLUt0xExATQQoyBDqNTPuTm/X3+vfa9971nEPzjJyX333O/93vG7+xVsQwZUVFQcNm/evF5z584dQDiccGRlZeUN8HJ+d1uyZMkuNv1/oaqqan9VFr4Df6PCjW2wAVbACTT0TLvZeqAiPVz5jaog4UrC6YTjCPtTydMIj+MLHYV2Cjwf3oT2LGEN3Ox8C+Gt/O5g1/8N+ALHU/AXrsQaOIXKntXY2LidTbJCWVnZgeQdg596+/oV3oSf7W2yZVBSUrIjBT5KYRuhGvDQnDlz9nVy3lAH0BGX4rvKDSqns452cvuCYXIwBcxSQXAGPNxJKSgvL++M7UDSJ8I34OcieT+Ak2pqag6waQsUFRXtgM0tUJ0kDnBS+4BePwanP8EN8BbLMaqrq/emsneSVkMYJjS/Nf5/gXX81hxYyu9V0eTmdw9YSM+fFJwkwNc5lrRq+YB3W/53oOCuOPsVx38Qnm85YPbs2buh3UvaaqjKzyd8BPbky+xls7TARktxtEg8ZDkGX2539GKlwwct5wcqcyjO1JO/00snWw5APwN+54JKaPC5TsoaHq7jCa+wlIL6+vqd8a1hrMaOtJwbNLHJ/A38C0cXWg6g4CHomvD6Uv0tZ0S2qxC+bsfnVH0NS9I6oM1SeZSb+56Dg0ehenuUpQAc3iGdkNWz7EDLMUjbzz8DiN8Of3A0QMMu3e6Oz6FQ86KcPB0th2Wa+Ar0pW0N2RR4n1CPv28pAGc3uqBizQ/LMdC7YNNAz/WzJG0yWqOj4eugfQcnWUoBtjrKNJD+pYaWZfnpLT+Ez1hqG2T4jAxrCOMllnh3qGFWCtPuwAzHXbFZCT+xJF+3Ef/eUa1IfdFUoaGWWoA0nc00GgotBaC/jtbAKnqCpczQOHRBD1sq0DBAW4S2HHayHCYj8bGkxcOJ+Gg42dGA5BzBticsIe8elpRnAA083dEAbF5G30x9+lgKh1E0deY0S5mBoc5Oa5I7NnEtsY04TVldiB8hx7Amp7GbAH41h9T7EywFqKH4XQqXJIcYds/BBsrrbKklfIrV3JhiqaC0tHRP4r/BLyylAKdXKw/hRZZyAnkXwOIMk/86/KoDh1vSVzlRGmn3WWoJEsPYJDzbkjSdShtxcJ6lFkgul60BP+fg7/1kpdE66KzlaAoSC8P8pA15qtAWONoSJGpYrUxmIj6veSbi98IRqoSlrIB9CdQwSntOw2d30gqTw4Z4NPR6WJLdeGnYHWopFRjoUjTdUcU72UnKEYH4V9brLLUJvujJzpO2IWg3O22dFhzLOscdQj205D9hSXPzXNnic5ClJmhFsKN7LKkhA6z1tBSgT452CekjLLUJ7F+xr7QN8d41Nt3pGL2WPKWORguBDqUTLTWBVvZyIfGRA+PHpOW7IkXATyeoZXOdy0g7tDKBvC/CtY4G4ONnOMPRJmAYVgi+TLyeE58GVziaN/ChE7EWkecVwpwagv0DyqdV1ZLqOxuWO9oERE1eGR9vSQ4+goscDSCusTxBoaVW4aO+7vIlhNGOnbYhlH2SfGP3eHLCR/mSdUP7FM53tAmRMWEXS6q0jiqxsefGj7JTqLiTMoL8Yflm6PaLyoBpG0L6GKixv4mRcaXluG5M/CMtSfsQxkefGIiDZaxesSTtLbjc0ZyhhpI/3gcIW21IJmB/j/LRGQdbUt2+RqtytAkYXeFC4rsHxpPhBt2lLeUE8l5mnzc6nldDyPc0eRqSGylaLZzpaBP4lNHWHy+pxEdJS34lQT2NfV9sL7CUFuTVvFgZHfn5nbEhOtvRmcMz3HG0b9U7Gg3x9fiLj1IxdAQnUS+Az1rSVzqVeNyjgs5exBdJx1HK+UsFkEePcloQwr6BzVrCxYRlcKG1oc2XdPL1cdpGOulay2rETlDvBa9Zkm1X2461lAoSK0iscTR6x1oFP7YUjvTYaMgNi85YFKyXxIeh7vdquLiKeBmhFgw9Bc2E3xPfpHR+69RcDC/W/JEffusdQL7jRw5sL5E9lR9oSXbDrKW/+pI4EaO/+cwHWZJWCHXriydaBD8DPQU3KR8sxm5IcnVpDjUeG22+ejbVnV+N1pNR2mGK/ib8s9k9vgj+oY62lAoS9dak3hpjSY5OsfakpQA0HVF0G1QDXqRyXZ2UNfR1yafXel3YtOy+kKwwc/NoNHXSS5bCfZ/4WjjVUnqQcTGsiz63QKZ30dZT6BGK8/suqHv1QoZVys0uH/jsNAnqcFhJOeHrU0YR8b80dIMhIK5HcA2r+OaYFhjGG5glzQG9/KlndKT/UOmE7yR7rz2Az6vwvY5wGdT5Sg2LHxv8RKV5tlgLi+X08JFCY7c6uX9oJUFfhq4VZHzzvQX9BvQpuZA8hYTxG4CApr8qvoW6qb5JPL7zEA+TnHCwpdaBYTh3wZQ3rdaA7avk05U4Fy7Pdmhqn6EMraDVGSd5c+izUYgeyVZrWFneqqAub1OnzdTnLEvZwfNiNZn1wp7VlVZ2UM+e45pxmNJ12yP9LmnMwazfislzG9SQesxSbqCwa8gcXhaTTzKZoKMMtuGFvRl/18JAeL38WStytlZBh+oPoPDymO+ZLwAn4eQJZ+gYYzkjsOtQW1u7T5LJw56OONLaXHUAvvpDnal01Y3fgvMGTh6E+rSzkrv+lgRljaZMnf3qon2lXYDjEVDDRq/ivS23O/C/H5wO1XEz9fWc1H7Audb46GA4LeO7Uh7wZjcMRseeJ7JeZvOBDooU8gwN0U6vE6w2t25Ozhn46Qj1d4V2bHVQdc5L7L8BhZ1I4eGZXxXgt44vegHs2drRRZNch0Fs9afOW9hHT0R67R+8Rb9Ca9CRnQrcDxe4QoHE9ZKu+0j4a5pQt7w6uF7ppvapqXRK32xWsf8MmjNUahAV1H/s70Fd1NTAHwgroS5Xz0P9p9Jjq/X+NmxDLigo+Adc1wfuir/gWAAAAABJRU5ErkJggg==">

                                Рабочий стол
                            </a>
                        </li>
                        @if(!$domainObj->custom('hide_doctors'))
                            <li>
                                <a href="{{url('/user/doctors')}}">
                                    <i class="fa fa-dashboard fa-lg"></i>
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAXsSURBVGhD1ZkNaFZVHMbXN9kHRfZN0hc1iooy7MPMkDJCSYppakRBpBEUFCIVUhaWFYzIYmFozUoWC0RDB2G10LHvr9YKW4Wl5YrEltVS11y/5/Tc9vG+297d9+1uPvBw7n3O//z//3Pvufece25eLtDT03NYfX39DfBpuKGurq6Fcjf8DfbALrQ98EeOK2AxxwsbGhrOsovRB0ndCrdBJTxS/k2H1jY3N59md6OD2traqUpGSZHQN/AlrvKdlJfV1NSc0tbWdoxN86qrq0/UHaBuMuUC2pTATrdtbm1tPdqmyYMEXnMiRaWlpUdYzhhciHNov0M+6NzllpMHCWxQEiR0u6V+0J1xR7daSgF1G2UDZ1pKHgRvckeutNQPGXakyB15yFLyILjeTEpivKU8DbGWlpaTRZKcrHrKqkirqKg4waYB1D9hm+WWkgVvmuOcwJ+WAjgPd2EwUt/v7qDdbf1dS8mC4PlObpulgMbGxktISnPGHuoGziPSymwawEN+o2yo22IpWRB4upPcbCkF2Az7jPBaPs8231lKFgS+3x1ZbSkFmXSE+qOo74YH4rzCswaBlzrJpZZSEA0z7PoNp4HA5gf5ampqOttSciD4m+7Is5Qz4RyOC3gV38zxFMb+RGb3izg/X9SER/0k1VPOwmY22l2U0zj/jFKT4nV2nxwIvFnBc0l1zO6TA4G/cgK7uaLrKEuhZnp1sAKtHn7J8bdmI+d1ri+DshfLkQ9Sytciu08OBP1LwbmKV1uKDTqyUr4oX7GUDHiIT3XgPyxlBXxFk+I6S8mAgGcqMNxhKSvgb4Y7stFSMtB3hgPn9I7AYkvJgaBhwahhZik2uCBL5IvyeUvJgcDrFRzeZyk26ECVfDHH3GYpOagDCk4SlZZigTt6BT668bW376dxYqisrDyW4DvdmQLLI4J3X8LEOirDKgIJhLsCO2C+5YxB8mG9Bn/S5oTl0QFJrHEy7fBay0OCO3E4nVgOD8L9PBtTXTV60DYOyXygzigpysKh9qmon4JdeLgp98F5rhp9+Ao/R3JdSlAl5x9RvkD5CKW+y1+HWnOpXp34njtxvV2MLZBgPgm+RxnWYemoDsDFelm42diFNiZIdhZ8lOQLKZ+BD8KrbDK2oeeFj6lLNWQ0ucG5rI4XiHRoDtQHmD66xs7mteYALeG5yovhWhL8HEbPyLCkze+wgeNVdPgeygl2/f/DD/UtsAiG7+wB7ET/gnITLOFY3xkrXK6GpRxvpdxJqdl8YHt9fC3h2/1ch8wttHQggHZNBv4+aEd/C/6sc+7QRDcZFrSJPqY+hB9DvbqDX47VyTLKm2yeHbyEeABqsouCbKd8SsNK9bLjfJnrB90a6guu+En40dDq1r6WNJ6v42l/B1yDvs/+xE/13IWGccC4vRiHWyKHHFdSzk6394Q+Aeo/Saf2dy0PCnw9LJ9wk6V+IPEzsNGb7hfZUe7X+YgXlTS6F0ZXRQvDYbf8sQm/GGj3mKW00F3ERpsSsp1hOS2wGQcLYfQzSdtGmb0UMFykRm74RqaLOez1C05t2qIhlw7YTLPddr08LA8JbCfRJjyfHO9iWF/oqvTAaJ6MzSctZwQlRfuvHWy65RRQ/779P24pI2A/Hr/aTgoXYdAhXFVVdTpGex1kRJ2IQLvobq631A+aCKnTOkxvqP/+qWQKjQ7a6o+xYqT/vqfiRRlgWBl3M1nJQf3g1MSYMpbxHe0Xx/4Xole8/ONDG98XWO4FlWHnUMsLS7GAj2L5gcssBZSXlx9J4F05ivG2/OAv9cWCeIDKLgW0FAv4uUZBYHvfX87oBQ7ebCk28KO5TTFetdQLV3T4NCuQrNZQuvJzLUn7RBrlQkuxgY/59lVkqRc57kj4CUQZfqf5P4k2qjs0gwejLJBYR/AzjiC/Opj+Wq2w/9ShEAOJdUQgyMsO9o786lh3xtVZIdGOeK0W/fcQy12VNTLpSCfv6fBFlwviL2zgmSXpbOIQX6vkc6iOHFJM2xHElYcg5/+bfV7eP/t0BoxmWIPiAAAAAElFTkSuQmCC">
                                    Все врачи
                                </a>
                            </li>
                        @endif
                        <li>
                            <a href="{{url('/user/requests')}}">
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANxSURBVGhD7ZpJaBRBGIXjgkvQg6BoUNxQPAURFxQ8KAgeIghCED3MTRBcDupVEQUFcUGIHiIiIggmKgouQcSAhplMMklkmIsTJB4iRsEtorgkGb+/eTO0k04yUZgudR48quuvv7rey1R1V3enrIT/AYlEYnpra+txymeU3+En+LCtrW1rJpMZozS3gdgVGHgFM0HE0I3GxsZJSncTzc3NcxD6RqLvYWo5v8DYpqamqcQj8LXaLqqLm0BorYReNwMK54CxRbT1kjdgv5zCboHpMh6RH0xkR0fHfIUHgfZjZpbypEJuwaaVBHYpFAhyNloevKuQW4jH47NkpFuhQJCzSUZuK+QWbE1gwlvMlJUKDwJtNco5opB7QNwJiXwcjUYnK5wDC3wNbd/I+dHS0rJEYfeQTCanIbRTZpKU1Qhe2N7evpT6YepfrA0eVRd3waJfgNCUBP9CzAxQnqqrqxundLeRSqUmIHgHwhsobZuShLVwlVLchq0L1sE8m07DEUMVQTfM0IGwKhiF/dlpVADt5nmBm+dsnSZcIOiQT1wf4l5QPh+O5GT3Y8Ye6kNerosCBFSZGEq7pB6A5WoaEVzJFpP/QP070+n0RDUVHwiISsh+hUYFE09/7wrHOSIKFxe2sBnc1kQfLPiXyAfn2GtG4GWFiotYLDYTEYz/Z5s/zrHejFDeUejvBCbWhm7Etu72FDgUuWcss6dDpXvgZjlFbd5eK1QjDDyXgWMmYCSS1w/PZbclxLLCn+TVi2+EQR9p8JcwMQI/Wy7cZX19wsM1wqDlDGpXq492rPCQIHezhN63OsduGLE9EgP3QruLb89fF36yDlaSc15Cr1h/n3AnptZBG7xQkv/VFr71pe6OEQMDRxBwizJoXXgEccpL9mClbu4Z+V04b8TuD0yfDYharVAgnDeCkEoJSioUiHwjlOucN5L9lbiC5V6P+ozYG/ptHNerfkYp4SLIiC/m/fUN1D0jftL+3l5aKCVcjNYIsW54DZ51xoTBJ7pQI7mYUygZcQ0lI66hZMQ1lIy4Bp45ZphA2ON70eB9+ER0F0+W3n88sO/aothNqzsJxD2VmXqOd0Pv65V4lfoeaC+5zchOdXMPtstF5FufeBNsnxve5cUanP9qhcgKuA/WwIi9rLBvIBiwzw+nYXV2mpXw76Ks7CdUjtYpWObHwQAAAABJRU5ErkJggg==">
                                Текущие обращения
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/user/history')}}">
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAMqSURBVGhD7Zg9aBRRFIWDv0S00YBoEVFTChZCrLQQG42g0S6VGvCv0MYQQbHVQrEWRKOJCClsDViskp/NZpM0rgFXRGJSGi0koEZZvzPckVHnJ+vOblZ8Bw733Ttn3n1nZ97MJA0ODg4OPzE+Pr5vYmLi1FJQvW0ZlSOfzz+En5j0TS2pnupty6gcZiS9CReJ1Ps6IxXCGYmCM1IhnJEoxE3IS2sXx6YUlTNuhbfL4cjIyHpvst/Asdoa4eVV+OeNVBPOSBT+CyNuswfoNnu5cEYqRE2NuM0eYIyRTtHSysFkS3JrpQ5npExwiw7rvyd/wWGbIh5xRtLc7CzoEPlr+IT5Qv/PFSS6x9LrPJ2fCMQ12+zkg7AcI4N2ajIQ12yPaGHwo90ySZyrdyPXLI0FRi7Vs5HnizWC7or0liZDJkRLqwrrddfSWKC7B/ss/RNspJbgpuIS5sRgLU0WCoW11lq3Sxd8VSqVlikfGhpax2JbNVZUrnF/f/9y8iLai8pDgaADwQIM22CpkT7TxBJsttYy0kR9nnhcOePdjHvsWI9yjcfGxk6arkl5KBB0wGlLqwYW0Qx/MSJwlU7Q/ws8rFxXh3Gnf5XQt+s40TMbCUSeEU0EvY1H1LN9S5o1FrInzIjA8TPwMxwYHR3dSuy1OGD10yaNBiLPSDab3cgl3KEaC9lLw5Vp1uixnXqUkTb4Ab7knG2qKZJPWb3NE8aBJkcRzmtDWakqyOVyG2SEft5XgQ96H4Tf4Xn/dvKhnHMuwG8YO2DlcDDxZoQLCI9ZqWqgT4EF37K0QU8wanOwy0oNfMI0BqPAOd3SBZ94oUB4A+F7XZ1MJrPCyqmDHkfs1++2/Bx8xwJXWb6PY97bW1G5xsVicTXjGWpnlUdCtxXCm3ABsR5zoY/QNMj8X4kl9SU+IO/VWB+TxkZ+0BZFv6bj6PrQ39c4EYg3IW5notCXWZpUP3pl6Ok/2S6L1NbAGUW/Ztqr0mtcd2CR1ftorCXMyFsW+TSJ6PRHWF0bybLQ64vgs7o0Mjk5uZPFvYCPePTvTyK6O9LrPJuiPsCvO8vC9KYvi/l8fvYHmsgTg56rOc4AAAAASUVORK5CYII=">
                                Моя история болезни
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/user/settings')}}">
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAbTSURBVGhD7ZkJiFZVGIanxSKjLMsWS22xsH3RoAWKjMwoolJxqcQQDTQsiqRSCq00t1YyxEyLNoaShNKKYijHcXanabIcwVwy09Q0dXIcdXrey3v//n2Z+ceG6oWXe//3+853znfuvWf7C/7HfwEVFRVLKisrm5OwAdvNdmvfoLGd4hofQxKZbdf2jfLy8mvd6DJLAfjd14kstdS+QUNHusHzLQWorq7u4gR3WPpnQE9fQeMmlpWVnWQpKfCZ6UTGW4oAfZNspaWlZ1pKiqqqqutVvqSk5BhL+QFBr6MBO93AjVR0k00JwGex/W6zFAH6F7JRvr+lGGDrAJ+n7AHH+JxrR5tbh7gkfvb1AJxRV1d3lN0C1NTUnBL68ATPsRwB+izZ4LTm5ubDLQcguZ7o5bITYx/8zfetT4Yg0UnM59qB63i4z1oV1156Qlz1JPZb3x7fUAHbPbLbZyOcrFeV3yO432V9NZ1wFfr53Icd1/JkaNylFI4kUVhYeIRNsvVGXyUbDBpvP/XiPDXErjHApo54kOsyruHr06ir7+cXFxcfZ/eC6GTgYsu5gYJjHWBrso+O1+hYbG/Ih8pWw+FFRUVH2pwRfPBnU3YebII7SH6ITTFQXNehhHN/KipE4R8cZLrlBGgEin5auYKn25WO6uyfMVixYsUJ1L3WbZhgOXdQSR8C6MM7QI/dYPmQgXrfdRJLW9NZAdQTDrZWPWS5zUF9Q1Uv/CPZCJgz1BMELXHQty23KWh4N+rc7jpHWG491CMEDEanTLNyNPTx8xTPqK2tPdFSVqCeF5zEIkv5AYncqsD00spk80M01Hj8RsKvKRMZmuFWuIAh9XK7pgTf5mCVIUalpfyAgF8pMAklHSJD4Hcu/NYNV0P2wnXc/xqlaVUwM93Hq87Ct85l+lpuHfy+avLalO5p0Is98dvixq6k3IDo+Qd7D/TnsP9pn3eId5jNCcD+gP1iVtFZQe8yDdB+YhQBXoRaGgS9yf1rdkuAX6da+y1Mt2olod74hAmPs5wA7CdDTZZiNXwP/wnqIGJcwO8Odo0FTqm2p6pwM/uJy+yaAOzhHuR7rhlnXxrS37G3RS9J4kG82fCgfePbtDfpq26j3t9SOBc+inYL7G6XlMBXH7bKD7SUEfh+6TLDLCWFlkJ6ivBefKfAhZTb4LKJ22YZYM67N71WlNtP0MZcNkKUGePGzLGUNSgzzGXzlwjBTnfQdZayAv79XGfOc0WbJKIBwkE3W8oKvCp3u85CS1kjYyIY9sAbM+3L40HZbSpP43pYygjqmaIy8FlLGaG5h0HnPMrOcHsTE1ESDhyQ39rBfQan83tQugkM+wKXy6pR+pbwDT5YDfeWE6A9C/U/At+EFfg3uJ6Qs+z6N+hNbVdfx38p1x1RzgE1atg1ARqaKadJswG/PpZTAr9XHHeZpaTA/qP9AlKHhuI18GPuJy1fvvxUu6aGXhMK3A5fVRD4qU1JQeDgGAhu5b6f5Rj4SQRJ4LMnw9x0pf1+4joKXp1uzsmI+vr6own2O2zUKYnlBHi5H2yETB39jOHV0YLzLmxTua6XjfvdMOG4KBr4BScu+LV8ZxgPgr3soDMtJYXWTviMw1cr3TChGGIvSfckBOxd8NsFG7UNsNx60KvdaISW5PrYOllOCT7S42nEUDgHfkKZQq5T0n3Y0cD/aajE37KUH/C9jHbgtGujfIF6ghMckv8l16kgJQjYi4C7FZgeHWC5TaHtAnUGeyC4KN1yPyvoSJSAxAp6Z67lQwLegq7UG35rYyy3DDRek6ECrdIq1PIhA2/AHa6/gfuLLecGekRH+1rWN3Lf23IMNAhgnwpb9Hca80pnyk6koWNTrRywz3Yy36VbXaQEBR9XAAJt0frGcgQkNxhbeGQjFpPYkGyeHB/wRZTVvBKcLYv8/oah9iy7RIDtMfs0aVi2nD0o2BEG/2fADWEy3Hei0sjEx/0SWB/+hhqipwVB4oDeHd+aKN8mxwomSriT38PtLv+H+a1lyX467n7LuYMAMcnQ4/cRWKciQaVhcI0y/L4Tfohda64mrQiCIFFAfwiq7Hr8JoVPwFuB920TC+GTSkLxWpVECAJGJxOyWCtTu8SAisNDiEssRYAenN7TsNGWYkAZ7TMiC9a8JRGCoDqZ1yuko/0n0n10+HzgRgy1FAF6mWw07hpLCcDeHRaprrwmEQ0qyHhCgs9Taix8xlIAT3JaLB7MZnWQTV1tCho6UIlw/chSAHpX/xEqwTWW2jcY3S50IvqDZlBIfodb2/weTLcVaKj+Yg6ORZORhCbbtf2DxoZL+Hi+xGR4mt3+x78YBQV/AUuyWVz3z7u5AAAAAElFTkSuQmCC">
                                Настройки профиля
                            </a>
                        </li>
                        <hr>
                        <li>
                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAJYSURBVHhe7Zs9TkJBFIUprIxGCwtdg4lRE1s3ANq4CQ0bsHIN0vizBbXAFdBQyOMRGnagiY2FjSY26jnmmsjkIUaYd2+Y+yU3zJl5w1wPzLwfxorjOI7jOH+m3+8vd7vdBuIOkRsJ5tJgbpJmPPI8v0F8IAZBEmrBXCSna0kzHhjkFXEl0gzMCWa8iIwHncZAZyLNwJyYm8h4uAGBAdAr0A+9Xm9XdIaos4z6S4bU19nGMo9lH/ZlTNKfmkDrGNBqteZQd9LpdFap0XaYZdkWy6ivQVdZZh3bWOax7MO+k/anJmjzKaBiwGAwWEDdLT6VdamKDsfimBxbqpI0oGnCACuoGRAuYlqoGQA9dBrTAuPvIY83kfEIDbAETpUHUoxHkQHQVdTPi5xtQgM496HfUV+Tqtmm6BugvQC22+1FrAEbIuMyYgqsSVEF5NREPGMN2JGqeIQGWJgCyGET4z+VYkJoAIFWXwRLM6HIACuUYkJoALSJC6FvopsQGmDlUvgnUU0IDbBKNBNCA/57O4w+x3ifwsfc0wrwyHwR0zOBb4g3dwNEmqW0KZD8Igid9mnQEtH/eFJkAHS6l8Kc+9Bp3wzRBCmqgJz0bocJtOrzANUHIhamQKmM+Aak+1DUElgDyn8sDp32DyPJ/zRmBTUDFH8e9/0BJgywgpoB4SKItiOcjr42OeF1n6uz1G+zjWUeyz7sO2l/aoI2HQOgR25zw+sF4lzKQ9vcUL5H/LpNDq9j+1MTNQOs4AaUaEDym6Utb5eP/8EY/oeJUxiwJGk6juM4jjOOSuUT2Zjnq/YcTLYAAAAASUVORK5CYII=">
                                Выйти
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <hr>
                        <ul class="visible-xs visible-sm list-unstyled">
                            <li>
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAerSURBVHhe7ZsJbFVFFIZx33DHBTdc4hZ3lKgBIkTjEo1G4xI0ApqouEVCUFGMokZcMMoaxWhMFAELjVGIGBUQqC20pa1QF4pGjRSkKCBGUBHw+yf/NF3e63uFtrwL/ZM/M/fMmTvnnDczd2bufR3a0Y62QV5e3m6lpaVjSkpK5hQWFu5j8c6B2bNn747zE+EWuG7BggWHumjHR2Vl5Z786vl2fi3s4aKdAzg/Us6Tri4uLu5mcTJRVVW118KFC+/GmelwOfzXrIFF8Gqr1gLZAFhBtz/XomSirKzsHH7JH9yV0/E/nO3lKlmDSfEQAnsH9Y+zKLeAU2dhnMavnFwM+8MQDLr1tZrU0HlO16SlW7Zs2cVVmwS63anzDtygunCSi3IHGHUchi6XgaSTNQwkJ/+oZdN07cnuZ8kIymWSpUJBQcH+6NwPFcjQc6i3Cc7IuWHiZ3eBjZwpJ12kwHRC9o+Mp/t2kYz8UOvmB6UUoOxz6ZgruB4+f/78E1ycW8C4e+3QqoqKisMtrgXyCS4f7uvO8B9kGwnKUUGpASh/EE6DN6K3h8W5BxYve2Nk6PrwZovrgXKNYQVgZewd5N+3bFhQSio8K8uREotSguKvrNfH1718Xa0VYFBKInBghhyB/S1KCfQG2OG5Fkn2tWU3WpQsaKbH+DCW9Yy2OCXo+h3RWyeHmf3PlIy6DzgAs4JS0oDhXeUArLSoSaA/zg6P0zWz+gHk/4SbGUqnByVDkynyOLcESg+OsMr2B8bcauOyWpjol7f+OvUIyci/ZtnooGQoAMhWuCxnAxC6MBxjUUZQZ66dGaBrfvmzfb0Gp/cLSkkBhg+x8c9blBHo9nGdryzSfcIiCqZ8jOYsMPghOzPSoozwUnil63WXjPQZX2cdyJwAY/p2GQ4nWpQVcHS4HZ6gDRFpka/7WSUZIADdHIDFFmUF7QlwVhsb7RFG2PllcWJMDLQMxviN8D94oMVZAYenyXE7r2A0OiRJBDB8lpzgV73FoqxAvfgEEQdbnDxg/EA7MdWijED3NBgOTghEsia+hsABbW3jmV9ni5sEeh/Z+fxsT4VyGjgzxQ69ZFGTQDes8HL2gKO54GlwHs5vxqn15eXlR1ucFuh94YANtSj5wJnJdmq8RWmBTu8YMNKTLE42ysrKTsYhPRI3pju0pKxnUVHREcrj+LsKGCzQmWJQSDpw6lU7VQ7rneMxTE5VGTrf6qWnj8jjKfKOMRS0m8OZ7x2EpywO8B5gkR0eJRnp5VBbXD1BegfFpINf+hKc2YSjGg4XWRygoUGZlr+byZ8hGfm4FK5hAj0+KCYdOPSinfqRtN4Smev7YAFlYc2g8U8+nCuSqoc0a0mdk1B3x5FiOQUznhbJafid9Rfo2kXJBb/mSTgSDkHhXRbXA/LR8HUFzDtE9Rj1hEKdF1ptm6H7O9u2wJnb7JDGfTj8qAvKfnF5fsMgwLJtmRMUQO7xJtRaQ5PsHPI3sfTe1SptAxp9BcrJGibIYy0O0KRI2W8qh1P0gkROk1/iOqtgs58Onldm+h7aamtSVhu6Xgrv0Vbe6q0LG/OJG68grTe+kXWFq21gnoNwELLplulpMrg5vxz6T6ku91hGkE+hd3Wkd92NLATW/BU+1pJDLS1o6ECMqVLDpDPjq/MIjDufsrg91lvhTnKY9FmutWSWvDA+OpsCenoPqQMa1RlZ97Wb7klbN1CmiTYG4nf4JGzdiRdjNCmGMQ8/bLj8xbAL0ImHpT9qeEjO9TWxHnLNJcMXLVp0cKjUANTR6vIn6UZyXQX71g2EQHuXIte8EPXWwKfT3btFQANnwRo3OKFhELQ9pqzS5X/B8CJV3ZT8WBjH8lryw+K+QtC9kH/muvNgP7jU+uISrhsFAlkvGE61XFeBeKShbS0Gbn4hDcXH49SGjyjP3nkuF9/SnKAy95Iwn4jk/yadRDqI9GPLqmFYZMlZ8n1hxkAg74m89qMM8q332p4GLqKBOPHJ8H1dVAvKdWa4Xjrkl8E+8fQIWQ+uPyANY70O9USpt/wW6gQizENmuh7R1+VlFrUOaEgfVMV3f8X8uo2+FPEO8kvrKBCF9IbaAxfqdEE+EPl40odJmzySyyYQtHmi5c066t8q6AyBxsPukbSaxht9IKmZG/mdlMetc5gXtgVNBYK0h9sptXrrQt8V0FhYtMAN5AfErl4XepZT1jtV2dYiTSD+UIrsPau1PmzI2GgE+WmpPrZqLaQJxBAXtx0wQF+EaWGiIKyk69/ekr94JigQtB3mJdrvanHbQpMcBoTnuQ0pJBDnubhVQXtXuM3qtgx8I6hxjFCXjBOfNjT5WgdYpcXh/Uc8zsuNV3X6XBajXsag+I2w+Cm8HrbYx5M8jQ6jnRLdX+l2Oz9IB9b4R2KcjtnCDG1qrL6g3Z7VtgoMr6vweZnvuYSV6DEuyj2om2LkQxgcTpcjuV5IOlh7CatmBHW0Da+7rJ6nQLs496FFE0ZrBRg/z4/UgkaP1NvQOVN7C+n7vwYXU/YEDF+tmlpuPw5z93vkpqCXLDh0HXwbJ+LpUkaivxqOqru0Tjy8JdZGaRCcDL+BOmPQP1S0+SonfYOecCX5ZP7i7WhHO9rRjjZHhw7/AxsoXhtyMHOMAAAAAElFTkSuQmCC">
                                {{$domainObj->phone}}
                            </li>
                            <li>
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAkfSURBVHhe7ZsJjF1lGYbrUlFcQINLccEQAbcqWmI1FEQTKoQQA4JamrhL3QqxssWKWk1JKglhU6QGKjLSWKopEQVFaYTamekyHSYDai1VWwmWHarULjA+7+S9k3v/851t7tyZmsybvDl3/u/9vv8755777zNlEpMYX2zYsGHqunXrjtq4ceOpfJ7L9ez169dfyOcvwc+rnL+nDw4OvsQu/98YGBh4OTd0Bvw+NzgI98KhMqJ/Bt4Ll8Gz1q5d+wqH3P+xevXq55P0h+HP4X+jGxwF9xLrNt6Q0/k81VXtX9i8efMBJDmPBO9vSrwT3KZ6+Jm8wFVPPPhdf4Sk/hEk2zFS3xbeiJOcwsSARN5AIrenyZVwFz732G8F1+t1hbfwuZ/rEzDyC4lPV09Pz8uc0viBik8hgUfShAI+CW9C/7m+vr4jhoaGnusQueCbfRM+c+33mOMU8c+9vb1vs3vnQVKL4LNBIs1cg0Yt+IvsNiq4bZmjeEn8lHpzZtmtM1ixYsXzSGZpUnELsd9Fm/B+u4wpiD0b6meSV/d/uH7Q8rGFXl0q6EorbRDbQ/AT6J5jl45A3Sz1nUddeV3sE9imWz52IOgPgsoaXL1p06bXWjouIJ+Z1LstyaPBbRqEWdo+qOxrQSUNXqmfhqXjCupWL/SnJJ8Gl1vWHgg0C4ZDWCpfaNmEgS7wdeSyPc3NPM2y0YHX+mBu8p9BYN38Ystqg27uHcQ4nxhqU+4wNRZYQtmc/v7+V1laCfhMh//GP83xr1xHP3TG+ZrmgA0S+EeWVIYaUXqHz+CvSVEmZjOJv4frKt2Y3YdBX380ZRfDT2JvuTHKvtAco0GVW1IPOM6EzwRB+2iJX2hZJegbJ1ZPEKuMmgQtUs/Cdbb+btj4+3aHH4Y0lP+xYW/ioCX1QAV3BcF2aTRnSSmc+HfwqzQNLuA1xOHSWk5ZyxtC2XGpRuQLON6SasApDESFlRs9zdbwWZ7GGEvyZbzT1Y2A8nWpjryvsLkacIgmOPdrWGpJITQ5IcbvgxjNfApeB7US9DH050BNjB61vZBot6hdcZUjINbZkdbmctDyvxGHzG+fsnmWFALtgWj/kPo3iO1xuCBv2Uvl2LWCFPo3iOZCu7RAA7JIz4M5zJJiIP5m6gwfrLoAgfbmxLeZmv5Os7QQaH+S+I6QGLuLukrsmcERZafYXAyE96bOcInNhUD35cRvmMR8Fl6sRtHSUqCfBndH8WDhKA/7ykQvnm9zPjyqSh31+rzFklzQxx+O9unUVzev/t+yWsD31jSeWTjjw++S1Ieyy23OByINMFoc4VabC1GQ7HctqQ1i3hjE+0vZm4TfBakfZdfbnA+EmZEfjtfanAs0x6Z+5qo6r30zNAzHP3qjFliSCzRfTP3gSpvzgWOm9eb1/7TNucDvttSPsoe5HmJJbeA/P4ipNYDSmGgybRG+P7M5H046dZxpc4iCbvMrlowKxMjMGYjZZXMh0H0j8F1qcwyN71MnUa+iJSGiyuCDdecLzcBfU/A0pnicJYUgp2WpL2WX2Byju7v71akT3GdzLgi8NvCr1G3mgZiZxo+y+2wuBdqB1B/OtTkGU80jA6dHbQ6hIS+afYnPEN3euyypDe0DEiPT+MFzLSkEbdahPIDMinXpsjlJH5U6wUdsDoE9M2mi8oejMXpV4P/VNCZ8uuoaH/4LUn/KHijtjXhCr0kdYeFPIJp4wDtsrg0lSbLRGt8NlhTCG7ObU3/KihtAQRsYqaNY1AgSeHHgc5XNtcEDPT6Ipxs41pJCoNU5g4w/rLZhgjCz1UXlud0g9isD/SKbawP/m4J4f6symPIUfEfgP2BJORDfnQaAn7I5A/TXBvqLbK4F/A4hXmbyQ9mllhQCndYSWnzNMy0pB+IfJs5KIHcojH1JoK+UcAr8rk5jifws3mdJLvDVuYSML+yt1SATqNZkCNu5iVYP4G6bKwOf9+Cb6U4p31O2CoXmAzCaNu/j4c2wrBrypsN0kW+3pAVUrO2pVK8bebMlpfDu7z1JjGFS3m9ZCOw6fxTuEVI+urYIx8yCCGXha+1u64FAf2fV7TK0UTvS4K8ta4F3qXW6LPPWiNh+N+rtOpwXBgF35O3zY4+W0OTz06L5gH6b6C5L/RJmVn54rbWr1JvoRki9GgccZHl94KwNx2h2d44lLdAiJvZ/pXoRn/u4zm1eAOXvqfBD2KI5RAvR3CofPUhu/CTKdIwm2qxpcFCLosMVtQMq+WUQfHveSi7J6YBjbmLYdkMdotoKo3F+HvWKyyd81ZtJ/J4xO09IwPemFYhU8j1LMsB+UaofR97S39//YqcyNiDob5NKxL1828dYkgEPaB7UxmbqV0h8tGq8GF4e2fOIfiecn44U1QBi15dYuTfKgMDaco5uZmvJ/ECbqt2BX0i0OhI7W776FvlcuomKZg9cFv3eNanDtrFJu7TKUDoEATIjPfNXRRslqtDtQhd8PPWnTIeZVjK++LhmcHYbBuXaWdIkK90i007xBrhQ4xXLW4DmIOyZxRDKTrCkHvyNqCVvCeigXVWerLo7tNN4IDP089FnldmcC73GfX19b6WuWVrM4HqgTSFkh+FROuo8y7L6UBIE0DeWCQyXV90y6yR8Kj1vP/JJ7uGVlo4OBJ8Dw4ORlN/ZdgVtgLfqMHKItvOUm7rfEy1tDwTKLDU1iE3D4c4cUCwAdZ5J3Q8159JETYZOt3RsQNBvJZWMkET0htyg1WXLOwZu7FDqKjp8oZsv3dAZFbjR+bBoOPoU9kvVHdllzKAbJ/YV1LErqXOE2HeiO9UunYFeLSrTKfAwCZFENE1dBU/LG0JXgR6kdpeJo4FZ2ZB4O/qj7dpZkNTh3KROLkWJpNQhKZ3gugwXjRZP5CHOINkjHefd+pvPJ/P5s/DbaJdz/bv9q/DmuucL24a6QJK8AO4MEhoXUvcOeIZTmhjwzb2eZH4M2z0OV4dawf76mjVrXuo0Jh6MzbVTrMXNWv/2UofE11D3vP3qxlN4k+WjJPsLWOVfXnKJvyY+3XAxb1q4NrlfQ2N6GrhjuBkdWriKG/kN1Le4letjIp/1r3BbuOqQo3oONZY6N3hC3lLcJCYxiTYxZcr/AJjxziNu7Ka5AAAAAElFTkSuQmCC">
                                {{$domainObj->feedback_email}}
                            </li>
                            <li>
                                <i class="fa fa-dashboard fa-lg"></i>
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAYqSURBVHhe7ZppaFxVGIbHfReXuC9gEbValPaPokWR4gJqacGlKPaHClptqqKo0NJFwVLBarQuBe0frWCwrVWsjT8Masy+kQVrRAm2ilVLGzGt1pD4vJd3hptJJjOTmWFu6H3h5Z7vO9/5znfOPfdsM4kYMWLEiBEjRoxSo6+v75jW1tZ5LS0tb8BGuBseNJVuJP912cjWxaY+enp6TqRxy2jYHjhCehj2wa/gRlPpH+CwbGRLemlnZ+cJdjM1QSMWwl/d8AaejzY3N1/g7DFQnmxgk8uo7H3Onjqora09kuDXuRG9POc5K2dQZj5lv7OPKvl0VrTBkD+agD934O8T+LHOSnR0dJzS1dV1qkWNkPWixURTU9PpsrGYqK+vPw4/H8gX3CbfzoouCPQdN37VyMjIYVYHQPch3G5Rtrcj32ZR+dtlYzGAfKB73j5TnRVJEOASBapOsGoU+MYvbWtrm25xDJTHKLjE4ijge4M7YZFV0UJDQ8NZBPgXATYkh2pdXd1JyL00bE5glAdURmXlQ7J84l+T40B7e/sZgVGUQLDroJaxa6zS8D0c3erGxsaLrMoZKqOy8mGVRtC1HgVVVkUDBHQOPEhwH1lVMlDPFvivRpxV5QcBPeU3c7NkZvLzSC9Fd1RgkAYNZ4b4PeSvNe/KNMPLB76Wyadkyt2KTvPME4FBFEAwX8B9yUYwVG8g6F0sY6cFBiFgdxncAdWIMHdokrRZCvIhX/Ip2VtqzTWp1aTsUEBwq8WM4Ls+GbudBP83rIT6dESlpetPTnoTQXXBAYvlBcvW2QSj4f+qVRmBzbOy5W0usCoF8u5VHnzaqozAtkq2nBXOtKp8oAOucODPSdaWlfRydUxgEAKB18C96RskwRuevXDM0HYnL09uh1WX6mQ5vDwwKCeSHUDgSySTriC9i8nq+sAgBPTfkP+TxTFQHjZfW0xBvsjbKd+SeT4OI9MBwScAX7AqI2ic9v7DfALTrEqBRl4sP+S/aVVGYLNatpH4BHzyG4SbrcoIGj6TwIewrQ+v4+pEdDouD9ERV1qdEdhp1dkz3qdUFhD8l/D3ZEBaswlwzXg3O9g9CbVjHOD5qei0LkoqbZaCfKB/KbkPsKwV45PAIAqgASughu/VknleB/vDR98weMtzsNdb1PKpNb0muc6nQz7kSz4lYz/fdS0ODKIAgp+hoOBaq0oGGr6JeoYitRUWCKqb4P4IX4AUG9RxIfwPfmZVdEBQusvT0FxoVdGB7xdVB7zFquiAc4Buf7WR6Q0fYYsFLXk0XPNFV2Rm/3QQnE6A2ureb1XRgN+X7XuuVdGDb4B2E+iPcNyj8GTAIep8/B3Ad71V0QWBBttUgn3YqoKBr/XymWmpjBS8UdG6/UsuR9tsYL9/FY3XzL/NquiDN7XAoyDrEXkiaLLDz7f4+Yft8ri3xZEFQevHEe37Z1mVNyj7oDtypVVTB4yCaQSuQ1JrdXX1EVbnDBpeAf+E35dyc1VS0Phn9AZh3heYlH1PZXVusGrqwbdD7fAAI2KG1VlB4+9U43lusCo6ILDZOr8rrdug5K2ML0ZmK83zeIIPfvNTPvJ+2J3LUOaNn4ut/h/Qr0tU6eRLPpXmmbX+koJKfoYrlCawzXCT0yuVpzRP/eg5nAyUdCU6fQqvSc4EzfrY6t5QdwM3SicfkuVTsuqAE9ZfUlBJhYa20tr/i0p7uAd3dkKy8UJaw1K/BqeDvMX40NB/xaoAYV+51l8ShCvMFzRK9//aJv823lmeoT+dvP3YdOl/AVbnhULiywkEp9/lst79ZQJlb6KR2hvUhJdG/xGiHf2gOsLqvFFofFnBUExNOpMFAa6isZoP1lilYf2udOQ9ZNWkUIz4Sg69eRoazAc0+m6Wxwfc+I02iSYI8jHYbLEg6H9ANFh/i9NOcRC/3TBY5gqFYlSsFosH3tRMgn3EYsHQcCXQfSLpoh10FKNitRhtEOhcJr07LEYXfltbS7rEFAmKUbEqZqsKhzvg40O2Aw5peIuZ+r2fSWZRcpIJf8PoZylPadmqjMqWu7zkgoCjUb/3I6eWGZ5vw7ecTi2TsiUd/J4vlrO85BgxYsSIESNGjLFIJP4Hjr+Uk7NpHv8AAAAASUVORK5CYII=">
                                {{$domainObj->address}}
                            </li>
                        </ul>
                    </ul>
                </div>
                <div class="hidden-xs hidden-sm contacts">
                    <ul class="list-unstyled">
                        <li>
                            <i class="fa fa-dashboard fa-lg"></i>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAerSURBVHhe7ZsJbFVFFIZx33DHBTdc4hZ3lKgBIkTjEo1G4xI0ApqouEVCUFGMokZcMMoaxWhMFAELjVGIGBUQqC20pa1QF4pGjRSkKCBGUBHw+yf/NF3e63uFtrwL/ZM/M/fMmTvnnDczd2bufR3a0Y62QV5e3m6lpaVjSkpK5hQWFu5j8c6B2bNn747zE+EWuG7BggWHumjHR2Vl5Z786vl2fi3s4aKdAzg/Us6Tri4uLu5mcTJRVVW118KFC+/GmelwOfzXrIFF8Gqr1gLZAFhBtz/XomSirKzsHH7JH9yV0/E/nO3lKlmDSfEQAnsH9Y+zKLeAU2dhnMavnFwM+8MQDLr1tZrU0HlO16SlW7Zs2cVVmwS63anzDtygunCSi3IHGHUchi6XgaSTNQwkJ/+oZdN07cnuZ8kIymWSpUJBQcH+6NwPFcjQc6i3Cc7IuWHiZ3eBjZwpJ12kwHRC9o+Mp/t2kYz8UOvmB6UUoOxz6ZgruB4+f/78E1ycW8C4e+3QqoqKisMtrgXyCS4f7uvO8B9kGwnKUUGpASh/EE6DN6K3h8W5BxYve2Nk6PrwZovrgXKNYQVgZewd5N+3bFhQSio8K8uREotSguKvrNfH1718Xa0VYFBKInBghhyB/S1KCfQG2OG5Fkn2tWU3WpQsaKbH+DCW9Yy2OCXo+h3RWyeHmf3PlIy6DzgAs4JS0oDhXeUArLSoSaA/zg6P0zWz+gHk/4SbGUqnByVDkynyOLcESg+OsMr2B8bcauOyWpjol7f+OvUIyci/ZtnooGQoAMhWuCxnAxC6MBxjUUZQZ66dGaBrfvmzfb0Gp/cLSkkBhg+x8c9blBHo9nGdryzSfcIiCqZ8jOYsMPghOzPSoozwUnil63WXjPQZX2cdyJwAY/p2GQ4nWpQVcHS4HZ6gDRFpka/7WSUZIADdHIDFFmUF7QlwVhsb7RFG2PllcWJMDLQMxviN8D94oMVZAYenyXE7r2A0OiRJBDB8lpzgV73FoqxAvfgEEQdbnDxg/EA7MdWijED3NBgOTghEsia+hsABbW3jmV9ni5sEeh/Z+fxsT4VyGjgzxQ69ZFGTQDes8HL2gKO54GlwHs5vxqn15eXlR1ucFuh94YANtSj5wJnJdmq8RWmBTu8YMNKTLE42ysrKTsYhPRI3pju0pKxnUVHREcrj+LsKGCzQmWJQSDpw6lU7VQ7rneMxTE5VGTrf6qWnj8jjKfKOMRS0m8OZ7x2EpywO8B5gkR0eJRnp5VBbXD1BegfFpINf+hKc2YSjGg4XWRygoUGZlr+byZ8hGfm4FK5hAj0+KCYdOPSinfqRtN4Smev7YAFlYc2g8U8+nCuSqoc0a0mdk1B3x5FiOQUznhbJafid9Rfo2kXJBb/mSTgSDkHhXRbXA/LR8HUFzDtE9Rj1hEKdF1ptm6H7O9u2wJnb7JDGfTj8qAvKfnF5fsMgwLJtmRMUQO7xJtRaQ5PsHPI3sfTe1SptAxp9BcrJGibIYy0O0KRI2W8qh1P0gkROk1/iOqtgs58Onldm+h7aamtSVhu6Xgrv0Vbe6q0LG/OJG68grTe+kXWFq21gnoNwELLplulpMrg5vxz6T6ku91hGkE+hd3Wkd92NLATW/BU+1pJDLS1o6ECMqVLDpDPjq/MIjDufsrg91lvhTnKY9FmutWSWvDA+OpsCenoPqQMa1RlZ97Wb7klbN1CmiTYG4nf4JGzdiRdjNCmGMQ8/bLj8xbAL0ImHpT9qeEjO9TWxHnLNJcMXLVp0cKjUANTR6vIn6UZyXQX71g2EQHuXIte8EPXWwKfT3btFQANnwRo3OKFhELQ9pqzS5X/B8CJV3ZT8WBjH8lryw+K+QtC9kH/muvNgP7jU+uISrhsFAlkvGE61XFeBeKShbS0Gbn4hDcXH49SGjyjP3nkuF9/SnKAy95Iwn4jk/yadRDqI9GPLqmFYZMlZ8n1hxkAg74m89qMM8q332p4GLqKBOPHJ8H1dVAvKdWa4Xjrkl8E+8fQIWQ+uPyANY70O9USpt/wW6gQizENmuh7R1+VlFrUOaEgfVMV3f8X8uo2+FPEO8kvrKBCF9IbaAxfqdEE+EPl40odJmzySyyYQtHmi5c066t8q6AyBxsPukbSaxht9IKmZG/mdlMetc5gXtgVNBYK0h9sptXrrQt8V0FhYtMAN5AfErl4XepZT1jtV2dYiTSD+UIrsPau1PmzI2GgE+WmpPrZqLaQJxBAXtx0wQF+EaWGiIKyk69/ekr94JigQtB3mJdrvanHbQpMcBoTnuQ0pJBDnubhVQXtXuM3qtgx8I6hxjFCXjBOfNjT5WgdYpcXh/Uc8zsuNV3X6XBajXsag+I2w+Cm8HrbYx5M8jQ6jnRLdX+l2Oz9IB9b4R2KcjtnCDG1qrL6g3Z7VtgoMr6vweZnvuYSV6DEuyj2om2LkQxgcTpcjuV5IOlh7CatmBHW0Da+7rJ6nQLs496FFE0ZrBRg/z4/UgkaP1NvQOVN7C+n7vwYXU/YEDF+tmlpuPw5z93vkpqCXLDh0HXwbJ+LpUkaivxqOqru0Tjy8JdZGaRCcDL+BOmPQP1S0+SonfYOecCX5ZP7i7WhHO9rRjjZHhw7/AxsoXhtyMHOMAAAAAElFTkSuQmCC">
                            {{$domainObj->phone}}
                        </li>
                        <li>
                            <i class="fa fa-dashboard fa-lg"></i>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAkfSURBVHhe7ZsJjF1lGYbrUlFcQINLccEQAbcqWmI1FEQTKoQQA4JamrhL3QqxssWKWk1JKglhU6QGKjLSWKopEQVFaYTamekyHSYDai1VWwmWHarULjA+7+S9k3v/851t7tyZmsybvDl3/u/9vv8755777zNlEpMYX2zYsGHqunXrjtq4ceOpfJ7L9ez169dfyOcvwc+rnL+nDw4OvsQu/98YGBh4OTd0Bvw+NzgI98KhMqJ/Bt4Ll8Gz1q5d+wqH3P+xevXq55P0h+HP4X+jGxwF9xLrNt6Q0/k81VXtX9i8efMBJDmPBO9vSrwT3KZ6+Jm8wFVPPPhdf4Sk/hEk2zFS3xbeiJOcwsSARN5AIrenyZVwFz732G8F1+t1hbfwuZ/rEzDyC4lPV09Pz8uc0viBik8hgUfShAI+CW9C/7m+vr4jhoaGnusQueCbfRM+c+33mOMU8c+9vb1vs3vnQVKL4LNBIs1cg0Yt+IvsNiq4bZmjeEn8lHpzZtmtM1ixYsXzSGZpUnELsd9Fm/B+u4wpiD0b6meSV/d/uH7Q8rGFXl0q6EorbRDbQ/AT6J5jl45A3Sz1nUddeV3sE9imWz52IOgPgsoaXL1p06bXWjouIJ+Z1LstyaPBbRqEWdo+qOxrQSUNXqmfhqXjCupWL/SnJJ8Gl1vWHgg0C4ZDWCpfaNmEgS7wdeSyPc3NPM2y0YHX+mBu8p9BYN38Ystqg27uHcQ4nxhqU+4wNRZYQtmc/v7+V1laCfhMh//GP83xr1xHP3TG+ZrmgA0S+EeWVIYaUXqHz+CvSVEmZjOJv4frKt2Y3YdBX380ZRfDT2JvuTHKvtAco0GVW1IPOM6EzwRB+2iJX2hZJegbJ1ZPEKuMmgQtUs/Cdbb+btj4+3aHH4Y0lP+xYW/ioCX1QAV3BcF2aTRnSSmc+HfwqzQNLuA1xOHSWk5ZyxtC2XGpRuQLON6SasApDESFlRs9zdbwWZ7GGEvyZbzT1Y2A8nWpjryvsLkacIgmOPdrWGpJITQ5IcbvgxjNfApeB7US9DH050BNjB61vZBot6hdcZUjINbZkdbmctDyvxGHzG+fsnmWFALtgWj/kPo3iO1xuCBv2Uvl2LWCFPo3iOZCu7RAA7JIz4M5zJJiIP5m6gwfrLoAgfbmxLeZmv5Os7QQaH+S+I6QGLuLukrsmcERZafYXAyE96bOcInNhUD35cRvmMR8Fl6sRtHSUqCfBndH8WDhKA/7ykQvnm9zPjyqSh31+rzFklzQxx+O9unUVzev/t+yWsD31jSeWTjjw++S1Ieyy23OByINMFoc4VabC1GQ7HctqQ1i3hjE+0vZm4TfBakfZdfbnA+EmZEfjtfanAs0x6Z+5qo6r30zNAzHP3qjFliSCzRfTP3gSpvzgWOm9eb1/7TNucDvttSPsoe5HmJJbeA/P4ipNYDSmGgybRG+P7M5H046dZxpc4iCbvMrlowKxMjMGYjZZXMh0H0j8F1qcwyN71MnUa+iJSGiyuCDdecLzcBfU/A0pnicJYUgp2WpL2WX2Byju7v71akT3GdzLgi8NvCr1G3mgZiZxo+y+2wuBdqB1B/OtTkGU80jA6dHbQ6hIS+afYnPEN3euyypDe0DEiPT+MFzLSkEbdahPIDMinXpsjlJH5U6wUdsDoE9M2mi8oejMXpV4P/VNCZ8uuoaH/4LUn/KHijtjXhCr0kdYeFPIJp4wDtsrg0lSbLRGt8NlhTCG7ObU3/KihtAQRsYqaNY1AgSeHHgc5XNtcEDPT6Ipxs41pJCoNU5g4w/rLZhgjCz1UXlud0g9isD/SKbawP/m4J4f6symPIUfEfgP2BJORDfnQaAn7I5A/TXBvqLbK4F/A4hXmbyQ9mllhQCndYSWnzNMy0pB+IfJs5KIHcojH1JoK+UcAr8rk5jifws3mdJLvDVuYSML+yt1SATqNZkCNu5iVYP4G6bKwOf9+Cb6U4p31O2CoXmAzCaNu/j4c2wrBrypsN0kW+3pAVUrO2pVK8bebMlpfDu7z1JjGFS3m9ZCOw6fxTuEVI+urYIx8yCCGXha+1u64FAf2fV7TK0UTvS4K8ta4F3qXW6LPPWiNh+N+rtOpwXBgF35O3zY4+W0OTz06L5gH6b6C5L/RJmVn54rbWr1JvoRki9GgccZHl94KwNx2h2d44lLdAiJvZ/pXoRn/u4zm1eAOXvqfBD2KI5RAvR3CofPUhu/CTKdIwm2qxpcFCLosMVtQMq+WUQfHveSi7J6YBjbmLYdkMdotoKo3F+HvWKyyd81ZtJ/J4xO09IwPemFYhU8j1LMsB+UaofR97S39//YqcyNiDob5NKxL1828dYkgEPaB7UxmbqV0h8tGq8GF4e2fOIfiecn44U1QBi15dYuTfKgMDaco5uZmvJ/ECbqt2BX0i0OhI7W776FvlcuomKZg9cFv3eNanDtrFJu7TKUDoEATIjPfNXRRslqtDtQhd8PPWnTIeZVjK++LhmcHYbBuXaWdIkK90i007xBrhQ4xXLW4DmIOyZxRDKTrCkHvyNqCVvCeigXVWerLo7tNN4IDP089FnldmcC73GfX19b6WuWVrM4HqgTSFkh+FROuo8y7L6UBIE0DeWCQyXV90y6yR8Kj1vP/JJ7uGVlo4OBJ8Dw4ORlN/ZdgVtgLfqMHKItvOUm7rfEy1tDwTKLDU1iE3D4c4cUCwAdZ5J3Q8159JETYZOt3RsQNBvJZWMkET0htyg1WXLOwZu7FDqKjp8oZsv3dAZFbjR+bBoOPoU9kvVHdllzKAbJ/YV1LErqXOE2HeiO9UunYFeLSrTKfAwCZFENE1dBU/LG0JXgR6kdpeJo4FZ2ZB4O/qj7dpZkNTh3KROLkWJpNQhKZ3gugwXjRZP5CHOINkjHefd+pvPJ/P5s/DbaJdz/bv9q/DmuucL24a6QJK8AO4MEhoXUvcOeIZTmhjwzb2eZH4M2z0OV4dawf76mjVrXuo0Jh6MzbVTrMXNWv/2UofE11D3vP3qxlN4k+WjJPsLWOVfXnKJvyY+3XAxb1q4NrlfQ2N6GrhjuBkdWriKG/kN1Le4letjIp/1r3BbuOqQo3oONZY6N3hC3lLcJCYxiTYxZcr/AJjxziNu7Ka5AAAAAElFTkSuQmCC">
                            {{$domainObj->feedback_email}}
                        </li>
                        <li>
                            <i class="fa fa-dashboard fa-lg"></i>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAYqSURBVHhe7ZppaFxVGIbHfReXuC9gEbValPaPokWR4gJqacGlKPaHClptqqKo0NJFwVLBarQuBe0frWCwrVWsjT8Masy+kQVrRAm2ilVLGzGt1pD4vJd3hptJJjOTmWFu6H3h5Z7vO9/5znfOPfdsM4kYMWLEiBEjRoxSo6+v75jW1tZ5LS0tb8BGuBseNJVuJP912cjWxaY+enp6TqRxy2jYHjhCehj2wa/gRlPpH+CwbGRLemlnZ+cJdjM1QSMWwl/d8AaejzY3N1/g7DFQnmxgk8uo7H3Onjqora09kuDXuRG9POc5K2dQZj5lv7OPKvl0VrTBkD+agD934O8T+LHOSnR0dJzS1dV1qkWNkPWixURTU9PpsrGYqK+vPw4/H8gX3CbfzoouCPQdN37VyMjIYVYHQPch3G5Rtrcj32ZR+dtlYzGAfKB73j5TnRVJEOASBapOsGoU+MYvbWtrm25xDJTHKLjE4ijge4M7YZFV0UJDQ8NZBPgXATYkh2pdXd1JyL00bE5glAdURmXlQ7J84l+T40B7e/sZgVGUQLDroJaxa6zS8D0c3erGxsaLrMoZKqOy8mGVRtC1HgVVVkUDBHQOPEhwH1lVMlDPFvivRpxV5QcBPeU3c7NkZvLzSC9Fd1RgkAYNZ4b4PeSvNe/KNMPLB76Wyadkyt2KTvPME4FBFEAwX8B9yUYwVG8g6F0sY6cFBiFgdxncAdWIMHdokrRZCvIhX/Ip2VtqzTWp1aTsUEBwq8WM4Ls+GbudBP83rIT6dESlpetPTnoTQXXBAYvlBcvW2QSj4f+qVRmBzbOy5W0usCoF8u5VHnzaqozAtkq2nBXOtKp8oAOucODPSdaWlfRydUxgEAKB18C96RskwRuevXDM0HYnL09uh1WX6mQ5vDwwKCeSHUDgSySTriC9i8nq+sAgBPTfkP+TxTFQHjZfW0xBvsjbKd+SeT4OI9MBwScAX7AqI2ic9v7DfALTrEqBRl4sP+S/aVVGYLNatpH4BHzyG4SbrcoIGj6TwIewrQ+v4+pEdDouD9ERV1qdEdhp1dkz3qdUFhD8l/D3ZEBaswlwzXg3O9g9CbVjHOD5qei0LkoqbZaCfKB/KbkPsKwV45PAIAqgASughu/VknleB/vDR98weMtzsNdb1PKpNb0muc6nQz7kSz4lYz/fdS0ODKIAgp+hoOBaq0oGGr6JeoYitRUWCKqb4P4IX4AUG9RxIfwPfmZVdEBQusvT0FxoVdGB7xdVB7zFquiAc4Buf7WR6Q0fYYsFLXk0XPNFV2Rm/3QQnE6A2ureb1XRgN+X7XuuVdGDb4B2E+iPcNyj8GTAIep8/B3Ad71V0QWBBttUgn3YqoKBr/XymWmpjBS8UdG6/UsuR9tsYL9/FY3XzL/NquiDN7XAoyDrEXkiaLLDz7f4+Yft8ri3xZEFQevHEe37Z1mVNyj7oDtypVVTB4yCaQSuQ1JrdXX1EVbnDBpeAf+E35dyc1VS0Phn9AZh3heYlH1PZXVusGrqwbdD7fAAI2KG1VlB4+9U43lusCo6ILDZOr8rrdug5K2ML0ZmK83zeIIPfvNTPvJ+2J3LUOaNn4ut/h/Qr0tU6eRLPpXmmbX+koJKfoYrlCawzXCT0yuVpzRP/eg5nAyUdCU6fQqvSc4EzfrY6t5QdwM3SicfkuVTsuqAE9ZfUlBJhYa20tr/i0p7uAd3dkKy8UJaw1K/BqeDvMX40NB/xaoAYV+51l8ShCvMFzRK9//aJv823lmeoT+dvP3YdOl/AVbnhULiywkEp9/lst79ZQJlb6KR2hvUhJdG/xGiHf2gOsLqvFFofFnBUExNOpMFAa6isZoP1lilYf2udOQ9ZNWkUIz4Sg69eRoazAc0+m6Wxwfc+I02iSYI8jHYbLEg6H9ANFh/i9NOcRC/3TBY5gqFYlSsFosH3tRMgn3EYsHQcCXQfSLpoh10FKNitRhtEOhcJr07LEYXfltbS7rEFAmKUbEqZqsKhzvg40O2Aw5peIuZ+r2fSWZRcpIJf8PoZylPadmqjMqWu7zkgoCjUb/3I6eWGZ5vw7ecTi2TsiUd/J4vlrO85BgxYsSIESNGjLFIJP4Hjr+Uk7NpHv8AAAAASUVORK5CYII=">
                            {{$domainObj->address}}
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-10 admin">
                @yield('content')
            </div>
        </div>
    </div>

    <div class="modal fade feedback">
        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-body">
                    <form class="form-horizontal" role="form" method="post" action="/feedback">
                        {{ csrf_field() }}
                        <div class="form-group">

                            <label for="feedback_email" class="col-sm-2 control-label">Ваш email для связи</label>
                            <div class="col-sm-10">
                                <input type="email" name="feedback_email" class="form-control" id="feedback_email"
                                       placeholder="ivanov@mail.ru"
                                       @if(!Auth::guest()) value="{{Auth::user()->email}}" @endif required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="text" class="col-sm-2 control-label">Сообщение</label>
                            <div class="col-sm-10">
                        <textarea class="form-control" name="text" id="text"
                                  placeholder="Сообщение об ошибке, предложение, пожелание, вопрос" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-info">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::check())
        <input class="hidden" id="js-user-role" value="{{Auth::user()->role}}">
    @endif
<!-- Scripts-->
    @stack('opentok')
    <script src="/js/app.js?20180528"></script>
    <script src="/js/main.js?20180528"></script>
    <script src="/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    @stack('scripts')
    @if(session('guide'))
        <script>
            $(document).ready(function () {
                $('.js-welcome').modal();
            });
        </script>
    @endif

    {{-- Приветствие --}}
    <div class="modal fade bs-example-modal-lg js-welcome" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Добро пожаловать в {{$domainObj->name}}.ru !</h4>
                </div>
                <div class="modal-body">
                    <h4>Для того, чтобы начать пользоваться кабинетом, Вы можете:</h4>
                    <ol>
                        <li>Выбрать нужного врача из списка на странице <a href="/user/doctors" target="_blank">"Все
                                врачи"</a> или зайти в уже существующее обращение на главной странице кабинета.
                            <p>
                                <strong>Внимание!</strong><br>
                                Для того, чтобы открыть текстовый или видеочат с выбранным врачом, надо дождаться
                                принятия
                                им заявки и нажать соответствующую кнопку на главной странице кабинета. <br>
                                А для того, чтобы врач увидел Ваши документы ( в том числе, снимки ), необходимо
                                добавлять
                                их на странице <strong>обращения к вашему врачу</strong>, в разделе
                                <strong>"Документы".</strong><br>
                                <small>Снимки лучше добавлять группами (все снимки на диске сразу), а прочие документы
                                    по
                                    одному
                                </small>
                            </p>
                        </li>
                        <li>В <a href="/user/account" target="_blank">"личном кабинете"</a> Вы можете найти настройки
                            вашего
                            профиля и менять их в любой момент. Так же, там доступен список Ваших документов и их
                            удаление.
                        </li>
                    </ol>
                </div>
                <div class="modal-footer">
                    <p>Спасибо, что пользуетесь платформой {{$domainObj->name}}.ru! Предложения по работе сервиса,
                        отзывы и
                        жалобы
                        можно писать, нажав "Написать нам" внизу каждой страницы!</p>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>
@include('common.metrika')
</body>
</html>
