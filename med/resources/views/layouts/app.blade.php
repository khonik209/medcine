<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/img/stethoscope_logo.png"/>

    <title>{{ $domainObj->company }}</title>

    <!-- Styles -->
    @stack('styles')
    <link href="/css/app.css?20180528" rel="stylesheet">
    <link href="/css/main.css?20180528" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="{{$domainObj->color}}">
<div id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 admin">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<div class="modal fade feedback">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="post" action="/feedback">
                    {{ csrf_field() }}
                    <div class="form-group">

                        <label for="feedback_email" class="col-sm-2 control-label">Ваш email для связи</label>
                        <div class="col-sm-10">
                            <input type="email" name="feedback_email" class="form-control" id="feedback_email"
                                   placeholder="ivanov@mail.ru"
                                   @if(!Auth::guest()) value="{{Auth::user()->email}}" @endif required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="text" class="col-sm-2 control-label">Сообщение</label>
                        <div class="col-sm-10">
                        <textarea class="form-control" name="text" id="text"
                                  placeholder="Сообщение об ошибке, предложение, пожелание, вопрос" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="/js/app.js?20180528"></script>
<script src="/js/main.js?20180528"></script>
<script src="/js/jquery.maskedinput.min.js" type="text/javascript"></script>
@stack('scripts')
@include('common.metrika')

</body>
</html>
