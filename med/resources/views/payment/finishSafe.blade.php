<!DOCTYPE html>
<html lang="en">
<head>
    <title>Подождите...</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <style>
        html {
            height: 100%;
        }

        body {
            height: 100%;
        }

        img {
            position: absolute;
            left: 50%;
            top: 20%;
            margin-left: -32px;
            margin-top: -32px;
        }
    </style>

</head>
<body>

<img src="/img/preloader.gif" class="img-responsive center-block" alt="Подождите...">

<form role="form" action="/yandex/safe" method="post" id="payment_form">

    <input type="hidden" name="illness" value="{{$illness->id}}">

    <input type=submit value="Нажмите, если переадресация не сработала" class="btn btn-info"
           style="position: absolute;left: 50%;transform: translate(-50%,0);top: 60%;">
</form>

<script type="text/javascript">
    window.onload = function () {
        document.getElementById('payment_form').submit()
    }
</script>

</body>
</html>