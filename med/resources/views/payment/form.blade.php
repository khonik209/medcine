<!DOCTYPE html>
<html lang="en">
<head>
    <title>Подождите...</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <style>
        html {
            height: 100%;
        }

        body {
            height: 100%;
        }

        img {
            position: absolute;
            left: 50%;
            top: 25%;
            margin-left: -32px;
            margin-top: -32px;
        }
    </style>

</head>
<body>
@if(isset($domainObj->options->payment_type))
    <img src="/img/preloader.gif"
         style="position: absolute;    left: 50%;    transform: translate(-50%,-80%);    top: 50%;"
         alt="Подождите...">


    @if($domainObj->options->payment_type=="kassa")
        @if(config('services.yandex.mode') == 'demo')
            @php $action = 'https://demomoney.yandex.ru/eshop.xml'; @endphp
        @else
            @php $action = 'https://money.yandex.ru/eshop.xml'; @endphp
        @endif
    @else
        @php $action = 'https://money.yandex.ru/quickpay/confirm.xml'; @endphp
    @endif



    <form method="POST" action="{{ $action }}" id="payment_form">
        @if($domainObj->options->payment_type=="kassa")
            {{-- Обязательные для Я.Кассы --}}
            @if($payment->type=="request")
                <input type="hidden" name="scid" value="{{ $domainObj->yandex_scid }}">
                <input type="hidden" name="shopId" value="{{ $domainObj->yandex_shopid }}">
            @else
                <input type="hidden" name="scid" value="{{ config('services.yandex.scId') }}">
                <input type="hidden" name="shopId" value="{{ config('services.yandex.shopId') }}">
            @endif

            <input type="hidden" name="payment" value="{{ $payment->id }}">
            {{--Обязательные для АТОЛ --}}
            {{--<input name="ym_merchant_receipt" value='
    {
      "customerContact": "{{$payment->user->email}}",
      "items": [
        {
          "quantity": 1,
          "price": {
            "amount": {{ number_format($payment->sum, 2, '.', '') }}
                    },
                    "tax": 1,
                    "text": "Пополнение счёта на ТелеДоктор"
                  }
                ]
              }
' type="hidden"/>
--}}
            {{-- Обязательные для ЛК --}}
            {{-- <input type="hidden" name="email" value="{{ $payment->email }}">
             <input type="hidden" name="payment_form" value="{{ $payment->payment_form }}">
             <input type="hidden" name="payment_type" value="{{ $payment->type }}">
             <input type="hidden" name="customerNumber" value="email: {{ $payment->user->email}}">
 --}}
            {{-- Для отправки Яндексом чека --}}
            <input type="hidden" name="cps_email" value="{{ $payment->user->email }}">
        @else
            <input type="hidden" name="receiver" value="{{$domainObj->payment_account}}">
            <input type="hidden" name="quickpay-form" value="small">
            <input type="hidden" name="targets" value="Оплата услуг компании {{$domainObj->company}}">
            <input type="hidden" name="formcomment" value="{{$domainObj->company}}">
            <input type="hidden" name="short-dest" value="{{$domainObj->company}}">
            <input type="hidden" name="successURL" value="http://{{$domainObj->name}}/yandex/success">
            <input type="hidden" name="label" value="{{$payment->id}}">
        @endif
        <input type="hidden" name="paymentType" value="AC">
        <input type="hidden" name="sum" value="{{ $payment->sum }}">


        <input type=submit value="Нажмите, если переадресация не сработала" class="btn btn-info"
               style="position: absolute;left: 50%;transform: translate(-50%,0);top: 50%;">
    </form>

    <script type="text/javascript">
        window.onload = function () {
               document.getElementById('payment_form').submit()
        }
    </script>
@else
    Произошла какая-то ошибка. Обратитесь к администратору
@endif
</body>
</html>