@extends('layouts.user')

@section('content')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

    <div class="records">
        <ol class="breadcrumb">
            <li><a href="/user">Главная</a></li>
            <li><a href="{{url('/user/doctor/'.$doctor->id)}}">{{$doctor->name}}</a></li>
            <li class="active">Записи</li>
        </ol>
        <div class="panel panel-default">
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')
            <div class="panel-heading">
                <h4>{{$doctor->name}}</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-9">
                        <script>
                            buisness_hours = [];
                            work_days = [];
                            @if($doctor->doctor && $doctor->doctor->work_time)
                            @foreach($doctor->doctor->work_time['days'] as $k=>$day)
                            @php
                                if( $k == 6 )
                                  {
                                    $k = -1;
                                  }
                                if($day['starts'])
                                    {
                                        $start = $day['starts'];
                                    } else {
                                        $start = '00:00';
                                    }
                                 if($day['ends'])
                                    {
                                        $end= $day['ends'];
                                    } else {
                                        $end = '23:59';
                                    }
                            @endphp

                            work_days.push({{$k+1}});
                            buisness_hours.push(
                                {
                                    dow: [{{$k+1}}], // Monday, Tuesday, Wednesday
                                    start: "{{$start}}", // 8am
                                    end: "{{$end}}",// 6pm,
                                    rendering: 'inverse-background'
                                }
                            );
                            @endforeach
                                    @else
                                work_days = [
                                0, 1, 2, 3, 4, 5, 6
                            ];
                            @for($d = 0;$d<7 ;$d++)
                            buisness_hours.push(
                                {
                                    dow: [{{$d}}], // Monday, Tuesday, Wednesday
                                    start: '00:00', // 8am
                                    end: '23:59',// 6pm,
                                    rendering: 'inverse-background'
                                }
                            );
                            @endfor
                            @endif
                        </script>
                        <div id='calendar'></div>
                    </div>
                    <hr class="visible-xs">
                    <div class="col-sm-3">
                        <div class="calendar-info">
                            <button class="btn btn-primary btn-xs" id="new-record-btn" data-toggle="modal"
                                    data-target="#newRecord">
                                <span class="glyphicon glyphicon-plus"></span>&nbsp;Записаться на <span
                                        class="current_day_span"></span></button>
                            <div class="record-info hidden">
                                <hr>
                                <p><strong>Начало: </strong><span class="start"></span></p>
                                <p><strong>Конец: </strong><span class="end"></span></p>
                                <input type="hidden" id="current_record" value="">
                            </div>
                            <input type="hidden" id="current_day" value="">
                            <input type="hidden" id="doctor" value="{{$doctor->id}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Новая запись -->
    <div class="modal fade" id="newRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Новая запись</h4>
                </div>
                <div class="modal-body">
                    <h4>Новая запись на <span class="current_day_span"></span></h4>
                    <p><strong>График работы врача в этот день: <span id="work-time-info"></span></strong></p>
                    <hr>
                    <input type="hidden" name="time" value="">
                    <label for="patient">Ваше имя</label>
                    <input type="text" class="form-control" id="patient" name="patient" value="{{$user->name}}">
                    <label for="email">Email *</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}">
                    <label for="phone">Телефон</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{$user->phone}}">
                    <label for="name">Ваша проблема *</label>
                    <input type="text" class="form-control" id="name" name="name">
                    <label for="start">Начало *</label>
                    <input type="text" class="form-control timepicker" id="start" name="start">
                    <hr>
                    @if($domainObj->custom('illnesses'))
                        <div class="radio-inline">
                            <label>
                                <input class="record-place" type="radio" name="place" id="online" value="online"
                                       checked>
                                Запись на <strong>онлайн</strong> консультацию
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input class="record-place" type="radio" name="place" id="offline" value="offline">
                                Запись на <strong>очную (оффлайн)</strong> консультацию
                            </label>
                        </div>
                    @else
                        <input type="hidden" name="place" value="offline">
                    @endif
                    @if($doctor->doctor->base_payment)
                        <p class="text-info">
                            Стоимость консультации {{$doctor->doctor->base_payment}} руб. После записи, вы сможете
                            оплатить
                            консультацию на домашней странице личного кабинета
                        </p>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" id="js-new-record" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
@endsection