@extends('layouts.user')

@section('content')
    <div class="admin-doctor">
        <div class="row">
            <div class="col-sm-12">
                @include('user.components.doctor.name')
            </div>
            <div class="col-md-6">
                @include('user.components.doctor.form')
            </div>
            @if($domainObj->custom('records'))
                <div class="col-md-6" id="full-calendar">
                    @include('user.components.doctor.calendar')
                </div>
            @endif
            <div class="col-md-6">
                @include('user.components.doctor.reports')
            </div>
            @if($domainObj->id!=1)
                <div class="col-md-6">
                    @include('user.components.doctor.worktime')
                </div>
            @endif
        </div>
    </div>
    @push('styles')
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    @endpush
@endsection