@extends('layouts.user')

@section('content')
    <div class="admin-doctor">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Закрытые обращения
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            @each('user.components.requests.request',$illnesses,'illness','user.components.requests.empty')
        </div>
    </div>
@endsection