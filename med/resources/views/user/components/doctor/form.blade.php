<div class="panel panel-default">
    <div class="panel-heading">
        Анкета
    </div>
    <div class="panel-body">
        @if($doctor->doctor)
            @if($doctor->skills->count()>0)
                <p>Специальности:
                    <strong>
                        @foreach($doctor->skills as $skill)
                            {{$skill->name}},
                        @endforeach
                    </strong>
                </p>
            @endif
            <p>Стаж, лет: <strong>{{$form->experience}}</strong></p>
            <p>Образование: <strong>{{$form->education}}</strong></p>
            <p>Научная степень: <strong>{{$form->level}}</strong></p>
            <p>Дополнительная информация <strong>{{$form->infomation}}</strong></p>
        @else
            <div class="alert alert-warning">Анкета не заполнена</div>
        @endif
    </div>
</div>