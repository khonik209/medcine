<div id="js-chat">
    <div class="panel panel-default" id="group" data-group="{{$chat->id}}">
        <div class="panel-heading">{{$chat->name}}</div>
        <text-chat :group="{{$chat->id}}"></text-chat>
    </div>
</div>
