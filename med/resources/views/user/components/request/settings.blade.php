<div class="panel panel-default">
    <div class="panel-body">
        @if($illness)
            <h2> Обращение {{$user->name}}</h2>
            @if($illness->status != 'archive')
                <a data-placement="right"
                   title="Завершает текущее обращение и переносит его в архив. Следует использовать после взаимного с пациентом решения о завершении оказания услуг"
                   class="btn btn-info" data-toggle="modal"
                   data-target="#finish_request">
                    Завершить заявку
                </a>
            @endif
            @if($domainObj->custom('payments'))
                @if(count($illness->payments()->get())>0)
                    <hr>
                    <h4 class="text-center">Платежи</h4>
                    @if($illness->payments->where('aviso','=',1)->where('canceled','=',0)->where('test_deposition','=',0)->first())
                        <strong>Сумма будет удержана еще, дней:
                            <br> {{7 - \Carbon\Carbon::parse($illness->payments->where('aviso','=',1)->where('canceled','=',0)->where('test_deposition','=',0)->sortBy('updated_at')->first()->updated_at)->diffInDays(\Carbon\Carbon::now())}}
                        </strong>
                    @endif
                    <table class="table payments-table">
                        <tr>
                            <td>Дата</td>
                            <td>Номер транзакции</td>
                            <td class="hidden-xs">Плательщик</td>
                            <td class="hidden-xs">Получатель</td>
                            <td>Сумма, руб.</td>
                            <td>Статус</td>
                        </tr>
                        @foreach($illness->payments()->get() as $payment)
                            <tr>
                                <td>{{\Carbon\Carbon::parse($payment->created_at)->format('H:i d-m-Y')}}</td>
                                <td>{{$payment->invoice_id}}</td>
                                <td class="hidden-xs">{{$payment->user()->find($payment->user_id)->name}}</td>
                                <td class="hidden-xs">{{\App\User::find($payment->doctor_id)->name}}</td>
                                <td>{{$payment->sum}}</td>
                                <td>
                                    @if($payment->aviso)
                                        <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                    @else
                                        <a href="/yandex/payment/{{$payment->id}}" class="btn btn-primary">Оплатить</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            @endif
        @endif
        <hr>
        <h4>
            Список участников беседы:
        </h4>
        <ul>
            @foreach($chat->users as $mUser)
                <li>{{$mUser->name}}</li>
            @endforeach
        </ul>
        <hr>

    </div>
</div>