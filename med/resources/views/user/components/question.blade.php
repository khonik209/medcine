@if($question->type==="text")
    <div class="form-group">
        <label for="q_{{$question->id}}">{{$question->label}}</label>
        <input type="text" name="questions[{{$question->id}}]" class="form-control" id="q_{{$question->id}}" @if($question->required) required @endif>
        <p class="text-muted">{{$question->hint}}</p>

    </div>
@elseif($question->type==="textarea")
    <div class="form-group">
        <label for="q_{{$question->id}}">{{$question->label}}</label>
        <textarea id="q_{{$question->id}}" name="questions[{{$question->id}}]" class="form-control" @if($question->required) required @endif></textarea>
        <p class="text-muted">{{$question->hint}}</p>

    </div>
@elseif($question->type==="radio")
    @if(isset($question->options) && count($question->options)>0)
        <div class="form-group">
            <p><strong>{{$question->label}}</strong></p>
            @foreach($question->options as $o=>$option)
                <label>
                    <input type="radio" name="questions[{{$question->id}}]" value="{{$o}}" @if($question->required) required @endif>
                    {{$option}}
                </label><br>
            @endforeach
            <p class="text-muted">{{$question->hint}}</p>
        </div>
    @endif
@elseif($question->type==="select")
    @if(isset($question->options) && count($question->options)>0)
        <div class="form-group">
            <label for="q_{{$question->id}}">{{$question->label}}</label>
            <select class="form-control" name="questions[{{$question->id}}]" id="q_{{$question->id}}" @if($question->required) required @endif>
                @foreach($question->options as $i=>$option)
                    <option value="{{$i}}">{{$option}}</option>
                @endforeach
            </select>
            <p class="text-muted">{{$question->hint}}</p>
        </div>
    @endif
@elseif($question->type==="checkbox")
    @if(isset($question->options) && count($question->options)>0)
        <div class="form-group">
            <p><strong>{{$question->label}}</strong></p>
            @foreach($question->options as $o=>$option)
                <label>
                    <input type="checkbox" name="questions[{{$question->id}}]" value="{{$o}}" @if($question->required) required @endif>
                    {{$option}}
                </label>
                <br>
            @endforeach
            <p class="text-muted">{{$question->hint}}</p>
        </div>
    @endif
@elseif($question->type==="file")
    <div class="form-group">
        <label for="q_{{$question->id}}">{{$question->label}}</label>
        <input type="file" name="questions[{{$question->id}}]" @if($question->required) required @endif>
        <p class="text-muted">{{$question->hint}}</p>
    </div>
@endif