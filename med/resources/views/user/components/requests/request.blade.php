<div class="col-md-12">
    <div class="panel panel-default" id="illness_{{$illness->id}}">
        <div class="panel-heading">
            {{$illness->name}}
        </div>
        <div class="panel-body">
              <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
            <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

            <p>Ответственный:
                <strong>
                    <a href="{{$illness->doctor() && $illness->doctor()->role=="doc"?'/user/doctor/'.$illness->doctor()->id:'#'}}">
                        {{$illness->doctor()->name or 'Администратор'}}
                    </a>
                </strong>
            </p>
            <p>
                <strong>
                    {{$illness->name}}
                </strong>
            </p>
            <p>
                {{$illness->description}}
            </p>
            @if($domainObj->custom('custom_form'))
                <hr>
                <h4><strong>Анкета</strong></h4>
                @if($illness->answers && count($illness->answers)>0)
                    @each('user.components.home.answers',$illness->answers,'answer')
                @else
                    @php
                        $questions = $domainObj->questions()->whereNotNull('domain_id')->get();
                    @endphp
                    <form action="{{url('/illnesses/edit')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" value="{{Auth::id()}}">
                        <input type="hidden" name="illness_id" value="{{$illness->id}}">
                        @if($questions && count($questions)>0)
                            @each('user.components.question',$questions,'question')
                        @endif
                        <button type="submit" class="btn btn-primary">Сохранить
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </button>
                    </form>
                @endif
            @endif
            @if($illness->record)
                <hr>
                <h4>Запись</h4>
                <p>Запись к <strong>{{$illness->record->doctor()->name or 'Администратор'}}</strong></p>
                <p>Время записи:
                    <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
                </p>
            @endif
            @if($domainObj->custom('chat'))
                @if($illness->chat)
                    <hr>
                    <h4>Онлайн кабинет для встречи</h4>
                    <a href="{{url('/user/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти</a>
                @endif
            @endif
            @if($domainObj->custom('disease_history'))
                @if($illness->diseaseHistory)
                    <hr>
                    <h4>История болезни</h4>
                    <a href="{{url('/user/disease_history/'.$illness->diseaseHistory->id)}}"
                       class="btn btn-primary">Открыть</a>
                @endif
            @endif
            @if($domainObj->custom('payments'))
                @if($illness->payments->count()>0)
                    <hr>
                    <h4>Платежи</h4>
                    @foreach($illness->payments as $payment)
                        <p>
                            <strong> {{$payment->sum}}</strong> руб.

                            @if(!$payment->aviso)
                                <a href="{{url('/yandex/payment/'.$payment->id)}}"
                                   class="btn btn-primary btn-xs">Оплатить</a>
                            @else
                                <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                @if($domainObj->payment_form=="safe")
                                    @if(!$payment->canceled && !$payment->test_deposition)
                                        <strong>Сумма будет удержана еще, дней:
                                            <br> {{7 - \Carbon\Carbon::parse($payment->updated_at)->diffInDays(\Carbon\Carbon::now())}}
                                        </strong>
                                    @endif
                                @endif
                            @endif
                        </p>
                    @endforeach
                @endif
            @endif
            @if(!$illness->report)
                <hr>
                <h4>Оценить работу врача</h4>
                <form class="form-horizontal rating-form" role="form" action="{{url('/user/request/report')}}"
                      method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="illness_id" value="{{$illness->id}}">
                    <div class="col-xs-6">
                        <label class="radio-inline">
                            <input type="radio" name="rating" value="1" class="rating_plus">
                            <span class="glyphicon glyphicon-plus rating-plus"></span>
                        </label>

                    </div>
                    <div class="col-xs-6">
                        <label class="radio-inline">
                            <input type="radio" name="rating" value="-1" class="rating_minus">
                            <span class="glyphicon glyphicon-minus rating-minus"></span>
                        </label>

                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="text" class="control-label">Текст отзыва
                                <small>(не обязательно)</small>
                            </label>
                            <textarea class="form-control" rows="3" name="text" id="text"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <span class="label label-success hidden js-label-plus">Всё понравилось, спасибо!</span>
                            <span class="label label-danger hidden js-label-minus">К сожалению, врач не оправдал ожиданий</span>
                        </div>
                    </div>
                    <div class="col-xs-12">

                        <div class="form-group">
                            <button class="btn btn-info" type="submit">Поставить оценку</button>
                        </div>
                    </div>
                </form>
            @else
                <div class="alert alert-info">Вы уже оставили отзыв об этом обращении</div>
            @endif
        </div>
    </div>
</div>