<div class="well well-sm">
    <h4><a href="{{url('/user/requests#illness_'.$illness->id)}}">{{$illness->name}}</a></h4>

    <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
    <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

    <p>Ответственный:
        <strong>
            {{$illness->doctor()->name or 'Администратор'}}
        </strong>
    </p>
    <p>
        <strong>
            {{$illness->name}}
        </strong>
    </p>
    <p>
        {{$illness->description}}
    </p>
    @if($illness->record)
        <hr>
        <h4>Запись</h4>
        <p>Запись к <strong>{{$illness->record->doctor()->name or 'Администратор'}}</strong></p>
        <p>Время записи:
            <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
        </p>
    @endif
    @if($domainObj->custom('chat'))
        @if($illness->chat)
            <hr>
            <h4>Онлайн кабинет для встречи</h4>
            <a href="{{url('/user/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти</a>
        @endif
    @endif
    @if($domainObj->custom('disease_history'))
        @if($illness->diseaseHistory)
            <hr>
            <h4>История болезни</h4>
            <a href="{{url('/user/disease_history/'.$illness->diseaseHistory->id)}}"
               class="btn btn-primary">Открыть</a>
        @endif
    @endif
    @if($domainObj->custom('payments'))
        @if($illness->payments->count()>0)
            <hr>
            <h4>Платежи</h4>
            @foreach($illness->payments as $payment)
                <p>
                    <strong> {{$payment->sum}}</strong> руб.

                    @if(!$payment->aviso)
                        <a href="{{url('/yandex/payment/'.$payment->id)}}"
                           class="btn btn-primary btn-xs">Оплатить</a>
                    @else
                        <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                    @endif
                </p>

            @endforeach
        @endif
    @endif
</div>