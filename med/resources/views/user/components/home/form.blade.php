<div class="col-sm-12 col-md-6">
    <div class="panel panel-default">

        <div class="panel-heading">
            Создать обращение
        </div>
        <div class="panel-body" id="user_form_container">
            <form action="{{url('/illnesses/new')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                @if($domainObj->id==1)
                    <input type="hidden" value="online" name="place">
                @elseif($domainObj->custom('chat'))
                    <div class="form-group">
                        <label for="place">
                            Формат обращения
                        </label>
                        <select class="form-control" name="place" id="place" required>
                            <option value="offline">Очно</option>
                            <option value="online">Онлайн</option>
                        </select>
                    </div>
                @else
                    <input type="hidden" value="offline" name="place">
                @endif
                @if(!$domainObj->custom('hide_doctors'))
                    @if($doctors && count($doctors)>0)
                        <div class="form-group">
                            <label for="doctor">
                                Выберите врача
                            </label>
                            <select id="doctor" class="form-control" name="target_id" required>
                                @foreach($doctors as $doctor)
                                    <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                @endforeach
                            </select>
                            <p><strong>Посмотрите всех врачей <a
                                            href="{{url('/user/doctors')}}">здесь.</a> Там
                                    же вы можете обратиться к нужному вам врачу лично</strong></p>
                        </div>
                    @endif
                @else
                    <input type="hidden" value="target_id" name="{{$doctors->first()->id}}">
                @endif
                <div class="form-group">
                    <label for="name">Суть вашей проблемы</label>
                    <input type="text" name="name" class="form-control" id="name" required>
                    <p class="text-muted">Название обращения</p>
                </div>
                <div class="form-group">
                    <label for="description">Подробное описание жалобы</label>
                    <textarea id=description name=description class="form-control" required></textarea>
                    <p class="text-muted">Опишите подбродней всё, связанное с вашей проблемой</p>
                </div>
                @if($questions && count($questions)>0)
                    @each('user.components.question',$questions,'question')
                @endif
                <button type="submit" class="btn btn-primary">Спросить
                    <span class="glyphicon glyphicon-arrow-right"></span>
                </button>
            </form>
        </div>
    </div>
</div>