@extends('layouts.user')

@section('content')
    <div class="doctors">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    Поиск врача
                </h4>
                <hr>
                <p><strong>Фильтр</strong></p>
                <form action="{{url('/user/doctors')}}" method="get" class="form-inline" role="form"
                      id="js-search-form">
                    <div class="input-group">
                        <input class="form-control" name="filter" placeholder="Введите имя"
                               value="{{$request->filter}}"/>
                        <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button>
                            </span>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="skills"> или выберите специальность </label>
                        <select id="skills" class="form-control" name="skill"
                                onchange="$('#js-search-form').submit()">
                            <option value="0" @if(!$request->skill) selected @endif >Выберите специальности из
                                списка
                            </option>
                            @foreach($skills as $skill)
                                <option value="{{$skill->id}}"
                                        @if($request->skill == $skill->id) selected @endif
                                        class="js-skill">{{$skill->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @if($request->skill || $request->filter)
                        <a href="{{url('/user/doctors')}}" class="btn btn-default btn-xs">
                            Показать всех врачей
                        </a>
                    @endif
                </form>
            </div>
            <div class="panel-body">

                @include('common.error')
                @include('common.customError')
                @include('common.info')
                @include('common.success')
                <div class="table-responsive ">
                    <table class="table table-bordered table-striped table-hover">
                        <thead class="hidden-xs">
                        <tr>
                            <th>ФИО</th>
                            <th>Специальность</th>
                            @if($domainObj->id==1)
                                <th>Статус</th>
                            @endif
                            <th>Стоимость консультации, руб.</th>
                            @if($domainObj->custom('records'))
                                <th>График консультаций</th>
                            @endif
                            @if($domainObj->custom('chat'))
                                <th>Консультации онлайн</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activeDoctors as $activeDoctor)
                            <tr class="hidden-xs">
                                <td>
                                    <a href="{{url('/user/doctor/'.$activeDoctor->id)}}"
                                       id="js-btn-doc-{{$activeDoctor->id}}"> {{$activeDoctor->name}}</a>
                                </td>
                                <td>
                                    <a href="{{url('/user/doctor/'.$activeDoctor->id)}}"
                                       id="js-btn-doc-{{$activeDoctor->id}}">
                                        @if($activeDoctor->skills()->first())
                                            {{$activeDoctor->skills()->first()->name}}
                                        @endif
                                        @if($activeDoctor->skills()->count()>1)
                                            <small>и еще {{$activeDoctor->skills()->count() - 1 }}</small>
                                        @endif
                                    </a>
                                </td>
                                @if($domainObj->id==1)
                                    <td>
                                        <a href="{{url('/user/doctor/'.$activeDoctor->id)}}"
                                           id="js-btn-doc-{{$activeDoctor->id}}">
                                            @if($activeDoctor->doctor->status=="online")
                                                <strong class="status-online">Свободен</strong>
                                            @elseif($activeDoctor->doctor->status=="offline")
                                                <strong class="status-offline">Занят</strong>
                                            @else
                                                <strong class="status-holiday">В отпуске</strong>
                                            @endif
                                        </a>
                                    </td>
                                @endif
                                <td><a href="{{url(''.$activeDoctor->id)}}"
                                       id="js-btn-doc-{{$activeDoctor->id}}">
                                        {{$activeDoctor->doctor->base_payment or 'Устанавливается индивидуально'}}
                                    </a>
                                </td>
                                @if($domainObj->custom('records'))
                                    <td>
                                        @if($activeDoctor->doctor->status == 'online')
                                            <a style="color:white" class="btn btn-primary btn-xs"
                                               href="{{url('/user/doctor/'.$activeDoctor->id)}}">Записаться к
                                                врачу</a>
                                        @endif
                                    </td>
                                @endif
                                @if($domainObj->custom('chat'))
                                    <td>
                                        @if($activeDoctor->doctor->status == 'online')
                                            <button class="btn btn-primary btn-xs" data-toggle="modal"
                                                    data-target="#doctor{{$activeDoctor->id}}_request"
                                                    data-dismiss="modal">
                                                Обратиться
                                            </button>
                                        @endif
                                    </td>
                                @endif
                            </tr>
                            <tr class="visible-xs">
                                <td>
                                    <a class="btn btn-doc" href="{{url('/user/doctor/'.$activeDoctor->id)}}">
                                        {{$activeDoctor->name}}
                                    </a>
                                </td>
                            </tr>
                            <!-- Modal Обращение -->
                            <div class="modal fade" id="doctor{{$activeDoctor->id}}_request" tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">{{$activeDoctor->name}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if($activeDoctor->doctor->base_payment)
                                                <h4>Стоимость: {{$activeDoctor->doctor->base_payment}} руб.</h4>
                                                <hr>
                                            @endif
                                            <form class="form-horizontal" action="{{url('/illnesses/new')}}"
                                                  method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="target_id" value="{{$activeDoctor->id}}">
                                                <input type="hidden" name="user_id" value="{{Auth::id()}}">

                                                <div class="form-group">
                                                    <label for="request_name" class=" control-label">Тема
                                                        обращения</label>
                                                    <input type="text" class="form-control" name="request_name"
                                                           placeholder="Что вас беспокоит?">
                                                </div>

                                                <div class="form-group">
                                                    <label for="request_description" class=" control-label">Описание
                                                        проблемы</label>
                                                    <textarea class="form-control" rows="3"
                                                              name="request_description"
                                                              placeholder="Опишите проблему подробнее"></textarea>
                                                </div>
                                                @if($domainObj->id==1)
                                                    <input type="hidden" value="online" name="place">
                                                @elseif($domainObj->custom('chat'))
                                                    <div class="form-group">
                                                        <label for="place">
                                                            Формат обращения
                                                        </label>
                                                        <select class="form-control" name="place" id="place">
                                                            <option value="offline">Очно</option>
                                                            <option value="online">Онлайн</option>
                                                        </select>
                                                    </div>
                                                @else
                                                    <input type="hidden" value="offline" name="place">
                                                @endif
                                                <div class="modal-footer">
                                                    <a href="{{url('/user/doctor/'.$activeDoctor->id)}}"
                                                       class="btn btn-info pull-left"
                                                       id="js-btn-doc-{{$activeDoctor->id}}">
                                                        Открыть профиль врача
                                                    </a>
                                                    <button type="button" class="btn btn-info hidden-xs"
                                                            data-dismiss="modal">
                                                        Закрыть
                                                    </button>
                                                    @if($activeDoctor->doctor->base_payment)
                                                        <button type="submit" class="btn btn-primary">
                                                            Перейти на оплату
                                                        </button>
                                                    @else
                                                        <button type="submit" class="btn btn-primary">
                                                            Отправить заявку доктору
                                                        </button>
                                                    @endif

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
