@extends('layouts.user')

@section('content')
    <div class="admin-doctor">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Обращения
                    </div>
                    <div class="panel-body">
                        @include('common.success')
                        @include('common.customError')
                        @include('common.error')
                        @include('common.info')
                    </div>
                </div>
            </div>
            @each('user.components.requests.request',$illnesses,'illness','user.components.requests.empty')
        </div>
    </div>
@endsection