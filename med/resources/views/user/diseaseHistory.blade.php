@extends('layouts.user')

@section('content')
    <div class="admin-doctor">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2>История болезни № {{$diseaseHistory->id}}</h2>
                    <h4>Лечащий врач: <a
                                href="{{url('/user/doctor/'.$diseaseHistory->doctor->id)}}">{{$diseaseHistory->doctor->name}}</a>
                    </h4>
                    @if($diseaseHistory->doctor->id == Auth::id())
                        <a class="btn btn-primary btn-xs"
                           href="{{url('/doc/disease_history/'.$diseaseHistory->id.'/edit')}}">Редактировать</a>
                    @endif
                </div>
                @include('common.error')
                @include('common.customError')
                @include('common.info')
                @include('common.success')

                <p>
                    <strong>Пациент:</strong>

                    {{$diseaseHistory->user->name}}
                </p>
                <p><strong>Возраст:</strong> {{$diseaseHistory->user->age}}</p>
                <p><strong>Дата поступления:</strong> {{\Carbon\Carbon::parse($text->date)->format('d.m.Y H:i')}}</p>
                <p><strong>Жалобы:</strong> {{$text->complaint}}</p>
                <p><strong>Результат первичного осмотра:</strong> {{$text->primary_inspection}}</p>
                <p><strong>Первичный диагноз:</strong> {{$text->primary_diagnosis}}</p>
                <p><strong>Окончательный диагноз:</strong> {{$text->final_diagnosis}}</p>
                <p><strong>Назначенное лечение:</strong> {{$text->treatment}}</p>
                <p><strong>План лечения:</strong> {{$text->treatment_plan}}</p>
                @if(count($text->additional_views)>0)
                    <p><strong>Дополнительные обследования</strong></p>
                    @foreach($text->additional_views as $key=>$additional_view)
                        <ul>
                            <strong>Осмотр {{$key+1}}</strong>
                            <li>
                                <strong>Результат осмотра:</strong> {{$additional_view['inspection']}}
                            </li>
                            <li>
                                <strong>Корректировка
                                    лечения:</strong> {{$additional_view['treatment_adjustments']}}
                            </li>
                        </ul>
                    @endforeach
                @endif
                <p><strong>Эпикриз: </strong> {{$text->result}}</p>
            </div>
        </div>
    </div>
@endsection