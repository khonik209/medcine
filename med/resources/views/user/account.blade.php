@extends('layouts.user')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4>Настройки профиля</h4>
                </div>
                @include('common.customError')
                @include('common.info')
                @include('common.success')
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Настройки аккаунта
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" action="/user/edit" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="avatar" class="col-sm-2 control-label"> Аватар</label>
                            <div class="col-sm-10">
                                <a role="button" data-toggle="modal" data-target="#addAvatar">
                                    @if($user->avatar)
                                        <img src="{{$user->avatar_url}}" alt="..." style="max-width: 100px"
                                             class="img-thumbnail img-responsive">
                                    @else
                                        <img src="/storage/doctors/emptyavatar.png" alt="..."
                                             style="max-width: 100px"
                                             class="img-thumbnail img-responsive">
                                    @endif
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">ФИО</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" class="form-control" id="name"
                                       value="{{$user->name}}"
                                       placeholder="Иванов Иван Иванович">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email"
                                       placeholder="ivanov@gmail.ru" value="{{$user->email}}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="{{url('/user/resetpassword/')}}" class="btn btn-primary btn-xs">
                                    Сменить пароль
                                </a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="age" class="col-sm-2 control-label">Возраст</label>
                            <div class="col-sm-10">
                                <input name="age" type="number" class="form-control" id="age"
                                       value="{{$user->age}}">
                            </div>
                        </div>
                        <div class="col-sm-offset-2">
                            <label for="notification" class=" control-label">Получать уведомления по email</label>
                            <input class="" id="notification" type="checkbox" @if($user->notification) checked
                                   @endif name="notification" value="1">
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Список документов
                </div>
                <div class="panel-body">
                    @if(count($urls)>0)
                        <table class="table">
                            @foreach($urls as $url)
                                <tr>
                                    @if($url->type == 'image')
                                        <td>
                                            <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                                <span class="glyphicon glyphicon-picture"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                                Открыть
                                            </a>
                                        </td>
                                        <td>
                                            <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                                <p>{{$url->name}}</p>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/user/account/document/{{$url->id}}/delete"
                                               class="btn btn-danger btn-xs btn-delete"
                                               onclick="return confirm('Вы действительно хотите удалить {{ $url->name }}?');">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                        <div id="file{{$url->id}}" class="modal fade bs-example-modal-lg"
                                             tabindex="-1"
                                             role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <img src="/storage/{{$url->url}}">
                                                </div>
                                            </div>
                                        </div>

                                    @elseif($url->type == 'doc')
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <span class="glyphicon glyphicon-file"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                Скачать .doc
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <p>{{$url->name}}</p>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/user/account/document/{{$url->id}}/delete"
                                               class="btn btn-danger btn-xs btn-delete"
                                               onclick="return confirm('Вы действительно хотите удалить {{ $url->name }}?');">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    @elseif($url->type == 'pdf')
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <span class="glyphicon glyphicon-book"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                Скачать PDF
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <p>{{$url->name}}</p>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/user/account/document/{{$url->id}}/delete"
                                               class="btn btn-danger btn-xs btn-delete"
                                               onclick="return confirm('Вы действительно хотите удалить {{ $url->name }}?');">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    @elseif($url->type=='dicom')
                                        <td>
                                            <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                                Открыть просмотрщик
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                                <p>{{$url->name}}</p>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/user/account/document/{{$url->id}}/delete"
                                               class="btn btn-danger btn-xs btn-delete"
                                               onclick="return confirm('Вы действительно хотите удалить {{ $url->name }}?');">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    @else
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <span class="glyphicon glyphicon-cloud-download"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                Скачать
                                            </a>
                                        </td>
                                        <td>
                                            <a target="_blank" href="/storage/{{$url->url}}">
                                                <p>{{$url->name}}</p>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/user/account/document/{{$url->id}}/delete"
                                               class="btn btn-danger btn-xs btn-delete"
                                               onclick="return confirm('Вы действительно хотите удалить {{ $url->name }}?');">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <div class="alert alert-warning">
                            <strong>Вы пока не прикрепляли документы.</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">История платежей</div>
                <div class="panel-body">
                    @if(count($user->payments)>0)
                        <div class="table-responsive ">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Номер обращения</th>
                                    <th>Врач</th>
                                    <th>Проблема</th>
                                    <th>Сумма, руб.</th>
                                    <th>Статус</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->payments->where('aviso','=',1) as $payment)
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($payment->created_at)->format('d.m.Y')}}</td>
                                        <td>{{$payment->illness->id}}</td>
                                        <td>{{$payment->illness->doctor()->name or 'Администратор'}}</td>
                                        <td>{{$payment->illness->name}}</td>
                                        <td>{{$payment->sum}}</td>
                                        <td>
                                            <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    @else
                        <div class="alert alert-warning">Платежей не найдено</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Добавить аватар -->
    <div class="modal fade" id="addAvatar" tabindex="-1" role="dialog" aria-labelledby="addAva"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="addDoc">Загрузить аватар</h4>
                </div>
                <form enctype="multipart/form-data" method="post" action="{{url('/user/avatar')}}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">

                            <div class="form-group">
                                <label for="form-photo_front" class="col-sm-5 control-label">Обзор<br>
                                    <small>(размер файла: не более 3 Мб)</small>
                                </label>

                                <input type="file" class="" id="form-photo_front" name="avatar"
                                       placeholder="Загрузить" required>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection