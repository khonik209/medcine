@extends('layouts.user')

@section('content')
    <div class="user-home" id="user-dashdoard">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>Рабочий стол</h4>
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>

            @if($domainObj->custom('records'))
                @include('user.components.home.calendar')
            @else
                @include('user.components.home.form')
            @endif

            <div class="col-sm-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Обращения
                    </div>
                    <div class="panel-body">
                        @each('user.components.home.request',$illnesses,'illness','user.components.home.empty')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
