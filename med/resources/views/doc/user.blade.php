@extends('layouts.doc')

@section('content')
    <div class="admin-doctor">
        <div class="row" id="create_record_container">
            <div class="col-md-6">
                @include('doc.components.user.name')
            </div>
            <div class="col-md-6">
                @include('doc.components.user.form')
            </div>
            <div class="clearfix"></div>
            @each('doc.components.user.illness',$illnesses,'illness')
        </div>
    </div>
    @push('styles')
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet">
        <link href="/css/jquery-ui.theme.min.css" rel="stylesheet">
    @endpush
@endsection