@extends('layouts.doc')

@section('content')
    <img id="dcm-preloader" src="/img/preloader.gif" class=""
         style="position: absolute;    left: 50%;    transform: translate(-50%,0);    top: 50%;"
         alt="Подождите...">
    <div class="row hidden" id="dcm-page">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$file->name}}
                </div>
            </div>
            @include('common.error')
            @include('common.info')
            @include('common.success')
        </div>
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="slick-carousel" id="dcm-slick-carousel">

                        @for($i=0;$i<$fileCount;$i++)
                            <div id="dwv{{$i}}" class="slick-item">
                                <div class="layerContainer dwv-border">
                                    <canvas class="imageLayer">Only for HTML5 compatible browsers</canvas>
                                </div>
                                <div class="toolbar">
                                </div>
                            </div>
                        @endfor
                    </div>
                    <div class="dcm-toolbar">
                        <button class="js-back btn btn-info">Назад</button>
                        <button class="js-forward btn btn-info ">Вперёд</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Инструкция
                </div>
                <div class="panel-body">

                    <div id="content">
                        <div class="row">
                            <div class="col-xs-12">

                            </div>
                            <div class="col-xs-12">
                                <div class="alert alert-info">При включенном <strong>"Зафиксировано"</strong>, Вы
                                    можете
                                    работать над текущим снимком.
                                    При снятии галочки с "Зафиксировано", Вы можете листать снимки колёсиком мыши
                                    или
                                    кнопками
                                    "Вперёд/Назад".
                                </div>
                                <div class="alert alert-info">Режим "Увеличить/Уменьшить" изменяет масштаб колёсиком
                                    мыши и
                                    двигает изображение при нажатой левой кнопке мыши
                                </div>
                                <div class="alert alert-info">Режим "Контраст" изменяет баланс белого. Двигайте мышь
                                    выше/ниже <strong>при зажатой</strong> левой кнопке мыши
                                </div>
                                <div class="alert alert-info">Кнопка "Сброс" вернёт снимок к исходному состоянию
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @push('scripts')
            <script>
                var file = '{{$file->url}}'
                var fileCount = {{$fileCount}}
            </script>
            <script src="/js/dicom.js?27022018"></script>
        @endpush
    </div>
@endsection