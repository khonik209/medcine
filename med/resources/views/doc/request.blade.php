@extends('layouts.doc')

@section('content')
    <div class="request">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$chat->user->name or 'Онлайн чат'}}
                    </div>
                    <div class="panel-body">
                        <div class="btn-group btn-group-justified js-chat-toolbar">
                            <a class="btn btn-primary " href="#text_chat" data-toggle="tab">
                                <img style="max-width: 20px"
                                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAoZSURBVHhe7ZsHzBZVGoVRwYZi18WCrgVDlJjYcEXsWIOIETEaNYoaRdR1WVcT27oqNhSUxKBr7MaGNSpii4qVxIYlClbcJeC6dte1gc9558z83zAD/PB/ZTCe5OTeOe87977zzcxtc79OHcWcOXOWmD17di94FBwHH4Pvwc/hD/D/cAZ8Az4Nr+OcY0k3J13SxSx+4AK2hqPhx1zIouJLzr8e7kh+CRddXShIgj0QvhDhzwX07+EUeC+8Go6Bo5y/Db4EP7d7DujT4Mlkl3Z11QLB9YOTk3ATcPwLfA6O4HAr2Nnu8wX+PaFemfvh91GYwbF+iEF2bT0IpiscC39xjAryv/BishvYbZFBGStT1l/gB1G4wfFEuIbdWgPi2JAgXk9CiqD0iF9IdmW71A2UuTRlD4OfRmWA/CdwO7s0F1S8DfzMsSiY52EvmxsG6lgD3u5qVa96kcNsbg6odwcq/SoJIYK4kqRd73e9QJ0nwB9d/8+wOe0CFW0M486TCifZ1HRQ917wO8eiMcVuNjUGVKAG711VKJA/waaWgRj6wx8cksYN69pUf1D4OFekix9lueUglqEOS3E9QlL/QROF6r2f7UpeJFnKpkqAmG5WbAL5oZbrA8rUCO95F653radNlQGhrUJcMx3jp7CrTR0HZe6uggUKHmO5ciC2YQ5TcY6w3HFQ2L0uVHe/u+XKgRA1WJrhWNVYd7wtoJCVKCztb2+1XFkQ4/mKVSD/BDwELmPzwoNyBifFBfa3XFlwsb0dawY0zU0uJ7u63doPThzrQvQUrGC50iDWTxTz3ED/Ampm2v4pNc5P+eQplioPYn3AMWuydIfytUB7B25h9/kDx3RF505LlQcxX6KASbUecZPzD8M3lRfIfwcP9ynzBk4x6SG9ylLlQax/i6tM0IPj3eBy5NVLnAl/SkxxXZf4tHLYT46VGfouCMR6vMMWelgOYPsj1BJc7Wz2HzYXgf1LO42zVHkQ62mK2ci1/Njetn4D+enO6/pOtUseGD60w3hLlQexXuaYfybpYjmAdjdU23Aw3AhGj2FtJ7u1AfFRO7xlqfIg1gmOeZqlDMhLoq/qQ/luC2M6TfohXNGmBAijbVTDsZLlyoIYdYGxbkiq5fdN4eVwc7vkgH4orO0d/mlTAoQBtgmDLVcWxNvHsepihsN7nJ9olwxoq8J0ip8+BbrRG9olftEVENO1+cqPBYhVQ15diC5Mq9a6w9PhMXbJgF1Pi54SPQH7k6Y/Rr7LR7jFhp9g45acOghCXJ74vnCsz1huNzjnPp/7P7im5TD0lUEgX9kBEbGd4TAV51jLOWDSh9fHYaFNQNszOTvOP8JyAoS0N9BTUNqgtBLEtA6sHdx8ZlMO6NH3kxYGdmj94mRAPj/1R9gSql+V8VWSynykJBYt2cUNEshrHeAom3NAV5ugsUBhWY9TNVSe5DLUk+Q/zyPqe1+AfGVeBWI522Eprg4N2Cji9KSkQP7bJsKyVPBSYgucblPLQDxHwLT11pfj1WwKcHwM1GaMwgccTlkKXRs2PoIbSSPdT2UJ5HcNx1ogrgv/bR+hZT8CcRwN09fyR9jbpgxo422fYCkDmsYB6flDrG2vY4H8geE4NzBsBmu/0v7ZpqaAKjtT56ik9qhfc/sDbM4B8wbYzoGbWMoBu/r/U0hjzkA+W1Ijf0g4lQHjcfaT4zDLDQd1ab9RtgOFvB7v7W0OcKweoXQhlFO6YJvnWAabtuMEyA+0nAc2tbqxG4RU3WLboKFBoI7V4KUw2zFCXhurNrZLgOMhUJhkKQd0fTrTuUdayoCm2WEMpATy/WzKA8Nw+8jpNssNAeVvArXZKuvja7CD3TLgd6IMpFMt5YD+WpxZ0nZh6w2jTRDIr2VTGxD3gbVfY9e2ab7Arzv+Cxw74NcVv51J9d6+ErXUAE2juIFkCxcvoGt8rxhL40JfC+6LX+l+BmwTXc8sS21A1CJCOmvSpoT9bCoFbnrfDoBPQi046Bx1VXoM7yS9TnRegxfNx7M7UAv0mXAXF50DujZoFXoAAV2f9AfCbpZyQFebEuWSrgjjFSNtGwlyrP5yJEz7W11I6UhLwNYLt4tI4zPVooLztaVO9aYTnOGuIgNaDF9JtT+psGkK7Xrbb7eUAa0b/MZ2PTVDlBfIJz0AeQ1+HkzkMHwDB4SxBpi2Qj8PpmtuBWBTi30jnAS1W3QW/A9Ml91ehufCQXA9F61A5S8U+mU0vbe6eO02za/mADRt2lLZhY+6yLo2DYL0zVMrQ8/aVz/48uHEwVUSBfLakKRWVo/NAHgqvAfOsksGNEFbX4+EH1mbCku7J/T1cCl9L9E1Rl/HhwVgXx3O86sV566PvfRDKTa9Itp4tRc+AfJX2BwOU6y3C/jrLmrtPYaWAvnDbZb9ryRqG06BpW0IujZKnotf6T5DbAdBtfaFTRroeqzPwlbaSGLrIzvMhswqh+OXSRXft7Ct9edAgZZ1QQFseozHQwVUGjC6WuY37K/FhviCS6oGbxW7ZUBP++q7LGVA01A8bYv2tpwBLdYESGdaygE9NlySjrQkbaQ0gfzZltuAqMdEj8ixInltY9XXluw9XRDw7QvjiwypdnapC9WFlt3FE6G+6B5tKQP+eh30aul1Kozo0NQjqMe4xlIO6BpTaAdJfx/vAWOXK6me9sZN86kg+2RFXg1O0tC0CMSwHTGkH37UuDd2oYd6NISO3SYC+SdItAFDS9dqTA+1aw7omtLejW/hFUPToq221F9AvtDQoWlz1wMwt4eQ4z9hSy9eY5TSyVTdQX3qeh5SxQL5t2G6BWe63XJAT7fQ/91SBmyDbNP561vOgHa/bY9bkqYf/GvrQn13lS0I1Kv3OAJLwbG62MLStYB+MpwAC1NatG6crpHkGNLCE4CutkqjzH0sSdM6QDqinWy5uaBudYUXwtpt9upCCy17I0A9sQQukO9rufmgct2h2hUmBfQK/Bc82G4ZMOvpeQS+Bgs71tDUz78PR1vKAV1Da43+zoBpd/ouLJ0rNAXEoD9F6GmIjc8pOP4K6q80Q2FPpM6wRxgB2r4uIoCk9iXd0zTDcg7o6bD7YqiJWID8s7B1P4JAAN3hFcQTrfPcwKZ1P/X/SjUh0/L2Q1D/VdDnr7ijAlnZCgs1aDtCXfyauOlLUvY3H/Lqmuv+Z4+FBoFoO8tg0sJ/hhYGnKudbWogNYjL7RNIgW1tOC1OAOT1OjT8jx/tBjF1gZppHg+vhQ9C3XHt9NK7/CbUbFGvimajerenxtXUAE1dqXaFaG1gOZet9iQunrT2P0kapfaJABZHcAEacO0J74PZhqgUaJr+Tobx0VcgfyXMpvLkNcep34brVoGL0GKqpuTaJpcu5bUL+JeuMi224JrU2xwGtSiT+xteLbCpgb0DxqvymwXX+gcucieobvY0OAJq3bOyO+J/x+9oKjp1+hVi7o3RFNuocgAAAABJRU5ErkJggg==">
                                &nbsp;
                                Чат
                            </a>
                            @if($domainObj->custom('videochat'))
                                @if(!isset($illness) or $illness->status != "archive")
                                    <a class="btn btn-primary" href="#video_chat" data-toggle="tab">
                                        <img style="max-width: 20px"
                                             src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKRSURBVHhe7Zq/axRhEIYveoWIIMohESGVKGilphQVWwP26dJYpBCsBc0fYGEVbARtFItoKXYKgl3AwkosFE1pCgt/5s7nXV7CRfN5cbO3+e52HnjZmflm7pudLHe5220FQRAEQRAEQRD00+v1dnW73WMcz+y06OM0x45bGz5seBG9Y9NsoJ819AQdcpvDgQ2m0U/0Ed1g7ys7LfqY53iPo4bwEnu3260eNniBPqPDDmUDPV3j5HU1zDpUPbz4N/TIblZw7gc8gNsODYT0Dvl30JpqHU7jDRbtZsdW+yOtTd48WnVNcfRyGieP9ABIuUDOa+e+sb8o3ylpXDSSA2BpirWHzllFVzHbWlON4kXiv3DxSA2A0B5i19EXpE+Ku8Q2/N+gGtXaTaMkJdvNjj/7w76M3jr+Ck17aQOqUY7dNH6h7AeAjqOn9lfQHOaE0/5CNcq1m8YvmPsA3qPv6Ae6RWi/l5PonFRrN403yHoAgh6foRMOD2SsBkB/D+xumXEbwH/3FwOIAcQAYgCqtZum7AZ1Uba/GEAMIAYQA1Ct3TRlN6iLsv3FAMZsAM3+MiTosdFfh5v9g4j6Q43+SWy9P+xm/ihqt4BQc38W74el6m+M4J5Fk7KJn0TFO69iWrO9l/gl2UK2YrKVo1zHt1UvsAf+gUip7tYY9gd00/ZjtCSbtAWtOT6DusR0UpOyYcZrldUL1gcOQJBW7uYoiV/R+u1xajqouIw47pNst7UmW2D3/5X67Srra7k9/pxkPSBxxKFsoKdaHpA4h36hT+y1gDZ9bKVO0Ysu5ftI7/DLxIorYmiwyXlUfLbmgk9+CbOep8XYaIINj3Lc9NG1OkUfp9BBtxYEQRAEQRAEQbBNWq3f5vBCEskkbywAAAAASUVORK5CYII=">
                                        &nbsp;
                                        Видео
                                    </a>
                                @endif
                            @endif
                        </div>
                        @include('common.success')
                        @include('common.customError')
                        @include('common.error')
                        @include('common.info')
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="tab-content">
                    {{-- ТЕКСТОВЫЙ ЧАТ --}}
                    <div class="tab-pane fade in active " id="text_chat">
                        @include('doc.components.request.textChat')
                    </div>

                    @if($domainObj->custom('videochat'))
                        {{-- ВИДЕОЧАТ --}}
                        @if(!isset($illness) or $illness->status != "archive")
                            <div class="tab-pane fade" id="video_chat">
                                @include('doc.components.request.videoChat')
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                @include('doc.components.request.settings')
            </div>
        </div>
    </div>
    @push('opentok')
        <script src="/js/opentok.js"></script>
        <script src="https://static.opentok.com/v2/js/opentok.js" charset="utf-8"></script>
    @endpush
@endsection