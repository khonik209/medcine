@extends('layouts.doc')

@section('content')
    <div class="admin-doctor">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Закрытые обращения
                    </div>
                </div>
            </div>
            @each('doc.components.history.request',$illnesses,'illness','doc.components.history.empty')
        </div>
    </div>
@endsection