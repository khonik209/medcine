@extends('layouts.doc')

@section('content')
    <div class="chats">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Список чатов
                    </div>
                    <div class="panel-body">
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body" id="chat-new">
                        <chat-new></chat-new>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body" id="chat-list">
                        <chats :user="{{Auth::user()}}"></chats>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection