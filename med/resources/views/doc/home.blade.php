@extends('layouts.doc')

@section('content')
    <div class="user-home" id="user-dashdoard">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Рабочий стол</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-sm-6 statuses">
                            <a href="#" onclick="toggleStatuses()"> Текущий статус:
                                @if($doctor->doctor->status=='online')
                                    <strong class="status-online"> Online </strong>
                                @elseif($doctor->doctor->status=="offline")
                                    <strong class="status-offline"> Занят </strong>
                                @else
                                    <strong class="status-holiday"> Offline </strong>
                                @endif
                            </a>
                            @if($doctor->doctor->status =="holiday")
                                <div class="alert alert-danger"><strong>Ваш статус - "Offline".</strong> Вы
                                    <strong>не</strong>
                                    можете принимать обращения от пользователей. Чтобы разрешить пациентам писать
                                    Вам - <strong>смените
                                        статус на "Online"</strong></div>
                            @endif

                            <form class=" js-statuses " role="form" action="/doc/account/status" method="post"
                                  style="display: none">
                                {{ csrf_field() }}

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value="online"
                                               @if($doctor->doctor->status =="online") checked @endif> Online
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="status" value="holiday"
                                               @if($doctor->doctor->status =="holiday") checked @endif> Offline
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label for="holiday" class="control-label">Освобожусь после:</label>
                                    <input id="holiday" class="form-control datepicker" type="text"
                                           name="holiday_until_d">
                                </div>
                                <button type="submit" class="btn btn-success btn-sm">Сменить статус</button>

                            </form>
                        </div>
                        @if($domainObj->id!=1)
                            <div class="col-sm-6">
                                @if($doctor->doctor->work_time)
                                    @if($doctor->doctor->workTimeStatus()=='holiday')
                                        <span class="label label-success">Сегодня выходной</span>
                                    @elseif($doctor->doctor->workTimeStatus()=='free_time')
                                        <span class="label label-warning">Рабочий день завершен</span>
                                    @elseif($doctor->doctor->workTimeStatus()=='work_time')
                                        <span class="label label-danger">Рабочее время</span>
                                    @endif
                                @else
                                    <p>Рабочее время не настроено</p>
                                    <a href="{{url('/doc/account')}}">Настроить</a>
                                @endif
                            </div>
                        @endif
                    </div>
                    @include('common.customError')
                    @include('common.info')
                    @include('common.success')
                </div>
            </div>
            @if($domainObj->id !=1)
                @include('doc.components.home.calendar')
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Обращения
                        </div>
                        <div class="panel-body">
                            @each('doc.components.home.request',$illnesses,'illness','doc.components.home.empty')
                        </div>
                    </div>
                </div>
            @else
                @foreach($illnesses as $illness)
                    <div class="col-sm-12 col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <p>
                                    <strong>
                                        {{$illness->name}}
                                    </strong>
                                </p>
                            </div>
                            <div class="panel-body">
                                <div class="well well-sm">
                                    <h4><a href="{{url('/doc/requests#illness_'.$illness->id)}}">{{$illness->name}}</a>
                                    </h4>

                                    <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
                                    <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

                                    <p>Пациент:
                                        <strong>
                                            {{$illness->user->name or 'Не найден (удален)'}}
                                        </strong>
                                    </p>
                                    @if($domainObj->custom('records'))
                                        @if($illness->record)
                                            <hr>
                                            <p>Время записи:
                                                <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
                                            </p>
                                        @endif
                                    @endif
                                    @if($domainObj->custom('chat'))
                                        @if($illness->chat)
                                            <hr>
                                            <h4>Онлайн кабинет для встречи</h4>
                                            <a href="{{url('/doc/request/'.$illness->chat->id)}}"
                                               class="btn btn-primary">Зайти &nbsp;
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </a>
                                        @endif
                                    @endif
                                    @if($domainObj->custom('disease_history'))
                                        <hr>
                                        @if($illness->diseaseHistory)
                                            <h4>История болезни</h4>
                                            <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}"
                                               class="btn btn-primary">Открыть</a>
                                        @else
                                            <form action="{{url('/doc/disease_history/new')}}" method="post">
                                                {{ csrf_field() }}
                                                <input class="hidden" name="user_id" value="{{$illness->user->id}}">
                                                <input class="hidden" name="illness_id" value="{{$illness->id}}">

                                                <button type="submit" class="btn btn-primary">
                                                    Создать историю болезни &nbsp; <span
                                                            class="glyphicon glyphicon-chevron-right"></span>
                                                </button>

                                            </form>
                                        @endif
                                    @endif
                                    @if($domainObj->custom('payments'))
                                        @if($illness->payments->count()>0)
                                            <hr>
                                            <h4>Платежи</h4>
                                            @foreach($illness->payments as $payment)
                                                <p>
                                                    <strong> {{$payment->sum}}</strong> руб.
                                                    <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                                </p>
                                            @endforeach
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    @push('scripts')
        <script>
            function toggleStatuses() {
                $('.js-statuses').fadeToggle('fast');
                return false;
            }
        </script>
    @endpush

@endsection
