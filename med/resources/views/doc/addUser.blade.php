@extends('layouts.doc')

@section('content')
    <ol class="breadcrumb">
        <li><a href="/doc">Главная</a></li>
        <li><a href="/doc/requests">Заявки</a></li>
        <li class="active">Новый пользователь</li>
    </ol>
    <div class="panel panel-default">

        <div class="panel-heading">Регистрация нового пациента</div>

        <div class="panel-body">
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')

            <form class="form-horizontal" method="POST" action="/doc/adduser/save">
                {{ csrf_field() }}


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">ФИО</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name"
                               value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email"
                               value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-offset-4">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" class="js-offline-form" name="form" value="offline" checked>
                            <strong>Очная</strong> консультация
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" class="js-online-form" name="form" value="online">
                            <strong>Онлайн</strong> консультация
                        </label>
                    </div>
                </div>
                <div class="js-time-offline-request">
                    <div class="form-group">
                        <label for="date" class="col-md-4 control-label">Дата приема</label>

                        <div class="col-md-6">
                            <input id="date" type="text" class="form-control datepicker" name="date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="time" class="col-md-4 control-label">Время приема</label>

                        <div class="col-md-6">
                            <input id="time" type="text" class="form-control timepicker" name="time">
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-4 col-md-6">
                    <div class="well">
                            <span class="js-text-for-offline">При выборе очной консультации, так же будет создана запись на указанное время, которая отразится в Вашем календаре, а так же на домашней странице пациента. <a
                                        href="{{url('/records/get/'.$doctor->id)}}" target="_blank">Посмотреть мой календарь</a></span>
                        <span class="js-text-for-online hidden">При выборе онлайн консультации, будет создана страница для контакта с пациентом, вы сможете найти её на странице "Все заявки".</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Добавить
                        </button>
                    </div>
                </div>

            </form>
            <hr>

            <div class="alert alert-warning">Если пользователя с таким email нет в системе, ссылка на кабинет,
                логин
                и пароль будут высланы пациенту по указанному email-адресу
            </div>
        </div>
    </div>
@endsection