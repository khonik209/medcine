@extends('layouts.doc')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Смена пароля
                </div>
                <div class="panel-body">

                    @include('common.customError')
                    @include('common.error')
                    @include('common.success')
                    @include('common.info')
                    <form class="form-horizontal" role="form" action="{{url('/doc/resetpassword/reset')}}"
                          method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="old_password" class="col-sm-2 control-label">Старый пароль</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="old_password" name="old_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="new_password" class="col-sm-2 control-label">Новый пароль</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="new_password" name="new_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password" class="col-sm-2 control-label">Подтвердите пароль</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="confirm_password"
                                       name="confirm_password">
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Сменить пароль</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection