<div class="col-md-12">
    <div class="panel panel-default" id="illness_{{$illness->id}}">
        <div class="panel-heading">
            {{$illness->name}}
        </div>
        <div class="panel-body">
              <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
            <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

            <p>Пациент:
                <strong>
                    <a href="{{$illness->user?url('/doc/user/'.$illness->user->id):'#'}}">{{$illness->user->name or 'Не найден'}}</a>
                </strong>
            </p>
            <p>
                <strong>
                    {{$illness->name}}
                </strong>
            </p>
            <p>
                {{$illness->description}}
            </p>
            @if($domainObj->custom('custom_form'))
                <hr>
                <h4><strong>Анкета</strong></h4>
                @if($illness->answers && count($illness->answers)>0)
                    @each('doc.components.home.answers',$illness->answers,'answer')
                @else
                    Не заполнена
                @endif
            @endif
            @if($domainObj->custom('records'))
                @if($illness->record)
                    <hr>
                    <h4>Время записи:
                        <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
                    </h4>
                @endif
            @endif
            @if($domainObj->custom('chat'))
                @if($illness->chat)
                    <hr>
                    <h4>Онлайн кабинет для встречи</h4>
                    <a href="{{url('/doc/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти</a>
                @endif
            @endif
            @if($domainObj->custom('disease_history'))
                <hr>
                @if($illness->diseaseHistory)

                    <h4>История болезни</h4>
                    <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}"
                       class="btn btn-primary">Открыть</a>
                @else
                    <form action="{{url('/doc/disease_history/new')}}" method="post">
                        {{ csrf_field() }}
                        <input class="hidden" name="user_id" value="{{$illness->user->id}}">
                        <input class="hidden" name="illness_id" value="{{$illness->id}}">

                        <button type="submit" class="btn btn-primary">
                            Создать историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>

                    </form>
                @endif
            @endif
            @if($domainObj->custom('payments'))
                @if($illness->payments->count()>0)
                    <hr>
                    <h4>Платежи</h4>
                    @foreach($illness->payments as $payment)
                        <p>
                            <strong> {{$payment->sum}}</strong> руб.
                            <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                            @if($domainObj->payment_form=="safe")
                                @if(!$payment->canceled && !$payment->test_deposition)
                                    <strong>Сумма будет удержана еще, дней:
                                        <br> {{7 - \Carbon\Carbon::parse($payment->updated_at)->diffInDays(\Carbon\Carbon::now())}}
                                    </strong>
                                @endif
                            @endif
                        </p>
                    @endforeach
                @endif
            @endif
        </div>
    </div>
</div>