<div class="panel panel-default">
    <div class="panel-body">
        <div class="media">
            <div class="pull-left" href="#">
                @if($user->avatar)
                    <img class="media-object" src="/storage/{{$user->avatar}}" alt="..."
                         style="max-width: 100px">
                @else
                    <img class="media-object" src="/storage/doctors/emptyavatar.png" alt="..."
                         style="max-width: 100px">
                @endif

            </div>
            <div class="media-body">
                <h4 class="media-heading">{{$user->name}}</h4>

                @if($lastActivity)
                    <span class="label label-default">Последний раз в сети: {{\Carbon\Carbon::parse($lastActivity->created_at)->format('d.m.Y H:i')}}</span>
                @else
                    <span class="label label-default">Последняя активность не зафиксирована</span>
                @endif
            </div>
        </div>
        @include('common.info')
        @include('common.success')
        @include('common.error')
        @include('common.customError')
    </div>
</div>