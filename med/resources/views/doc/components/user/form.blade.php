<div class="panel panel-default">
    <div class="panel-heading">
        Профиль
    </div>
    <div class="panel-body">
        <p>Email: <strong>{{$user->email}}</strong></p>
        <p>Телефон: <strong>{{$user->phone}}</strong></p>
        <p>Возраст, лет: <strong>{{$user->age}}</strong></p>
    </div>
</div>