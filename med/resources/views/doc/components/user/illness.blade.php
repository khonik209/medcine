<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="text-center">
                <span>{{$illness->name?$illness->name:'Обращение #'.$illness->id}}</span>
                <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
                <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>
            </p>
            @if($illness->record)
                <hr>
                <p>
                    <strong>
                        Запись к {{$illness->record->doctor()->name}}
                        на {{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}

                    </strong>
                </p>
            @endif
        </div>
        <div class="panel-body">
            <h4>Описание обращения</h4>
            <p><strong>{!! $illness->name?$illness->name:'<i>Название отсутствует</i>'!!}</strong></p>
            <p>{!! $illness->description?$illness->description:'<i>Описание отсутствует</i>'!!}</p>
            <hr>
            @if($domainObj->custom('custom_form'))
                <h4><strong>Анкета:</strong></h4>
                @if($illness->answers && count($illness->answers)>0)
                    @each('admin.components.user.answer',$illness->answers,'answer')
                @else
                    <p>Не заполнена</p>
                @endif
            @endif
            @if($domainObj->custom('disease_history'))
                <hr>
                @if($illness->diseaseHistory)

                    <h4>История болезни</h4>
                    <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}"
                       class="btn btn-primary">Открыть</a>
                @elseif($illness->doctor()->id == Auth::id())
                    <form action="{{url('/doc/disease_history/new')}}" method="post">
                        {{ csrf_field() }}
                        <input class="hidden" name="illness_id" value="{{$illness->id}}">
                        <input class="hidden" name="user_id" value="{{$illness->user_id}}">
                        <button type="submit" class="btn btn-primary">
                            Создать историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>

                    </form>
                @endif
            @endif
        </div>
    </div>
</div>