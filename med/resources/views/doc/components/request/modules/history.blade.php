@if(isset($illness) && $illness->user)
    @if($illness->diseaseHistory)

        <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}" class="btn btn-primary">
            Открыть историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    @else
        <form action="{{url('/doc/disease_history/new')}}" method="post">
            {{ csrf_field() }}
            <input class="hidden" name="user_id" value="{{$illness->user->id}}">
            <input class="hidden" name="illness_id" value="{{$illness->id}}">
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Создать историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                </div>
            </div>
        </form>
    @endif
    <div class="clearfix"></div>
@endif