<div id="js-teledoctor">
    @if($chat->type=="public")
    <invite-chat :chat_id ="{{$chat->id}}" :members="{{json_encode($chat->users)}}"></invite-chat>

    <hr>

    <p class="text-center">Переименовать чат:</p>
    <form action="{{url('/doc/chat/rename')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <input class="form-control" type="text" name="name" value="{{old('name')}}"
                   placeholder="Введите новое название чата">
        </div>
        <input type="hidden" name="chat_id" value="{{$chat->id}}">
        <button type="submit" class="btn btn-primary btn-xs">Переименовать</button>
    </form>
    <hr>
    @endif
    @if($illness)
        <h4>Блокировка {{$illness->user->name}}</h4>
        <form action="/doc/blacklist/add" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="user" value="{{$illness->user_id}}">
            <button type="submit" class="btn btn-danger btn-xs">Нажмите, чтобы
                заблокировать {{$illness->user->name}}*
            </button>
        </form>
        <small>* этот пользователь больше не сможет обращаться к Вам. Разблокировать пользователя Вы сможете <a
                    href="/doc/account">в настройках профиля</a></small>
    @endif
</div>