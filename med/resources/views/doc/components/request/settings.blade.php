@if(isset($illness) || $chat->type=="public")
    <div class="panel panel-default">
        <div class="panel-body">
            @if(isset($illness))
                <p>Статус {!! $illness->status() !!}</p>

                <hr>

                @if($illness->record)
                    <div class="alert alert-info">
                        <p>
                            Пациент записан на
                            <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}
                                .</strong>
                            Он не сможет зайти раньше.
                        </p>
                    </div>
                    <hr>
                @endif

                @if($illness->status == "new")
                    <p class="text-muted">
                        Вы еще не приняли текущее обращение. Пациент не может зайти на эту страницу</p>
                    <hr>
                @endif

                @if($illness->status != 'archive')
                    <div class="btn-group btn-group-justified">
                        @if($illness->status == 'new')
                            <a href="/doc/request/accept/{{$illness->id}}" class="btn btn-success">
                                Принять
                            </a>
                            <a href="/doc/request/deny/{{$illness->id}}" class="btn btn-danger"
                               onclick="return confirm('Вы уверены?');">
                                Отклонить
                            </a>
                        @else
                            @if($domainObj->custom('payments'))
                                <a class="btn btn-info" data-toggle="modal" data-target="#payment">Выставить счёт</a>
                            @endif
                            <a data-placement="right"
                               title="Создаёт заметку, которая будет отображаться у пациента на главной странице. Следует использовать для предоставления информации, которую не хочется потерять или забыть"
                               class="btn btn-info" data-toggle="modal"
                               data-target="#addNote">
                                Дать рекомендации
                            </a>
                        @endif
                    </div>
                @endif
                @if($domainObj->custom('payments'))
                    {{-- Платеж --}}
                    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="newPayment"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="addDoc">Выставить счёт</h4>
                                </div>
                                <form method="post" action="{{url('/yandex/payment/create')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="illness_id" value="{{$illness->id}}">
                                    <input type="hidden" name="type" value="request">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="payment" class="control-label">Введите сумму
                                            </label>
                                            <input id="payment" type="number" class="form-control" name="sum"
                                                   placeholder="Введите сумму">
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        @if($domainObj->payment_form=="safe")
                                            <div class="alert alert-info alert-dismissable text-left">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">
                                                    &times;
                                                </button>
                                                Введите сумму, которую должен оплатить пациент. <br>
                                                <strong>Внимание!</strong> Комиссия за операцию может достигать 5% + 30
                                                руб.
                                                фиксированно за операцию. <br>
                                                <strong>Внимание!</strong> После <strong>успешного</strong> завершения
                                                обращения,
                                                средства поступают на счёт Исполнителя в течении 48 часов (в зависимости
                                                от
                                                Банка)
                                            </div>
                                        @endif
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                                        <button type="submit" class="btn btn-primary"
                                                onclick="$(this).addClass('disabled', true);">
                                            Выставить
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            <!-- Modal Добавить рекомендации -->
                <div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="addNote"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title" id="addDoc">Новая рекомендация</h4>
                            </div>
                            <form enctype="multipart/form-data" method="post" action="/doc/request/note/add">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" value="{{$patient->id}}">
                                <div class="modal-body">
                                    <div class="form-group">

                                        <input class="form-control" id="note_name" name="note_name"
                                               placeholder="Название заметки" required>

                                    </div>
                                    <div class="form-group">
                                        <label for="note_text" class="col-sm-5 control-label">Текст *<br>

                                        </label>

                                        <textarea class="form-control" id="note_text" name="note_text"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            @endif
            @if($chat->type=="public")
                <div id="js-teledoctor">

                    <invite-chat :chat_id="{{$chat->id}}" :members="{{json_encode($chat->users)}}"></invite-chat>

                    <hr>

                    <p><strong>Переименовать чат:</strong></p>
                    <form action="{{url('/doc/chat/rename')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" value="{{old('name')}}"
                                   placeholder="Введите новое название чата">
                        </div>
                        <input type="hidden" name="chat_id" value="{{$chat->id}}">
                        <button type="submit" class="btn btn-primary btn-xs">Переименовать</button>
                    </form>


                    @if($illness)
                        <hr>
                        <h4>Блокировка {{$illness->user->name}}</h4>
                        <form action="/doc/blacklist/add" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="user" value="{{$illness->user_id}}">
                            <button type="submit" class="btn btn-danger btn-xs">Нажмите, чтобы
                                заблокировать {{$illness->user->name}}*
                            </button>
                        </form>
                        <small>* этот пользователь больше не сможет обращаться к Вам. Разблокировать пользователя Вы
                            сможете
                            <a
                                    href="/doc/account">в настройках профиля</a></small>
                    @endif
                </div>
            @endif
        </div>
    </div>
@endif