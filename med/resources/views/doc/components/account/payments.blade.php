<form action="{{url('/doc/account/setwallet')}}" method="post" role="form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-6">
            <div class="radio">
                <label>
                    <input type="radio" class="js-wallet-type js-wallet-card" name="wallet"
                           value="bank" checked>
                    Банковская карта
                    <img class="img hidden-xs hidden-sm" src="/img/icon_Mastercard.png">
                    <img class="img " src="/img/icon_Visa.png">
                </label>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="radio">
                <label>
                    <input type="radio" class="js-wallet-type js-wallet-yandex" name="wallet"
                           value="yandex">
                    Яндекс.кошелёк
                    <img class="img " src="/img/icon_yandex.png">
                </label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="input-group yandex-account hidden">
                <label>
                    <input type="number" class="form-control js-yandex-account-number"
                           placeholder="Введите номер яндекс. кошелька"
                           name="account_number"> Номер Яндекс.Кошелька
                </label>
            </div>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>

@if( $user->doctor->accountNumber)
    <hr>
    <h4>Базовая стоимость консультации</h4>
    <p class="text-muted">
        Если установить базовую стоимость консультации, то при обращении пацента, счет на выбранную
        сумму будет создан <strong>автоматически</strong>, а пациент будет перенаправлен на оплату. Вы получите
        соответствующее уведомление по email, а заявка будет считаться <strong>принятой</strong> и иметь статус <strong>"в
            работе".</strong>
    </p>
    <form action="{{url('/doc/account/base_payment')}}" method="post" role="form">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="base_payment" class="control-label">
                        <input id="base_payment" type="checkbox" name="base_payment" value="1"
                               @if($form->base_payment) checked @endif>
                        Функция "Базовый платёж" включена</label>
                </div>
            </div>
            <div class="col-sm-6">
                <p>Базовая сумма платежа</p>
                <div class="form-group">
                    <input type="number" name="sum" class="form-control"
                           @if($form->base_payment) value="{{$form->base_payment}}" @endif>
                </div>
            </div>
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </form>
    <hr>
    <h4>Кнопка приёма обращений</h4>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <code class="js-magic-button">
                <small>Нажмите "Сгенерировать", чтобы получить код</small>
            </code>
        </div>
        <div class="col-xs-12 col-sm-6">
            <p>Базовая сумма платежа</p>
            <div class="input-group">
                <input type="hidden" class="js-id" value="{{$user->id}}">
                <input type="number" class="form-control js-sum" value="0">
                <span class="input-group-btn">
                                    <button class="btn btn-primary js-magic-button-generate"
                                            type="button">Сгенерировать</button>
                                </span>
            </div><!-- /input-group -->
            <div class="radio">
                <label>
                    <input type="radio" class="js-type-button" name="magic_type" value="button">
                    Кнопка
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" class="js-type-link" name="magic_type" value="link" checked>
                    Гиперссылка
                </label>
            </div>
        </div>
    </div>
    <div class="alert alert-info">
        Эту ссылку (или кнопку) Вы можете рассылать вашим пациентам.
        При переходе по этой ссылке, паценту будет предложено пройти быструю регистрацию в
        системе teledoctor, а так же будут автоматически созданы обращение к Вам и платёж на
        сумму, которую Вы указываете при генерации ссылки. Далее, пациент автоматически
        перенаправляется на оплату счёта. Такая система позволяет быстро замораживать средства
        на счёте пациента без необходимости множества действий по выставлению счёта и
        предварительном общении.
    </div>
@endif