@if(count($payments)>0)
    <div class="table-responsive ">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>Номер обращения</th>
                <th>Дата</th>
                <th>ФИО</th>
                <th>Проблема</th>
                <th>Сумма</th>

            </tr>
            </thead>
            <tbody>
            @foreach($payments as $payment)
                <tr>
                    <td>{{$payment->illness->id}}</td>
                    <td>{{\Carbon\Carbon::parse($payment->created_at)->format('d-m-Y')}}</td>
                    <td>{{$payment->user->name}}</td>
                    <td>{{$payment->illness->name}}</td>
                    <td>{{$payment->sum}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert alert-warning">Платежей не найдено</div>

@endif