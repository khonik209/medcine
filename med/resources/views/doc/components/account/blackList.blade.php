@if($user->black_list)
    <ol class="black-list">
        @foreach($user->black_list as $user_id)
            <li>
                @if(\App\User::find($user_id))
                    {{\App\User::find($user_id)->name}} <a class="btn btn-default btn-xs"
                                                           href="/doc/blacklist/remove/{{$user_id}}">Разблокировать
                        пользователя</a>
                @else
                    Пользователь удалён
                @endif
            </li>
        @endforeach
    </ol>
@else
    <div class="alert alert-success">Чёрный список пуст. На странице "Информация" любого Вашего
        обращения Вы можете заблокировать пациента. После этого его имя появится здесь, а он не
        сможет
        обращаться к Вам вновь.
    </div>
@endif