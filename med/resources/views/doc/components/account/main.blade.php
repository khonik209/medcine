
<form class="form-horizontal" role="form" action="{{url('/doc/account/edit')}}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="avatar" class="col-sm-3 control-label">Аватар</label>
        <div class="col-sm-9">
            <a role="button" data-toggle="modal" data-target="#addAvatar">
                @if($user->avatar)
                    <img src="{{$user->avatar_url}}" alt="..."  style="max-width: 100px"
                         class="img-thumbnail img-responsive">
                @else
                    <img src="/storage/doctors/emptyavatar.png" alt="..."  style="max-width: 100px"
                         class="img-thumbnail img-responsive">
                @endif
            </a>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">ФИО</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name"
                   placeholder="Иванов Иван Иванович" value="{{$user->name}}">
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-3 control-label">E-mail</label>
        <div class="col-sm-9">
            <input type="email" class="form-control" id="email" name="email"
                   placeholder="ivanov@gmail.ru" value="{{$user->email}}">

        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3">
            <a href="{{url('/doc/resetpassword/')}}" class="btn btn-info btn-xs">
                Сменить пароль
            </a>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="job" class="col-sm-3 control-label">Место работы</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="job" id="job"
                   placeholder="ГКБ №1 г.Москва" value="{{$user->doctor->job}}">
        </div>
    </div>
    <div class="form-group">
        <label for="skills" class="col-sm-3 control-label">Специализация</label>
        <div class="col-sm-9">
            <select id="skills" class="form-control">
                <option value="0" selected>Выберите специальности из списка</option>
                @foreach($skills as $skill)
                    <option value="{{$skill->id}}" data-name="{{$skill->name}}"
                            class="js-skill">{{$skill->name}}</option>
                @endforeach
            </select>
            @if($domainObj->id==1)
                <small>если вашей специальности нет в списке - напишите нам, мы её добавим!</small>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <ul class="list-unstyled js-skill-form ">
                @foreach($user->skills as $skill)
                    <li class="js-skill-item alert alert-info">
                        <button type="button" class="close js-delete-skill" data-dismiss="alert"
                                aria-hidden="true">×
                        </button>
                        {{$skill->name}}
                        <input type="hidden" name="skills[]" value="{{$skill->id}}">
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="form-group">
        <label for="experience" class="col-sm-3 control-label">Стаж, лет</label>
        <div class="col-sm-9">
            <input type="number" class="form-control" name="experience" id="experience"
                   placeholder="15" value="{{$user->doctor->experience}}">
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="education" class="col-sm-3 control-label">Образование</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="education" name="education"
                   placeholder="Первый Московский государственный медицинский университет имени И.М. Сеченова."
                   value="{{$user->doctor->education}}">
        </div>
    </div>
    <div class="form-group">
        <label for="level" class="col-sm-3 control-label">Научная степень</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="level" name="level"
                   placeholder="Кандидат медицинских наук" value="{{$user->doctor->level}}">
        </div>
    </div>
    {{--
    <hr>
    <div class="form-group">
        <label for="document" class="col-sm-3 control-label">Документ, подтверждающий
            квалификацию</label>
        <div class="col-sm-9">
            <a class="btn btn-primary" role="button" data-toggle="modal" data-target="#addDocument">
                @if($user->doctor->document)
                    <img src="/storage/{{$user->doctor->document}}" alt="..."
                         style="max-width: 100px"
                         class="img-thumbnail img-responsive">
                @else
                    Добавить документ
                @endif
            </a>
        </div>
    </div>--}}
    <hr>
    <div class="form-group">
        <label for="infomation" class="col-sm-3 control-label">Дополнительная
            информация</label>
        <div class="col-sm-9">
                                    <textarea class="form-control" rows="5" id="infomation" name="infomation"
                                              placeholder="Имею множество публикаций... дополнительно владею...преподаю в...прошёл курсы повышения квалификации...">{{$user->doctor->infomation}}</textarea>
        </div>
    </div>
    <div class="form-group col-sm-12">
        <label for="notification" class=" control-label">Получать уведомления по
            email</label>
        <input class="" id="notification" type="checkbox" @if($user->notification) checked
               @endif name="notification" value="1">
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>