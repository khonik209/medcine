<div class="col-md-12">
    <div class="panel panel-default" id="illness_{{$illness->id}}">
        <div class="panel-heading">
            {{$illness->name}}
        </div>
        <div class="panel-body">
              <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
            <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

            <p>Пациент:
                <strong>
                    <a href="{{$illness->user?url('/doc/user/'.$illness->user->id):'#'}}">{{$illness->user->name or 'Не найден'}}</a>
                </strong>
            </p>
            <p>
                <strong>
                    {{$illness->name}}
                </strong>
            </p>
            <p>
                {{$illness->description}}
            </p>
            @if($domainObj->custom('custom_form'))
                <hr>
                <h4><strong>Анкета</strong></h4>
                @if($illness->answers && count($illness->answers)>0)
                    @each('doc.components.home.answers',$illness->answers,'answer')
                @else
                    Не заполнена
                @endif
            @endif
            @if($domainObj->custom('records'))
                @if($illness->record)
                    <hr>
                    <h4>Время записи:
                        <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
                    </h4>
                @endif
            @endif
            @if($domainObj->custom('chat'))
                @if($illness->chat)
                    <hr>
                    <h4>Онлайн кабинет для встречи</h4>
                    <a href="{{url('/doc/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти</a>
                @endif
            @endif
            @if($domainObj->custom('disease_history'))
                <hr>
                @if($illness->diseaseHistory)

                    <h4>История болезни</h4>
                    <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}"
                       class="btn btn-primary">Открыть</a>
                @else
                    <form action="{{url('/doc/disease_history/new')}}" method="post">
                        {{ csrf_field() }}
                        <input class="hidden" name="user_id" value="{{$illness->user->id}}">
                        <input class="hidden" name="illness_id" value="{{$illness->id}}">

                        <button type="submit" class="btn btn-primary">
                            Создать историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>

                    </form>
                @endif
            @endif
            @if($domainObj->custom('payments'))
                @if($illness->payments->count()>0)
                    <hr>
                    <h4>Платежи</h4>
                    @foreach($illness->payments as $payment)
                        <p>
                            <strong> {{$payment->sum}}</strong> руб.
                            <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                            @if($domainObj->payment_form=="safe")
                                @if(!$payment->canceled && !$payment->test_deposition)
                                    <strong>Сумма будет удержана еще, дней:
                                        <br> {{7 - \Carbon\Carbon::parse($payment->updated_at)->diffInDays(\Carbon\Carbon::now())}}
                                    </strong>
                                @endif
                            @endif
                        </p>
                    @endforeach
                @endif
            @endif
            <a class="btn btn-danger pull-right" href="#finish_request"
               data-toggle="modal">
                <img style="max-width: 20px"
                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPBSURBVHhe7ZtLbhNREEUzyg7IYhBzEPsCJLYFzMiIsIFAmLEBBgn3vJw8+dO2X8dtu9vpI5X86teuW3bbUZRczMzMzJw79/f3V7Fr7crwyyCCX8VuHiTnn01LSO1lCj/H7h5bp0dm/xv75fkH5nn3ElLwieJzQPG8E7C2JST5x8LXhiYDwhDo/EW8qadboiwhfDe8jgUPupMh4jaKB3ziRdy5LSDCEF8+8BCJWFMFfOLmt98CFIHu6EFMbBjxQCHoVtL4Pvbb9GjITP983F88UAy6lTSXr5UxgsjY/uKhXDHoVgyP4tZADKKYB5GxYcQDTaBbMXzyBSAGUcyCyNhG8ZBzu3iwb5QLQEysWTyYase+0S0gwnp92vMIptuxb1QLiKBF8Texnfc8Z7CkHftGswDExHqJJ44PpagP9o1iAYiJ9RYPxEC3Hft6L4Anjw32ywev1yQecl56TsPHWQBPHqsfPJxXB+pDehHXLB5MVQwffgEZpIpnqKfB8sjgvZeQnmbxedz4aW/4sAvIAKviGW5xwF5LsHdR/FKv+SqePGewpGL44Au4JsZQsfpKcY5tFNLFrh7zS+KJ40MpWsDwaRYA+LGNb+VFOmqbxAMx0K0YPvgC1m4BUwX82NYldNQ0iwfioFsxfNgFAAMxGDkGja0ugXznEjjHNn5erOQ7v1nIgW7F8OEXAAzGgOQZOLZtCSWvbRS3K/8EedCtGD7OAoABGZQaBo/tWsLe4oEa0K0YPt4CgEEZmDoExLqWUPLAmZjpAj2xJvFQLhR0K4aPuwBgYAanFiGxriV0/shMbaxZPFALuhXDx18AMDgCqEdQbGkJXVBDrT1N4oF60K0YPs0CAAEIoQdhsY1LIEeNtc3igR7QrRg+3QIAIQiiD4GxtSUQI2dNL/FAH+hWDJ92AYAghNGL0FhdAmdi5nqLB3pBt2L49AsAhCGQfgTHEL63eKAfdCuGx7EAQCBCuQbCMc/PFg9cA3QrhsezAEAogr3U3uLBS01jAeASBvvVmWNNZwFD41jzAnQrhucF6LZj37wA3YrheQG6o8Gx5gXoVgwfdwH5Xv8W+6pbGDq2imONYwGWLNUYGiy2iiVrNYa39nZi31pjXom1P5JafXUM7xRh6Fkx3hWGK4ndmq6YWuptwr61xjzJO57IdCH+F9MF/GPEfPpC/NvYW9MV08MtYGooY16Abjv2vdwF5H4qfy4f3hiaHMyOgGi5M9ROms7pHyY+KKud9PEvMx/Z3uNlpgezoyHHS2XNzMzMzMxULi7+A0+G/zc9lcKjAAAAAElFTkSuQmCC">
                &nbsp;
                Завершить обращение
            </a>
            <!-- Modal Завершить обращение -->
            <div class="modal fade" id="finish_request" tabindex="-1" role="dialog" aria-labelledby="addNote"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                            </button>
                            <h4 class="modal-title" id="addDoc">Завершение обращения</h4>
                        </div>
                        <form enctype="multipart/form-data" method="post" action="{{url('/doc/request/finish')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="illness_id" value="{{$illness->id}}">
                            <div class="modal-body">
                                @if($domainObj->payment_form=="safe")
                                    @if($illness->pac_opinion!="none")
                                        <p><strong>Пациент принял решение о завершении обращения. Для завершения
                                                обращения и
                                                выплаты или возврата суммы, примите своё решение</strong></p>
                                    @endif
                                    <p class="bg-info">Укажите, была ли оказана услуга. Решение принимается Исполнителем
                                        и
                                        Заказчиком в двустороннем порядке. В зависимости от выбранного решения сумма
                                        переходит
                                        на счёт Исполнителя или возвращается Заказчику</p>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="opinion" value="success">
                                            <strong>Успешно.</strong>&mdash;Подтверждаю, что услуга была оказана.
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="opinion" value="fail">
                                            <strong>Отказ.</strong>&mdash;Подтверждаю, что услуга не была оказана
                                        </label>
                                    </div>
                                @else
                                    <p>Вы уверены, что хотите завершить обращение? После этого оно будет перенесено в архив.</p>
                                @endif
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="submit" class="btn btn-primary">Подтвердить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>