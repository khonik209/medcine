<div class="well well-sm">
    <h4><a href="{{url('/doc/requests#illness_'.$illness->id)}}">{{$illness->name}}</a></h4>

    <span class="pull-left">
                   {!! $illness->status()!!}
                 </span>
    <span class="text-muted pull-right">
                        {{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}
                        </span>

    <p>Пациент:
        <strong>
            {{$illness->user->name or 'Не найден (удален)'}}
        </strong>
    </p>
    <p>
        <strong>
            {{$illness->name}}
        </strong>
    </p>
    <p>
        {{$illness->description}}
    </p>
    @if($domainObj->custom('records'))
        @if($illness->record)
            <hr>
            <p>Время записи:
                <strong>{{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}</strong>
            </p>
        @endif
    @endif
    @if($domainObj->custom('chat'))
        @if($illness->chat)
            <hr>
            <h4>Онлайн кабинет для встречи</h4>
            <a href="{{url('/doc/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти &nbsp;
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        @endif
    @endif
    @if($domainObj->custom('disease_history'))
        <hr>
        @if($illness->diseaseHistory)
            <h4>История болезни</h4>
            <a href="{{url('/doc/disease_history/'.$illness->diseaseHistory->id)}}"
               class="btn btn-primary">Открыть</a>
        @else
            <form action="{{url('/doc/disease_history/new')}}" method="post">
                {{ csrf_field() }}
                <input class="hidden" name="user_id" value="{{$illness->user->id}}">
                <input class="hidden" name="illness_id" value="{{$illness->id}}">

                <button type="submit" class="btn btn-primary">
                    Создать историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
                </button>

            </form>
        @endif
    @endif
    @if($domainObj->custom('payments'))
        @if($illness->payments->count()>0)
            <hr>
            <h4>Платежи</h4>
            @foreach($illness->payments as $payment)
                <p>
                    <strong> {{$payment->sum}}</strong> руб.
                    <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                </p>
            @endforeach
        @endif
    @endif
</div>