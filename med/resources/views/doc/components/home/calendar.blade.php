<div class="col-md-6" id="full-calendar">
    <div class="panel panel-default">
        <div class="panel-body">
            <app-calendar :user_role="'doc'">
            </app-calendar>
        </div>
    </div>
</div>
@push('styles')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endpush