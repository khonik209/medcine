@extends('layouts.admin')

@section('content')
    <div class="admin-users">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Пациенты</h3>
            </div>
            <div class="panel-body">
                @include('common.info')
                @include('common.success')
                @include('common.error')
                @include('common.customError')


                <form action="{{url('/doc/users')}}" method="get" class="form-inline">
                    <div class="input-group">
                        @if($request->filter)
                            <span class="input-group-btn">
                                        <a href="{{url('/doc/users')}}" class="btn btn-default">
                                            Назад
                                        </a>
                                    </span>
                        @endif
                        <input type="text" name="filter" class="form-control"
                               placeholder="Данные пациета"
                               @if($request->filter) value="{{$request->filter}}" @endif>
                        <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                        Найти
                                    </button>
                                </span>
                    </div><!-- /input-group -->
                </form>
                <hr>
                @if($users && count($users)>0)
                    <div class="table-responsive ">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <p>ID</p>
                                    @if($request->sort == "id" && $request->dir == "desc")
                                        <a href="{{url('/doc/users?sort=id&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/doc/users?sort=id&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>Имя</p>
                                    @if($request->sort == "name" && $request->dir == "desc")
                                        <a href="{{url('/doc/users?sort=name&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/doc/users?sort=name&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>E-mail </p>
                                    @if($request->sort == "email" && $request->dir == "desc")
                                        <a href="{{url('/doc/users?sort=email&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/doc/users?sort=email&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>Телефон</p>
                                    @if($request->sort == "phone" && $request->dir == "desc")
                                        <a href="{{url('/doc/users?sort=phone&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/doc/users?sort=phone&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>Количество обращений</p>
                                    @if($request->sort == "illnesses" && $request->dir == "desc")
                                        <a href="{{url('/doc/users?sort=illnesses&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/doc/users?sort=illnesses&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <a href="{{url('/doc/user/'.$user->id)}}">{{$user->id}}</a>
                                    </td>

                                    <td>
                                        <a href="{{url('/doc/user/'.$user->id)}}">{{$user->name}}</a>
                                    </td>
                                    <td>
                                        <a href="{{url('/doc/user/'.$user->id)}}">{{$user->email}}</a>
                                    </td>
                                    <td>
                                        <a href="{{url('/doc/user/'.$user->id)}}">{{$user->phone}}</a>
                                    </td>
                                    <td>
                                        <a href="{{url('/doc/user/'.$user->id)}}">{{$user->illnesses->count()}}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-info">Список пациентов пуст</div>
                @endif
                {!! $users->appends(Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection