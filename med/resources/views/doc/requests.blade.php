@extends('layouts.doc')

@section('content')
    <div class="admin-doctor">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Обращения
                    </div>
                </div>
            </div>
            @each('doc.components.requests.request',$illnesses,'illness','doc.components.requests.empty')
        </div>
    </div>
@endsection