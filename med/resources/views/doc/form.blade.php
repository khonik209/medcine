@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Информация об аккаунте</div>


                <div class="panel-body">
                    @include('common.customError')
                    @include('common.info')
                    @include('common.success')
                    <form enctype="multipart/form-data" class="form-horizontal" method="POST"
                          action="{{url('/doc/form/add')}}">
                        {{ csrf_field() }}
                        @if($domainObj->id==1)
                            <div class="form-group">
                                <label for="job" class="col-sm-3 control-label">Место работы *</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="job" id="job"
                                           placeholder="ГКБ №1 г.Москва" value="{{ old('job') }}">
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <label for="job" class="col-sm-3 control-label">Место работы *</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="job" id="job"
                                           placeholder="ГКБ №1 г.Москва" value="{{$domainObj->company}}">
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="skills" class="col-sm-3 control-label">Специализация *</label>
                            <div class="col-sm-9">
                                <select id="skills" class="form-control">
                                    <option value="0" selected>Выберите специальности из списка</option>
                                    @foreach($skills as $skill)
                                        <option value="{{$skill->id}}" data-name="{{$skill->name}}"
                                                class="js-skill">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                                @if($domainObj->id==1)
                                    <small>
                                        Если вашей специальности нет в списке - напишите нам, мы её добавим!
                                    </small>
                                @endif
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <ol class="js-skill-form"></ol>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="experience" class="col-sm-3 control-label">Стаж, лет *</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name='experiance' id="experience"
                                       placeholder="15" value="{{old('experiance')}}">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="education" class="col-sm-3 control-label">Образование *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="education" name="education"
                                       placeholder="Первый Московский государственный медицинский университет имени И.М. Сеченова."
                                       value="{{old('education')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="level" class="col-sm-3 control-label">Научная степень *</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="level" name="level"
                                       value="{{old('level')}}"
                                       placeholder="Кандидат медицинских наук">
                            </div>
                        </div>
                        @if($domainObj->id == 1)
                            <hr>
                            <div class="form-group">
                                <label for="document" class="col-sm-3 control-label">Документ, подтверждающий
                                    квалификацию</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" id="document" name="document"
                                           value="{{old('document')}}">
                                    <small>Вы можете прикрепить файл позже, в разделе "Настройки профиля"</small>
                                </div>
                            </div>
                        @endif
                        <hr>
                        <div class="form-group">
                            <label for="infomation" class="col-sm-3 control-label">Дополнительная
                                информация</label>
                            <div class="col-sm-9">
                                    <textarea class="form-control" rows="5" id="infomation" name="infomation"
                                              placeholder="Имею множество публикаций... дополнительно владею...преподаю в...прошёл курсы повышения квалификации...">{{old('infomation')}}</textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection