@extends('layouts.doc')

@section('content')
    <div class="panel panel-default">

        <div class="panel-heading">История болезни № {{$history->id}}</div>

        <div class="panel-body" id="js-edit-history">
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')

            <edit-history :history="{{json_encode($text)}}" :id="{{$history->id}}"></edit-history>
        </div>
    </div>
@endsection