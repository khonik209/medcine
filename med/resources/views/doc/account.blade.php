@extends('layouts.doc')

@section('content')
    <div class="account">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        Настройка профиля
                        @include('common.customError')
                        @include('common.error')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Личная информация
                    </div>
                    <div class="panel-body">
                        @include('doc.components.account.main')
                    </div>
                </div>
            </div>
            @if($domainObj->id!=1)
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            График работы
                        </div>
                        <div class="panel-body">
                            @include('doc.components.account.workTime')
                        </div>
                    </div>
                </div>
            @endif
            @if($domainObj->id==1)
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Счёт получения средств
                        </div>
                        <div class="panel-body">
                            @include('doc.components.account.payments')
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            История платежей
                        </div>
                        <div class="panel-body">
                            @include('doc.components.account.history')
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Черный список пользователей
                        </div>
                        <div class="panel-body">
                            @include('doc.components.account.blackList')
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- Modal Добавить аватар -->
    <div class="modal fade" id="addAvatar" tabindex="-1" role="dialog" aria-labelledby="addAva"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="addDoc">Загрузить аватар</h4>
                </div>
                <form enctype="multipart/form-data" method="post" action="/doc/avatar">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="form-photo_front" class="col-sm-5 control-label">Обзор<br>
                                <small>(размер файла: не более 3 Мб)</small>
                            </label>

                            <input type="file" class="" id="form-photo_front" name="avatar"
                                   placeholder="Загрузить" required>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection