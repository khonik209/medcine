@extends('layouts.doc')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Новая история болезни</div>
                <div class="panel-body">
                    @include('common.error')
                    @include('common.customError')
                    @include('common.info')
                    @include('common.success')
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body" id="js-history">
                    <add-history :user="{{json_encode($user)}}" :illness="{{json_encode($illness)}}"></add-history>
                </div>
            </div>
        </div>
    </div>
@endsection