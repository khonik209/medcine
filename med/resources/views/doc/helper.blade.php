@extends('layouts.doc')

@section('content')
    <div class="helper">
        <ol class="breadcrumb">
            <li><a href="/doc">Главная</a></li>
            <li class="active">Помощник</li>
        </ol>
        <div class="panel panel-default">
            @include('common.error')
            @include('common.info')
            @include('common.success')
            <div class="panel-heading">Заметки

                <button data-toggle="modal" data-target="#addNote"
                        class="btn btn-primary btn-xs pull-right">
                    <span class="glyphicon glyphicon-plus"></span>Новая заметка
                </button>
            </div>
            <div class="panel-body">
                @if(count($doctor->notes()->where('type','note')->get())>0)
                    <div class="js-sortable notes">
                        @foreach($doctor->notes()->where('type','note')->orderBy('order','asc')->get() as $note)
                            <div class="col-xs-12 note" data-id="{{$note->id}}">
                                <div class="col-xs-10 note-text">
                                    <span class="glyphicon glyphicon-align-justify"></span>
                                    <a role="button" data-toggle="modal" data-target="#note{{$note->id}}">
                                        {{$note->name}}
                                    </a>
                                </div>
                                <div class="col-xs-2 note-delete">
                                    <a href="/doc/helper/note/{{$note->id}}/delete"
                                       class="btn btn-danger btn-xs"
                                       onclick="return confirm('Вы уверены, что хотите удалить заметку?')"><span
                                                class="glyphicon glyphicon-trash"></span></a>
                                </div>
                            </div>
                            {{-- Просмотр заметки --}}
                            <div class="modal fade" id="note{{$note->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">{{$note->name}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{$note->text}}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info"
                                                    data-toggle="modal"
                                                    data-target="#noteEdit{{$note->id}}">
                                                Редактировать
                                            </button>
                                            <button type="button" class="btn btn-info"
                                                    data-dismiss="modal">
                                                Закрыть
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Редактирование заметки --}}
                            <div class="modal fade" id="noteEdit{{$note->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="form-horizontal" role="form"
                                              action="/doc/helper/note/edit"
                                              method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="note_id" value="{{$note->id}}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;
                                                </button>

                                                <input type="text" class="form-control" name="note_name"
                                                       value="{{$note->name}}">
                                            </div>
                                            <div class="modal-body">
                                                        <textarea class="form-control"
                                                                  name="note_text">{{$note->text}}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">
                                                    Сохранить
                                                </button>
                                                <button type="button" class="btn btn-info"
                                                        data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-warning"><strong>Список заметок пуст. </strong>
                        <small>Здесь Вы можете писать и удобно сортировать личные заметки, напоминания,
                            прочую информацию.
                        </small>
                    </div>

                @endif

            </div>
            <div class="panel-heading">Файлы

                <button data-toggle="modal" data-target="#addDocument"
                        class="btn btn-primary btn-xs pull-right">
                    <span class="glyphicon glyphicon-plus"></span>Добавить документ
                </button>
            </div>
            <div class="panel-body">
                @if(count($urls)>0)
                    <div class="">
                        @foreach($urls as $url)
                            <div class="col-xs-12 note">
                                <div class="col-xs-10 note-text">
                                    @if($url->type == 'image')
                                        <a target="_blank"
                                           href="/storage/{{$url->url}}"> {{$url->name}}</a>
                                    @elseif($url->type == 'doc')
                                        <a target="_blank"
                                           href="/storage/{{$url->url}}">{{$url->name}}</a>
                                    @elseif($url->type == 'pdf')
                                        <a target="_blank"
                                           href="/storage/{{$url->url}}">{{$url->name}}</a>
                                    @elseif($url->type=='dicom')
                                        <a href="/chat/dicom/{{$url->id}}" target="_blank">{{$url->name}}</a>
                                    @else
                                        <a target="_blank"
                                           href="/storage/{{$url->url}}">{{$url->name}}</a>
                                    @endif
                                </div>
                                <div class="col-xs-2 note-delete">
                                    <a href="/doc/account/document/{{$url->id}}/delete"
                                       class="btn btn-danger btn-xs"
                                       onclick="return confirm('Вы уверены, что хотите удалить файл?')"><span
                                                class="glyphicon glyphicon-trash"></span></a>
                                </div>

                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-warning"><strong>Вы еще не загружали файлы. </strong>
                        <small>Здесь Вы можете загрузить файлы для хранения и просмотра. В том числе и
                            снимки ( файлы формата DICOM ).
                        </small>
                    </div>

                @endif

            </div>

        </div>

        <!-- Modal добавить заметку -->
        <div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Новая заметка</h4>
                    </div>
                    <form action="/doc/helper/note/add" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <input type="text" name="note_name" value="" class="form-control"
                                   placeholder="Название заметки">
                            <textarea name="note_text" class="form-control"
                                      placeholder="Текст заметки"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть
                            </button>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Modal Добавить документ -->
        <div class="modal fade" id="addDocument" tabindex="-1" role="dialog" aria-labelledby="addDoc"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                        </button>
                        <h4 class="modal-title" id="addDoc">Прикрепить документ</h4>
                    </div>
                    <form enctype="multipart/form-data" method="post" action="{{url('/doc/helper/document/add')}}">
                        {{ csrf_field() }}

                        <div class="modal-body">
                            <div class="form-group">

                                <input class="form-control" id="file_name" name="file_name"
                                       placeholder="Введите название документа">

                            </div>
                            <div class="form-group">
                                <label for="form-photo_front" class="col-sm-5 control-label">Ваш документ
                                    *<br>
                                    <small>(размер файла: не более 3 Мб)</small>
                                </label>

                                <input type="file" class="" id="form-photo_front" name="documents[]"
                                       placeholder="Загрузить" multiple>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                    &times;
                                </button>
                                Снимки следует загружать группой, выделив все (или нужные) файлы внутри
                                папки DICOM на вашем диске. (как правило, файлы с расширением .dcm)
                            </div>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть
                            </button>
                            <button type="submit" class="btn btn-primary"
                                    onclick="$(this).addClass('disabled', true);">
                                Сохранить
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection