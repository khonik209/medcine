@extends('layouts.doc')

@section('content')
    <div class="doctors">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Список всех врачей
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p><strong>Фильтр</strong></p>
                        <form action="{{url('/doc/doctors')}}" method="get" class="form-inline" role="form">
                            <div class="input-group">
                                <input class="form-control" name="filter" value="{{$request->filter}}"
                                       placeholder="Поиск врача"/>
                                <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary">
                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                        </button>
                                    </span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="skills"> или выберите специальность </label>
                                <select id="skills" class="form-control" name="skill"
                                        onchange="$(this).parents('form').submit()">
                                    <option value="0" @if(!$request->skill) selected @endif >
                                        Выберите специальность из списка
                                    </option>
                                    @foreach($skills as $skill)
                                        <option value="{{$skill->id}}"
                                                @if($request->skill == $skill->id) selected @endif
                                                class="js-skill">{{$skill->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($request->skill || $request->filter)
                                <a href="{{url('/doc/doctors')}}" class="btn btn-default btn-xs">
                                    Показать всех врачей
                                </a>
                            @endif
                        </form>

                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="page-header">
                        </div>
                        <div class="table-responsive ">
                            <table class="table table-hover">
                                <thead class="hidden-xs">
                                <tr>
                                    <th>ФИО</th>
                                    <th>Специальность</th>
                                    <th>Стаж</th>
                                    <th>Место работы</th>
                                    @if($domainObj->id==1)
                                        <th>Статус</th>
                                    @endif
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($activeDoctors as $activeDoctor)
                                    <tr class="hidden-xs">
                                        <td>
                                            <a href="{{url('/doc/doctor/'.$activeDoctor->id)}}">{{$activeDoctor->name}}</a>
                                        </td>
                                        <td>
                                            <a href="{{url('/doc/doctor/'.$activeDoctor->id)}}">
                                                @if($activeDoctor->skills()->first())
                                                    {{$activeDoctor->skills()->first()->name}}
                                                @endif
                                                @if($activeDoctor->skills()->count()>1)
                                                    <small>и еще {{$activeDoctor->skills()->count() - 1 }}</small>
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{url('/doc/doctor/'.$activeDoctor->id)}}">{{$activeDoctor->doctor->experience}}</a>
                                        </td>
                                        <td>
                                            <a href="{{url('/doc/doctor/'.$activeDoctor->id)}}">{{$activeDoctor->doctor->job}}</a>
                                        </td>
                                        @if($domainObj->id==1)
                                            <td>
                                                @if($activeDoctor->doctor->status=="online")
                                                    <strong class="status-online">Online </strong>
                                                @elseif($activeDoctor->doctor->status=="offline")
                                                    <strong class="status-offline">Занят </strong>
                                                @else
                                                    <strong class="status-holiday">Offline</strong>
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            <form action="{{url('/doc/chat/create')}}" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="member_id" value="{{$activeDoctor->id}}">
                                                <input type="hidden" name="type" value="private">
                                                <button type="submit" class="btn btn-primary btn-xs">
                                                    Открыть чат
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr class="visible-xs">
                                        <td>
                                            <a href="{{url('/doc/doctor/'.$activeDoctor->id)}}" class="btn btn-doc">
                                                {{$activeDoctor->name}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
