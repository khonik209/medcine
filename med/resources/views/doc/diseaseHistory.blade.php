@extends('layouts.doc')

@section('content')
    <div class="admin-doctor">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2>История болезни № {{$diseaseHistory->id}}</h2>
                    <h4>Лечащий врач: {{$diseaseHistory->doctor->name}}</h4>
                    @if($diseaseHistory->doctor->id == Auth::id())
                        <a class="btn btn-primary btn-xs"
                           href="{{url('/doc/disease_history/'.$diseaseHistory->id.'/edit')}}">Редактировать</a>
                    @endif
                </div>
                @include('common.error')
                @include('common.customError')
                @include('common.info')
                @include('common.success')
                <h3>Контакты</h3>
                <ul>
                    <li><strong>Email: </strong>{{$diseaseHistory->user->email}}</li>
                    <li><strong>Телефон: </strong>{{$diseaseHistory->user->phone}}</li>
                </ul>
                <hr>

                <p>
                    <strong>Пациент:</strong>
                    <a href="{{url('/doc/user/'.$diseaseHistory->user->id)}}">
                        {{$diseaseHistory->user->name}}
                    </a>
                </p>
                <p><strong>Возраст:</strong> {{$diseaseHistory->user->age}}</p>
                <p><strong>Дата поступления:</strong> {{\Carbon\Carbon::parse($text->date)->format('d.m.Y H:i')}}
                </p>
                <p><strong>Жалобы:</strong> {{$text->complaint}}</p>
                <p><strong>Результат первичного осмотра:</strong> {{$text->primary_inspection}}</p>
                <p><strong>Первичный диагноз:</strong> {{$text->primary_diagnosis}}</p>
                <p><strong>Окончательный диагноз:</strong> {{$text->final_diagnosis}}</p>
                <p><strong>Назначенное лечение:</strong> {{$text->treatment}}</p>
                <p><strong>План лечения:</strong> {{$text->treatment_plan}}</p>
                @if(count($text->inspections)>0)
                    <p><strong>Дополнительные обследования</strong></p>
                    @foreach($text->inspections as $key=>$inspection)
                        @if($inspection)
                            <ul>
                                <strong>Осмотр {{$key}}</strong>
                                <li>
                                    <strong>Результат осмотра:</strong> {{$inspection}}
                                </li>
                                <li>
                                    <strong>Корректировка
                                        лечения:</strong> {{$text->treatment_adjustments[$key]}}
                                </li>
                            </ul>
                        @endif
                    @endforeach
                @endif
                <p><strong>Эпикриз: </strong> {{$text->result}}</p>
            </div>
        </div>
    </div>
@endsection