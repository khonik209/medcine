<div>
    <p>Общение {{$user->name}} к {{$doctor->name}} "{{$illness->name}}" успешно отменено.</p>
    <p>
        С уважением, <br>
        Ваш ТелеДоктор
    </p>
</div>