<div>
    Здравствуйте, {{$user->name}}!

    Вы зарегистрированы на сайте  <a href="https://{{$user->domain->name}}">{{$user->domain->name}}</a> <br>

    С помощью данных, представленных ниже Вы сможете зайти в ваш Личный Кабинет.

    <ul>
        <li>Ваш логин: {{$user->email}}</li>
        <li>Ваш пароль для входа: {{$password}}</li>
    </ul>
    <a href="https://{{$user->domain->name}}/login">Вход в личный кабинет</a>
</div>