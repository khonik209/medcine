<div>
    <p>Здравствуйте, <strong>{{$user->name}}</strong></p>

    <p>
        Подписка кабинета <strong>{{$payment->domain->name}}</strong> успешно продлена.
    </p>
    <p>
        Текущая дата окончания подписки: <strong>{{\Carbon\Carbon::parse($payment->domain->active_to)->format('H:i d-m-Y')}}</strong>
    </p>
</div>