<div>
    <p>Здравствуйте, {{$user->name}}</p>
    <p>Уведомляем Вас, что обращение #{{$payment->illness_id}} отменено.</p>
    <p>Сумма, в размере {{$payment->sum}} руб. успешно возвращена Заказчику. </p>
    <ul>
        Информация о платеже:
        <li>Номер платежа: {{$payment->id}}</li>
        <li>Заказчик: {{$payment->user->name}}</li>
        <li>Исполнитель: {{\App\User::find($payment->doctor_id)->name}}</li>
        <li>Сумма: {{$payment->sum}}</li>
        <li>Номер транзакции: {{$payment->invoice_id}}</li>
    </ul>
    <p>Спасибо за использование нашей телемедицинской платформы. </p>
    <p>Связаться с тех. поддержкой можно по адресу: {{$payment->domain->feedback_email}} </p>
</div>