<div>
    Здравствуйте, {{$user->name}}! <br>

    Напоминаем о том, что у вас есть новые обращение ( {{count($illnesses)}} шт ) на <a href="https://{{$user->domain->name}}/login">{{$user->domain->name}}</a>: <br>
    @foreach($illnesses as $illness)
<ul>
    <li>От: {{$illness->user->name}}</li>
    <li>Название: {{$illness->name}}</li>
    <li>Описание: {{$illness->description}}</li>
</ul>
    @endforeach
    <p>
        <a @if($user->role=='doc') href="https://{{$user->domain->name}}/doc" @else href="https://{{$user->domain->name}}/user" @endif>Войти в личный кабинет</a>
    </p>
<br>
    <small>Вы можете отказаться от уведомлений в настройках личного кабинета</small>
</div>