<div>
    <p>Здравствуйте, {{$admin->name}}</p>
    <p>Напоминаем Вам, что <strong>{{\Carbon\Carbon::parse($domain->active_to)->format('H:i d-m-Y')}}</strong> заканчивается подписка на платформе ТелеДоктор</p>
    @if($domain->lastSubscription())<p>За последний оплаченный период вашей клиникой было открыто
        <strong>{{$domain->numberOfIllnesses()}}</strong> обращений. </p>@endif
    <p>Продлить подписку Вы можете в личном кабинете администратора или <a href="http://{{$domain->name}}/admin/account">по ссылке: </a></p>
    <p>Спасибо, что используете нашу платформу!</p>
</div>