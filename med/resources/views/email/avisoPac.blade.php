<div>
    <p>Здравствуйте, <strong>{{$user->name}}</strong></p>

    <p>
        Вы оплатили оказание услуги #{{$payment->illness_id}} в размере {{$payment->sum}} рублей.
    </p>
    @if($payment->payment_type=="safe")
        <p>
            По условиям безопасной сделки, эти средства будут заморожены на счету Заказчика(пациента) до момента
            завершения оказания услуг, но не более 30 календарных дней.
        </p>
        <p>
            Для того, чтобы успешно завершить обращение, Заказчику (пациенту) и Исполнителю (врачу) необходимо в
            двустороннем порядке подтвердить завершение оказания услуг. Для этого:
        </p>
        <ol>
            <li>Нажмите <strong>"Завершить обращение"</strong> во вкладке "Информация" вашего текущего обращения</li>
            <li>Выберите <strong>"Подтверждаю оказание услуг"</strong> или <strong>"Услуга оказана не была"</strong> во
                всплывшем окне.
            </li>
            <li>Дождитесь, пока Исполнитель (врач) проведёт аналогичные действия.</li>
            <li>
                <ul>
                    <li>
                        Если обе стороны подтвердят, что услуга оказана, средства поступят на счёт Исполнителя (врача).
                    </li>
                    <li>
                        Если обе стороны подтвердят, что услуга оказана не была, средства вернутся Заказчику.
                    </li>
                    <li>
                        Если Исполнитель и Заказчик не сходятся во мнении относительно завершения обращения, то решение
                        о
                        списании или возвращении средств ложится на администрацию {{$payment->domain->name}}. В
                        ближайшее
                        время с
                        обеими сторонами свяжется независимый представитель {{$payment->domain->name}} для решения
                        конфликта
                        и
                        принятия итогового решения.
                    </li>
                </ul>
            </li>
        </ol>
    @endif
    <p>
        По всем вопросам можно обращаться по адресу
        <strong>{{$payment->domain->feedback_email}}</strong> или в форме обратной связи на сайте
    </p>

</div>