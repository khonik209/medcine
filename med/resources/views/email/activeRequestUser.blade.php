<div>
    <p>
        Здравствуйте, {{$user->name}}!
    </p>
    <p>
        Врач @if($illness->status=="active")принял @elseif($illness->status=="denied") отклонил @else ответил на @endif
        вашу заявку на {{$user->domain->name}}.
    </p>
    <p>
        Подробней на <a href="http://{{$illness->domain->name}}">{{$illness->domain->name}}</a>
    </p>
</div>