<div>
    Здравствуйте, {{$user->name}}!<br>
    Вы запросили восстановление пароля от платформы <a href="https://{{$user->domain->name}}">{{$user->domain->name}}</a>

    <ul>
        <li>Ваш логин: {{$user->email}}</li>
        <li>Ваш пароль: {{$password}}</li>
    </ul>

    <a href="https://{{$user->domain->name}}/login">Вход в личный кабинет</a>
</div>