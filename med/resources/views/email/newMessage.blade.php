<div>
    <p>
        Здравствуйте, {{$user->name}}!
    </p>
    <p>
        У вас есть непрочитанное сообщение на портале <a href="https://{{$user->domain->name}}">{{$user->domain->name}}</a>
    </p>
    @if($newMessage)
        <p>
            <strong>
                {{$newMessage->author}} : {{$newMessage->text}}
            </strong>
        </p>
    @endif
    <p>
        <a @if($user->role=='doc') href="https://{{$user->domain->name}}/doc" @else href="https://{{$user->domain->name}}/user" @endif>ответить</a>
    </p>
    <p>
        <a @if($user->role=='doc') href="https://{{$user->domain->name}}/doc" @else href="https://{{$user->domain->name}}/user" @endif>Войти в личный кабинет</a>
    </p>
    <p>
        <small>Вы можете отказаться от уведомлений в настройках личного кабинета</small>
    </p>
</div>