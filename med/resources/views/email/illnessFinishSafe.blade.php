<div>
    <p>Здравствуйте, {{$user->name}}.</p>
    @if($user->role=="user")
        <p>Информируем Вас, что врач {{\App\User::find($illness->doctor_id)->name}} инициализировал завершение Вашего
            обращения.</p>
    @else
        <p>Информируем Вас, что пациент {{$illness->user->name}} инициализировал завершение своего обращения.</p>
    @endif
    <p>Для завершения работы и перевода денежных средств, Вам следует так же завершить обращение и указать, оказана
        услуга или нет</p>
</div>