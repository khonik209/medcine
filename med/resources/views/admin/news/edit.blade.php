@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    <form class="form-inline" action="{{url('/news/'.$news->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-danger btn-xs pull-right">Удалить</button>
                    </form>
                    {{$news->title}}
                </h4>
            </div>
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')
            <div class="panel-body">
                <form enctype="multipart/form-data" class="form-horizontal" role="form" action="{{url('/news/'.$news->id)}}"
                      method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Заголовок</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" value="{{$news->title}}"
                                   placeholder="Введите заголовок">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text" class="col-sm-2 control-label">Текст</label>
                        <div class="col-sm-10">
                                <textarea class="form-control" id="text" name="text"
                                          placeholder="Текст новости">{{$news->text}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">
                            <img src="{{$news->image_url}}" alt="..." style="max-width: 100px">
                        </label>
                        <div class="col-sm-10">
                            <input id="image" type="file" name="image">
                        </div>
                    </div>
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection