@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    Создание новости
                </h4>
            </div>
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')
            <div class="panel-body">
                <form enctype="multipart/form-data" class="form-horizontal" role="form" action="{{url('/news')}}"
                      method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Заголовок</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}"
                                   placeholder="Введите заголовок">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="text" class="col-sm-2 control-label">Текст</label>
                        <div class="col-sm-10">
                                <textarea class="form-control" id="text" name="text"
                                          placeholder="Текст новости">{{old('text')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-sm-2 control-label">Изображение</label>
                        <div class="col-sm-10">
                                <input id="image" type="file" name="image">
                        </div>
                    </div>
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection