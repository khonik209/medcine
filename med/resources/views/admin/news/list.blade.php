@extends('layouts.admin')

@section('content')
    <div class="admin-users">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{url('/news/create')}}" class="btn btn-default btn-xs pull-right">Добавить новость</a>
                <p>Новости</p>
            </div>
            <div class="panel-body">
                @include('common.info')
                @include('common.success')
                @include('common.error')
                @include('common.customError')

                @if($news->count()>0)
                    <div class="row">
                        @foreach($news as $single)
                            <div class="col-sm-4">
                                <div class="media">
                                    <a class="pull-left" href="{{url("/news/$single->id/edit")}}">
                                        <img class="media-object" src="{{$single->image_url}}" alt="..."
                                             style="max-width: 100px">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{$single->title}}</h4>
                                        {{$single->text}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="alert alert-warning">
                        Список новостей пуст. <a href="{{url('/news/create')}}">Создайте первую новость</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection