@extends('layouts.admin')

@section('content')
    <div class="admin-statistics">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2>Статистика</h2>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <form action="/admin/statistics" method="get" class="form-inline">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" id="optionsRadios1" value="full_time"
                                           @if(!$request->period || $request->period=="full_time")checked
                                           @endif onclick="$('form').submit()">
                                    За всё время
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" value="year" onclick="$('form').submit()"
                                           @if($request->period=="year")checked @endif>
                                    За последний год
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" value="half_year" onclick="$('form').submit()"
                                           @if($request->period=="half_year")checked @endif>
                                    За последние 6 месяцев
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="period" value="month" onclick="$('form').submit()"
                                           @if($request->period=="month")checked @endif>
                                    За последний месяц
                                </label>
                            </div>
                        </form>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
            @include('admin.components.statistics.graphics')
        </div>
    </div>
@endsection