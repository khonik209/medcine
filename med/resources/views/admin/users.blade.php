@extends('layouts.admin')

@section('content')
    <div class="admin-users">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                        data-target="#myModal">Новый
                    пациент
                </button>
                <h3>Пациенты</h3>
            </div>
            <div class="panel-body">
                @include('common.info')
                @include('common.success')
                @include('common.error')
                @include('common.customError')

                <form action="{{url('/admin/users')}}" method="get" class="form-inline">
                    <div class="input-group">
                        @if($request->filter)<span class="input-group-btn"><a href="{{url('/admin/users')}}"
                                                                              class="btn btn-default">Назад</a></span>@endif
                        <input type="text" name="filter" class="form-control"
                               placeholder="Данные пациета"
                               @if($request->filter) value="{{$request->filter}}" @endif>
                        <span class="input-group-btn"><button class="btn btn-primary"
                                                              type="submit">Найти</button></span>
                    </div>
                </form>
                <hr>
                @if($users && count($users)>0)
                    <div class="table-responsive ">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <p>ID</p>
                                    @if($request->sort == "id" && $request->dir == "desc")
                                        <a href="{{url('/admin/users?sort=id&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/admin/users?sort=id&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>Имя</p>
                                    @if($request->sort == "name" && $request->dir == "desc")
                                        <a href="{{url('/admin/users?sort=name&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/admin/users?sort=name&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="text-center">
                                    <p>E-mail </p>
                                    @if($request->sort == "email" && $request->dir == "desc")
                                        <a href="{{url('/admin/users?sort=email&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/admin/users?sort=email&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="hidden-xs text-center">
                                    <p>Телефон</p>
                                    @if($request->sort == "phone" && $request->dir == "desc")
                                        <a href="{{url('/admin/users?sort=phone&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/admin/users?sort=phone&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                                <th class="hidden-xs text-center">
                                    <p>Количество обращений</p>
                                    @if($request->sort == "illnesses" && $request->dir == "desc")
                                        <a href="{{url('/admin/users?sort=illnesses&dir=asc')}}">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </a>
                                    @else
                                        <a href="{{url('/admin/users?sort=illnesses&dir=desc')}}">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </a>
                                    @endif
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <a href="{{url('/admin/user/'.$user->id)}}">{{$user->id}}</a>
                                    </td>

                                    <td>
                                        <a href="{{url('/admin/user/'.$user->id)}}">{{$user->name}}</a>
                                    </td>
                                    <td>
                                        <a href="{{url('/admin/user/'.$user->id)}}">{{$user->email}}</a>
                                    </td>
                                    <td class="hidden-xs ">
                                        <a href="{{url('/admin/user/'.$user->id)}}">{{$user->phone}}</a>
                                    </td>
                                    <td class="hidden-xs ">
                                        <a href="{{url('/admin/user/'.$user->id)}}">{{$user->illnesses->count()}}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-info">Список пациентов пуст</div>
                @endif
                {!! $users->appends(Request::except('page'))->render() !!}
                <hr class="visible-xs">
                <form action="{{url('/admin/export/users')}}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default pull-right" style="margin-right: 22px;">
                        Скачать в формате Excel&nbsp;&nbsp;&nbsp;<span
                                class="glyphicon glyphicon-download-alt"></span>
                    </button>
                </form>
                <div class="clearfix visible-xs"></div>
                {{-- Добавление пациента --}}
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title">Добавить пациента</h4>
                            </div>
                            <form class="form-horizontal" role="form" action="{{url('/admin/user/add')}}"
                                  method="post">
                                <div class="modal-body">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label">Ф.И.О. пациента</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name" id="name"
                                                   placeholder="Имя" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="age" class="col-sm-2 control-label">Возраст</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="age" id="age"
                                                   placeholder="Возраст, лет">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email пациента</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" id="email"
                                                   placeholder="Email" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label">Телефон</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="phone" id="phone"
                                                   placeholder="Контактный телефон">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                    </button>
                                    <button type="submit" class="btn btn-primary">Добавить</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
        </div>
    </div>
@endsection