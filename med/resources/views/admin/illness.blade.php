@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-center">
                        <span>{{$illness->name?$illness->name:'Обращение #'.$illness->id}}</span>
                        <span class="text-muted pull-left">{{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}</span>
                    </p>
                    <form action="{{url('/illnesses/delete')}}" method="post" class="form-inline pull-right">
                        {{ csrf_field() }}
                        <input type="hidden" name="illness_id" value="{{$illness->id}}">
                        <button type="submit" class="btn btn-danger btn-xs"><span
                                    class="glyphicon glyphicon-trash"></span>
                            Удалить
                        </button>
                    </form>
                    <form action="{{url('/illnesses/edit')}}" class="form-inline" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="illness_id" value="{{$illness->id}}">
                        <div class="form-group">
                            <label for="status" class="control-label">Статус:</label>
                            <select name="status" class="form-control" id="status"
                                    onchange="$(this).parents('form').submit()">
                                <option value="new"
                                        @if($illness->status=="new") selected @endif >
                                    Новое
                                </option>
                                <option value="active"
                                        @if($illness->status=="active") selected @endif >
                                    В работе
                                </option>
                                <option value="archive"
                                        @if($illness->status=="archive") selected @endif >
                                    Архив
                                </option>
                                <option value="denied"
                                        @if($illness->status=="denied") selected @endif >
                                    Отклонено
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="doctor" class="control-label">Ответственный:</label>
                            <select id="doctor" class="form-control" onchange="$(this).parents('form').submit()"
                                    name="doctor_id">
                                <option @if($illness->doctor_id==$domainObj->getAdmin()->first()->id) selected
                                        @endif value="{{$domainObj->getAdmin()->first()->id}}">{{$domainObj->getAdmin()->first()->name}}</option>
                                @foreach($domainObj->getDoctors()->get() as $doctor)
                                    <option @if($illness->doctor_id==$doctor->id) selected
                                            @endif value="{{$doctor->id}}">{{$doctor->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                    @if(!$illness->record)
                        <hr>
                        <create-record :illness="{{json_encode($illness)}}"></create-record>
                    @else
                        <p>
                            <strong>
                                Запись к {{$illness->record->doctor()->name}}
                                на {{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}

                            </strong>
                        </p>
                    @endif
                </div>
                <div class="panel-body">
                    <h4>Описание обращения</h4>
                    @if($illness->diseaseHistory)
                        <a class="btn btn-primary btn-xs "
                           href="{{url('/admin/user/disease_history/'.$illness->diseaseHistory->id)}}">
                            История болезни
                        </a>
                    @endif
                    <p><strong>{!! $illness->name?$illness->name:'<i>Название отсутствует</i>'!!}</strong></p>
                    <p>{!! $illness->description?$illness->description:'<i>Описание отсутствует</i>'!!}</p>

                    @if($domainObj->custom('custom_form'))
                        <h4><strong>Анкета:</strong></h4>
                        @if($illness->answers && count($illness->answers)>0)
                            @each('admin.components.user.answer',$illness->answers,'answer')
                        @else
                            <p>Не заполнена</p>
                        @endif
                    @endif


                    @if($domainObj->custom('chat'))
                        @if($illness->chat)
                            <hr>
                            <h4>Онлайн кабинет для встречи</h4>
                            <a href="{{url('/admin/request/'.$illness->chat->id)}}" class="btn btn-primary">Зайти</a>
                        @endif
                    @endif

                </div>
                <div class="panel-footer">
                    @if($domainObj->custom('payments'))
                        @if($illness->payments->count()>0)
                            <ol>

                                @foreach($illness->payments as $payment)
                                    <li>
                                        <span class="pull-left">{{\Carbon\Carbon::parse($payment->updated_at)->format('d.m.Y H:i')}}</span>&nbsp;
                                        <strong>{{$payment->sum}} руб.</strong>
                                        <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                    </li>
                                @endforeach
                            </ol>
                        @endif
                        <form method="post" action="{{url('/yandex/payment/create')}}" class="form-inline pull-left">
                            {{ csrf_field() }}
                            <input type="hidden" name="type" value="request">
                            <input type="hidden" name="illness_id" value="{{$illness->id}}">
                            <div class="input-group">
                                <input class="form-control" name="sum" placeholder="Введите сумму">
                                <span class="input-group-btn">
                                <button class="btn btn-primary">
                                    Создать платёж
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button>
                            </span>
                            </div>
                        </form>
                        <div style="clear:both"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @push('styles')
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet">
        <link href="/css/jquery-ui.theme.min.css" rel="stylesheet">
    @endpush
@endsection