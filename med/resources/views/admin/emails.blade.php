@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Email рассылки</h4>
                    </div>
                    <div class="panel-body">
                        <p class="">
                            Email рассылки - это возможность составлять и рассылать единое письмо для группы ваших
                            клиентов.
                        </p>

                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Шаблоны</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Шаблоны - это напомленение вашего письма. К вашим услугам простой текстовый редактор писем.
                        </p>
                        <a class="btn btn-primary btn-xs" href="{{url('/admin/emails/templates')}}">Посмотреть мои
                            шаблоны</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Книги</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Книги - это набор email адресов, на которые будут отправлены email, составленные в разделе
                            "Шаблоны".
                        </p>
                        <a class="btn btn-primary btn-xs" href="{{url('/admin/emails/books')}}">Посмотреть мои книги</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Кампании</h4>
                    </div>
                    <div class="panel-body">
                        <p>
                            Кампания - это история ваших рассылок "шаблонов" писем "книгам" пользователей.
                        </p>
                        <a class="btn btn-primary btn-xs" href="{{url('/admin/emails/campaigns')}}">Посмотреть мои
                            кампании</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection