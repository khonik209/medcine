@extends('layouts.admin')

@section('content')
    <div class="admin-home" id="admin-dashdoard">
        <div class="row">
            <div class="col-md-6 ">
                <div class="panel panel-default item-bg item-bg-2">
                    <div class="panel-body item">
                        <dashboard-table
                                :custom_functions="{{json_encode($domainObj->custom_functions?$domainObj->custom_functions:[])}}">
                        </dashboard-table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default item-bg item-bg-1">
                    <div class="panel-body item">
                        <app-calendar :is_admin="1"
                                      :business_hours="{{json_encode($domainObj->workingHours())}}"
                                      :custom_functions="{{json_encode($domainObj->custom_functions?$domainObj->custom_functions:[])}}"
                                      :doctor="{{json_encode([])}}"
                                      :doctors="{{$doctors}}">

                        </app-calendar>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <div class="panel panel-default item-bg item-bg-3">
                    <div class="panel-body item">
                        <chart :data="{{$records}}"></chart>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default item-bg item-bg-4">
                    <div class="panel-body item">
                        <div class="main-item text-center">
                            <chart :data="{{$illnesses}}"></chart>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-4">
                <div class="panel panel-default item-bg item-bg-4">
                    <div class="panel-body item">
                        <div class="main-item text-center">
                            <chart :data="{{$cities}}"></chart>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default item-bg item-bg-4">
                    <div class="panel-body item">
                        <div class="main-item text-center">
                            <chart :data="{{$platforms}}"></chart>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-default item-bg item-bg-4">
                    <div class="panel-body item">
                        <div class="main-item text-center">
                            <chart :data="{{$browsers}}"></chart>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @push('styles')
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    @endpush
@endsection