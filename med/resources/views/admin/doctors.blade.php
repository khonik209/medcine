@extends('layouts.admin')

@section('content')
    <div class="admin-doctors">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Все специалисты</h3>
            </div>
            <div class="panel-body">
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span
                            class="glyphicon glyphicon-plus"></span> Добавить врача
                </button>
                <hr>
                @include('common.info')
                @include('common.success')
                @include('common.error')
                @include('common.customError')

                @if($doctors && count($doctors)>0)
                    <div class="table-responsive" >
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Имя</th>
                                <th class="hidden-xs">E-mail</th>
                                <th class="hidden-xs">Телефон</th>
                                <th class="hidden-xs">Специализации</th>
                                @if($domainObj->id==1)
                                    <th>Статус</th>
                                    <th>Верификация</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($doctors as $doctor)
                                <tr>
                                    <td>
                                        <a href="/admin/doctor/{{$doctor->id}}">{{$doctor->id}}</a>
                                    </td>

                                    <td>
                                        <a href="/admin/doctor/{{$doctor->id}}">{{$doctor->name}}</a>
                                    </td>
                                    <td class="hidden-xs">
                                        <a href="/admin/doctor/{{$doctor->id}}">{{$doctor->email}}</a>
                                    </td>
                                    <td class="hidden-xs">
                                        <a href="/admin/doctor/{{$doctor->id}}">{{$doctor->phone}}</a>
                                    </td>
                                    <td class="hidden-xs">
                                        @if($doctor->skills()->first())
                                            <a href="/admin/doctor/{{$doctor->id}}">
                                                @if($doctor->skills()->first())
                                                    {{$doctor->skills()->first()->name}}
                                                @endif
                                                @if($doctor->skills()->count()>1)
                                                    <small>и еще {{$doctor->skills()->count() - 1 }}</small>
                                                @endif
                                            </a>
                                        @endif
                                    </td>
                                    @if($domainObj->id==1)
                                        <td>
                                            <a href="/admin/doctor/{{$doctor->id}}">
                                                @if($doctor->doctor)
                                                    {{$doctor->doctor->status}}
                                                @else
                                                    Врач не активен
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            @if($doctor->doctor && $doctor->doctor->verified)
                                                <span class="label label-success">Подтверждён</span>
                                            @elseif($doctor->doctor && $doctor->doctor->document)
                                                <span class="label label-warning">Прикрепил документ</span>
                                            @else
                                                <span class="label label-danger">Не подтверждён</span>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-info">Список врачей пуст</div>
                @endif
            </div>
        </div>
    </div>
    {{-- Добавление врача --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Новый врач</h4>
                </div>
                <form class="form-horizontal" role="form" action="{{url('/admin/doctor/add')}}" method="post">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Ф.И.О.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Имя"
                                       required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Добавить</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection