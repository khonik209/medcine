@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
            <div class="row">
                <div class="col-md-6">
                    @include('admin.components.diseaseHistory.head')
                </div>
                <div class="col-md-6">
                    @include('admin.components.diseaseHistory.patient')
                </div>
                <div class="col-md-12">
                    @include('admin.components.diseaseHistory.text')
                </div>
                <div class="col-md-12">
                    @include('admin.components.diseaseHistory.additional')
                </div>
            </div>
        </div>
@endsection