@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <div class="row" id="email-campaigns">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Email кампании клиники
                    </div>
                    <div class="panel-body">
                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                    </div>
                </div>
            </div>
            <email-campaigns :campaigns="{{$campaigns}}" :books="{{$books}}"
                             :templates="{{$templates}}"></email-campaigns>
        </div>
    </div>
@endsection