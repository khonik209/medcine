@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$campaign->message->subject}}
                        <a class="pull-right btn btn-primary btn-xs"
                           href="{{url('/admin/emails/campaigns')}}">Назад</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Тело письма
                    </div>
                    <div class="panel-body">
                        {!! $campaign->message->body !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Статистика рассылки
                    </div>
                    <div class="panel-body">
                        <ul>
                            @if(isset($campaign->statistics[0]))
                                <li>Отправлено: {{$campaign->statistics[0]->count}}</li>
                            @endif
                            @if(isset($campaign->statistics[1]))
                                <li>Доставлено: {{$campaign->statistics[1]->count}} </li>
                            @endif
                            @if(isset($campaign->statistics[2]))
                                <li>Открыто: {{$campaign->statistics[2]->count}}</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection