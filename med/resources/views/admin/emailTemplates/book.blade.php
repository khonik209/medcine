@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
            <div class="row" id="edit-book-container">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Книга "{{$book['name']}}"
                        </div>
                    </div>
                </div>
                <book-edit :contacts="{{json_encode($contacts)}}" :book="{{$book['sp_id']}}"></book-edit>
            </div>
        </div>
@endsection