@extends('layouts.admin')

@section('content')
    <div class="admin-emails" id="vue-new-template">
        <input type="hidden" id="currentDomain" value="{{$domainObj->id}}">
        <template-constructor @savetemplate="saveTemplate">
        </template-constructor>
    </div>
@endsection