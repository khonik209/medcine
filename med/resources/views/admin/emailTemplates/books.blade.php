@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <div class="row" id="new-book-container">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Адресные книги
                    </div>
                </div>
            </div>
            <new-book-form :books="{{$books}}"></new-book-form>
        </div>
    </div>
@endsection