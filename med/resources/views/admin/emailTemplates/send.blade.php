@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <ol class="breadcrumb">
            <li><a href="/admin">Главная</a></li>
            <li><a href="/admin/emails">Шаблоны</a></li>
            <li class="active">{{$book['name']}}</li>
        </ol>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2>Сделать рассылку</h2>
                </div>
                @include('common.error')
                @include('common.customError')
                @include('common.info')
                @include('common.success')
                <div class="row">
                    <div class="col-md-6">
                        <h4>Письмо</h4>
                        <div class="template">
                            {!! base64_decode($template->body) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Адресная книга</h4>

                        <p><strong>{{$book['name']}}</strong></p>
                    </div>
                    <div class="col-xs-12">
                        <form action="{{url('/admin/emails/send')}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="book" value="{{$book['sp_id']}}">
                            <input type="hidden" name="template" value="{{$template->id}}">
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </form>
                    </div>
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
        </div>
    </div>
@endsection