@extends('layouts.admin')

@section('content')
    <div class="admin-templates">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Выбрать шаблон
                        <a href="{{url('/admin/emails/template/new')}}" class="pull-right btn btn-primary btn-xs">
                            <span class="glyphicon glyphicon-plus"></span> &nbsp;
                            Новый шаблон
                        </a>
                    </div>
                    <div class="panel-body">
                        @include('common.info')
                        @include('common.success')
                        @include('common.error')
                        @include('common.customError')
                    </div>
                </div>
            </div>


            @foreach($templates as $key=>$template)

                @if($key!=0 && $key%2==0)
                    <div class="clearfix"></div>
                @endif
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{$template->name}}
                        </div>
                        <div class="panel-body">
                            <div class="template">
                                {!! base64_decode($template->body) !!}
                            </div>
                            <form action="{{url('/admin/emails/template/delete')}}" class="form-inline"
                                  method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="template_id" value="{{$template->id}}">
                                <button type="submit" class="btn btn-danger btn-xs pull-right">Удалить &nbsp; <span
                                            class="glyphicon glyphicon-trash"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
@endsection