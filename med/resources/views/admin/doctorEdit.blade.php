@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/doctors')}}">Все врачи</a></li>
            <li><a href="{{url("/admin/doctor/$doctor->id")}}">{{$doctor->name}}</a></li>
            <li class="active">Редактирование профиля</li>
        </ol>
        @include('common.error')
        @include('common.customError')
        @include('common.info')
        @include('common.success')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    {{$doctor->name}}
                </h4>
            </div>
            <div class="panel-body">
                <h4>Анкета</h4>
                <form class="form-horizontal" role="form" action="{{url('/admin/doctor/'.$doctor->id.'/edit')}}"
                      method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">ФИО</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="Иванов Иван Иванович" value="{{$doctor->name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" disabled
                                   placeholder="ivanov@gmail.ru" value="{{$doctor->email}}">
                        </div>
                        <hr>

                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="job" class="col-sm-2 control-label">Место работы</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="job" id="job"
                                   placeholder="ГКБ №1 г.Москва" value="@if($form){{$form->job}}@endif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="specialty" class="col-sm-2 control-label">Специальность</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="specialty" id="specialty"
                                   placeholder="Хирург" value="@if($form){{$form->specialty}}@endif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="skills" class="col-sm-2 control-label">Специализация</label>
                        <div class="col-sm-10">
                            <select id="skills" class="form-control">
                                <option value="0" selected>Выберите специальности из списка</option>
                                @foreach($skills as $skill)
                                    <option value="{{$skill->id}}" data-name="{{$skill->name}}"
                                            class="js-skill">{{$skill->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <ol class="js-skill-form">
                                @foreach($doctor->skills as $skill)
                                    <li class="js-skill-item alert alert-info">
                                        <button type="button" class="close js-delete-skill" data-dismiss="alert"
                                                aria-hidden="true">×
                                        </button>
                                        {{$skill->name}}
                                        <input type="hidden" name="skills[]" value="{{$skill->id}}">
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="experience" class="col-sm-2 control-label">Стаж, лет</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="experience" id="experience"
                                   placeholder="15" value="@if($form){{$form->experience}}@endif">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="education" class="col-sm-2 control-label">Образование</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="education" name="education"
                                   placeholder="Первый Московский государственный медицинский университет имени И.М. Сеченова."
                                   value="@if($form){{$form->education}}@endif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="level" class="col-sm-2 control-label">Научная степень</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="level" name="level"
                                   placeholder="Кандидат медицинских наук" value="@if($form){{$form->level}}@endif">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="infomation" class="col-sm-2 control-label">Дополнительная
                            информация</label>
                        <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="infomation" name="infomation"
                                              placeholder="Имею множество публикаций... дополнительно владею...преподаю в...прошёл курсы повышения квалификации...">@if($form){{$form->infomation}}@endif</textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info">Сохранить</button>
                </form>
                <hr>
                <h4>График работы</h4>
                <form class="form-horizontal" role="form" action="{{url('/admin/doctor/work_time')}}"
                      method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                    <table class="table">
                        <tr>
                            <th></th>
                            <th>День недели</th>
                            <th>Начало рабочего дня</th>
                            <th>Окончание рабочего дня</th>
                        </tr>
                        @foreach($days as $k=>$day)
                            <tr class="js-day-row">
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="days[{{$k}}]" value="1" id="{{$k}}"
                                                   class="js-days-checkbox"
                                                   @if(isset($form->work_time['days'][$k])) checked @endif>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    {{$day}}
                                </td>
                                <td class="js-daytime-start">
                                    @if(isset($form->work_time['days'][$k]))
                                        <input class="form-control timepicker js-daytime" type="text"
                                               name="days[{{$k}}][starts]"
                                               value="{{$form->work_time['days'][$k]['starts']}}">
                                    @endif
                                </td>
                                <td class="js-daytime-end">
                                    @if(isset($form->work_time['days'][$k]))
                                        <input class="form-control timepicker js-daytime" type="text"
                                               name="days[{{$k}}][ends]"
                                               value="{{$form->work_time['days'][$k]['ends']}}">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <button type="submit" class="btn btn-info">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
@endsection