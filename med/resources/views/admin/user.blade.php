@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/users')}}">Все пациенты</a></li>
            <li class="active">{{$user->name}}</li>
        </ol>
        <div class="row" id="create_record_container">
            <div class="col-md-6">
                @include('admin.components.user.name')
            </div>
            <div class="col-md-6">
                @include('admin.components.user.form')
            </div>
            <div class="clearfix"></div>
            @each('admin.components.user.illness',$illnesses,'illness')
        </div>
    </div>
    @push('styles')
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css" rel="stylesheet">
        <link href="/ui/jquery-ui-1.12.1.custom/jquery-ui.min.css" rel="stylesheet">
        <link href="/css/jquery-ui.theme.min.css" rel="stylesheet">
    @endpush
@endsection