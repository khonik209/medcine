<form class="form-horizontal" role="form" action="{{url('/admin/settings/functions')}}" method="post">
    {{ csrf_field() }}
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="records"
                       @if($domainObj->custom('records')) checked @endif >
                Запись
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="chat"
                       @if($domainObj->custom('chat')) checked @endif >
                Онлайн консультации
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="videochat"
                       @if($domainObj->custom('videochat')) checked @endif >
                Видеоконференции
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="payments"
                       @if($domainObj->custom('payments')) checked @endif >
                Приём платежей
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="disease_history"
                       @if($domainObj->custom('disease_history')) checked @endif >
                История болезни
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="api"
                       @if($domainObj->custom('api')) checked @endif >
                Внешнее API
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="sendpulse"
                       @if($domainObj->custom('sendpulse')) checked @endif >
                Email рассылки
            </label>
        </div>
    </div>
    {{--
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="telephone"
                       @if($domainObj->custom('telephone')) checked @endif >
                Интернет телефония
            </label>
        </div>
    </div>--}}
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="hide_doctors"
                       @if($domainObj->custom('hide_doctors')) checked @endif >
                Скрыть врачей (запретить обращения к врачам на прямую)
            </label>
        </div>
    </div>
    <div class="col-sm-offset-2 col-sm-6">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="custom_functions[]" value="custom_form"
                       @if($domainObj->custom('custom_form')) checked @endif >
                Анкета для пациентов
            </label>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info">Сохранить</button>
        </div>
    </div>
</form>