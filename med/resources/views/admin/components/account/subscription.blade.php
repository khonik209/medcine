@foreach($plans as $plan)
    <div class="col-xs-12 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="plan-item text-center">
                    <h4>{{$plan->name}}</h4>
                    <p>{{$plan->price}} руб.</p>
                    <p>Число обращений:</p>
                    <strong>
                        @if($plan->limit!=0)
                            {{$plan->limit}}
                        @else
                            Без ограничений
                        @endif
                    </strong>

                    <p>
                        <small>в месяц</small>
                    </p>
                    <form action="/yandex/payment/create" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="type" value="subscription">
                        <input type="hidden" name="sum" value="{{$plan->price}}">
                        <button type="submit" class="btn btn-xs">Оплатить</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endforeach