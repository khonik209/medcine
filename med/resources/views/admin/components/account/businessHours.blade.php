<form class="form-horizontal" role="form" action="{{url('/admin/settings/business_hours')}}"
      method="post">
    {{ csrf_field() }}
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th></th>
                <th>День недели</th>
                <th>Начало рабочего дня</th>
                <th>Окончание рабочего дня</th>
            </tr>
            @foreach($days as $k=>$day)
                <tr class="js-day-row">
                    <td>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="days[{{$k}}]" value="1" id="{{$k}}"
                                       class="js-days-checkbox" @if($domainObj->busyDay($k)) checked @endif>
                            </label>
                        </div>
                    </td>
                    <td>
                        <label for="{{$k}}">{{$day}}</label>
                    </td>
                    <td class="js-daytime-start">
                        @if($domainObj->busyDay($k))
                            <input class="form-control timepicker js-daytime" type="text" name="days[{{$k}}][starts]"
                                   value="{{$domainObj->workingHours()[$k]->starts}}">
                        @endif
                    </td>
                    <td class="js-daytime-end">
                        @if($domainObj->busyDay($k))
                            <input class="form-control timepicker js-daytime" type="text" name="days[{{$k}}][ends]"
                                   value="{{$domainObj->workingHours()[$k]->ends}}">
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>