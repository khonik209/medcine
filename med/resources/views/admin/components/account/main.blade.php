<form class="form-horizontal" role="form" action="{{url('/admin/settings/admin')}}" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                   value="{{$admin->email}}" disabled>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Ф.И.О.</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" id="name" placeholder="Имя"
                   value="{{$admin->name}}" required>

        </div>
    </div>
    <a href="{{url('/admin/settings/reset_password/')}}" class="btn btn-primary btn-xs pull-right">Сменить
        пароль</a>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>