<form class="form-horizontal" enctype="multipart/form-data" role="form" action="{{url('/admin/settings/company')}}"
      method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="company" class="col-sm-2 control-label">Компания</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="company" id="company"
                   placeholder="Название компании, ООО, ИП"
                   value="{{$domainObj->company}}" required>
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Адрес</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="address" id="address"
                   placeholder="Юридический адрес компании"
                   value="{{$domainObj->address}}" required>
        </div>
    </div>
    <div class="form-group">
        <label for="feedback_email" class="col-sm-2 control-label">Email обратной связи</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="feedback_email" id="feedback_email"
                   placeholder="Почта для писем от пользователй"
                   value="{{$domainObj->feedback_email}}" required>
        </div>
    </div>
    <div class="form-group">
        <label for="phone" class="col-sm-2 control-label">Телефон</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="phone" id="phone" value="{{$domainObj->phone}}">
        </div>
    </div>
    <div class="form-group">
        <label for="logo" class="col-sm-2 control-label">Логотип</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="logo" id="logo" value="{{$domainObj->phone}}">
        </div>
    </div>
    <div class="form-group">
        <label for="custom-color" class="col-sm-2 control-label">Цветовая гамма</label>
        <div class="col-sm-10">
            <select class="form-control" name="color" id="custom-color">
                <option value="bg-blue" class="bg-blue"  @if($domainObj->color=="bg-blue") selected @endif ></option>
                <option class="bg-dark-red" value="bg-dark-red"
                        @if($domainObj->color=="bg-dark-red") selected @endif ></option>
                <option class="bg-pink" value="bg-pink" @if($domainObj->color=="bg-pink") selected @endif></option>
                <option class="bg-yellow" value="bg-yellow"
                        @if($domainObj->color=="bg-yellow") selected @endif></option>
                <option class="bg-light-green" value="bg-light-green"
                        @if($domainObj->color=="bg-light-green") selected @endif></option>
                <option class="bg-sea-color" value="bg-sea-color"
                        @if($domainObj->color=="bg-sea-color") selected @endif></option>
                <option class="bg-purple" value="bg-purple"
                        @if($domainObj->color=="bg-purple") selected @endif></option>
                <option class="bg-grey" value="bg-grey" @if($domainObj->color=="bg-grey") selected @endif></option>
                <option class="bg-black" value="bg-black" @if($domainObj->color=="bg-black") selected @endif></option>
                <option class="bg-light-blue" value="bg-light-blue" @if($domainObj->color=="bg-light-blue") selected @endif></option>
                <option class="bg-green" value="bg-green" @if($domainObj->color=="bg-green") selected @endif></option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>