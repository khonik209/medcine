<form class="form-horizontal" role="form" action="{{url('/admin/settings/yandex')}}" method="post">
    {{ csrf_field() }}
    <div class="col-sm-offset-2">
        <div class="radio-inline">
            <input type="radio" name="payment_type" value="button" id="button" class="js-payment-type"
                   @if($domainObj->options && isset($domainObj->options->payment_type) && $domainObj->options->payment_type=="button") checked @endif>
            <label for="button" class="control-label">Яндекс.Кнопка</label>
        </div>
        <div class="radio-inline">
            <input type="radio" name="payment_type" value="kassa" id="kassa" class="js-payment-type"
                   @if($domainObj->options && isset($domainObj->options->payment_type) && $domainObj->options->payment_type=="kassa") checked @endif>
            <label for="kassa" class="control-label">Яндекс.Касса</label>
        </div>
    </div>
    <hr>
    <div class="form-group js-ya-button">
        <label for="payment_account" class="col-sm-2 control-label">Номер Яндекс кошелька</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="payment_account" id="payment_account" placeholder=""
                   value="{{$domainObj->payment_account}}">
        </div>
    </div>
    <div class="form-group js-ya-button">
        <label for="secret_key_b" class="col-sm-2 control-label">Секретный ключ</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="secret_button" id="secret_key_b" placeholder=""
                   value="{{$domainObj->yandex_secret}}">
        </div>
    </div>
    <div class="alert alert-info js-ya-button">
        <p> Узнать номер кошелька можно <a href="https://money.yandex.ru/quickpay/" target="_blank"> тут </a>
            <img class="img-responsive" src="/img/yandex/button_number.jpg" style="max-width: 180px"></p>
        <p>Узнать секретный код можно <a href="https://money.yandex.ru/myservices/online.xml" target="_blank"> тут </a>.
            Ваш адрес для уведомления: <strong>https://{{$domainObj->name}}/yandex/quickpay</strong>
            <img class="img-responsive" src="/img/yandex/button_secret.jpg" style="max-width: 180px">
        </p>
    </div>
    <div class="form-group js-ya-kassa">
        <label for="sc_id" class="col-sm-2 control-label">sc_id</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="sc_id" id="sc_id" placeholder=""
                   value="{{$domainObj->yandex_scid}}">
        </div>
    </div>
    <div class="form-group js-ya-kassa">
        <label for="shop_id" class="col-sm-2 control-label">shop_id</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="shop_id" id="shop_id" placeholder=""
                   value="{{$domainObj->yandex_shopid}}">
        </div>
    </div>
    <div class="form-group js-ya-kassa">
        <label for="secret_key_k" class="col-sm-2 control-label">secret_key</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="secret_kassa" id="secret_key_k"
                   placeholder=""
                   value="{{$domainObj->yandex_secret}}">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info">Сохранить</button>
        </div>
    </div>
</form>
@push('scripts')
    <script>
        function togglePaymentType(el) {
            if (el.val() === 'button') {
                $('.js-ya-button').removeClass('hidden');
                $('.js-ya-kassa').addClass('hidden');
            } else {
                $('.js-ya-button').addClass('hidden');
                $('.js-ya-kassa').removeClass('hidden');
            }
        }

        $(document).ready(function () {
            togglePaymentType($('.js-payment-type:checked'))
        });
        $('.js-payment-type').change(function () {
            togglePaymentType($(this));
        });

    </script>
@endpush