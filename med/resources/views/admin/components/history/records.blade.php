@extends('layouts.admin')

@section('content')
    <div class="admin-statistics">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Все записи
                </div>
                <div class="panel-body">
                    @if($filtered)
                        <a href="{{url('/admin/history/records')}}" class="pull-right btn btn-primary btn-xs">Показать
                            все записи</a>
                    @endif
                    <form action="{{url('/admin/history/records')}}" method="get" class="form-inline">
                        <div class="form-group">
                            <input @if($request->start) value="{{$request->start}}" @endif placeholder="Дата записи"
                                   type="text" class="form-control datepicker" name="start"
                                   onchange="$(this).parents('form').submit()">
                        </div>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control"
                                   placeholder="Введите данные пациента"
                                   @if($request->search) value="{{$request->search}}" @endif>
                            <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">Найти</button>
                                        </span>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if($records && $records->count()>0)
                        <div class="table-responsive ">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Дата создания записи</th>
                                    <th>Дата записи</th>
                                    <th>Пациент</th>
                                    <th>Врач</th>
                                    <th>Проблема</th>
                                    <th>Комментарии</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($records as $record)
                                    <tr>
                                        <td>{{$record->id}}</td>
                                        <td>{{\Carbon\Carbon::parse($record->created_at)->format('d.m.Y H:i')}}</td>
                                        <td>{{\Carbon\Carbon::parse($record->start_date)->format('d.m.Y H:i')}}</td>
                                        <td>
                                            {{$record->user->name or 'Пользователь удалён'}}
                                        </td>
                                        <td>
                                            {{$record->doctor()->name or 'Врач удалён'}}
                                        </td>
                                        <td>{{$record->name}}</td>
                                        <td>{{$record->comment}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info">Записей не найдено</div>
                    @endif
                    {{$records->render()}}
                    <form action="{{url('/admin/history/export/records')}}" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-default pull-right" style="margin-right: 22px;">
                            Скачать в формате Excel&nbsp;&nbsp;&nbsp;
                            <span class="glyphicon glyphicon-download-alt"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection