@extends('layouts.admin')

@section('content')
    <div class="admin-statistics">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Все обращения

                        </div>
                        <div class="panel-body">
                            @if($filtered)
                                <a href="{{url('/admin/history/illnesses')}}" class="pull-right btn btn-primary btn-xs">
                                    Показать все
                                </a>
                            @endif
                            <form action="{{url('/admin/history/illnesses')}}" method="get">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <input type="text" name="search" class="form-control"
                                                   placeholder="Введите данные пациента"
                                                   @if($request->search) value="{{$request->search}}" @endif>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="submit">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                        </div><!-- /input-group -->
                                    </div><!-- /.col-lg-6 -->
                                    <div class="col-sm-3">
                                        <label>Статус</label>
                                        <ul class="list-unstyled">
                                            <li class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="new"
                                                           onchange="$(this).parents('form').submit()"
                                                           @if($request->status && $request->status=="new") checked @endif>
                                                    Новое
                                                </label>
                                            </li>
                                            <li class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="active"
                                                           onchange="$(this).parents('form').submit()"
                                                           @if($request->status && $request->status=="active") checked @endif>
                                                    Активное
                                                </label>
                                            </li>
                                            <li class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="archive"
                                                           onchange="$(this).parents('form').submit()"
                                                           @if($request->status && $request->status=="archive") checked @endif>
                                                    Завершённое
                                                </label>
                                            </li>
                                            <li class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="denied"
                                                           onchange="$(this).parents('form').submit()"
                                                           @if($request->status && $request->status=="denied") checked @endif>
                                                    Отклонено
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    @if($domainObj->payment_form=='safe')
                                        <div class="col-sm-3">
                                            <label>Результат</label>
                                            <ul class="list-unstyled">
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="result" value="success"
                                                               @if($request->result && $request->result=="success") checked @endif>
                                                        Успех
                                                    </label>
                                                </li>
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="result" value="fail"
                                                               @if($request->result && $request->result=="fail") checked @endif>
                                                        Возврат
                                                    </label>
                                                </li>
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="result" value="none"
                                                               @if($request->result && $request->result=="none") checked @endif>
                                                        В работе
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                    @if($domainObj->custom('chat'))
                                        <div class="col-sm-3">
                                            <label>Форма</label>
                                            <ul class="list-unstyled">
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="form" value="online"
                                                               onchange="$(this).parents('form').submit()"
                                                               @if($request->form && $request->form=="online") checked @endif>
                                                        Онлайн
                                                    </label>
                                                </li>
                                                <li class="radio">
                                                    <label>
                                                        <input type="radio" name="form" value="offline"
                                                               onchange="$(this).parents('form').submit()"
                                                               @if($request->form && $request->form=="offline") checked @endif>
                                                        Очно
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                </div><!-- /.row -->
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @if($illnesses && $illnesses->count()>0)

                                <div class="table-responsive ">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Дата</th>
                                            <th>Заявитель</th>
                                            <th>Врач</th>
                                            <th>Название</th>
                                            <th>Статус</th>
                                            <th>Изменить</th>
                                            @if($domainObj->custom('disease_history'))
                                                <th>История болезни</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($illnesses as $illness)
                                            <tr>
                                                <td>{{$illness->id}}</td>
                                                <td>{{\Carbon\Carbon::parse($illness->created_at)->format('d.m.Y H:i')}}</td>
                                                <td>{{$illness->user->name or 'Пользователь удалён'}}</td>
                                                <td>{{$illness->doctor()->name or 'Врач удалён'}}</td>
                                                <td>{{$illness->name}}</td>
                                                <td>
                                                    @if($illness->chat)
                                                        <span class="label label-warning">Онлайн</span>
                                                        @if($domainObj->payment_form=="safe")
                                                            @if($illness->result=="success")
                                                                <span class="label label-success">Выполнено</span>
                                                            @elseif($illness->result=="fail")
                                                                <span class="label label-warning">Отказ</span>
                                                            @elseif($illness->result=="conflict")
                                                                <span class="label label-danger">Конфликт</span>
                                                            @else
                                                                <span class="label label-default">В работе</span>
                                                            @endif
                                                        @else
                                                            @if($illness->status=="active")
                                                                <span class="label label-success">В работе</span>
                                                            @elseif($illness->status=="new")
                                                                <span class="label label-warning">Новое</span>
                                                            @elseif($illness->status=="archive")
                                                                <span class="label label-default">Завершено</span>
                                                            @else
                                                                <span class="label label-danger">Отклонено</span>
                                                            @endif
                                                        @endif

                                                    @else
                                                        <span class="label label-info">Очно</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($illness->chat)
                                                        <form action="{{url('/admin/history/illness/update')}}"
                                                              method="post"
                                                              id="form_{{$illness->id}}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="illness_id"
                                                                   value="{{$illness->id}}">
                                                            <select name="status" class="form-control"
                                                                    onchange="$('#form_{{$illness->id}}').submit()">
                                                                <option value="new"
                                                                        @if($illness->status=="new") selected @endif >
                                                                    Новое
                                                                </option>
                                                                <option value="active"
                                                                        @if($illness->status=="active") selected @endif >
                                                                    В работе
                                                                </option>
                                                                <option value="archive"
                                                                        @if($illness->status=="archive") selected @endif >
                                                                    Архив
                                                                </option>
                                                                <option value="denied"
                                                                        @if($illness->status=="denied") selected @endif >
                                                                    Отклонено
                                                                </option>
                                                            </select>
                                                        </form>
                                                    @endif
                                                </td>
                                                @if($domainObj->custom('disease_history'))
                                                    <td>
                                                        @if($illness->diseaseHistory)
                                                            <a href="{{url('/admin/user/disease_history/'.$illness->diseaseHistory->id)}}"><span
                                                                        class="glyphicon glyphicon-list-alt"></span>
                                                                Открыть</a>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="alert alert-info">Обращений не найдено</div>
                            @endif

                            {{$illnesses->render()}}


                            <form action="{{url('/admin/history/export/illnesses')}}" method="post">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-default pull-right" style="margin-right: 22px;">
                                    Скачать в формате Excel&nbsp;&nbsp;&nbsp;<span
                                            class="glyphicon glyphicon-download-alt"></span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection