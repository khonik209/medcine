@extends('layouts.admin')

@section('content')
    <div class="admin-statistics">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Все платежи
                    </div>
                    <div class="panel-body">
                        @if($filtered)
                            <a href="{{url('/admin/history/payments')}}" class="pull-right btn btn-primary btn-xs">Показать
                                все платежи</a>
                        @endif
                        <form action="{{url('/admin/history/payments')}}" method="get">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">

                                        <input type="text" name="search" class="form-control"
                                               placeholder="Введите данные пациента"
                                               @if($request->search) value="{{$request->search}}" @endif>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">Найти</button>
                                        </span>
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if($payments && $payments->count()>0)

                            <div class="table-responsive ">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Дата</th>
                                        <th>Плательщик</th>
                                        <th>Сумма</th>
                                        <th>Статус</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($payments->sortByDesc('created_at') as $payment)
                                        <tr>
                                            <td>{{$payment->id}}</td>
                                            <td>{{\Carbon\Carbon::parse($payment->created_at)->format('d.m.Y H:i')}}</td>
                                            <td>
                                                {{$payment->user->name or 'Пользователь удалён'}}
                                            </td>
                                            <td>{{$payment->sum}}</td>
                                            <td>
                                                <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger btn-xs"
                                                   href="{{url('/admin/history/payment/'.$payment->id.'/delete')}}"
                                                   onclick="return confirm('Вы уверены, что хотите удалить платёж?')"><span
                                                            class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="alert alert-info">Платежей не найдено</div>
                        @endif

                        {{$payments->render()}}

                        <form action="{{url('/admin/history/export/payments')}}" method="post">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default pull-right" style="margin-right: 22px;">
                                Скачать в формате Excel&nbsp;&nbsp;&nbsp;<span
                                        class="glyphicon glyphicon-download-alt"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection