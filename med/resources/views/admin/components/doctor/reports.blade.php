<div class="panel panel-default">
    <div class="panel-heading">
        Отзывы
    </div>
    <div class="panel-body">
        @if($reports->count()>0)
            @foreach($reports as $report)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>{{$report->user->name}}</strong>
                        <small class="pull-right">
                            @if($report->rating>0)
                                <span class="label label-success">+1</span>
                            @else
                                <span class="label label-danger">-1</span>
                            @endif
                            {{\Carbon\Carbon::parse($report->created_at)->format('d.m.Y')}}
                            @if($report->user->id == Auth::id())
                                <a href="{{url('/user/report/delete/{id}')}}"
                                   class="btn btn-danger btn-xs"
                                   onclick="return confirm('Вы уверены, что хотите удалить свой отзыв?')"><span
                                            class="glyphicon glyphicon-trash"></span></a>
                            @endif
                        </small>
                    </div>
                    <div class="panel-body">
                        {{$report->text}}
                    </div>
                </div>
            @endforeach
            {{$reports->render()}}
        @else
            <div class="alert alert-info">У специалиста еще нет отзывов</div>
        @endif
    </div>
</div>