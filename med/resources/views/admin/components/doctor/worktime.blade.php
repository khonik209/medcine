<div class="panel panel-default">
    <div class="panel-heading">
        График работы
    </div>
    <div class="panel-body">
        @if($form && $form->work_time)
            @php
                $week = ['Понедельник','Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота','Воскресенье']
            @endphp
            <ul>
                @foreach($form->work_time['days'] as $key=>$day)
                    <li>{{$week[$key]}}: {{$day['starts']}} - {{$day['ends']}}</li>
                @endforeach
            </ul>
        @endif
    </div>
</div>