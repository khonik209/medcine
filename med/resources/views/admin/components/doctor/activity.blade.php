<div class="panel panel-default">
    <div class="panel-heading">
        Активность
    </div>
    <div class="panel-body">
        @if($activities && $activities->count()>0)
            <div class="table-responsive ">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Action</th>
                        <th>Description</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $activity)
                        <tr>
                            <td>{{\Carbon\Carbon::parse($activity->created_at)->format('d.m.Y H:i:s')}}</td>
                            <td>{{$activity->action}}</td>
                            <td>{{$activity->description}}</td>
                            <td> {{$activity->details}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="alert alert-info">Активность не зафиксирована</div>
        @endif
    </div>
</div>