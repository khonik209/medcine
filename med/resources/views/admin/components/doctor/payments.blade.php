<div class="panel panel-default">
    <div class="panel-heading">
        Платежи
    </div>
    <div class="panel-body">
        @if($payments && $payments->count()>0)
            <div class="table-responsive ">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Дата</th>
                        <th>E-mail</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments->sortByDesc('created_at') as $payment)
                        <tr>
                            <td>{{$payment->id}}</td>
                            <td>{{\Carbon\Carbon::parse($payment->created_at)->format('H:i d.m.Y')}}</td>
                            <td>@if($payment->user){{$payment->user->email}}@else
                                    Пользователь не
                                    найден @endif</td>
                            <td>{{$payment->sum}}</td>
                            <td>
                                <span class="label label-{{json_decode($payment->getStatus())->level}}">{{json_decode($payment->getStatus())->text}}</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="alert alert-info">Платежей не найдено</div>
        @endif
    </div>
</div>