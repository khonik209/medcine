<div class="panel panel-default">
    <div class="panel-body">
        <app-calendar :doctor="{{json_encode($doctor)}}"  :custom_functions="{{json_encode($domainObj->custom_functions)}}" :is_admin="1"
                      :doctors="{{json_encode([])}}">
        </app-calendar>
    </div>
</div>