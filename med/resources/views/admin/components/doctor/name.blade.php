<div class="panel panel-default">
    <div class="panel-body">
        <div class="media">
            <div class="pull-left" href="#">
                <img class="media-object" src="{{$doctor->avatar_url}}" alt="..." style="max-width: 100px">
            </div>
            @if($form)
                <div class="pull-right">
                    <p class="text-muted">
                        Рейтинг
                    </p>
                    <h1>
                        @if($form->rating>0)
                            <span class="label label-success"> + {{$form->rating}}</span>
                        @elseif($form->rating<0)
                            <span class="label label-danger">{{$form->rating}}</span>
                        @else
                            <span class="label label-default">{{$form->rating}}</span>
                        @endif
                    </h1>
                </div>
            @endif
            <div class="media-body">
                <h4 class="media-heading">{{$doctor->name}}</h4>

                @if($lastActivity)
                    <span class="label label-default">Последний раз в сети: {{\Carbon\Carbon::parse($lastActivity->created_at)->format('d.m.Y H:i')}}</span>
                @else
                    <span class="label label-default">Последняя активность не зафиксирована</span>
                @endif
                @if($domainObj->id==1)
                    <br>
                    <strong>Статус:</strong>
                    @if($form->status == 'online')
                        <span class="label label-success">Доктор готов к приему</span>
                    @elseif($form->status == 'offline')
                        Доктор сейчас занят, он сможет принять Вас c
                        <h4>{{\Carbon\Carbon::parse($form->free_from)->format("H:i d-m-Y")}}</h4>
                        по
                        <h4>{{\Carbon\Carbon::parse($form->free_to)->format("H:i d-m-Y")}}</h4>
                    @else
                        <span class="label label-danger">Врач в отпуске.</span>
                        <p>Пожалуйста, попробуйте
                            после {{\Carbon\Carbon::parse($form->holiday_until)->format("d.m.Y")}}</p>
                    @endif
                @endif
            </div>

        </div>
        @include('common.info')
        @include('common.success')
        @include('common.error')
        @include('common.customError')
    </div>
</div>