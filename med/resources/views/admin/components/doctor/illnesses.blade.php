<div class="panel panel-default">
    <div class="panel-heading">
        Обращения
    </div>
    <div class="panel-body">
        @if($illnesses && $illnesses->count()>0)
            @foreach($illnesses as $illness)
                <div class="panel @if($illness->chat) panel-info @else panel-warning @endif dashboard-item">
                    <a href="{{"/illnesses/get/$illness->id"}}" class="panel-body">
                        <div class="media">
                            <div class="pull-left">
                                <img src="{{$illness->user?$illness->user->avatar_url:'/storage/doctors/emptyavatar.png'}}"
                                     class="media-photo" style="max-width:80px">
                            </div>
                            <p class="pull-right text-muted">{{\Carbon\Carbon::parse($illness->created_at)->format('d.m.Y H:i')}}</p>

                            @if($illness->chat)
                                <span class="pull-right label label-info"> Очно </span>
                            @else
                                <span class="pull-right label label-warning"> Онлайн </span>
                            @endif

                            <div class="media-body">
                                <h4 class="title">
                                    {{$illness->user?$illness->user->name: 'Пользователь удалён'}}
                                </h4>
                                <p class="summary">{{$illness->name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            {{$illnesses->render()}}
        @else
            <div class="alert alert-info">Обращений не найдено</div>
        @endif
    </div>
</div>