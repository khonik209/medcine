<div class="panel panel-default">
    <div class="panel-heading">
        Анкета
        <div class="pull-right">
            <a href="{{url('/admin/doctor/'.$doctor->id.'/edit')}}"
               class="btn btn-default btn-xs ">Редактировать профиль</a>
        </div>
    </div>
    <div class="panel-body">
        <p>Email: <strong>{{$doctor->email}}</strong></p>
        @if($doctor->doctor)
            @if($skills->count()>0)
                <p>Специальности:
                    <strong>
                        @foreach($skills as $skill)
                            {{$skill->name}},
                        @endforeach
                    </strong>
                </p>
            @endif
            <p>Стаж, лет: <strong>{{$form->experience}}</strong></p>
            <p>Образование: <strong>{{$form->education}}</strong></p>
            <p>Научная степень: <strong>{{$form->level}}</strong></p>
            <p>Дополнительная информация <strong>{{$form->infomation}}</strong></p>
            @if($domainObj->payment_form=='safe')
                @if($doctor->doctor->document)
                    <p>
                        <a href="/storage/{{$form->document}}"
                           target="_blank">Посмотреть</a>
                    </p>
                @endif
                <form action="{{url('/admin/doctor/verify')}}" method="post" id="verification">
                    {{ csrf_field() }}
                    <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                    <strong>Верификация: {{$form->verified}}</strong>
                    <select name="verified" onchange="$('#verification').submit()">
                        <option value="1" @if($form->verified==1) selected @endif>
                            Проверено
                        </option>
                        <option value="0" @if($form->verified==0) selected @endif>
                            Не проверено
                        </option>
                    </select>
                </form>
            @endif
        @else
            <div class="alert alert-warning">Анкета не заполнена</div>
        @endif

        <form action="{{url('admin/doctor/delete')}}" method="post" class="pull-right">
            {{ csrf_field() }}
            <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
            <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Вы уверены, что хотите удалить врача?');"><span
                        class="glyphicon glyphicon-trash"></span> Удалить
            </button>
        </form>
    </div>
</div>