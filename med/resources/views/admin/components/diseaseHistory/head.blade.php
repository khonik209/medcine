<div class="panel panel-default">
    <div class="panel-body">
        <h2>История болезни № {{$diseaseHistory->id}}</h2>
        <h4>Лечащий врач: <a
                    href="{{url('/admin/doctor/'.$diseaseHistory->doctor->id)}}">{{$diseaseHistory->doctor->name}}</a>
        </h4>
        @if($diseaseHistory->illness)
            <p class="text-muted"><strong>{{$diseaseHistory->illness->name}}</strong></p>
            <p class="text-muted">{{$diseaseHistory->illness->description}}</p>
        @endif
        @include('common.error')
        @include('common.customError')
        @include('common.info')
        @include('common.success')
    </div>
</div>