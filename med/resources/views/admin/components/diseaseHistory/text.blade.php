<div class="panel panel-default">
    <div class="panel-heading">
        Основной текст
    </div>
    <div class="panel-body">
        <p><strong>Дата поступления:</strong> {{\Carbon\Carbon::parse($text->date)->format('d.m.Y H:i')}}
        </p>
        <p><strong>Жалобы:</strong> {{$text->complaint}}</p>
        <p><strong>Результат первичного осмотра:</strong> {{$text->primary_inspection}}</p>
        <p><strong>Первичный диагноз:</strong> {{$text->primary_diagnosis}}</p>
        <p><strong>Окончательный диагноз:</strong> {{$text->final_diagnosis}}</p>
        <p><strong>Назначенное лечение:</strong> {{$text->treatment}}</p>
        <p><strong>План лечения:</strong> {{$text->treatment_plan}}</p>

        <p><strong>Эпикриз: </strong> {{$text->result}}</p>
    </div>
</div>