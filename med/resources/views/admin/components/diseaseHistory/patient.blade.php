<div class="panel panel-default">
    <div class="panel-heading">
        Контакты
    </div>
    <div class="panel-body">
        <p>Имя: <strong><a href="{{url('/admin/user/'.$user->id)}}">{{$user->name}}</a></strong></p>
        <p>Возраст, лет: <strong>{{$user->age}}</strong></p>
        <p>Email: <strong>{{$user->email}}</strong></p>
        <p>Телефон: <strong>{{$user->phone}}</strong></p>
    </div>
</div>