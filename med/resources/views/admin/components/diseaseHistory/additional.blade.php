<div class="panel panel-default">
    <div class="panel-heading">
        Дополнительные обследования
    </div>
    <div class="panel-body">
        @if(count($text->additional_views)>0)
            @foreach($text->additional_views as $key=>$additional_view)
                <ul>
                    <strong>Осмотр {{$key+1}}</strong>
                    <li>
                        <strong>Результат осмотра:</strong> {{$additional_view['inspection']}}
                    </li>
                    <li>
                        <strong>Корректировка
                            лечения:</strong> {{$additional_view['treatment_adjustments']}}
                    </li>
                </ul>
            @endforeach
        @else
            Не проводились
        @endif
    </div>
</div>