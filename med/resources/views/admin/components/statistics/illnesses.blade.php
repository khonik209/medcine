@if($illnesses && $illnesses->count()>0)
    <hr>
    <h4>
        Обращения
        <a href="/admin/history/illnesses" class="btn btn-primary btn-xs">Показать все</a>
    </h4>
    <div class="table-responsive ">
        <table class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Дата</th>
                <th>Заявитель</th>
                <th>Врач</th>
                <th>Название</th>
                <th>Статус</th>
                <th>История болезни</th>
            </tr>
            </thead>
            <tbody>
            @foreach($illnesses as $illness)
                <tr>
                    <td>{{$illness->id}}</td>
                    <td>{{\Carbon\Carbon::parse($illness->created_at)->format('H:i d-m-Y')}}</td>
                    <td>{{$illness->user->email or 'Не найден (удален)'}}</td>
                    <td>
                        {{$illness->doctor()->name or 'Не найден (удален)'}}
                    </td>
                    <td>{{$illness->name}}</td>
                    <td>
                        @if($illness->chat)
                            <span class="label label-success">Онлайн</span>
                            @if($domainObj->payment_form=="safe")
                                @if($illness->result=="success")
                                    <span class="label label-success">Выполнено</span>
                                @elseif($illness->result=="fail")
                                    <span class="label label-warning">Отказ</span>
                                @elseif($illness->result=="conflict")
                                    <span class="label label-danger">Конфликт</span>
                                @else
                                    <span class="label label-default">В работе</span>
                                @endif
                            @else
                                @if($illness->status=="active")
                                    <span class="label label-success">В работе</span>
                                @elseif($illness->status=="new")
                                    <span class="label label-warning">Новое</span>
                                @elseif($illness->status=="archive")
                                    <span class="label label-default">Завершено</span>
                                @else
                                    <span class="label label-danger">Отклонено</span>
                                @endif
                            @endif

                        @else
                            <span class="label label-info">Очно</span>
                        @endif
                    </td>
                    <td>
                        @if($illness->diseaseHistory)
                            <a href="{{url('/admin/user/disease_history/'.$illness->diseaseHistory->id)}}">Открыть</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert alert-info">Обращений не найдено</div>
@endif