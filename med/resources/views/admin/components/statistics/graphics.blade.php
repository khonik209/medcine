    <div class="row">
        <div class="col-xs-12 col-md-3">

            <ul class="hidden-xs hidden-sm nav nav-pills nav-stacked report-tabs"
                role="tablist">
                <li class="active">
                    <a href="#illnesses" role="tab" data-toggle="tab">Обращения</a>
                </li>
                <li>
                    <a href="#payments" role="tab" data-toggle="tab">Платежи</a>
                </li>
            </ul>

            <ul class="hidden-md hidden-lg nav nav-tabs report-tabs" role="tablist">
                <li class="active">
                    <a href="#illnesses" role="tab" data-toggle="tab">Обращения</a>
                </li>
                <li>
                    <a href="#payments" role="tab" data-toggle="tab">Платежи</a>
                </li>
            </ul>

        </div>
        <div class="col-xs-12 col-md-9">
            <!-- TAB NAVIGATION -->
            <!-- TAB CONTENT -->
            <div class="tab-content">
                <div class="active tab-pane fade in" id="illnesses">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <canvas id="jsStartChart" height="200" width="500"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="payments">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <canvas height="200" width="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                chartData = [];
                illnesses = [];
                payments = [];
                dates = [];

                @for($i = $startDate; $i<\Carbon\Carbon::now();$i->addDay())
                illnesses.push('{{$illnesses->where('created_at','<=',$i)->count()}}');
                payments.push('{{$payments->where('created_at','<=',$i)->count()}}');
                dates.push('{{$i->format('d.m.Y')}}');
                @endfor

                    chartData['illnesses'] = {
                    labels: dates,
                    datasets: [
                        {
                            backgroundColor: "rgba(0, 200, 243, 0.25)",
                            borderColor: "#00C8F3",
                            label: 'Обращения',
                            data: illnesses,
                        }
                    ]
                };
                chartData['payments'] = {
                    labels: dates,
                    datasets: [
                        {
                            backgroundColor: "rgba(169, 111, 247, 0.25)",
                            borderColor: "#A96FF7",
                            label: 'Платежи',
                            data: payments,
                        }
                    ]
                };
            </script>
        </div>
    </div>