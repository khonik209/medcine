<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Документы</h4>
        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#addDocument">
         <span class="glyphicon glyphicon-plus"></span>Добавить
        </button>
    </div>
    <div class="panel-body">

        @if(count($urls)>0)
            <table class="table">
                @foreach($urls as $url)
                    <tr>
                        @if($url->type == 'image')
                            <td>
                                <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                    <span class="glyphicon glyphicon-picture"></span>
                                </a>
                            </td>
                            <td>
                                <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                    Открыть
                                </a>
                            </td>
                            <td>
                                <a role="button" data-toggle="modal" data-target="#file{{$url->id}}">
                                    <p>{{$url->name}}</p>
                                </a>
                            </td>

                            <div id="file{{$url->id}}" class="modal fade bs-example-modal-lg" tabindex="-1"
                                 role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <img src="/storage/{{$url->url}}">
                                    </div>
                                </div>
                            </div>
                        @elseif($url->type == 'doc')
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <span class="glyphicon glyphicon-file"></span>
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    Скачать .doc
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <p>{{$url->name}}</p>
                                </a>
                            </td>
                        @elseif($url->type == 'pdf')
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <span class="glyphicon glyphicon-book"></span>
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    Скачать PDF
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <p>{{$url->name}}</p>
                                </a>
                            </td>
                        @elseif($url->type=='dicom')
                            <td>
                                <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                    <span class="glyphicon glyphicon-th"></span>
                                </a>
                            </td>
                            <td>
                                <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                    Открыть просмотрщик
                                </a>
                            </td>
                            <td>
                                <a href="/chat/dicom/{{$url->id}}" target="_blank">
                                    <p>{{$url->name}}</p>
                                </a>
                            </td>
                        @else
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <span class="glyphicon glyphicon-cloud-download"></span>
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    Скачать
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="/storage/{{$url->url}}">
                                    <p>{{$url->name}}</p>
                                </a>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </table>
            {{$urls->render()}}
        @else
            <div class="alert alert-warning">Документы еще не добавлены</div>
        @endif
    </div>
</div>