@if(isset($illness) && $illness->user)
    @if($illness->diseaseHistory)

        <a href="{{url('/admin/disease_history/'.$illness->diseaseHistory->id)}}" class="btn btn-primary">
            Открыть историю болезни &nbsp; <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    @endif
    <div class="clearfix"></div>
@endif