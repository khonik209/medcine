<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <p class="text-center">
                <span>{{$illness->name?$illness->name:'Обращение #'.$illness->id}}</span>
                <span class="text-muted pull-left">{{\Carbon\Carbon::parse($illness->created_at)->format('H:i d.m.Y')}}</span>
            </p>

            <form action="{{url('/illnesses/edit')}}" class="form-inline" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="illness_id" value="{{$illness->id}}">
                <div class="form-group">
                    <label for="status" class="control-label">Статус:</label>
                    <select name="status" class="form-control" id="status"
                            onchange="$(this).parents('form').submit()">
                        <option value="new"
                                @if($illness->status=="new") selected @endif >
                            Новое
                        </option>
                        <option value="active"
                                @if($illness->status=="active") selected @endif >
                            В работе
                        </option>
                        <option value="archive"
                                @if($illness->status=="archive") selected @endif >
                            Архив
                        </option>
                        <option value="denied"
                                @if($illness->status=="denied") selected @endif >
                            Отклонено
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="doctor" class="control-label">Ответственный:</label>
                    <select id="doctor" class="form-control" onchange="$(this).parents('form').submit()"
                            name="doctor_id">
                        <option @if($illness->doctor_id==$domainObj->getAdmin()->first()->id) selected
                                @endif value="{{$domainObj->getAdmin()->first()->id}}">{{$domainObj->getAdmin()->first()->name}}</option>
                        @foreach($domainObj->getDoctors()->get() as $doctor)
                            <option @if($illness->doctor_id==$doctor->id) selected
                                    @endif value="{{$doctor->id}}">{{$doctor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
            @if(!$illness->record)
                <hr>
                <create-record :illness="{{json_encode($illness)}}"></create-record>
            @else
                <p>
                    <strong>
                        Запись к {{$illness->record->doctor()->name}}
                        на {{\Carbon\Carbon::parse($illness->record->start_date)->format('d.m.Y H:i')}}

                    </strong>
                </p>
            @endif
        </div>
        <div class="panel-body">
            <p><strong>{!! $illness->name?$illness->name:'<i>Название отсутствует</i>'!!}</strong></p>
            <p>{!! $illness->description?$illness->description:'<i>Описание отсутствует</i>'!!}</p>
        </div>
        <div class="panel-footer">

            <a class="btn btn-primary btn-xs" href="{{url('/illnesses/get/'.$illness->id)}}">Подробнее</a>
            <form action="{{url('/illnesses/delete')}}" method="post" class="pull-right">
                {{ csrf_field() }}
                <input type="hidden" name="illness_id" value="{{$illness->id}}">
                <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Вы уверены, что хотите удалить обращение?')">
                    <span class="glyphicon glyphicon-trash"></span>
                    Удалить обращение
                </button>
            </form>
            <div style="clear:both"></div>
        </div>
    </div>
</div>