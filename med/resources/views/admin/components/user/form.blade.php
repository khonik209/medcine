<div class="panel panel-default">
    <div class="panel-heading">
        Профиль
        <div class="pull-right">
            <a href="{{url('/admin/user/'.$user->id.'/edit')}}"
               class="btn btn-default btn-xs ">
                <span class="glyphicon glyphicon-pencil"></span> Редактировать профиль</a>
        </div>
    </div>
    <div class="panel-body">
        <p>Email: <strong>{{$user->email}}</strong></p>
        <p>Телефон: <strong>{{$user->phone}}</strong></p>
        <p>Возраст, лет: <strong>{{$user->age}}</strong></p>

        <form action="{{url('admin/user/delete')}}" method="post" class="pull-right">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <button type="submit" class="btn btn-danger btn-xs"
                    onclick="return confirm('Вы уверены, что хотите удалить пациента?');"><span
                        class="glyphicon glyphicon-trash"></span> Удалить пользователя
            </button>
        </form>
    </div>
</div>