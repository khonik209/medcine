@php
    $q = $answer->question;
    if($q){
    $type = $q->type;
    } else {
    $type='';
    }
    $value = $answer->value;
@endphp

@if($type=="text")
    <p>{{$q->label}}: <strong>{{$value}}</strong></p>
@elseif($type=="textarea")
    <p>{{$q->label}}: <strong>{{$value}}</strong></p>
@elseif($type=="radio")
    <p>{{$q->label}}: <strong>{{$q->options[$value]}}</strong></p>
@elseif($type=="checkbox")
    <p>{{$q->label}}: <strong>{{$q->options[$value]}}</strong></p>
@elseif($type=="select")
    <p>{{$q->label}}: <strong>{{$q->options[$value]}}</strong></p>
@elseif($type=="file")
    <p><a href="{{\Illuminate\Support\Facades\Storage::disk('s3')->url($value)}}" target="_blank">{{$q->label}}</a></p>
@endif