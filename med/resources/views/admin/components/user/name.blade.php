<div class="panel panel-default">
    <div class="panel-body">
        <div class="media">
            <div class="pull-left" href="#">
                <img class="media-object" src="{{$user->avatar_url}}" alt="..." style="max-width: 100px">
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{$user->name}}</h4>

                @if($lastActivity)
                    <span class="label label-default">Последний раз в сети: {{\Carbon\Carbon::parse($lastActivity->created_at)->format('d.m.Y H:i')}}</span>
                @else
                    <span class="label label-default">Последняя активность не зафиксирована</span>
                @endif
            </div>

            <button class="pull-right btn btn-primary btn-xs js-email-btn">Написать...</button>
        </div>
        <div class="hidden js-email-send">
            <hr>
            <form action="{{url('/admin/user/email')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="form-group">
                    <label for="text">Текст сообщения</label>
                    <textarea id="text" name="text" class="form-control"></textarea>
                </div>
                <button class="btn btn-primary">Отправить email</button>
            </form>
        </div>
        @include('common.info')
        @include('common.success')
        @include('common.error')
        @include('common.customError')
    </div>
    @push('scripts')
        <script>
            $('.js-email-btn').click(function () {
               $('.js-email-send').toggleClass('hidden')
            });
        </script>
    @endpush
</div>