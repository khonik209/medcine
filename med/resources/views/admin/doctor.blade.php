@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/doctors')}}">Все врачи</a></li>
            <li class="active">{{$doctor->name}}</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @include('admin.components.doctor.name')
            </div>
            <div class="col-md-6">
                @include('admin.components.doctor.form')
            </div>
            @if($domainObj->custom('records'))
                <div class="col-md-6" id="full-calendar">
                    @include('admin.components.doctor.calendar')
                </div>
            @endif
            <div class="col-md-6">
                @include('admin.components.doctor.illnesses')
            </div>
            @if($domainObj->custom('payments'))
                <div class="col-md-6">
                    @include('admin.components.doctor.payments')
                </div>
            @endif
            <div class="col-md-6">
                @include('admin.components.doctor.reports')
            </div>
            <div class="col-md-6">
                @include('admin.components.doctor.worktime')
            </div>
            @if($domainObj->id==1)
                <div class="col-xs-12">
                    @include('admin.components.doctor.activity')
                </div>
            @endif
        </div>
    </div>
    @push('styles')
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    @endpush
@endsection