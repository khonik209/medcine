@extends('layouts.admin')

@section('content')
    <div class="chats">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Список чатов
                    </div>
                </div>
                @include('common.error')
                @include('common.customError')
                @include('common.info')
                @include('common.success')
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body" id="chat-list">
                        <chats :user="{{Auth::user()}}"></chats>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection