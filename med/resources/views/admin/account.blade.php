@extends('layouts.admin')

@section('content')
    <div class="admin-account">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Настройки кабинета
                        </div>
                        <div class="panel-body">
                            <p>Подписка заканчивается: <span
                                        class="label label-info">{{\Carbon\Carbon::parse($domainObj->active_to)->format('d-m-Y')}}</span>
                            </p>
                            <p>Обращений за текущий период:
                                @if(count($domainObj->subscriptions()->get())>0)
                                    @if($domainObj->subscriptions->last()->plan->limit>0)
                                        <span class="label label-info"> {{$domainObj->subscriptions->last()->illnesses}}
                                            / {{$domainObj->subscriptions->last()->plan->limit}} </span>
                                    @else
                                        <span class="label label-info"> {{$domainObj->subscriptions->last()->illnesses}}</span>
                                    @endif
                                @else
                                    <span class="label label-warning">Пробный период </span>
                                @endif
                            </p>
                            @include('common.info')
                            @include('common.success')
                            @include('common.error')
                            @include('common.customError')
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Профиль администратора
                        </div>
                        <div class="panel-body">
                            @include('admin.components.account.main')
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Информация о компании
                        </div>
                        <div class="panel-body">
                            @include('admin.components.account.company')
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Список специальностей врачей
                        </div>
                        <div class="panel-body" id="specialities_container">
                            <specialities :skills="{{$skills}}"></specialities>
                        </div>
                    </div>
                </div>
                @if($domainObj->custom('payments'))
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Настройки платежей
                            </div>
                            <div class="panel-body">
                                @include('admin.components.account.payments')
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                @endif
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Продление подписки
                        </div>
                        <div class="panel-body">
                            @include('admin.components.account.subscription')
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Управление модулями
                        </div>
                        <div class="panel-body">
                            @include('admin.components.account.functions')
                        </div>
                    </div>
                </div>
                @if($domainObj->custom('custom_form'))
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Вопросы анкеты
                            </div>
                            <div class="panel-body" id="form-questions-container">
                                <form-questions :questions="{{$questions}}"></form-questions>
                            </div>
                        </div>
                    </div>
                @endif
                @if($domainObj->custom('records'))
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Часы работы клиники
                            </div>
                            <div class="panel-body">
                                @include('admin.components.account.businessHours')
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
@endsection