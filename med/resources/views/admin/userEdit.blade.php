@extends('layouts.admin')

@section('content')
    <div class="admin-doctor">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>
                    {{$user->name}}
                </h4>
            </div>
            @include('common.error')
            @include('common.customError')
            @include('common.info')
            @include('common.success')
            <div class="panel-body">
                <h4>Анкета</h4>
                <form class="form-horizontal" role="form" action="{{url('/admin/user/edit')}}"
                      method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">ФИО</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="Иванов Иван Иванович" value="{{$user->name}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" disabled
                                   placeholder="ivanov@gmail.ru" value="{{$user->email}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">Телефон</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="phone" name="phone"
                                   placeholder="+7(123)456-78-90" value="{{$user->phone}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="age" class="col-sm-2 control-label">Возраст, лет</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="age" name="age"
                                   placeholder="57" value="{{$user->age}}">
                        </div>
                    </div>
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection