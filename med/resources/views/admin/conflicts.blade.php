@extends('layouts.admin')

@section('content')
    <div class="admin-conflicts">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-heading">
                    <h2>Конфликты</h2>
                </div>
                @include('common.info')
                @include('common.success')
                @include('common.error')

                @if(count($conflicts)>0)
                    <div class="table">
                        <table class="table table-responsive table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Пациент</th>
                                <th>Врач</th>
                                <th>Описание проблемы</th>
                                <th>История сообщений</th>
                                <th>Прикреплённые документы</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($conflicts as $conflict)
                                <tr>
                                    <td>{{$conflict->id}}</td>
                                    <td>{{$conflict->user->email}}</td>
                                    <td>{{\App\User::find($conflict->doctor_id)->email}}</td>
                                    <td>{{$conflict->description}}</td>
                                    <td><a href="#" data-toggle="modal" data-target="#message{{$conflict->id}}">Сообщения</a>
                                    </td>
                                    <td><a href="#" data-toggle="modal"
                                           data-target="#file{{$conflict->id}}">Документы</a>
                                    </td>
                                    <td><a href="#" data-toggle="modal" data-target="#judge{{$conflict->id}}">Принять
                                            решение</a></td>
                                </tr>
                                {{-- Сообщения --}}
                                <div class="modal fade" id="message{{$conflict->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;
                                                </button>
                                                <h4 class="modal-title">История сообщений. Обращение
                                                    №{{$conflict->id}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                @if(count($conflict->chat->messages)>0)
                                                    <ul>
                                                        @foreach($conflict->chat->messages as $message)
                                                            <li><strong>{{$message->author}}
                                                                    :</strong> {{$message->text}}</li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    История сообщений пуста
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                {{-- Документы --}}
                                <div class="modal fade" id="file{{$conflict->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;
                                                </button>
                                                <h4 class="modal-title">Документы по обращению
                                                    #{{$conflict->id}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                @if(count($conflict->chat->files)>0)
                                                    <ul>
                                                        @foreach($conflict->chat->files as $file)
                                                            <li>
                                                                <small>{{$file->type}}</small> {{$file->name}} </li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    Документы не были прикреплены
                                                @endif
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                                {{-- Решение --}}
                                <div class="modal fade" id="judge{{$conflict->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">&times;
                                                </button>
                                                <h4 class="modal-title">Решение</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="/admin/conflict/solve" method="post">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="illness_id" value="{{$conflict->id}}">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="result" value="doc">
                                                            Решение в пользу
                                                            <strong>Врача: {{\App\User::find($conflict->doctor_id)->name}}</strong>
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="result" value="pac">
                                                            Решение в пользу
                                                            <strong>Пациента: {{$conflict->user->name}}</strong>
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-info"
                                                                onclick="return confirm('Вы уверены?');">Принять
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="alert alert-info">На данный момент нерешённых конфликтов нет</div>
                @endif
            </div>
        </div>
    </div>
@endsection