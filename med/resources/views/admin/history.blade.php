@extends('layouts.admin')

@section('content')
    <div class="admin-statistics">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-heading">
                    История
                </div>
            </div>
            @if($domainObj->custom('records'))
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <a class="btn btn-default btn-block" href="{{url('/admin/history/records')}}">
                                <img class="img" style="max-width: 50px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAMXSURBVGhD7ZlPqExRHMdHlFKIbDyykB2rJ8/CRlLYCaWsRFk8VnqJ3kayYIFevCRvQbKZiLERemZhGvPXaOqJKUVvYSWhLKTG5zv95jYPM2+45x4zr/utb+ec3z33+7vfd/7cM+8mYsSIEWMGCoXCBnioXC4f8clSqbTNHiE8ENyJ4I9isfiO8q1PkvM75Vl7lHBAaALBe9b0CnIPwzfWDAdM3BatKfEbtDeLqkccOwDfKxYaCM0wQn0ok8ksFlWPMkYZnREfmDNGvEwtnwhtJJlMzkdgFL6EX0X+StoSX/M+2WvdAnD9mV3/a7K9D5hMAOJuRoSbjyH2EY60vJxewDpMWbcASkZ8otm3G9L/pOmtMZkA6LlZI9z8kAQXVSfpOkZhrd7uxMfy+fz6RqcWKJmSWrMryEDkRhA/j8ArOAjv0r5pl/4IJYMNIxgfMONtqX6djBBzM7UQWoTAI0tUp95xsSuZklpd66VxXzuqH2VbI01IU9rW/HeQZAVMIda1kZ4akVYgNOv2q2RKas2u0MkIWm7WSK1WW4jANZJ8tmQNEtNpdNi6BVAy4j25a42SYJqEe5gK20Vik5Y4ad0CcK033yMIPYEXVEdoCCObcrncaurHVTY6eUBoI9x8AiPTlPtkivKWXfICZyOiIwpiV+EXOOv26xrkmxunX+dGstnsckYkJWr/r1Qqq+xSpCCfs8W+FGrBB1tvkwiftm6Rw4WRcwhMwUGNhEjsjpnpeO5yAXK4GRFufgovqY6JXQjvYNtdoveK5m6jU4Qgt7MX4mH4CQMjME+9Pxd7vV6fh4FTiFShfiF6NeJsarVCJnwbacKJkXQ6vYA1sRGhSZG1ovPWFsWtS2RwNiLcvBJOIfjb9gsvW7fIQG5ni30cPq9Wq8sspNiVvjPCwz7Wg1v9oKgpRWywr6YWQrvhN0TGKCuU/bvYWej7EXmAkQ++jTgbkVbIhG8j5HN7+hV+NdJMYon697NCy7D33Yee6wjft6ZXkPcodPPpjUX/Xz+GwjP2KOGhf1xzRPH+eRoTW+0RYsSIESOR+AkMrQu8eBFe6wAAAABJRU5ErkJggg==">
                                Открыть список записей
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a class="btn btn-default btn-block" href="{{url('/admin/history/illnesses')}}">
                            <img class="img" style="max-width: 50px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAWTSURBVHhe7ZpbaFxVFIbj/X7De1UUQVuxLyKooFEogkUL9YJSS33QVgu+KKiIVUFUlFIRQaqCKHjDmghqH4qiOBUnl8ltQshDiIKKFcSHGGylapvE79+sPezMnDOXJJM5Z2Z+WOy91l5n5v/X2ee+O9KM4eHhTUNDQxODg4N/0nbjr7Ch5gdi1yJ8FuFz3vB/In6xpTQ3EPuRCd/W19d3Lm1PSxUhKMDj8vv7+09tqSIg9BaE6hA4TLtRsZYrAiKfkdh2EZqhCCMjI5dBdjekf8X2DAwMrLahssjn85ewnS6D6S0CIk6H5G8mwhn+AUjfaCmRMPE/W/4vtm36isDevs9IfieS9N8zP7YIoXisR2Lx03k4QGyzEfxM/tzc3JH037VYSRGixNtQOs8JEFoBuWkjvl2xuCKUE+/B+LM2riLcq1jiiwCxddghEcTiirCJ1ovPRon3IO85y4ssAjbR29t7gktOAiDpp663kiIEVlZ8JpM5mpzuIH9eEegPW/xBt0GjEYgX0dewf82PK8I6t2EEQvFso8vjm7ZNoQj0X7Tx591GjQQkSk5atOux2CLQRl4disWTc7Xi9N05gXaGNoMdlI+tcRs2ChAqEe+BX1KErq6uo4gVikDb6ZJBnHgP4o8S/8fGVYgXbKgxgESJ+PHx8ZPpbx4bGzvDcqoqQiXxHqOjo+eQs4b7joss1BhAMhTvjksB/2GL53O53JmKMV5NEb62fqz4xACSkeIFngfOJj5h4xkLVyyCjH66xXsQPx8bJOdTCzkQqjgTKEDZZ4eGAoJx0/5z/N06/i0UC/JqOjEmBhCL3fP4Pxj575uyCBAqO+05I19K3D3K0jZXESBSIp5L0Un4dyD0WJcEFlIEcm8nN7lFgEDcMb/N4nsmJyePs3BJEcKxOJBbqQh/6NrvkpcT/HHstEfoShEzgh9a2CEsAmf0KyxcFuSWFMFum79UjN98wCUuF/jjsse8AKnVjO0jZ96lTrCPHFVPXV36+K2/7T9V1B06vOh/IZ/xhyy1/ogSr72B343/hqanSwSKW3fBCMXTfoW5mUDrP5lN5/P5Cyy9vuBPI/e8jmX8KSO2KyzCYlC059+hoEcoRj9P/D/aEdrrLb2+4I/KTnti1zLmXnnRX3QRosTbkEOxX1dEiad/ItYZElmqIlQSv6yIEi/gb7f4W3FFwD6ulXwqxAvEOj1R2pcs7EDsOmya+CyCql7QkBrxHlzqbmZsP/aBhQpgu1UIWmtuRSRKPASeMiLhMX8M9j7+ky7JkM1mT6n3CW9ZAZHbMP9NPjzmz8L31+CXLbxoJEq8AIm8kXnEQgUw5e9ayiIkTnwulzvPxE3pJSR2PKJX2rADpO9ciiIkTrxAAS43QhPyIbjT/CdcgiEsAlbzq+dEihf0gAGxA9gMJPUx8x7ssIgS22ppDr4I2KxmioUrIrHiPSC3y8i5PQvh++nrhPi2SwhA/Abi682tiMSLF0RSBCE65T9e6GkrfLuzEKRCvAcEs1aEVyy0KKRKfNHihIPFV4FakVrxtPut1Svt0yylJqRKvFZSQNJ/surRPQHk3SID2m9qOdMLqRIvMNX9Kq5hvzKD9kJiWtOneD+iqnqyS514AcKPGeHXLeSAv4qxHzVGuw9xZZ/wUileYAZcBWHd9GgR090WdtBnbGJ7MYlSIb5FaMlXWn7jplSK94C8X3o2rwjh4oQi28s2W2i1qnsHpheViqdPvMBe1RI1/7r5EHt0g9740i+szMBulVh8/9qr2F5NrXjIu/t+TIuMnCDE+jU38xYn6EUIsS3YJ5je1+/ErrHhdCEUj4inFaPdiv877QzWy3ngSpfcbIgSH6LW63+qUEl8U6Mtvi2+Lb61xCN4Y8uKFxD+V8uKF3Rbq0PA3DZaBx0d/wNURYgrGZtjRgAAAABJRU5ErkJggg==">
                            Открыть список обращений
                        </a>
                    </div>
                </div>
            </div>
            @if($domainObj->custom('payments'))
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <a class="btn btn-default btn-block" href="{{url('/admin/history/payments')}}">
                                <img class="img" style="max-width: 50px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAUcSURBVHhe7ZppaF1FHMXjEqlr64IWN1QUigoiiqhUxQ8WxWhVxEpbP0itSBHBftCqqAVFpSJKg6DQL4oWmpK4YIwW6RMNL/tiDCqEqljcamu0CqXaJv7O89zHrb19S3Jf3rXMgcPM/Gc7/7lz587Mew0BAQEBAQEBAQEBAQEBAQHTxuTk5KEDAwN39/f3t/f19fVngeB9uLylpeUwy5weRkdHj6Dhe2g0R/gr4U7CTwlXELYSThKOwwGYKGoGKQ3j0gQ32oWpg0bmwy12chvcAN+AP7sTsTmXyx3uKnUHehrR97K0MTuX2lwdNH1o4An4N9xOg8viTiqO7ROoGdFoc2YgTWjTTGi3qXLk8/kTqPwRlfV0c0NDQ6c5ax9QZhiOOJk5SJs0OlkZcHgelcbgBFxdaiFxB2NMs0uySPtR+QOi8AKoxexPeLvNB4Qa9yzJLKXRckuDgg9QYQ/cqtGzuSQOigGgUCN8xRV6cP5UZ5XF/34AvNhtduH1rOyznFUR1HjZEa4jyuojsxVqsXuMHd0hNleMpA4YyPk9PT1zFSe8YHBw8HzH5ypPccKjqHej4rVEkr590NvbeyUFrnWyaiR1gHPfwScVJ68Ntjq+WnmKEzaRnogGqlaQNtHJ9JHUAc6dFG2Y2EIfIyoum/IUF2rtvFCXAagEGghm34VssObYVBNkbgD4wlxN+Q64l0HQwruHeBvhmS6SKqrVVzUq6UCLKw42wU47vQu+Rr2V8E2owfipu7v7dFdJDZXomxZKdeDD0mL4mR3/Ha4aGRk53kUKYFZcj10zobBYpolS+lJBUgfaS2C7D6eiI7SOzY/A2fFFMQ7y1kOdOmfblAqS9KWKeAdM4eOIP4QTP0I98a/hCjZbRxYKA/Lfgm1OFoFtpepo32BTKpA20cn0ocYRrif9NPHoFuZzuCT6FMYR3xjFQd2XoNaC4mcyDUif6GS64BN2FoK32mlN9Tzv803ldpR6BSj/TvS0qXMuaV2xfVgokCJqMgDDw8MnI/h1qHdWjnfwTb/G2WURHwDtBaivW8zf4DwXSQ01GQBE30mje+EG4hfbXDWofz/cDX+BU96OlwLt1uYV6OzsPNbRquEZoFVfs6ePxfNsZ6UO+vgK9jpZf2ia4/MXdv5VfTKdlTpYbM+jH13wrLOpvmCxW4TTf8h5uJZ04j1eCrwULqWPLfS3m4G4yBLqA4ToxmmtHZ8x4ryu62+xjPpA+3uEdFlQG0/m3mpIvUddtyMp/0Ck/MLprFOpABHXwW04oM/lOuLvJm2D/7sviIM6iz0AC23KPvRjKYIfR7gOON8TXiXnCN+udgCw69T4V1K9TAInTkTwewjXU9tMesq3PnKaNrS93mRTtoHYy+A3cALRz0VnAW2WsD3Iu7nfdTvlGv3OLrCpCPKe8kBme/qzNT4aoS9AveticeX1tjk6Fq+xuQicv015cBcDdZeeumYNaf0oq1foA71SLp4taKOBwGcRul1OENf7LmfaceZmwjuwfUsYrQU7sJ/h6tEU/xJq1oy5bvwzltcr5eK1A6K0t3+4HBGlp9IMN8LiKRAOwSb/nN5M2ei+T06M0/4NxC+HugrbgW0N4SqoY3Ph93vdGWgGkPcifB6bTpUz8+Tp8AcJqYI74cfUewbR+x2G9JRxYBFlbtUlic2a7ldQB/O/7RDXJ3K5s+sHTUVEn1OO+o9A/HZnqujq6jpFlyI4n7k/VQQEBAQEBAQEBAQEHKxoaPgH/88wl0ZYIZwAAAAASUVORK5CYII=">
                                Открыть список платежей
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection