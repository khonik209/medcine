@if (session('error'))
    <!-- Список ошибок формы -->
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Ошибка!</strong>

        {!! session('error') !!}
    </div>
@endif