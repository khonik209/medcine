<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Heal IT</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="/colid/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="/colid/images/favicon.ico"/>
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="/colid/css/bootstrap.min.css">
    <link rel="stylesheet" href="/colid/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/colid/css/themify-icons.css">
    <link rel="stylesheet" href="/colid/css/magnific-popup.css">
    <link rel="stylesheet" href="/colid/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="/colid/css/normalize.css">
    <link rel="stylesheet" href="/colid/style.css">
    <link rel="stylesheet" href="/colid/css/responsive.css">
    <script src="/colid/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body data-spy="scroll" data-target="#primary-menu">

<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<!--Mainmenu-area-->
<div class="mainmenu-area" data-spy="affix" data-offset-top="100">
    <div class="container">
        <!--Logo-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand logo">
                <h2>Heal IT</h2>
            </a>
        </div>
        <!--Logo/-->
        <nav class="collapse navbar-collapse" id="primary-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#home-page">Heal IT</a></li>
                <li><a href="#service-page">Преимущества</a></li>
                <li><a href="#feature-page">Возможности</a></li>
                <li><a href="#price-page">Цены</a></li>
                {{--<li><a href="#team-page">Team</a></li>--}}
                {{--<li><a href="#faq-page">FAQ</a></li>--}}
                <li><a href="#blog-page">Примеры работ</a></li>
                <li><a href="#contact-page">Написать нам</a></li>
            </ul>
        </nav>
    </div>
</div>
<!--Mainmenu-area/-->
<!--Header-area-->
<header class="header-area overlay full-height relative v-center" id="home-page">
    <div class="absolute anlge-bg"></div>
    <div class="container">
        <div class="row v-center">
            <div class="col-xs-12 col-md-7 header-text">
                <h2>Современная IT поддержка для вашей клиники</h2>
                <p>
                    Комплексное создание интернет ресурсов для медицинских учреждений. Современный и представительный
                    сайт, система управления клиникой, сбор статистики пациентов и врачей.
                </p>
                <a href="#feature-page" class="button white">Подробнее</a>
            </div>
            <div class="hidden-xs hidden-sm col-md-5 text-right">
                <div class="screen-box screen-slider">
                    <div class="item">
                        <img src="/colid/images/healit/Screenshot_1.png" alt="" style="max-width: 180px;">
                    </div>
                    <div class="item">
                        <img src="/colid/images/healit/Screenshot_2.png" alt="" style="max-width: 180px;">
                    </div>
                    <div class="item">
                        <img src="/colid/images/healit/Screenshot_3.png" alt="" style="max-width: 180px;">
                    </div>
                    <div class="item">
                        <img src="/colid/images/healit/Screenshot_4.png" alt="" style="max-width: 180px;">
                    </div>
                    <div class="item">
                        <img src="/colid/images/healit/Screenshot_5.png" alt="" style="max-width: 180px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--Header-area/-->
@include('common.success')
@include('common.customError')
@include('common.error')
<!--Feature-area-->
<section class="gray-bg section-padding" id="service-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/service-icon-1.png" alt="">
                    </div>
                    <h4>Интуитивно понятно</h4>
                    <p>
                        Современный интерфейс, в котором сочетается большая информативность, простота использования,
                        наглядность предоставляемой информации
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/service-icon-2.png" alt="">
                    </div>
                    <h4>Высокая скорость работы</h4>
                    <p>
                        Построение платформы с использованием современных технологий, которые обеспечивают высокую
                        надежность и скорость работы системы
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/service-icon-3.png" alt="">
                    </div>
                    <h4>Индивидуальный подход</h4>
                    <p>
                        Широкий ассортимент функциональных возможностей позволяет настраивать платформу непосредственно
                        под нужды вашей клиники. Индивидуальная доработка возможностей под заказ.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Feature-area/-->

<section class="angle-bg sky-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Порядок работы</h1>
                <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Прототипирование</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <ol>
                                            <li>Сбор и анализ информации о ваших бизнес-процессах и желаниях.</li>
                                            <li>Согласование всех предложений по дизайну, размещению и набору
                                                возможностей
                                                будущей системы
                                            </li>
                                            <li>Составление и согласование полной сметы работ</li>
                                        </ol>
                                    </div>
                                    {{--<div class="caption-button" data-animation="animated fadeInUp">
                                        <a href="#" class="button">Read more</a>
                                    </div>--}}
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/pexels-photo-265152.jpeg" alt="" style="max-height: 170px">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo two" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/pexels-photo-401683.jpeg" alt="" style="max-height: 170px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Создание "посадочной" страницы</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <p>
                                            Исходя из согласованного прототипа, мы создаем для вас красивый и
                                            современный лендинг, который будет привлекать новых клиентов и хорошо
                                            индексироваться поисковыми системами
                                        </p>
                                    </div>
                                    {{--<div class="caption-button" data-animation="animated fadeInUp">
                                        <a href="#" class="button">Read more</a>
                                    </div>--}}
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/pexels-photo-270632.jpeg" alt="" style="max-height: 170px">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo two" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/pexels-photo-209151.jpeg" alt="" style="max-height: 170px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Создание личного кабинета</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <p>
                                            Актуальное расписание врачей, запись на прием, проведение онлайн
                                            консультаций, прием платежей, email рассылки клиентам и многое другое. Мы
                                            подключаем и настраиваем личный кабинет с тем набором функций, которые
                                            необходимы именно для вас.
                                        </p>
                                    </div>
                                    {{--<div class="caption-button" data-animation="animated fadeInUp">
                                        <a href="#" class="button">Read more</a>
                                    </div>--}}
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/Screenshot_6.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo two" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/Screenshot_7.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item row">
                            <div class="v-center">
                                <div class="col-xs-12 col-md-6">
                                    <div class="caption-title" data-animation="animated fadeInUp">
                                        <h2>Постоянная поддержка</h2>
                                    </div>
                                    <div class="caption-desc" data-animation="animated fadeInUp">
                                        <p>
                                            Мы не останавливаемся на "сдаче" проекта. После того, как ваш сайт будет
                                            готов - мы останемся на связи и всегда будем рады помочь с поддержкой вашего
                                            проекта.
                                        </p>
                                    </div>
                                    {{--<div class="caption-button" data-animation="animated fadeInUp">
                                        <a href="#" class="button">Read more</a>
                                    </div>--}}
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo one" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/Screenshot_8.png" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="caption-photo two" data-animation="animated fadeInRight">
                                        <img src="/colid/images/healit/Screenshot_9.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators caption-indector">
                        <li data-target="#caption_slide" data-slide-to="0" class="active">
                            <strong>Шаг 1</strong>Прототипирование.
                        </li>
                        <li data-target="#caption_slide" data-slide-to="1">
                            <strong>Шаг 2</strong>Создание "посадочной" страницы.
                        </li>
                        <li data-target="#caption_slide" data-slide-to="2">
                            <strong>Шаг 3</strong>Создание личного кабинета.
                        </li>
                        <li data-target="#caption_slide" data-slide-to="3">
                            <strong>Шаг 4</strong>Постоянная поддержка.
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gray-bg section-padding" id="feature-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="page-title">
                    <h2>Возможности</h2>
                    <p>
                        Представляем вам набор <strong>основных</strong> функций личного кабинета. Вы сможете выбрать
                        себе любую комбинацию из них.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-1.png" alt="">
                    </div>
                    <h3>Календарь</h3>
                    <p>Актуальное расписание врачей, онлайн запись на прием</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-2.png" alt="">
                    </div>
                    <h3>Email рассылки</h3>
                    <p>Отправка индивидуальных писем или массовых рассылкок клинтам</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-3.png" alt="">
                    </div>
                    <h3>Онлайн консультации</h3>
                    <p>Возможность предоставления услуги онлайн консультаций через современный real-time чат.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-4.png" alt="">
                    </div>
                    <h3>История болезни</h3>
                    <p>Подготовка история болезни пациента</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-5.png" alt="">
                    </div>
                    <h3>Прием платежей</h3>
                    <p>
                        Возможность принимать платежи онлайн с банковской карты или электронных кошельков
                        непосредственно
                        на расчетный счет вашей компании
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="box-icon">
                        <img src="/colid/images/portfolio-icon-6.png" alt="">
                    </div>
                    <h3>Сбор статистики о клиентах</h3>
                    <p>Сбор и наглядное предоставление информации о конкультациях, записях, платежах вашей клиники, а
                        так же о трафике, который приводит клиентов к вам на сайт</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="overlay section-padding" id="price-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="page-title">
                    <h2>Тарифы*</h2>
                    <p>
                        Вы всегда можете обратиться к нам с предложением о сотрудничестве за рамками данных тарифов
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="price-table">
                    <h3 class="text-uppercase price-title">Базовый</h3>
                    <hr>
                    <ul class="list-unstyled">
                        <li><strong class="amount"><span class="big">5 000</span></strong> руб/мес</li>
                        <li>Современный лендинг</li>
                        <li>Выделенный домен</li>
                        <li>Онлайн запись</li>
                        <li>История болезни</li>
                        <li>Предварительное анкетирование пациентов</li>
                        <li>Постоянная поддержка</li>
                    </ul>
                    <hr>
                    <a href="#contact-page" class="button">Заказать</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-table active">
                    <span class="price-info"><span class="ti-crown"></span></span>
                    <h3 class="text-uppercase price-title">Стандартный</h3>
                    <hr>
                    <ul class="list-unstyled">
                        <li><strong class="amount"><span class="big">10 000</span></strong> руб/мес</li>
                        <li>Современный лендинг</li>
                        <li>Выделенный домен</li>
                        <li>Онлайн запись</li>
                        <li>История болезни</li>
                        <li>Предварительное анкетирование пациентов</li>
                        <li>Онлайн консультации</li>
                        <li>Видеоконференции</li>
                        <li>Email рассылки</li>
                        <li>Постоянная поддержка</li>
                    </ul>
                    <hr>
                    <a href="#contact-page" class="button">Заказать</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-table">
                    <h3 class="text-uppercase price-title">Бизнес</h3>
                    <hr>
                    <ul class="list-unstyled">
                        <li><strong class="amount"><span class="big">15 000</span></strong> руб/мес</li>
                        <li>Современный лендинг</li>
                        <li>Выделенный домен</li>
                        <li>Онлайн запись</li>
                        <li>История болезни</li>
                        <li>Предварительное анкетирование пациентов</li>
                        <li>Онлайн консультации</li>
                        <li>Видеоконференции</li>
                        <li>Email рассылки</li>
                        <li>Прием платежей</li>
                        <li>Постоянная поддержка</li>
                    </ul>
                    <hr>
                    <a href="#contact-page" class="button">Заказать</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>* В стоимость входит стандартный лендинг, при условии единовременной оплаты кабинета на 6 месяцев и более</p>
            </div>
        </div>
    </div>
</section>

<section class="section-padding gray-bg" id="blog-page">
    <div class="container">
        <h1 class="text-center" style="margin-bottom: 50px">Примеры работ</h1>
        <div class="row">

            <div class="col-xs-12 col-sm-4">

                <div class="single-blog">
                    <div class="blog-photo">
                        <img src="/susan/img/works/pexels-photo-305568.jpeg" alt="">
                    </div>
                    <div class="blog-content">
                        <h3><a href="http://mydentist.healit.ru" target="_blank">Стоматологическая клиника</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="single-blog">
                    <div class="blog-photo">
                        <img src="/asclinic/img/about-img.jpg" alt="">
                    </div>
                    <div class="blog-content">
                        <h3><a href="http://awesomeclinic.healit.ru" target="_blank">AS Клиника</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="single-blog">
                    <div class="blog-photo">
                        <img src="/medcare/images/staff-4.jpg" alt="">
                    </div>
                    <div class="blog-content">
                        <h3><a href="http://medcare.healit.ru" target="_blank">Medcare</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="footer-area relative sky-bg" id="contact-page">
    <div class="absolute footer-bg"></div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Напишите нам</h2>
                        <p>
                            Мы перезвоним вам в рабочее время и с удовольствием ответим на все ваши вопросы.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <address class="side-icon-boxes">
                        <div class="side-icon-box">
                            <div class="side-icon">
                                <img src="/colid/images/phone-arrow.png" alt="">
                            </div>
                            <p><strong>Телефон: </strong>
                                <a href="callto:79637296764">+7 (963) 729-67-64 </a>
                            </p>
                        </div>
                        <div class="side-icon-box">
                            <div class="side-icon">
                                <img src="/colid/images/mail-arrow.png" alt="">
                            </div>
                            <p><strong>E-mail: </strong>
                                <a href="mailto:info@teledoctor-pro.ru">info@teledoctor-pro.ru</a> <br/>
                                <a href="mailto:nkhoreff@yandex.ru">nkhoreff@yandex.ru</a>
                            </p>
                        </div>
                    </address>
                </div>
                <div class="col-xs-12 col-md-8">
                    <form action="{{url('/feedback')}}" id="contact-form" method="post" class="contact-form">
                        {{csrf_field()}}
                        <div class="form-double">
                            <input type="text" id="form-name" name="name" placeholder="Как к вам можно обращаться?"
                                   class="form-control" required="required">
                            <input type="email" id="form-email" name="feedback_email" class="form-control"
                                   placeholder="Введите e-mail" required="required">
                        </div>
                        <input type="text" id="form-subject" name="subject" class="form-control"
                               placeholder="Тема сообщения">
                        <textarea name="text" id="form-message" rows="5" class="form-control"
                                  placeholder="" required="required"></textarea>
                        <div class="g-recaptcha" data-sitekey="6LfTHH8UAAAAAG9Q2GTPKhYgLFxpNOJky0KLV9SH"></div>
                        <button type="submit" class="button">Написать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>&copy;Copyright 2018 All right resurved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>


<!--Vendor-JS-->
<script src="/colid/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/colid/js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script src="/colid/js/owl.carousel.min.js"></script>
<script src="/colid/js/contact-form.js"></script>
<script src="/colid/js/jquery.parallax-1.1.3.js"></script>
<script src="/colid/js/scrollUp.min.js"></script>
<script src="/colid/js/magnific-popup.min.js"></script>
<script src="/colid/js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="/colid/js/main.js"></script>
</body>

</html>
