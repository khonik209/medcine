@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Создание обращения к <strong>{{$doctor->name}}</strong>
                    </div>
                    <div class="panel-body">
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                        <h4 class="text-center">Для продолжения требуется Ваш email</h4>
                        <form action="{{url('/api/adduser')}}" method="post" id="get-email-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="md5" value="{{$request->md5}}">
                            <input type="hidden" name="id" value="{{$request->id}}">
                            <input type="hidden" name="sum" value="{{$request->sum}}">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6" id="js-password-form">
                                    <div class="form-group">
                                        <input type="email" class="form-control js-check-user" id="pac_email"
                                               name="email"
                                               placeholder="Для продолжения, введите свой email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control hidden" id="pac_password"
                                               name="password" placeholder="Введите пароль">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-info" id="check-user-button">
                                            Продолжить
                                        </button>
                                        <button type="button" class="btn btn-info hidden" id="submit-user-button">
                                            Войти
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <small>Тем самым, вы <strong>не</strong> подписываетесь ни на какие сообщения рекламного
                            характера.
                            Email нужен <strong>только</strong> для регистрации в системе teledoctor-pro.
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection