<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="/img/stethoscope_logo.png"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $domainObj->company }}</title>

    <!-- Styles -->
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/lib/cntl.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">


    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/landing.js"></script>
    <script src="/lib/jquery.cntl.min.js"></script>
</head>
<body class="landing">

<div class="section1 ">
    <nav class="navbar navbar-default l-header">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav pull-left hidden-xs">
                    <li>
                        <a role="button" href="https://teledoctor-pro.ru">
                            <img class="img" src="img/stethoscope_logo.png"> </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav menu">
                    <li>
                        <a href="/faq">FAQ</a>
                    </li>

                    @if(Auth::user())
                        <li>
                            <a href="/user">Зайти в кабинет</a>
                        </li>
                    @else
                        <li>
                            <a href="{{url('/register')}}" class="js-menu-element">Регистрация</a>
                        </li>
                        <li>
                            <a href="{{url('/login')}}" class="js-menu-element">Вход</a>
                        </li>
                    @endif
                </ul>

            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                <div class="s1-content">
                    <h1 class="red">TELEDOCTOR PRO</h1>
                    <div class="left-border">
                        <h3 class="red">
                            Всё, чего Вам так не хватало</h3>
                        <h3>
                            Платформа для врачей, умеющих ценить своё время и эффективно организовывать свой рабочий
                            процесс.
                            Оптимизация задач позволяет работать продуктивней. Принимайте пациентов <span class="red">online</span>
                            уже сегодня
                        </h3>
                    </div>
                    <a href="/register" role="button" onclick="yaCounter46494495.reachGoal('pristupit');"
                       class="btn btn-info btn-signin pull-right js-menu-element">Присоединиться</a>
                </div>
            </div>

        </div>
    </div>
</div>
@include('common.error')
@include('common.info')
@include('common.success')

<div class="section2 parallax-window" data-parallax="scroll" data-image-src="/img/landing/bg_layout_1.jpg">
    <div class="container">
        <div class="row">
            <h1 class="red">Цель сервиса - экономия Вашего времени</h1>
            <div class="col-xs-12 col-sm-offset-6 col-sm-6 col-md-offset-6 col-md-6 col-lg-offset-6 col-lg-6">
                <div class="left-border">
                    <h4>Сделать работу с вашим пациентом удобной - наша приоритетная задача</h4>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item animated" data-anim="flipInX">
                    <img src="/img/landing/icon2.png" class="img img-responsive">
                    <br>
                    <h3 class="red uppercase">
                        Систематизация
                    </h3>
                    <p>
                        Платформа позволяет собираться и систематизировать данные пациентов. Хранить МРТ, рентген,
                        рекомендации и назначения врачей и любые другие документы
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item animated" data-anim="flipInX">
                    <img src="/img/landing/icon.png" class="img img-responsive">
                    <br>
                    <h3 class="red uppercase">
                        Анализ
                    </h3>
                    <p>
                        Платформа позволяет разложить задачу на составные части, путём рассмотрения её с различных
                        сторон, с разных точек зрения, при необходимости несколькими специалистами, для того чтобы
                        всесторонне изучить её и принять единственно верное решение
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item animated" data-anim="flipInX">
                    <img src="/img/landing/icon3.png" class="img img-responsive">
                    <br>
                    <h3 class="red uppercase">
                        Общение
                    </h3>
                    <p>
                        Платформа позволяет контактировать как врачу с пациентом, так и врачу с врачом. Общение может
                        протекать в форме текстового чата, так и видеосвязи.
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="item animated" data-anim="flipInX">
                    <img src="/img/landing/icon4.png" class="img img-responsive">
                    <br>
                    <h3 class="red uppercase">
                        Современность
                    </h3>
                    <p>
                        Платформа позволяет возможность шагать в ногу со временем. Иметь доступ к данным в электронном
                        виде, использовать их в любое время и в любом месте, где есть интернет. Это открывает новые
                        возможности для вашей деятельности
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section3" id="about">
    <div class="slick-carousel" id="landing-carousel">
        <div class="item_1 slick-item">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="item-text">
                    <h3>Просмотр снимков, МРТ и других документов</h3>
                    <h4>без необходимости установки дополнительных программ</h4>
                </div>
            </div>
        </div>
        <div class="item_3 slick-item">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="item-text">
                    <h3>Систематизация и ведение пациентов </h3>
                </div>
            </div>
        </div>
        <div class="item_4 slick-item">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="item-text">
                    <h3>Адаптивный чат для работы с любого устройства</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section4 parallax-window" data-parallax="scroll" data-image-src="/img/landing/bg_layout_1.jpg">
    <div class="container">
        <h2 class="red uppercase text-center">Организация работы</h2>
        <div class="cntl">
                <span class="cntl-bar cntl-center">
    <span class="cntl-bar-fill"></span>
  </span>
            <div class="cntl-states">
                <div class="cntl-state">
                    <div class="cntl-content">
                        <h4 class="red">Обращение к врачу</h4>
                        <p>Выберите врача, обратитесь к нему. Врач просмотрит вашу заявку и, если сможет помочь,
                            примет её</p>
                    </div>
                    <div class="cntl-icon cntl-center">1</div>
                </div>

                <div class="cntl-state">
                    <div class="cntl-content">
                        <h4 class="red">Оплата услуги</h4>

                        <p>Врач укажет, сколько будут стоить его услуги. После этого пациент оплатит счёт. Сумма
                            будет заморожена на счету пациента, но врач получит вознаграждение только <span
                                    class="red">после</span> оказания услуги</p>

                    </div>
                    <div class="cntl-icon cntl-center">2</div>
                </div>

                <div class="cntl-state">
                    <div class="cntl-content">
                        <h4 class="red">Оказание услуги</h4>
                        <p>Врач консультирует пациента по заявленной проблеме. Можно использовать любые ресурсы
                            платформы. Текстовый и видео чаты, обмен документами, просмотр снимков и многое
                            другое</p>
                    </div>
                    <div class="cntl-icon cntl-center">3</div>
                </div>

                <div class="cntl-state">
                    <div class="cntl-content">
                        <h4 class="red">Завершение обращения</h4>
                        <p>После оказания услуги, врач и пациент завершают обращение. Если услуга выполнена, сумма
                            переводится врачу, если нет - возвращается пациенту. Подброней о схеме работы можно узнать в
                            <a href="/rules.pdf" target="_blank">оферте</a></p>
                    </div>
                    <div class="cntl-icon cntl-center">4</div>
                </div>
            </div>
        </div>
        <a href="/register" role="button" onclick="yaCounter46494495.reachGoal('pristupit');"
           class="btn btn-info btn-signin pull-right js-menu-element">Присоединиться</a>
    </div>
</div>
<footer class="section8">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <ul class="list-inline">
                    <li>
                        <a href="/faq">FAQ</a>
                    </li>
                    <li>
                        <a href="#signin" class="js-menu-element">Регистрация</a>
                    </li>
                </ul>
                <img src="/img/landing/line.png" class="img img-responsive center-block">
                <h2>TeleDoctor-pro.ru</h2>
            </div>
            <div class="col-xs-12">
                <ul class="list-inline">
                    <li>
                        <span class="glyphicon glyphicon-envelope"></span>
                        &nbsp;info@teledoctor-pro.ru
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-phone-alt"></span>
                        &nbsp;+7(963)729-67-64
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-file"></span>
                        &nbsp;<a target="_blank" href="/rules.pdf">Пользовательское соглашение</a>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-file"></span>
                        &nbsp;<a target="_blank" href="/privacy.pdf">Политика конфиденциальности</a>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-link"></span>
                        &nbsp;<a target="_blank" href="https://kassa.yandex.ru/secure-deal">Операции совершаются с
                            помощью сервиса "Безопасная сделка"</a>
                    </li>
                    <li>
                        <span class="glyphicon glyphicon-thumbs-up"></span>
                        &nbsp;<a target="_blank" href="https://vk.com/teledoctorpro">Мы в ВКонтакте</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="/js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="/js/parallax.min.js"></script>
<script>
    // Лендинг

    $('.js-menu-element').click(function () {
        var elementId = $(this).attr('href');
        $('html, body').animate({scrollTop: $(elementId).position().top}, 1000);
    });
    $(document).ready(function (e) {
        timeline = document.getElementsByClassName('cntl');
        if (timeline.length > 0) {
            $('.cntl').cntl({
// The amount of "scroll padding" to allow
// The more, the later the state will be revealed
                revealbefore: 150,
// The animate class
// This class should have animation rules in css
                anim_class: 'cntl-animate',
// A callback once the state has been revealed
                onreveal: null
            });
        }
    });

    $(window).scroll(function () {
        $('.animated').each(function () {
            var imagePos = $(this).offset().top;
            var imageHght = $(this).outerHeight();
            var topOfWindow = $(window).scrollTop() + 40;
            var heightOfWindow = $(window).height();
            var animName = $(this).data('anim');
            var screenHeight = $(window).outerHeight();
            if (!$(this).data('atop')) {
                var animTop = 0.9;
            } else {
                var animTop = $(this).data('atop');
            }
            if (imagePos < topOfWindow + heightOfWindow * animTop && imagePos + imageHght > topOfWindow) {
                $(this).css('visibility', 'visible').addClass(animName);
            } else if (imagePos + imageHght < topOfWindow || imagePos > topOfWindow + screenHeight) {
                $(this).css('visibility', 'hidden').removeClass(animName);
            }
        });
    });

    $(document).ready(function () {
        // Анимация лендинга
        $('.animated').each(function () {
            var imagePos = $(this).offset().top;
            var imageHght = $(this).outerHeight();
            var topOfWindow = $(window).scrollTop() + 40;
            var heightOfWindow = $(window).height();
            var animName = $(this).data('anim');
            var screenHeight = $(window).outerHeight();
            if (imagePos < topOfWindow + heightOfWindow && imagePos + imageHght > topOfWindow) {
                $(this).css('visibility', 'visible').addClass(animName);
            } else if (imagePos + imageHght < topOfWindow || imagePos > topOfWindow + screenHeight) {
                $(this).css('visibility', 'hidden').removeClass(animName);
            }
        });
        // Карусель

        $('#landing-carousel').slick({
            //dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            //cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 2500,
            arrows: false,
            pauseOnHover: false,
            pauseOnFocus: false
        });
    });
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter46494495 = new Ya.Metrika({
                    id: 46494495,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>

</body>
</html>