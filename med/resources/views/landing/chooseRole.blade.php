@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="setrole">
                    <h3>Для продолжения работы в системе онлайн медицины ТелеДоктор, укажите цель вашей регистрации:<br>
                        - Вы врач и хотите работать с пациентами онлайн;<br>
                        - Вы пациент и желаете проконсультироваться с врачом онлайн
                    </h3>

                    <form action="/setrole" method="post">
                        {{ csrf_field() }}
                        <label for="pac">Пациент</label>
                        <input id="pac" type="radio" name="role" value="user"><br>
                        <label for="doc">Врач</label>
                        <input id="doc" type="radio" name="role" value="doc"><br>
                        <button type="submit" class="btn btn-info">Выбрать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection