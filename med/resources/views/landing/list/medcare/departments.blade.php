@extends('landing.list.medcare.layout')
@section('content')

    <div class="qbootstrap-departments">
        <div class="row">
            <div class="department-wrap animate-box">
                <div class="grid-1 col-md-6" style="background-image: url(images/img_bg_1.jpg);"></div>
                <div class="grid-2 col-md-6">
                    <div class="desc">
                        <h2><a href="departments-single.blade.php">Гинекология</a></h2>
                        <p>Врач гинеколог занимается диагностикой и лечением заболеваний женской репродуктивной
                            системы</p>
                        <p>
                            Прием пациенток в нашем гинекологическом отделении ведут лучшие специалисты своего дела -
                            высококвалифицированные акушеры-гинекологи, в том числе кандидаты медицинских наук из числа
                            сотрудников кафедры акушерства, гинекологии и перинаталогии ПМГМУ имени И.М. Сеченова,
                            доктора медицинских наук, профессоры и врачи высшей категории. Стаж работы каждого - не
                            менее 15 лет.
                        </p>
                        {{--<div class="department-info">
                            <div class="block">
                                <h2><a href="doctors-single.blade.php">Paul Merriweather</a></h2>
                                <span>Head Department</span>
                            </div>
                            <div class="block">
                                <h2><a href="departments-single.blade.php">Department info</a></h2>
                                <span>Block B, 3rd floor</span>
                            </div>
                            <div class="block">
                                <h2><a href="doctors.blade.php">Find a Doctor</a></h2>
                                <span>See doctors in this department</span>
                            </div>
                            <div class="block">
                                <h2><a href="appointment.blade.php">Request an appointment</a></h2>
                                <span>Call us or fill in a form</span>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="department-wrap animate-box">
                <div class="grid-1 col-md-6 col-md-push-6" style="background-image: url(images/img_bg_2.jpg);"></div>
                <div class="grid-2 col-md-6 col-md-pull-6">
                    <div class="desc">
                        <h2><a href="departments-single.blade.php">Отделение урологии</a></h2>
                        <p>
                            Врач уролог занимается диагностикой и лечением заболеваний органов мочевой системы

                            а также заболеваний мужских половых органов, надпочечников и др.процессов. Бытует мнение, что урологи занимаются исключительно мужской мочеполовой системой, и не многие
                            знают, что и женщины входят в предмет анализа и лечения данной специализации и занимают в ней отдельное место. Урологические заболевания мужчин, женщин и пожилых людей
                            имеют свои особенности развития и течения, что определяется их анатомо-физиологическими и возрастными особенностями. И у мужчин, и у женщин одинаково возникают мочекаменная
                            болезнь, пиелонефрит, опухолевые новообразования мочеполовых органов, хроническая почечная недостаточность. У пожилых людей среди урологических заболеваний на первый план
                            выходят такие болезни как: недержание мочи и опухолевые образования мочеполовых органов.
                        </p>
                        {{--<div class="department-info">
                            <div class="block">
                                <h2><a href="doctors-single.blade.php">Paul Merriweather</a></h2>
                                <span>Head Department</span>
                            </div>
                            <div class="block">
                                <h2><a href="departments-single.blade.php">Department info</a></h2>
                                <span>Block B, 3rd floor</span>
                            </div>
                            <div class="block">
                                <h2><a href="doctors.blade.php">Find a Doctor</a></h2>
                                <span>See doctors in this department</span>
                            </div>
                            <div class="block">
                                <h2><a href="appointment.blade.php">Request an appointment</a></h2>
                                <span>Call us or fill in a form</span>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="department-wrap animate-box">
                <div class="grid-1 col-md-6" style="background-image: url(images/img_bg_6.jpg);"></div>
                <div class="grid-2 col-md-6">
                    <div class="desc">
                        <h2><a href="departments-single.blade.php">Отделение Офтальмологии</a></h2>
                        <p>
                            Отделение офтальмологии применяет самые совершенные мировые технологии диагностики и лечения катаракты, глаукомы, миопии, астигматизма, косоглазия, отслойки сетчатки и
                            других глазных заболеваний (лазерная коррекция зрения Ласик, микрохирургия глаза, факоэмульсификация с имплантацией искусственной интраокулярной линзы).

                            Специалисты клиники в области офтальмологии быстро, безболезненно и эффективно помогут пациентам любого возраста, с любыми нарушениями зрения. Для решения некоторых
                            проблем, которые могут существенно ухудшать качество жизни, достаточно всего двух визитов – диагностического и для амбулаторно-хирургического вмешательства. Наша клиника
                            гарантирует высочайший результат, который может сохраниться пожизненно.
                        </p>
                        {{--<div class="department-info">
                            <div class="block">
                                <h2><a href="doctors-single.blade.php">Paul Merriweather</a></h2>
                                <span>Head Department</span>
                            </div>
                            <div class="block">
                                <h2><a href="departments-single.blade.php">Department info</a></h2>
                                <span>Block B, 3rd floor</span>
                            </div>
                            <div class="block">
                                <h2><a href="doctors.blade.php">Find a Doctor</a></h2>
                                <span>See doctors in this department</span>
                            </div>
                            <div class="block">
                                <h2><a href="appointment.blade.php">Request an appointment</a></h2>
                                <span>Call us or fill in a form</span>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection