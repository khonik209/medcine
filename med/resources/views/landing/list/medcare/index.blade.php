@extends('landing.list.medcare.layout')
@section('content')


    <div id="qbootstrap-services">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
                    <h2>Услуги</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-flow-merge"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Гинекология</a></h3>
                            <p>
                                Широкий спектр лапароскопических и малоинвазивных вмешательств в плановой и экстренной
                                гинекологии, онкогинекологии.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-params"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Урология</a></h3>
                            <p>
                                Весь спектр урологических операций
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-lab2"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Хирургия</a></h3>
                            <p>
                                Широкий спектр эндоскопических и малоинвазивных вмешательств в плановой и экстренной
                                хирургии, флебологии, онкологии
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-params"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Офтальмология</a></h3>
                            <p>
                                Широкий спектр операций под микроскопом.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-lab2"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Травматология и ортопедия</a></h3>
                            <p>
                                Любые виды пластических и микрохирургических операций на кисти у детей и взрослых.
                                Все виды реконструктивных операций на стопе
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-heart4"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">ЛОР</a></h3>
                            <p>
                                Хирургическое лечение с помощью лазера, холодноплазменной установки хронического
                                тонзиллита, вазомоторного, гипертрофического и аллергического ринитов, храпа, удаление
                                образований, устранение искривления перегородки носа
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="qbootstrap-choose">
        <div class="container-fluid">
            <div class="row">
                <div class="choose">
                    <div class="half img-bg" style="background-image: url(/medcare/images/img_bg_6.jpg);"></div>
                    <div class="half features-wrap">
                        <div class="qbootstrap-heading animate-box">
                            <h2>Почему именно мы?</h2>
                            <p>
                                Забота о пациенте - наша прямая обязанность.
                            </p>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-group-outline"></i></span>
                            <div class="desc">
                                <h3>Специалисты с большим опытом</h3>
                                <p>
                                    Средний стаж наших специалистов - 10 лет.
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-flow-merge"></i></span>
                            <div class="desc">
                                <h3>Бесплатная консультация</h3>
                                <p>
                                    Вы можете получить первую конкультацию бесплатно!
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-document-text"></i></span>
                            <div class="desc">
                                <h3>Онлайн поддержка</h3>
                                <p>
                                    Благодаря нашей онлайн-платформе, Вы можете оставаться на связи с врачом в любое
                                    время
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-gift"></i></span>
                            <div class="desc">
                                <h3>Современный подход</h3>
                                <p>
                                    Наша медицина - на острие современных технологий
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(count($doctors)>0)

        <div id="qbootstrap-doctor">
            <div class="container-fluid">
                <div class="row animate-box">
                    <div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
                        <h2>Наши специалисты</h2>
                    </div>
                </div>
                <div class="row">
                    @foreach($doctors as $doctor)
                        <div class="col-md-3 animate-box text-center">
                            <div class="doctor">
                                <div class="staff-img"
                                     style="background-image: url({{$doctor->avatar_url}});background-position: 0 0;"></div>
                                <h3><a href="#">{{$doctor->name}}</a></h3>
                                @if($doctor->skills->count()>0)
                                    <span>@foreach($doctor->skills as $skill) {{$skill->name}} @endforeach</span>
                                @endif
                                {{--<ul class="qbootstrap-social">
                                    <li><a href="#"><i class="icon-facebook2"></i></a></li>
                                    <li><a href="#"><i class="icon-twitter2"></i></a></li>
                                    <li><a href="#"><i class="icon-yahoo2"></i></a></li>
                                    <li><a href="#"><i class="icon-google2"></i></a></li>
                                </ul>--}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    @endif

    <div id="qbootstrap-counter" class="qbootstrap-counters"
         style="background-image: url(/medcare/images/img_bg_2.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-group-outline"></i></span>
                            <span class="qbootstrap-counter js-counter" data-from="0" data-to="3297" data-speed="5000"
                                  data-refresh-interval="50"></span>
                            <span class="qbootstrap-counter-label">Здоровых людей</span>
                        </div>
                        <div class="col-md-3 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-home-outline"></i></span>
                            <span class="qbootstrap-counter js-counter" data-from="0" data-to="11" data-speed="5000"
                                  data-refresh-interval="50"></span>
                            <span class="qbootstrap-counter-label">Филиалов по всей стране</span>
                        </div>
                        <div class="col-md-3 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-user-add-outline"></i></span>
                            <span class="qbootstrap-counter js-counter" data-from="0" data-to="45" data-speed="5000"
                                  data-refresh-interval="50"></span>
                            <span class="qbootstrap-counter-label">Квалифицированных врачей</span>
                        </div>
                        <div class="col-md-3 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-point-of-interest-outline"></i></span>
                            <span class="qbootstrap-counter js-counter" data-from="0" data-to="30" data-speed="5000"
                                  data-refresh-interval="50"></span>
                            <span class="qbootstrap-counter-label">Отделений</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($news)>0)
    <div id="qbootstrap-blog">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
                    <h2>Новости центра</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row animate-box">
                        <div class="owl-carousel owl-carousel-fullwidth">
                            @foreach($news as $single)
                            <div class="item">
                                <div class="blog-slide active">
                                    <a href="{{url("/blog/$single->id")}}" class="blog-box"
                                       style="background-image: url({{$single->image_url}});">
                                        <span class="date">{{$single->created_at}}</span>
                                    </a>
                                    <div class="desc">
                                        <h3><a href="{{url("/blog/$single->id")}}">{{$single->title}}</a></h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection