@extends('landing.list.medcare.layout')
@section('content')
    <div id="qbootstrap-blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-md-8">
                    <div class="blog-wrap">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="#" class="blog-img" style="background-image: url({{$news->image_url}});">

                                </a>
                            </div>
                            <div class="col-md-12">
                                <div class="blog-desc">
                                    <h2><a href="blog-single.blade.php">{{$news->title}}</a></h2>
                                    <div class="post-meta">
                                        <span>{{$news->created_at}}</span>
                                    </div>
                                    <p>
                                        {{$news->text}}
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection