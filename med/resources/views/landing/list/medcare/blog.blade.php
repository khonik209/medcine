@extends('landing.list.medcare.layout')
@section('content')
    <div id="qbootstrap-blog">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2 col-md-8">
                    @foreach($news as $single)
                        <div class="blog-wrap animate-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{url("/blog/$single->id")}}" class="blog-img" style="background-image: url({{$single->image_url}});">

                                    </a>
                                </div>
                                <div class="col-md-12">
                                    <div class="blog-desc">
                                        <h2><a href="{{url("/blog/$single->id")}}">{{$single->title}}</a></h2>
                                        <div class="post-meta">
                                            <span>{{$single->created_at}}</span>
                                        </div>
                                        <p>
                                            {{$single->text}}
                                        </p>
                                        <p><a href="{{url("/blog/$single->id")}}" class="btn btn-primary">Подробнее</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <ul class="pager">
                        <li class="previous disabled"><a href="#">&larr; Previous</a></li>
                        <li class="next"><a href="#">Next &rarr;</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection