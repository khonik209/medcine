@extends('landing.list.medcare.layout')
@section('content')
    <div id="qbootstrap-services">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
                    <h2>Услуги</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-flow-merge"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Гинекология</a></h3>
                            <p>
                                Широкий спектр лапароскопических и малоинвазивных вмешательств в плановой и экстренной
                                гинекологии, онкогинекологии.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-params"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Урология</a></h3>
                            <p>
                                Весь спектр урологических операций
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-lab2"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Хирургия</a></h3>
                            <p>
                                Широкий спектр эндоскопических и малоинвазивных вмешательств в плановой и экстренной
                                хирургии, флебологии, онкологии
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-md visible-lg"></div>

                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-params"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Офтальмология</a></h3>
                            <p>
                                Широкий спектр операций под микроскопом.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-lab2"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">Травматология и ортопедия</a></h3>
                            <p>
                                Любые виды пластических и микрохирургических операций на кисти у детей и взрослых.
                                Все виды реконструктивных операций на стопе
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 animate-box">
                    <div class="services">
						<span class="icon">
							<i class="icon-heart4"></i>
						</span>
                        <div class="desc">
                            <h3><a href="#">ЛОР</a></h3>
                            <p>
                                Хирургическое лечение с помощью лазера, холодноплазменной установки хронического
                                тонзиллита, вазомоторного, гипертрофического и аллергического ринитов, храпа, удаление
                                образований, устранение искривления перегородки носа
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="qbootstrap-choose">
        <div class="container-fluid">
            <div class="row">
                <div class="choose">
                    <div class="half img-bg" style="background-image: url(/medcare/images/img_bg_6.jpg);"></div>
                    <div class="half features-wrap">
                        <div class="qbootstrap-heading animate-box">
                            <h2>Почему именно мы?</h2>
                            <p>
                                Забота о пациенте - наша прямая обязанность.
                            </p>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-group-outline"></i></span>
                            <div class="desc">
                                <h3>Специалисты с большим опытом</h3>
                                <p>
                                    Средний стаж наших специалистов - 10 лет.
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-flow-merge"></i></span>
                            <div class="desc">
                                <h3>Бесплатная консультация</h3>
                                <p>
                                    Вы можете получить первую конкультацию бесплатно!
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-document-text"></i></span>
                            <div class="desc">
                                <h3>Онлайн поддержка</h3>
                                <p>
                                    Благодаря нашей онлайн-платформе, Вы можете оставаться на связи с врачом в любое
                                    время
                                </p>
                            </div>
                        </div>
                        <div class="features animate-box">
                            <span class="icon text-center"><i class="icon-gift"></i></span>
                            <div class="desc">
                                <h3>Современный подход</h3>
                                <p>
                                    Наша медицина - на острие современных технологий
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection