@extends('landing.list.medcare.layout')
@section('content')

    <div id="qbootstrap-doctor">
        <div class="container-fluid">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 text-center qbootstrap-heading">
                    <h2>Наши специалисты</h2>
                    <p>
                        Забота о Вашем здоровье - наша великая миссия
                    </p>
                </div>
            </div>
            <div class="row">
                @foreach($doctors as $doctor)
                <div class="col-md-3 animate-box text-center">
                    <div class="doctor">
                        <div class="staff-img" style="background-image: url({{$doctor->avatar_url}});background-position: 0 0;"></div>
                        <h3><a href="#">{{$doctor->name}}</a></h3>
                        @if($doctor->skills->count()>0)
                            <span>@foreach($doctor->skills as $skill) {{$skill->name}} @endforeach</span>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
