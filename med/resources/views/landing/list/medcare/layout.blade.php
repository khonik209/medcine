<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Medcare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content=""/>
    <meta name="twitter:image" content=""/>
    <meta name="twitter:url" content=""/>
    <meta name="twitter:card" content=""/>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="/medcare/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/medcare/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/medcare/css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/medcare/css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="/medcare/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/medcare/css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="/medcare/css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="/medcare/css/style.css">

    <!-- Modernizr JS -->
    <script src="/medcare/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="/medcare/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="qbootstrap-loader"></div>

<div id="page">
    <nav class="qbootstrap-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="top">
                            <div class="row">
                                <div class="col-md-4 col-md-push-4 text-center">
                                    <div id="qbootstrap-logo"><a href="{{url('/')}}"><i
                                                    class="icon-plus-outline"></i>Med<span>care</span></a></div>
                                </div>
                                <div class="col-md-4 col-md-pull-4">
                                    <div class="num">
                                        <span class="icon"><i class="icon-phone"></i></span>
                                        <p><a href="#">+7 (123) 456-78-90</a><br><a href="#">+7 (123) 456-78-90</a></p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="loc">
                                        <span class="icon"><i class="icon-location"></i></span>
                                        <p><a href="#">г. Москва, ул. Уральская, д.13</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <div class="menu-1">
                            <ul>
                                <li><a href="{{url('/')}}">Medcare</a></li>
                                <li><a href="{{url('/services')}}">Услуги</a></li>
                                <li>
                                    <a href="{{url('/departments')}}">Отделения</a>
                                </li>
                                <li>
                                    <a href="{{url('doctors')}}">Врачи</a>
                                </li>
                                <li>
                                    <a href="{{url('blog')}}">Новости</a>
                                </li>
                                <li><a href="{{url('contacts')}}">Контакты</a></li>
                                <li class="btn-cta">
                                    <a href="{{url('login')}}">
                                        <span>Личный кабинет <i class="icon-calendar3"></i></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </nav>
    <aside id="qbootstrap-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(/medcare/images/img_bg_5.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1><strong> Дубова Анна</strong></h1>
                                    <h2 class="doc-holder">Гинеколог</h2>
                                    <h2>
                                        Высшая категория, К.М.Н.
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background-image: url(/medcare/images/img_bg_1.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1><strong>Бондаренко Виктор</strong></h1>
                                    <h2 class="doc-holder">
                                        ЛОР
                                    </h2>
                                    <h2>
                                        Стаж 15 лет, высшая категория, к.м.н.
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background-image: url(/medcare/images/img_bg_2.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1>
                                        Современное оборудование
                                    </h1>
                                    <h2>
                                        Новейшие технологии
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background-image: url(/medcare/images/img_bg_3.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1>Специальное предложение! Первая консультация <strong>бесплатно!</strong></h1>
                                    <h2>
                                        Запись на сайте или по телефону
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="qbootstrap-intro">
        <div class="row">
            <div class="intro animate-box">
                <div class="intro-grid color-1">
                    <span class="icon"><i class="icon-calendar3"></i></span>
                    <h3>Гибкий график записей</h3>
                    <p>
                        Вы можете выбрать врача и записаться онлайн, запись сразу же попадет к нам на стол
                    </p>
                </div>
                <div class="intro-grid color-2">
                    <span class="icon"><i class="icon-wallet2"></i></span>
                    <h3>Гарантированно низкая цена</h3>
                    <p>
                        Мы предоставляем услуги высочайшего уровня по рыночным ценам. Это - главное наше преимущество
                        перед конкурентами
                    </p>
                </div>
                <div class="intro-grid color-3">
                    <span class="icon"><i class="icon-clock3"></i></span>
                    <h3>Мы работаем:</h3>
                    <p>
                        <span>Понедельник – Пятница: 08.00 – 18.00 </span><br>
                        <span>Суббота – Воскресенье: 08.00 – 16.00 </span><br>
                    </p>
                </div>
            </div>
        </div>
    </div>

    @yield('content')

    <div id="qbootstrap-register" style="background-image: url(images/img_bg_5.jpg);"
         data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <div class="date-counter text-center">
                        <h2>Предлагаем для вас <strong class="color">бесплатную консультацию</strong></h2>
                        <p class="countdown">
                            <span id="days"></span>
                            <span id="hours"></span>
                            <span id="minutes"></span>
                            <span id="seconds"></span>
                        </p>
                        <p><strong>Предложение ограничено, спешите!</strong></p>
                        <p><a href="{{url('/login')}}" class="btn btn-primary btn-lg">Записаться на приём <i class="icon-calendar3"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer id="qbootstrap-footer" role="contentinfo">
        <div class="overlay"></div>
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-3 qbootstrap-widget">
                    <h3>О нас</h3>
                    <p>
                        10 лет на рынке медицины!
                    </p>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 qbootstrap-widget">
                    <h3>Ссылки</h3>
                    <ul class="qbootstrap-footer-links">
                        <li><a href="{{url('/departments')}}">Отделения</a></li>
                        <li><a href="{{url('/blog')}}">Новости</a></li>
                        <li><a href="{{url('/contacts')}}">Контакты</a></li>
                        <li><a href="#">Политика конфиденциальности</a></li>
                    </ul>
                </div>

                <div class="col-md-2 col-sm-4 col-xs-6 qbootstrap-widget">
                    <h3>Услуги</h3>
                    <ul class="qbootstrap-footer-links">
                        <li>Гинекология</li>
                        <li>Урология</li>
                        <li>Хирургия</li>
                        <li>Офтальмология</li>
                        <li>Травматология и ортопедия</li>
                        <li>ЛОР</li>
                    </ul>
                </div>

                <div class="col-md-5 col-sm-4 col-xs-12 qbootstrap-widget">
                    <h3>Напишите нам</h3>
                    <form class="contact-form">
                        <div class="form-group">
                            <label for="name" class="sr-only">Name</label>
                            <input type="name" class="form-control" id="name" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="message" class="sr-only">Message</label>
                            <textarea class="form-control" id="message" rows="3" placeholder="Сообщение"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" id="btn-submit" class="btn btn-primary btn-send-message btn-md"
                                   value="Отправить">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-md-12 text-center">
                <p>
                    <small class="block">&copy; 2017 Все права защищены</small>
                    <small class="block">Создано на платформе <a href="https://healit.ru" target="_blank">HealIt</a>
                    </small>
                </p>
            </div>
        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<!-- jQuery -->
<script src="/medcare/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="/medcare/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="/medcare/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/medcare/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="/medcare/js/jquery.stellar.min.js"></script>
<!-- Carousel -->
<script src="/medcare/js/owl.carousel.min.js"></script>
<!-- Flexslider -->
<script src="/medcare/js/jquery.flexslider-min.js"></script>
<!-- countTo -->
<script src="/medcare/js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="/medcare/js/jquery.magnific-popup.min.js"></script>
<script src="/medcare/js/magnific-popup-options.js"></script>
<!-- Sticky Kit -->
<script src="/medcare/js/sticky-kit.min.js"></script>
<!-- Main -->
<script src="/medcare/js/main.js"></script>

</body>
</html>

