@extends('landing.list.medcare.layout')
@section('content')

    <div id="qbootstrap-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Контактная информация</h3>
                    <div class="row contact-info-wrap">
                        <div class="col-md-3">
                            <p><span><i class="icon-location"></i></span>Омск<br>  улица Мира, 76  </p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-phone"></i></span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
                        </div>
                        <div class="col-md-3">
                            <p><span><i class="icon-mail"></i></span> <a href="mailto:info@yoursite.com">info@medcare.com</a>
                            </p>
                        </div>

                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1 animate-box">
                    <h3>Напишите нам</h3>
                    <form action="#">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for="fname">Имя</label>
                                <input type="text" id="fname" class="form-control" placeholder="Иван">
                            </div>
                            <div class="col-md-6">
                                <label for="lname">Фамилия</label>
                                <input type="text" id="lname" class="form-control" placeholder="Иванов">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="email">Email</label>
                                <input type="text" id="email" class="form-control" placeholder="ivanov@myemail.ru">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="subject">Заголовок</label>
                                <input type="text" id="subject" class="form-control"
                                       placeholder="Тема вашего сообщения">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label for="message">Текст</label>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control"
                                          placeholder="Здравствуйте, ..."></textarea>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" value="Отправить" class="btn btn-primary">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection