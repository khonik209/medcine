<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="/asclinic/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="Colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Awesome Clinic</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="/asclinic/css/linearicons.css">
    <link rel="stylesheet" href="/asclinic/css/font-awesome.min.css">
    <link rel="stylesheet" href="/asclinic/css/magnific-popup.css">
    <link rel="stylesheet" href="/asclinic/css/nice-select.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/asclinic/css/bootstrap.css">
    <link rel="stylesheet" href="/asclinic/css/main.css">
</head>
<body>

<!-- Start Header Area -->
<header class="default-header">
    <div class="container">
        <div class="header-wrap">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="#home"><img src="/asclinic/img/logo.png" alt=""></a>
                </div>
                <div class="main-menubar d-flex align-items-center">
                    <nav class="hide">
                        <a href="#home">Awesome</a>
                        <a href="#service">Услуги</a>
                        <a href="#appoinment">Записаться</a>
                        <a href="#consultant">Специалисты</a>
                    </nav>
                    <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header Area -->

<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-center">
            <div class="banner-content col-lg-6 col-md-12">
                <h1 class="text-uppercase">
                    МЕДИЦИНСКИЙ ЦЕНТР <br>
                    ДИАГНОСТИКИ И ПРОФИЛАКТИКИ
                </h1>
                <p>
                    Когда здоровье – высшая ценность.
                </p>
                <a href="{{url('/register')}}" class="primary-btn2 mt-20 text-uppercase ">Войти в личный кабинет<span class="lnr lnr-arrow-right"></span></a>
            </div>
            <div class="col-lg-6 d-flex align-self-end img-right">
                <img class="img-fluid" src="/asclinic/img/header-img.png" alt="">
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start feature Area -->
<section class="feature-area section-gap" id="service">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="single-feature d-flex flex-row pb-30">
                    <div class="icon">
                        <span class="lnr lnr-rocket"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Поддержка 24/7</h4>
                        <p>
                            С Вами всегда на связи наши менеджеры и дежурный врач!
                        </p>
                    </div>
                </div>
                <div class="single-feature d-flex flex-row pb-30">
                    <div class="icon">
                        <span class="lnr lnr-chart-bars"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Исследования и анализы</h4>
                        <p>
                            Рентген легких, МРТ, КТ и многое другое
                        </p>
                    </div>
                </div>
                <div class="single-feature d-flex flex-row">
                    <div class="icon">
                        <span class="lnr lnr-bug"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Онлайн консультации</h4>
                        <p>
                            Вы всегда можете получить консультацию. В том числе, не выходя из дома!
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="single-feature d-flex flex-row pb-30">
                    <div class="icon">
                        <span class="lnr lnr-heart-pulse"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Реабилитация</h4>
                        <p>
                            Массаж, ЛФП, физиотерапия и многое другое для комфортной и эффективной реабилитации
                        </p>
                    </div>
                </div>
                <div class="single-feature d-flex flex-row pb-30">
                    <div class="icon">
                        <span class="lnr lnr-paw"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Проф. осмотры</h4>
                        <p>
                            Медицинские осмотры, оружейная комиссия, оформление медицинских книжек
                        </p>
                    </div>
                </div>
                <div class="single-feature d-flex flex-row">
                    <div class="icon">
                        <span class="lnr lnr-users"></span>
                    </div>
                    <div class="desc">
                        <h4 class="text-uppercase">Стоматология</h4>
                        <p>
                            Лечение зубов под общим наркозом, отбеливание зубов, чистка зубов, лечение кариеса и осложнений
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End feature Area -->


<!-- Start about Area -->
<section class="about-area" id="appoinment">
    <div class="container-fluid">
        <div class="row d-flex justify-content-end align-items-center">
            <div class="col-lg-6 col-md-12 about-left no-padding">
                <img class="img-fluid" src="/asclinic/img/about-img.jpg" alt="">
            </div>
            <div class="col-lg-6 col-md-12 about-right no-padding">
                <h1>Написать нам</h1>
                <form class="booking-form" id="myForm" action="#">
                    <div class="row">
                        <div class="col-lg-12 d-flex flex-column">
                            <input name="name" placeholder="Иван" class="form-control mt-20" type="text" required>
                        </div>
                        <div class="col-lg-6 d-flex flex-column">
                            <input name="phone" placeholder="+7 123 45 67" class="form-control mt-20" type="text" required>
                        </div>
                        <div class="col-lg-6 d-flex flex-column">
                            <input id="email" name="email" class="form-control mt-20" type="email" placeholder="ivan@mymail.ru" required>
                        </div>
                        <div class="col-lg-12 flex-column">
                            <textarea class="form-control mt-20" name="message" placeholder="Сообщение" required></textarea>
                        </div>

                        <div class="col-lg-12 d-flex justify-content-end send-btn">
                            <button class="submit-btn primary-btn mt-20 text-uppercase ">Отправить<span class="lnr lnr-arrow-right"></span></button>
                        </div>

                        <div class="alert-msg"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- End about Area -->
@if($doctors->count()>0)
    <!-- Start consultans Area -->
    <section class="consultans-area section-gap" id="consultant">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8 pb-80 header-text">
                    <h1>Наши специалисты</h1>
                    <p>
                        Все наши сотрудники - специалисты высшей категории, постоянно подтверждающие квалификацию и принимающие участие в крупнейших сипозиумах страны
                    </p>
                </div>
            </div>
            <div class="row">
                @foreach($doctors as $doctor)
                    <div class="col-lg-3 col-md-3 vol-wrap">
                        <div class="single-con">
                            <div class="content">
                                <div>
                                    <div class="content-overlay"></div>
                                    <img class="content-image img-fluid d-block mx-auto" src="{{$doctor->avatar_url}}" alt="">
                                    <div class="content-details fadeIn-bottom">
                                        <h4>{{$doctor->name}}</h4>
                                        @if($doctor->skills->count()>0)
                                            <p>@foreach($doctor->skills as $skill) {{$skill->name}} @endforeach</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End consultans Area -->
@endif
<!-- Start fact Area -->
<section class="facts-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 single-fact">
                <h2 class="counter">2536</h2>
                <p class="text-uppercase">Клиентов</p>
            </div>
            <div class="col-lg-3 col-md-6 single-fact">
                <h2 class="counter">6784</h2>
                <p class="text-uppercase">Сделано анализов</p>
            </div>
            <div class="col-lg-3 col-md-6 single-fact">
                <h2 class="counter">105</h2>
                <p class="text-uppercase">Сотрудников</p>
            </div>
            <div class="col-lg-3 col-md-6 single-fact">
                <h2 class="counter">397</h2>
                <p class="text-uppercase">Спасенных жизней</p>
            </div>
        </div>
    </div>
</section>
<!-- end fact Area -->
@if(count($news)>0)
    <!-- Start blog Area -->
    <section class="blog-area section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 pb-30 header-text">
                    <h1>Новости</h1>
                    <p>
                        Будьте в курсе последних событий в мире медицины!
                    </p>
                </div>
            </div>
            <div class="row">
                @foreach($news as $single)
                    <div class="single-blog col-lg-4 col-md-4">

                        <img class="f-img img-fluid mx-auto" src="{{$single->image_url}}" alt="">
                        <h4>
                            <a href="#">{{$single->title}}</a>
                        </h4>
                        <p>
                            {{$single->text}}
                        </p>
                        <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                            <div class="meta">
                                {{$single->created_at}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- end blog Area -->
@endif
<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4  col-md-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Контакты</h6>
                    <p>
                        г. Санкт-Петербург, ул. Обручева, д 74
                    </p>
                    <h3>+7 123 456-78-90</h3>
                    <h3>+7 123 456-78-90</h3>
                </div>
            </div>
            <div class="col-lg-6  col-md-12">
                <div class="single-footer-widget newsletter">
                    <h6>Подписаться на новости</h6>
                    <p>Мы не отправляем спам! Только актуальную информацию для вас и вашей семьи.</p>
                    <div id="mc_embed_signup">
                        <form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get"
                              class="form-inline">

                            <div class="form-group row" style="width: 100%">
                                <div class="col-lg-8 col-md-12">
                                    <input name="EMAIL" placeholder="Введите e-mail" required type="email">
                                    <div style="position: absolute; left: -5000px;">
                                        <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-12">
                                    <button class="nw-btn primary-btn">Подписаться<span class="lnr lnr-arrow-right"></span></button>
                                </div>
                            </div>
                            <div class="info"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row footer-bottom d-flex justify-content-between">
            <p class="col-lg-8 col-sm-12 footer-text m-0">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                Все права защищены | Сделано с помощью <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
            <div class="col-lg-4 col-sm-12 footer-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- End footer Area -->

<script src="/asclinic/js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="/asclinic/js/vendor/bootstrap.min.js"></script>
<script src="/asclinic/js/jquery.ajaxchimp.min.js"></script>
<script src="/asclinic/js/jquery.nice-select.min.js"></script>
<script src="/asclinic/js/jquery.sticky.js"></script>
<script src="/asclinic/js/parallax.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/asclinic/js/jquery.magnific-popup.min.js"></script>
<script src="/asclinic/js/waypoints.min.js"></script>
<script src="/asclinic/js/jquery.counterup.min.js"></script>
<script src="/asclinic/js/main.js"></script>
</body>
</html>
