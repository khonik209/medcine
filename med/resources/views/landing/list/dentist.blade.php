<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/susan/img/fav-icon.png" type="image/x-icon"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Стоматологическая клиника "У Дантиста"</title>

    <!-- Icon css link -->
    <link href="/susan/vendors/themify-icon/themify-icons.css" rel="stylesheet">
    <link href="/susan/css/font-awesome.min.css" rel="stylesheet">
    <link href="/susan/vendors/linears-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="/susan/css/bootstrap.min.css" rel="stylesheet">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/susan/vendors/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/susan/vendors/revolution/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="/susan/vendors/revolution/css/settings.css">

    <!-- Extra plugin css -->
    <link href="/susan/vendors/animate-css/animate.css" rel="stylesheet">
    <link href="/susan/vendors/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/susan/vendors/flipster/jquery.flipster.css" rel="stylesheet">

    <link href="/susan/css/style.css" rel="stylesheet">
    <link href="/susan/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--================Slider Area =================-->
<header class="main_menu_area">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="/susan/img/logo.png" alt="" style="max-width: 75px"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home">У Дантиста</a></li>
                    <li><a href="#feature">Услуги</a></li>
                    <li><a href="#feedback">Отзывы</a></li>
                    <li><a href="#team">Сотрудники</a></li>
                    <li><a href="#contact">Контакты</a></li>
                    <li><a href="{{url('/login')}}">Личный кабинет</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>
<!--================Slider Area =================-->

<!--================Slider Area =================-->
<section class="slider_area" id="home">
    <div class="rev_slider fullwidthabanner" data-version="5.3.0.2" id="main_slider">
        <ul>
            <li data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                data-masterspeed="600" data-rotate="0" data-saveperformance="off" class="secand_slider">
                <!-- MAIN IMAGE -->
                <img src="/susan/img/header-slider/slider-bg-2.jpg" alt="" data-bgposition="center center"
                     data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                     data-no-retina>
                <!-- LAYERS -->
                <div class="tp-caption first_text"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-voffset="['270','270','200']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-y="top"
                     data-fontsize="['36','36','25','20']"
                     data-lineheight="['52','52','35','30']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">Стоматология и лечение зубов <br> в Москве
                </div>
                <!-- LAYERS -->
                <div class="tp-caption some_text"
                     data-width="['570','570','570','350']"
                     data-height="none"
                     data-whitespace="normal"
                     data-voffset="['400','400','300','285']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-y="top"
                     data-fontsize="['16','16','15']"
                     data-lineheight="['26','26','26']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">
                    Наша отличительная черта – высочайший контроль качества при доступных ценах. <br> Мы дорожим своими
                    пациентами и всегда сопереживаем им.
                </div>
                {{--<div class="tp-caption download_btn"
                     data-whitespace="nowrap"
                     data-voffset="['515','515','430']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-y="top"
                     data-start="1800">
                    <a class="register_angkar_btn purple_s_btn" href="#"><i class="lnr lnr-download"></i>Download</a>
                </div>--}}
                <div class="tp-caption right_text"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-voffset="['0']"
                     data-hoffset="['0','0','0']"
                     data-x="right"
                     data-y="bottom"
                     data-fontsize="['48']"
                     data-lineheight="['55']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">
                    <!--<div class="slider_man_shap">
                        <img src="/susan/img/header-slider/teeth.png" alt="" style="max-width:150px">
                    </div>-->
                </div>
            </li>
            <li data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                data-masterspeed="600" data-rotate="0" data-saveperformance="off" class="first_slider">
                <!-- MAIN IMAGE -->
                <img src="/susan/img/header-slider/slider-bg-1.jpg" alt="" data-bgposition="center center"
                     data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                     data-no-retina>
                <!-- LAYERS -->
                <div class="tp-caption first_text"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-voffset="['270','270','200']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-y="top"
                     data-fontsize="['36','36','25','20']"
                     data-lineheight="['52','52','35','30']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">САМАЯ ЗАБОТЛИВАЯ СТОМАТОЛОГИЯ
                </div>
                <!-- LAYERS -->
                <div class="tp-caption some_text"
                     data-width="['570','570','570','350']"
                     data-height="none"
                     data-whitespace="normal"
                     data-voffset="['400','400','300','285']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-y="top"
                     data-fontsize="['16','16','15']"
                     data-lineheight="['26','26','26']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">
                    Дарим красивые улыбки взрослым и детям! <br>
                    Устанавливаем имплантат и коронку одномоментно с удалением зуба!
                </div>
                {{--<div class="tp-caption download_btn"
                     data-whitespace="nowrap"
                     data-voffset="['515','515','430']"
                     data-hoffset="['0','0','0']"
                     data-x="['left','left','left','left','30']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-y="top"
                     data-start="1800">
                    <a class="register_angkar_btn" href="#"><i class="lnr lnr-download"></i>Download</a>
                </div>--}}
                <div class="tp-caption right_text"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-voffset="['0']"
                     data-hoffset="['0','0','0']"
                     data-x="right"
                     data-y="bottom"
                     data-fontsize="['48']"
                     data-lineheight="['55']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="800"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on">
                    <!--<div class="slider_man_shap">
                        <img src="/susan/img/header-slider/teeth_2.jpg" alt="">
                    </div>-->
                </div>
            </li>
        </ul>
    </div><!-- END REVOLUTION SLIDER -->
</section>
<!--================End Slider Area =================-->

<!--================Service Area =================-->
<section class="our_service_area purple_service">
    <div class="container">
        <div class="main_title purple_title">
            <h2>Наши преимущества</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="service_item">
                    <i class="lnr lnr-cloud-check"></i>
                    <a href="#"><h3>Современное оборудование</h3></a>
                    <p>
                        Наша клиника обладает всем необходимым современным и технологичным оборудованием, достаточным
                        для того, что бы ваше лечение проходило максимально комфортно и эффективно.
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">
                    <i class="lnr lnr-phone"></i>
                    <a href="#"><h3>Высококлассные специалисты</h3></a>
                    <p>
                        Все наши сотрудники регулярно подтверждают уровень своей компетенции и принимают участие в
                        ведущих медицинских конференциях, осваивают современные подходы к лечению
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service_item">
                    <i class="lnr lnr-laptop"></i>
                    <a href="#"><h3>Низкие цены</h3></a>
                    <p>
                        Наша бизнес модель расчитана на постоянный, возвращающийся поток клиентов. Для этого, мы
                        поддерживаем не только высокий уровень качества, но и доступные цены. Наши клиенты - наша
                        главная ценность.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Service Area =================-->

<!--================Feature Area =================-->
<section class="feature_area purple_feature" id="feature">
    <div class="container">
        <div class="main_title purple_title">
            <h2>Услуги</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-screen"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Лечение зубов</h4></a>
                            <p>
                                Основная цель терапевта-стоматолога – максимально сохранить
                                здоровые ткани зубов и качественно восстановить поврежденные.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-laptop-phone"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Отбеливание зубов</h4></a>
                            <p>
                                Отбеливание зубов в стоматологии – это процедура изменения цвета эмали при помощи
                                аппаратного воздействия или применения безвредных для здоровья отбеливающих паст, гелей,
                                эмалей.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-film-play"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Лечение десен</h4></a>
                            <p>
                                Заболевания десен занимают второе место по частоте распространения, уступая только
                                кариесу. Различные заболевания пародонта по статистике встречаются у 86% взрослых и 65%
                                детей.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-screen"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Протезирование</h4></a>
                            <p>
                                Современные методы протезирования позволяют легко вернуть жевательную функцию и
                                возможность улыбаться.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-laptop-phone"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Удаление зубов</h4></a>
                            <p>
                                В нашей стоматологии негативные влияния экстракции сводятся к минимуму, а эффективная
                                анестезия полностью избавляет пациентов от боли.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-film-play"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Имплантация зубов</h4></a>
                            <p> Имплантация создаёт полноценную функциональную замену утраченного зуба. Причём установка
                                зубных имплантов производится без обтачивания соседних зубных единиц.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-dice"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Исправление прикуса</h4></a>
                            <p>
                                Хотите иметь безупречную улыбку?
                                В нашей клинике используются методики исправления прикуса,
                                которые ознаменовали начало
                                новой эры в ортодонтии
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-select"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Зуботехническая лаборатория</h4></a>
                            <p>
                                Мы имеем в своем составе собственную зуботехническую
                                лабораторию, которая позволяет изготавливать качественные протезы под постоянным
                                контролем со стороны лечащего врача на всех этапах изготовления.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right_side_feature">
                    <div class="media">
                        <div class="media-left">
                            <i class="lnr lnr-phone"></i>
                        </div>
                        <div class="media-body">
                            <a href="#"><h4>Виниры</h4></a>
                            <p>
                                Виниры – специальные накладки на зубы, изготовленные из керамики. Виниры позволяют
                                исправить различные эстетические дефекты без хирургического вмешательства и серьезного
                                лечения.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Feature Area =================-->

<!--================Works Area =================-->
<section class="works_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="works_right_content">
                    <div class="content_title purple_content">
                        <h2>О нас</h2>
                    </div>
                    <p>
                        В нашей поликлинике проводится профессиональная гигиена и терапевтическое лечение зубов и
                        полости рта, а также удаление зубов любой сложности. А также осуществляется вживление
                        имплантатов и изготовление зубных протезов.
                    </p>
                    {{-- <a href="#"><img src="/susan/img/works/apple-btn.jpg" alt=""></a>
                     <a href="#"><img src="/susan/img/works/google-btn.jpg" alt=""></a>--}}
                </div>
            </div>
            <div class="col-md-6">
                <div class="works_left_img">
                    <img src="/susan/img/works/pexels-photo-305568.jpeg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Works Area =================-->

<!--================Clients Feedback Area =================-->
<section class="clients_feedback_area purple_clients" id="feedback" style="background: #f1f1f1;">
    <div class="container">
        <div class="main_title purple_title">
            <h2>Отзывы клиентов</h2>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="clients_slider owl-carousel">
                    <div class="item">
                        <p>
                            Я очень боюсь стоматологов. Никогда не знаешь. что они там делают во рту своей бор машинкой!
                            :) Но только врачи этой клиники позволили мне расслабиться в кресле. Мой любимый стоматолог
                            - Елена. Благодаря ей у меня теперь шикарная улыбка :) Главное я еще
                            никогда в кресле не была так спокойна. Ни одного неприятного ощущения при лечении + все
                            услуги сравнительно не дорогие. Спасибо Вам!
                        </p>
                        <h4>Екатерина</h4>
                    </div>
                    <div class="item">
                        <p>
                            Хочу поделиться своим опытом и впечатлением о клинике, лечила зубы своему 3 летнему сыну,
                            под наркозом. Разумеется очень боялась, так как ребенок маленький, но все прошло на 5 .
                            Врачи профессионалы, все объясняют, клиника очень уютная, чистая, есть детский уголок с
                            красивым аквариумом. Стоимость услуг недорогая, адекватная и доступная (для платного
                            лечения). Еще хочу отметить, после наркоза у сына никаких побочных эффектов не было, даже
                            сонливости, скакал потом довольный и бодрый весь день. Лично я осталась довольна всем.
                        </p>
                        <h4>Людмила</h4>
                    </div>
                    <div class="item">
                        <p>
                            Хотелось выразить слова благодарности анестезиологам и врачам стоматологической клиники. Сын
                            (6,5 лет) долго и упорно играл на нервах у мамы, бился в истериках и не давал вырвать
                            молочный зуб с запущенным кариесом и гнойным воспалением под местной анестезией. Благодаря
                            профессиональному подходу врачей и постоянному контролю анестезиолога удалили зуб под общей
                            анестезией. Психологически комфортная обстановка способствовала "усыплению" бдительности
                            ребенка, он проснулся без каких либо дискомфортных ощущений, ничего не помнит и вполне
                            доволен ситуацией. Самое главное ситуацией довольна мама ребенка (т.е. я). Здоровья вам и
                            удачи в новом году, благополучия и поменьше нервных клиентов.
                        </p>
                        <h4>Наталья</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</section>
<!--================End Clients Feedback Area =================-->

<!--================Subscrib Area =================-->
<section class="subscrib_area purple_subs">
    <div class="container">
        <div class="content_title purple_content">
            <h2>Записаться к врачу</h2>
        </div>
        <form method="post" action="{{url('/register')}}" class="input-group subscrib_form">
            {{csrf_field()}}
            <input type="hidden" name="role" value="user">
            <input type="text" class="form-control" name="email" placeholder="Введите email">
            <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Записаться</button>
                    </span>
        </form>
    </div>
</section>
<!--================End Subscrib Area =================-->

<!--================Expert Team Area =================-->
@if($doctors->count()>0)
    <div class="expert_team_area purple_team" id="team" style="background: #f1f1f1;">
        <div class="container">
            <div class="main_title purple_title">
                <h2>Наши специалисты</h2>
            </div>
            <div class="expert_slider owl-carousel">
                @foreach($doctors as $doctor)
                    <div class="item">
                        <div class="expert_inner">
                            <img src="{{$doctor->avatar_url}}" alt="">
                            <div class="expert_hover">
                                <div class="expet_hover_inner">
                                    <div class="expert_hover_text">
                                        {{-- <ul>
                                             <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                             <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                             <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                         </ul>--}}
                                        <div><h4>{{$doctor->name}}</h4></div>
                                        @if($doctor->skills->count()>0)
                                            <h5>@foreach($doctor->skills as $skill) {{$skill->name}} @endforeach</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
<!--================End Expert Team Area =================-->

<!--================Latest Blog Area =================-->
@if(count($news)>0)
    <section class="latest_blog_area purple_blog" id="blog" style="background: #fff;">
        <div class="container">
            <div class="main_title purple_title">
                <h2>Новости клиники</h2>
            </div>
            <div class="row">
                @foreach($news as $single)
                    <div class="col-md-4">
                        <div class="latest_blog_item">
                            <div class="l_blog_img">
                                <img src="{{$single->image_url}}" alt="">
                                <div class="l_blog_hover">
                                    <a href="#" data-toggle="modal" data-target="#news_{{$single->id}}"><i class="lnr">Подробнее</i></a>
                                </div>
                            </div>
                            <a href="#"><h4>{{$single->title}}</h4></a>
                            <h5>Дата публикации <a href="#">{{$single->created_at}}</a></h5>
                            <div class="modal fade" id="news_{{$single->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">{{$single->title}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{$single->text}}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
<!--================End Latest Blog Area =================-->

<!--================Sponsors Area =================-->

<section class="sponsors_area">
    <div class="container">
        <div class="sponsor_slider owl-carousel">
            <div class="item">
                <img src="/susan/img/sponsors/sponsors-1.png" alt="">
            </div>
            <div class="item">
                <img src="/susan/img/sponsors/sponsors-2.png" alt="">
            </div>
            <div class="item">
                <img src="/susan/img/sponsors/sponsors-3.png" alt="">
            </div>
            <div class="item">
                <img src="/susan/img/sponsors/sponsors-4.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--================End Sponsors Area =================-->

<!--================Map Area =================-->
<section class="map_area purple_map" id="contact">
    <div id="mapBox" class="mapBox row m0"
         data-lat=" 55.813308"
         data-lon="37.797179"
         data-zoom="15">
    </div>
    <div class="contact_info">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact_form_left">
                        <div class="row">
                            <form class="comment_form_inner" action="#" method="post" id="contactForm">
                                {{csrf_field()}}
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" id="name"
                                           name="name" placeholder="Имя">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="email" class="form-control" id="email"
                                           name="email"
                                           placeholder="Email">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control" id="subject"
                                           name="subject"
                                           placeholder="Тема сообщения">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" name="message" id="message" rows="1"
                                              placeholder="Текст сообщения"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <button class="btn btn-default submit_btn purple_submit"
                                            type="button">Отправить
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact_details">
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-map-marker"></i>
                            </div>
                            <div class="media-body">
                                <h4>Адрес</h4>
                                <h5>г. Москва, ул. Амурская, д.76</h5>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="media-body">
                                <h4>Телефон</h4>
                                <h5>(+1) 234 567 7890</h5>
                                <h5>(+1) 234 567 7890</h5>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="media-body">
                                <h4>Электронная почта</h4>
                                <h5>backpiper.com@gmail.com</h5>
                                <h5>admin@yourdomain.com</h5>
                            </div>
                        </div>
                        <div class="media">
                            <div class="media-left">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="media-body">
                                <h4>График работы</h4>
                                <h5>Пн-Пт 10-20, сб 11-18, вс - выходной</h5>
                            </div>
                        </div>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Map Area =================-->

<!--================Footer Area =================-->
<footer class="footer_area purple_footer">
    <div class="container">
        <div class="pull-left">
            <h4>
                Copyright @2018 Все права защищены | Сделано на платформе <a
                        href="http://healit.ru" target="_blank">Heal
                    IT</a>
            </h4>
        </div>
        <div class="pull-right">
            <ul>
                <li><a href="#">Политика конфиденциальности</a></li>
                <li><a href="#">Правила</a></li>
            </ul>
        </div>
    </div>
</footer>
<!--================End Footer Area =================-->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/susan/js/jquery-2.1.4.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/susan/js/bootstrap.min.js"></script>

<script src="/susan/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/susan/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
<!--RS5.0 Extensions-->
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript"
        src="/susan/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript"
        src="/susan/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript"
        src="/susan/vendors/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/susan/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
<!-- Extra plugin js -->
<script src="/susan/vendors/owlcarousel/owl.carousel.min.js"></script>
<script src="/susan/vendors/flipster/jquery.flipster.min.js"></script>
<!-- contact js -->
<script src="/susan/js/jquery.form.js"></script>
<script src="/susan/js/jquery.validate.min.js"></script>
<script src="/susan/js/contact.js"></script>

<!--gmaps Js-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzO6UIOkzBUqpF0X1sxdqmEinB3fJf7HM"></script>
<script src="/susan/js/gmaps.min.js"></script>

<script src="/susan/js/video_player.js"></script>
<script src="/susan/js/theme.js"></script>
</body>
</html>