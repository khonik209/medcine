@extends('layouts.app')

@section('content')
    <div class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default login-form">
                        @if($domainObj->logo)
                            <img src="{{$domainObj->logo_url}}" class="img img-responsive center-block"
                                 style="margin-top: 25px; max-width: 250px;">
                        @endif
                        <div class="panel-heading text-center">
                            <p>Добро пожаловать на {{$domainObj->name}}</p>
                            <strong>{{$domainObj->company}}</strong>
                        </div>

                        <div class="panel-body" id="full-calendar">
                            @include('common.customError')
                            @include('common.error')
                            @include('common.info')
                            @include('common.success')
                            <form action="{{url('/register_with_form')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="checkbox" name="is_robot" value="1" style="opacity: 0">
                                <div class="form-group">
                                    <label for="user_name">Ф.И.О.</label>
                                    <input type="text" name="user_name" class="form-control" id="user_name" required>
                                    <p class="text-muted">Как к вам можно обращаться?</p>
                                </div>
                                <div class="form-group">
                                    <label for="user_email">Email</label>
                                    <input type="email" name="user_email" class="form-control" id="user_email" required>
                                    <p class="text-muted">В дальнейшем, email будет использоваться как логин для входа</p>
                                </div>
                               @if($domainObj->custom('chat'))
                                    <div class="form-group">
                                        <label for="place">
                                            Формат обращения
                                        </label>
                                        <select class="form-control" name="place" id="place" required>
                                            <option value="offline">Очно</option>
                                            <option value="online">Онлайн</option>
                                        </select>
                                    </div>
                                @else
                                    <input type="hidden" value="offline" name="place">
                                @endif
                                <input type="hidden" value="target_id" name="{{$domainObj->getAdmin()->first()->id}}">
                                <div class="form-group">
                                    <label for="name">Суть вашей проблемы</label>
                                    <input type="text" name="name" class="form-control" id="name" required>
                                    <p class="text-muted">Название обращения</p>
                                </div>
                                <div class="form-group">
                                    <label for="description">Подробное описание жалобы</label>
                                    <textarea id=description name=description class="form-control"
                                              required></textarea>
                                    <p class="text-muted">Опишите подбродней всё, связанное с вашей проблемой</p>
                                </div>
                                @if($questions && count($questions)>0)
                                    @each('user.components.question',$questions,'question')
                                @endif
                                <button type="submit" class="btn btn-primary">
                                    Получить помощь
                                    <span class="glyphicon glyphicon-arrow-right"></span>
                                </button>
                                <hr class="visible-xs visible-sm">
                                <span class="pull-right">Уже зарегистрированы?
                                    <a class="btn btn-primary btn-xs" href="{{url('/login')}}"> Войти в кабинет</a>
                                    или
                                     <a class="btn btn-primary btn-xs" href="{{url('/register')}}">Зарегистрироваться</a>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
