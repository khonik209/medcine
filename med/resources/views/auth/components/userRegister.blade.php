<form class="form-horizontal" role="form" method="POST" action="{{url('/register')}}">
    {{ csrf_field() }}
    <input id="user_role" type="hidden" name="role" value="user">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="col-md-4 control-label">ФИО</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">E-Mail</label>

        <div class="col-md-6">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">Пароль</label>

        <div class="col-md-6">
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="password-confirm" class="col-md-4 control-label">Подтвердите пароль</label>

        <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                Зарегистрироваться
            </button>
            <a class="btn btn-link" href="{{ url('login') }}">
                Уже зарегистрированы? Войти!
            </a>
        </div>
    </div>
</form>
@if($domainObj->id==1)
    <div class="col-md-offset-4  col-md-4">
        <a href="/login/vkontakte"><img src="/img/landing/icons/vk-icon.png"
                                        class="img " alt="vk"></a>
        <a href="/login/yandex"><img src="/img/landing/icons/ya-icon.png"
                                     class="img " alt="yandex"></a>
        <a href="/login/google"><img src="/img/landing/icons/google-icon.png"
                                     class="img " alt="google"></a>
    </div>
@endif