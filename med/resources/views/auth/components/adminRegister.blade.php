<form action="/clinic/register" method="post" role="form">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Имя администратора</label>
        <input type="text" name="user_name" placeholder="Как к Вам можно обращаться?" required
               class="form-control">
    </div>
    <div class="form-group">
        <label>Email администратора</label>
        <input type="email" name="email" placeholder="Введите email" required
               class="form-control">
    </div>
    <div class="form-group">
        <label>Телефон администратора</label><input type="text" name="phone" placeholder="Введите телефон"
                                                    class="form-control">
    </div>
    <div class="form-group">
        <label>Название клиники</label>
        <input type="text" name="clinic_name" placeholder="Введите название Вашей клиники"
               class="form-control" required>
    </div>
    <div class="form-group">
        <label>Бесплатный домен* </label>
        <div class="input-group">
            <input type="text" name="domain" class="form-control" required placeholder="Придумайте адрес для Вашего сайта">
            <span class="input-group-addon">.healit.ru</span>
        </div>
    </div>
    <div class="form-group pull-right">
        <button class="btn btn-info" type="submit">Отправить</button>
    </div>
</form>
<small>*в процессе работы можно прикрепить свой</small>