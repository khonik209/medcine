@extends('layouts.app')

@section('content')
    <div class="register">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if($domainObj->logo)
                        <a href="{{url('/')}}">
                        <img src="{{$domainObj->logo_url}}" class="img img-responsive center-block"
                             style="margin-top: 25px; max-width: 250px;">
                        </a>
                    @endif
                    <div class="panel-heading text-center"><strong>Регистрация</strong></div>
                    <div class="panel-body">
                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')

                        @include('auth.components.userRegister')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
