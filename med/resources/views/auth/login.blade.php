@extends('layouts.app')

@section('content')
    <div class="login">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default login-form">
                    @if($domainObj->logo)
                        <a href="{{url('/')}}">
                            <img src="{{$domainObj->logo_url}}" class="img img-responsive center-block"
                                 style="margin-top: 25px; max-width: 250px;">
                        </a>
                    @endif
                    <div class="panel-heading text-center"><strong>Вход в личный кабинет</strong></div>

                    <div class="panel-body">
                        @include('common.error')
                        @include('common.customError')
                        @include('common.info')
                        @include('common.success')
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Пароль</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password"
                                           required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" checked
                                                   name="remember" {{ old('remember') ? 'checked' : ''}}> Запомнить
                                            меня
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Войти
                                    </button>
                                    <a class="btn btn-link" href="{{ url('register') }}">
                                        Регистрация
                                    </a>
                                    <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                        Забыли пароль?
                                    </a>

                                </div>
                            </div>
                        </form>
                        @if($domainObj->id==1)
                            <div class="col-md-offset-4  col-md-4">
                                <a href="/login/vkontakte"><img src="/img/landing/icons/vk-icon.png"
                                                                class="img " alt="vk"></a>
                                <a href="/login/yandex"><img src="/img/landing/icons/ya-icon.png"
                                                             class="img " alt="yandex"></a>
                                <a href="/login/google"><img src="/img/landing/icons/google-icon.png"
                                                             class="img " alt="google"></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
