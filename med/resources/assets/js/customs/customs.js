/**
 * Функции, связанные с кастомизацией кабинета и разработкой кастомного лендинга
 * @type {*}
 */
/* Выбор корпоративного цвета в настройках администратора */
function toggleColor() {
    color_line = $('option:selected').css('background');
    $('#custom-color').css('background', color_line)
}
$('#custom-color').change(toggleColor);
$(document).ready(function () {
    toggleColor();
});
