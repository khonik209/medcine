window.$ = window.jQuery = require('jquery');
require('./plugins/jquery-ui.min');
require('bootstrap-sass');
window.Vue = require('vue');
require('./plugins/slick');

var healit = document.getElementById('healit');
if (healit != null) {
    Vue.component('healit', require('./components/common/RegisterClinic.vue'));
    let reg = new Vue({
        el: '#healit',
    });
}