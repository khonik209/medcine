/**
 * Created by Nikita on 08.09.2017.
 */
// Кабинет

/** Поиск врачей **/

if (typeof activeDoctors !== 'undefined' && activeDoctors.length > 0) {
    $("#doctors-search").autocomplete({
        source: activeDoctors,
        select: function (event, ui) {
        }
    });
}

/** UI **/
$("#draggable").draggable();
/*$('.js-sortable').sortable();*/

$('.js-sortable').sortable({
    handle: 'span',
    change: function (event, ui) {
        $('.js-entry-submit').removeClass('hidden');
    }
});
$('.js-ui-note').submit(function () {
    var entry_ids = [];
    var note_ids = [];
    $('.entries').find('.note').each(function () {
        entry_ids.push($(this).data('id'));

    });
    $('.js-entry-order').val(entry_ids);

    $('.notes').find('.note').each(function () {
        note_ids.push($(this).data('id'));

    });
    $('.js-note-order').val(note_ids);
});

/** DICOM Карусель **/
$(document).ready(function () {
    var dcm = $('#dcm-slick-carousel');
    dcm.slick({
        //dots: true,
        infinite: true,
        //speed: 500,
        fade: true,
        // cssEase: 'linear',
        autoplay: false,
        //autoplaySpeed:2500,
        // arrows:true,
        pauseOnHover: false,
        pauseOnFocus: false
    });
    $('.js-forward').click(function () {
        dcm.slick('slickNext');
    });
    $('.js-back').click(function () {
        dcm.slick('slickPrev');
    });
});

/* Подбор при создании чата */
if (typeof activeDoctors !== 'undefined' && activeDoctors.length > 0) {
    $("#doctors-search").autocomplete({
        source: activeDoctors,
        select: function (event, ui) {
            $('#doctors').append("" +
                "<li class=\"js-doctor-item alert alert-info\" id=\"doctor" + ui.item.value + "\">" +
                "<button type=\"button\" class=\"close js-delete-doctor\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + ui.item.label +
                "<input type=\"hidden\" name=\"members[]\" value=\"" + ui.item.value + "\">" +
                "</li>"
            );
        }
    });
    $('.js-delete-doctor').click(function () {
        $(this).parents('.js-doctor-item').remove();
    });

    $('#doctors').sortable();
}

/* Добавление врача к беседе */

if (typeof addListDoctors !== 'undefined' && addListDoctors.length > 0) {
    $("#doctors-search-chat").autocomplete({
        source: addListDoctors,
        select: function (event, ui) {
            $('#doctors').append("" +
                "<li class=\"js-doctor-item alert alert-info\" id=\"doctor" + ui.item.value + "\">" +
                "<button type=\"button\" class=\"close js-delete-doctor\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + ui.item.label +
                "<input type=\"hidden\" name=\"members[]\" value=\"" + ui.item.value + "\">" +
                "</li>"
            );
        }
    });
    $('.js-delete-doctor').click(function () {
        $(this).parents('.js-doctor-item').remove();
    });
}

// Маски
var status = document.getElementsByClassName('status-time');
if (status.length == 0) {
    jQuery(function ($) {
        $(".mask-datetime").mask("99:99 99-99-9999");
        $(".mask-date").mask("99-99-9999");
    });
}

// Генерация магической кнопки
$('.js-magic-button-generate').click(function () {
    id = $('.js-id').val();
    sum = $('.js-sum').val();
    if ($('.js-type-button').prop("checked")) {
        form = 'button';
    } else if ($('.js-type-link').prop("checked")) {
        form = 'link';
    } else {
        form = '';
    }
    $.ajax({
        type: 'POST',
        url: '/doc/account/magicbutton',
        data: {
            'sum': sum,
            'id': id,
            'form': form,
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (msg) {
            $('.js-magic-button').text(msg);
        }
    });
});
/* Magic button: Проверка, есть ли юзер с таким email в системе */
$('#check-user-button').click(function () {
    $.ajax({
        type: 'POST',
        url: '/api/checkuser',
        data: {
            'email': $('.js-check-user').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (msg) {
            if (msg == 1) {
                // спрашиваем пароль
                $('#pac_password').removeClass('hidden');
                $('#submit-user-button').removeClass('hidden');
                $('#check-user-button').addClass('hidden');
                $('form').append('<div class="alert alert-info">Вы уже зарегистрированы! Введите пароль.</div>');
            } else {
                // редиректим дальше
                $('#get-email-form').submit();
            }
        }
    });
});
/* Magic button: Логин юзера и переадрессация на дальшейшие действия */
$('#submit-user-button').click(function () {
    $('#get-email-form').submit();
});

function toggleWalletType() {
    // Определяем, что нажато:
    if ($('.js-wallet-card').prop('checked')) {
        // Показываем ссылку для привязки карты
        $('.yandex-account').addClass('hidden');
    } else if ($('.js-wallet-yandex').prop('checked')) {
        // Показываем форму сохранения яндекс.номера
        $('.yandex-account').removeClass('hidden');
    }
}

$('.js-wallet-type').click(function () {
    toggleWalletType();
});
$(document).ready(function () {
    toggleWalletType();
});

if (typeof chartData !== 'undefined') {
    var chartOptions = {
        legend: {
            display: false
        }
    };
    var wChart = new Chart($('#jsStartChart'), {
        type: 'line',
        data: chartData['illnesses'],
        options: chartOptions
    });

    $('.report-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // e.target // activated tab
        // e.relatedTarget // previous tab

        newChart = $(e.target.hash).find('canvas');
        var myChart = new Chart(newChart, {
            type: 'line',
            data: chartData[$(e.target.hash).attr('id')],
            options: chartOptions
        });
        return false;

    });

}

$('.rating_plus').change(function () {
    if ($(this).prop('checked')) {
        var radio_minus = $(this).parents('form').find('.rating-minus');
        var radio_plus = $(this).parents('form').find('.rating-plus');
        var label_plus = $(this).parents('form').find('.js-label-plus');
        var label_minus = $(this).parents('form').find('.js-label-minus');

        radio_plus.addClass('checked');
        label_plus.removeClass('hidden');
        radio_minus.removeClass('checked');
        label_minus.addClass('hidden');
    }
});

$('.rating_minus').change(function () {
    if ($(this).prop('checked')) {
        var radio_minus = $(this).parents('form').find('.rating-minus');
        var radio_plus = $(this).parents('form').find('.rating-plus');
        var label_plus = $(this).parents('form').find('.js-label-plus');
        var label_minus = $(this).parents('form').find('.js-label-minus');
        radio_minus.addClass('checked');
        radio_plus.removeClass('checked');
        label_minus.removeClass('hidden');
        label_plus.addClass('hidden');
    }
});

$('#skills').change(function () {
    option = $(this).find('option:selected');
    value = $(this).val();
    name = option.data('name');
    $('.js-skill-form').append("" +
        "<li class=\"js-skill-item alert alert-info\">" +
        "<button type=\"button\" class=\"close js-delete-skill\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" + name +
        "<input type=\"hidden\" name=\"skills[]\" value=\"" + value + "\">" +
        "</li>"
    );
});
$('.js-delete-skill').click(function () {
    $(this).parents('.js-skill-item').remove();
});

$('.request-toolbar a').click(function () {
    $('.request-toolbar a').removeClass('active');
    $(this).addClass('active')
});

/* Управление чекбоксами при заполнении графика работы врача */
$('.js-days-checkbox').change(function () {
    if ($(this).prop('checked')) {
        $(this).parents('.js-day-row').find('.js-daytime-start').append('<input class="form-control timepicker js-daytime" type="text" name="days[' + $(this).attr('id') + '][starts]" value="">');
        $(this).parents('.js-day-row').find('.js-daytime-end').append('<input class="form-control timepicker js-daytime" type="text" name="days[' + $(this).attr('id') + '][ends]" value="">');
        $('.timepicker').timepicker({'minTime': '07:00', 'maxTime': '23:00', 'timeFormat': 'H:i'});
    } else {
        $(this).parents('.js-day-row').find('.js-daytime').remove();
    }

});
$('.timepicker').timepicker({'minTime': '07:00', 'maxTime': '23:00', 'timeFormat': 'H:i'});

$(".datepicker").datepicker({
    dateFormat: "yy-mm-dd",
    locale:'ru'
});

/**
 * Управление дополнительными полями обследований
 */
$('.js-create-new-view').click(function () {
    var count = $('.addition-view-block').length + 1;
    var block = '' +
        '<div class="addition-view-block">' +
        '<div class="form-group">' +
        '   <label for="inspection_' + count + '" class="col-md-4 control-label">Осмотр #<span class="js-num"></span></label>' +
        '   <div class="col-md-6">' +
        '       <textarea id="inspection_' + count + '" class="form-control" name="inspections[]"></textarea>' +
        '   </div>' +
        '</div>' +
        '<div class="form-group">' +
        '      <label for="adjustment_' + count + '" class="col-md-4 control-label">Корректировка лечения #<span class="js-num"></span></label>' +
        '      <div class="col-md-6">' +
        '         <textarea id="adjustment_' + count + '" class="form-control" name="treatment_adjustments[]"></textarea>' +
        '      </div>' +
        '</div>' +
        '<div class="col-sm-offset-9">' +
        '<button type="button" class="btn btn-danger btn-xs js-delete-view-block"><span class="glyphicon glyphicon-trash"></span> удалить </button> ' +
        '</div>' +
        '<hr>' +
        '</div>';

    $('.js-additional-views').append(block);
    var i = 0;
    $('.addition-view-block').each(function () {
        i++;
        $(this).find('.js-num').text(i);
    });

    $('body').on('click', '.js-delete-view-block', function () {
        removeBlock($(this));
    });

});

function removeBlock(button) {
    button.parents('.addition-view-block').remove();
    var i = 0;
    $('.addition-view-block').each(function () {
        i++;
        $(this).find('.js-num').text(i);
    })
}

$('.js-offline-form').click(function () {
    if ($(this).prop('checked')) {
        $('.js-text-for-offline').removeClass('hidden');
        $('.js-time-offline-request').removeClass('hidden');
        $('.js-text-for-online').addClass('hidden');
    }
});

$('.js-online-form').click(function () {
    if ($(this).prop('checked')) {
        $('.js-text-for-online').removeClass('hidden');
        $('.js-text-for-offline').addClass('hidden');
        $('.js-time-offline-request').addClass('hidden');
    }
});