/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./plugins/bootstrap');
require('./plugins/owl.carousel.min');
require('./plugins/jquery.mousewheel.min');
require('./plugins/slick');
require('./customs/customs');
require('chart.js');
require('timepicker');

import Sortable from 'sortablejs'

Vue.directive('sortable', {
    inserted: function (el, binding) {
        new Sortable(el, binding.value || {})
    }
});

import 'fullcalendar';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var chat = document.getElementById('js-chat');
if (chat != null) {
    Vue.component('text-chat', require('./components/common/TextChat.vue'));
    var text_chat = new Vue({
        el: '#js-chat',
    });
}

var template = document.getElementById('vue-new-template');
if (template != null) {
    Vue.component('template-constructor', require('./components/admin/templateConstructor.vue'));

    var email_template = new Vue({
        el: '#vue-new-template',
        data: {
            inputs: [],
            order: 0, // походу нахуй не нужен
            currentInput: [],
        },
        created() {

        },
        // определяйте методы в объекте `methods`
        methods: {}
    });
}

var newbook = document.getElementById('new-book-container');
if (newbook != null) {
    Vue.component('new-book-form', require('./components/admin/NewBookForm.vue'));
    var newBookForm = new Vue({
        el: '#new-book-container',
    });
}

var editbook = document.getElementById('edit-book-container');
if (editbook != null) {
    Vue.component('book-edit', require('./components/admin/BookEdit.vue'));
    var editBook = new Vue({
        el: '#edit-book-container',
    });
}

var emailCampaigns = document.getElementById('email-campaigns');
if (emailCampaigns != null) {
    Vue.component('email-campaigns', require('./components/admin/EmailCampaigns.vue'));
    var email_campaigns = new Vue({
        el: '#email-campaigns',
    });
}

var a_db = document.getElementById('admin-dashdoard');
if (a_db != null) {
    Vue.component('calendar', require('./components/common/Calendar.vue'));
    Vue.component('app-calendar', require('./components/common/AppCalendar.vue'));
    Vue.component('calendar-day', require('./components/common/CalendarDay.vue'));
    Vue.component('chart', require('./components/admin/Chart.vue'));
    Vue.component('dashboard-table', require('./components/admin/DashboardTable.vue'));
    let dashboard = new Vue({
        el: '#admin-dashdoard',
    });
}


var calendar_container = document.getElementById('full-calendar');
if (calendar_container != null) {
    Vue.component('calendar', require('./components/common/Calendar.vue'));
    Vue.component('app-calendar', require('./components/common/AppCalendar.vue'));
    Vue.component('calendar-day', require('./components/common/CalendarDay.vue'));
    let calendaric = new Vue({
        el: '#full-calendar',
    });
}

var chart_container = document.getElementById('vue-chart');
if (chart_container != null) {
    Vue.component('chart', require('./components/admin/Chart.vue'));
    let chart = new Vue({
        el: '#vue-chart',
    });
}

var specialities_container = document.getElementById('specialities_container');
if (specialities_container != null) {
    Vue.component('specialities', require('./components/admin/Specialities.vue'));
    let specialities = new Vue({
        el: '#specialities_container',
    });
}

var form_questions_container = document.getElementById('form-questions-container');
if (form_questions_container != null) {
    Vue.component('form-questions', require('./components/admin/FormQuestions.vue'));
    let form_questions = new Vue({
        el: '#form-questions-container',
    });
}


var create_record_container = document.getElementById('create_record_container');
if (create_record_container != null) {
    Vue.component('create-record', require('./components/admin/CreateRecord.vue'));
    let create_record = new Vue({
        el: '#create_record_container',
    });
}

var invite_chat_container = document.getElementById('js-teledoctor');
if (invite_chat_container != null) {
    Vue.component('invite-chat', require('./components/doctor/InviteChat.vue'));
    let invite_chat = new Vue({
        el: '#js-teledoctor',
    });
}
var add_history_container = document.getElementById('js-history');
if (add_history_container != null) {
    Vue.component('add-history', require('./components/doctor/AddDiseaseHistory.vue'));
    let add_history = new Vue({
        el: '#js-history',
    });
}

var edit_history_container = document.getElementById('js-edit-history');
if (edit_history_container != null) {
    Vue.component('edit-history', require('./components/doctor/EditDiseaseHistory.vue'));
    let edit_history = new Vue({
        el: '#js-edit-history',
    });
}


var chat_new = document.getElementById('chat-new');
if (chat_new != null) {
    Vue.component('chat-new', require('./components/common/ChatNew.vue'));
    let chats = new Vue({
        el: '#chat-new',
    });
}


var chat_list = document.getElementById('chat-list');
if (chat_list != null) {
    Vue.component('chats', require('./components/common/Chats.vue'));
    let chats = new Vue({
        el: '#chat-list',
    });
}