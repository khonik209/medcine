import dwv from 'dwv';

dwv.utils.decodeQuery = dwv.utils.base.decodeQuery;
dwv.gui.getWindowSize = dwv.gui.base.getWindowSize;
dwv.gui.displayProgress = function () {
};
dwv.gui.getElement = dwv.gui.base.getElement;
dwv.gui.refreshElement = dwv.gui.base.refreshElement;
dwv.gui.Slider = null;
dwv.gui.DicomTags = null;
dwv.gui.Toolbox = function (app) {
    this.setup = function () {
    };
    this.display = function () {
    };
    this.initialise = function (list) {
        if (list[0] === false) {
            var inputScroll = app.getElement("scroll-button");
            inputScroll.style.display = "none";
            var inputZoom = app.getElement("zoom-button");
            inputZoom.checked = true;
        }
    };
};
dwv.gui.WindowLevel = function (app) {
    this.setup = function () {
        var button = document.createElement("button");
        button.className = "btn btn-info wl-button";
        button.value = "WindowLevel";
        button.onclick = app.onChangeTool;
        button.appendChild(document.createTextNode("Контраст"));
        var node = app.getElement("toolbar");
        node.appendChild(button);
    };
    this.display = function (bool) {
        var button = app.getElement("wl-button");
        button.disabled = bool;
    };
    this.initialise = function () {
    };
};
dwv.gui.ZoomAndPan = function (app) {
    this.setup = function () {
        var button = document.createElement("button");
        button.className = "btn btn-info hidden-xs hidden-sm zoom-button";
        button.value = "ZoomAndPan";
        button.onclick = app.onChangeTool;
        button.appendChild(document.createTextNode("Zoom"));
        var node = app.getElement("toolbar");
        node.appendChild(button);
    };
    this.display = function (bool) {
        var button = app.getElement("zoom-button");
        button.disabled = bool;
    };
};
dwv.gui.Scroll = function (app) {
    this.setup = function () {
        var button = document.createElement("button");
        button.className = "btn btn-info scroll-button";
        button.value = "Scroll";
        button.onclick = app.onChangeTool;
        button.appendChild(document.createTextNode("Вперёд/Назад"));
        var node = app.getElement("toolbar");
        node.appendChild(button);
    };
    this.display = function (bool) {
        var button = app.getElement("scroll-button");
        button.disabled = bool;
    };
};
dwv.gui.appendResetHtml = function (app) {
    var button = document.createElement("button");
    button.className = "btn btn-info reset-button";
    button.value = "reset";
    button.onclick = app.onDisplayReset;
    button.appendChild(document.createTextNode("Сброс"));
    var node = app.getElement("toolbar");
    node.appendChild(button);
};

function MRI(urls, numb) {

    dwv.gui.getElement = dwv.gui.base.getElement;
    dwv.gui.displayProgress = function (percent) {
    };
    dwv.gui.getWindowSize = function () {
        return {
            'width': ($(window).width() - 10),
            'height': ($(window).height() - 150)
        };
    };
    var image = null;
    document.getElementsByClassName("imageLayer").onmouseover = function () {
        image = $(e.currentTarget);
    };
    document.getElementsByClassName("imageLayer").onmouseout = function () {
        image = null;
    };

    var app = new dwv.App();
    app.init({
        "containerDivId": "dwv" + numb,
        "gui": ["tool"],
        "tools": ["WindowLevel", "ZoomAndPan"],
        "fitToWindow": true,
        "isMobile": true
    });
    app.loadURLs(urls);

    dwv.gui.appendResetHtml(app);
}

window.onload = function () {
    for (var i = 0; i < fileCount; i++) {
        MRI(['/storage/' + file + i + '.dcm'], i);
    }
    $('#dcm-preloader').addClass('hidden');
    $('#dcm-page').removeClass('hidden');
};