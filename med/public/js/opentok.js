function startSession() {

    $('#myPublisherDiv').draggable(); // Делаем свою пикчу перетаскиваемой
    $('#subsDiv').sortable(); // Делаем подписчиков сортабл
// credentials
    var apiKey = API_KEY; //your API key
    var sessionId = SESSION_ID; // your session id
    var token = TOKEN; // your token
// connect to session
    var session = OT.initSession(apiKey, sessionId)
    session.connect(token, function (error) {
        // create publisher
        var publisher = OT.initPublisher('myPublisherDiv');
        session.publish(publisher);
    });

// create subscriber
    session.on('streamCreated', function (event) {
        show("subsDiv");
        var subscriberProperties = {insertMode: 'append', height: 300};
        session.subscribe(event.stream, 'subsDiv', subscriberProperties);

        $('#subsDiv div').addClass('col-xs-12 col-sm-6 col-md-6 col-lg-4');
    });

    session.on("connectionDestroyed", function (event) {
    });

    session.on("sessionDisconnected", function (event) {
    });
}
function show(id) {
    document.getElementById(id).style.display = "block";
}

function hide(id) {
    document.getElementById(id).style.display = "none";
}

function disconnect(){
    session.disconnect();
}
